<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *     normalizationContext={"groups"={"client_read"}},
 *     itemOperations={
 *      "getMissionGroupCandidatureByClient"={"method"="get","path"="/clients/missionsAndCandidature/{id}","controller"="App\Controller\ClientController"},
 *      "getHebergementByClient"={"method"="get","path"="/clients/{id}/hebergements","controller"="App\Controller\ClientHebergementController"},
 *      "get", "put", "delete"
 *     },
 * )
 * @ApiFilter(SearchFilter::class,properties={
 *     "nomEtablissement":"partial",
 *     "raisonSocial":"partial",
 *     "idSellAndSign":"partial",
 *     "type":"partial",
 *     "telephone":"partial",
 *     "auteur":"partial",
 *     "adresse":"partial",
 *     "ville":"partial",
 *     "cp":"partial",
 *     "departement":"partial",
 *     "region":"partial",
 *     "email":"partial"
 * })
 * @ApiFilter(SearchFilter::class, properties={"recruteurs.user.email":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"hopitalConnecte","cible","activer"})
 * @ApiFilter(RangeFilter::class, properties={"createAt":DateFilter::EXCLUDE_NULL})
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"client_read", "fs_write", "mission_read", "garde_read", "planning_read", "recruteur_read", "recruteur_write","candidature_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"client_read", "garde_read", "mission_read", "planning_read", "recruteur_read","candidature_read"})
     */
    private $nomEtablissement;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client_read", "garde_read"})
     */
    private $raisonSocial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read"})
     */
    private $siret;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"client_read"})
     */
    private $cible;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client_read"})
     * @Groups({"client_read"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read", "recruteur_read"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read"})
     */
    private $fax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client_read"})
     */
    private $departement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"client_read"})
     */
    private $numeroDepartement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"client_read"})
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read"})
     */
    private $complementAdresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client_read"})
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client_read", "planning_read"})
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     * @Groups({"client_read"})
     */
    private $siteWeb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read"})
     */
    private $remarque;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"client_read"})
     */
    private $autreRemarques;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"client_read", "recruteur_read"})
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"client_read"})
     */
    private $hopitalConnecte;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Groups({"client_read"})
     */
    private $activer;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"client_read"})
     */
    private $majoration;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"client_read"})
     */
    private $coefficientDeMajoration;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"client_read"})
     */
    private $taux_tva;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"client_read"})
     */
    private $coefficientDeCommission;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"client_read"})
     */
    private $numeroTva;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read"})
     */
    private $rib;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client_read"})
     */
    private $modeDeCollaboration;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"client_read"})
     */
    private $periodesaison;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"client_read"})
     */
    private $idPld;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"client_read"})
     */
    private $idSellAndSign;

    /**
     * @ORM\OneToMany(targetEntity=FicheSpecialite::class, mappedBy="client")
     * @Groups({"client_read"})
     */
    private $ficheSpecialites;

    /**
     * @ORM\ManyToOne(targetEntity=ClientMajoration::class, inversedBy="client")
     */
    private $clientMajoration;

    /**
     * @ORM\OneToOne(targetEntity=FicheClient::class, mappedBy="client", cascade={"persist", "remove"})
     */
    private $ficheClient;

    /**
     * @ORM\ManyToMany(targetEntity=Recruteur::class, mappedBy="client")
     * @Groups({"client_read"})
     */
    private $recruteurs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $naf;

    public function __construct()
    {
        $this->ficheSpecialites = new ArrayCollection();
        $this->recruteurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEtablissement(): ?string
    {
        return $this->nomEtablissement;
    }

    public function setNomEtablissement(string $nomEtablissement): self
    {
        $this->nomEtablissement = $nomEtablissement;

        return $this;
    }

    public function getRaisonSocial(): ?string
    {
        return $this->raisonSocial;
    }

    public function setRaisonSocial(string $raisonSocial): self
    {
        $this->raisonSocial = $raisonSocial;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getCible()
    {
        return $this->cible;
    }

    public function setCible($cible): self
    {
        $this->cible = $cible;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getNumeroDepartement(): ?string
    {
        return $this->numeroDepartement;
    }

    public function setNumeroDepartement(?string $numeroDepartement): self
    {
        $this->numeroDepartement = $numeroDepartement;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complementAdresse;
    }

    public function setComplementAdresse(?string $complementAdresse): self
    {
        $this->complementAdresse = $complementAdresse;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getSiteWeb(): ?string
    {
        return $this->siteWeb;
    }

    public function setSiteWeb(?string $siteWeb): self
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getAutreRemarques(): ?string
    {
        return $this->autreRemarques;
    }

    public function setAutreRemarques(?string $autreRemarques): self
    {
        $this->autreRemarques = $autreRemarques;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHopitalConnecte(): ?bool
    {
        return $this->hopitalConnecte;
    }

    public function setHopitalConnecte(?bool $hopitalConnecte): self
    {
        $this->hopitalConnecte = $hopitalConnecte;

        return $this;
    }

    public function getActiver()
    {
        return $this->activer;
    }

    public function setActiver($activer): self
    {
        $this->activer = $activer;

        return $this;
    }

    public function getMajoration(): ?float
    {
        return $this->majoration;
    }

    public function setMajoration(?float $majoration): self
    {
        $this->majoration = $majoration;

        return $this;
    }

    public function getCoefficientDeMajoration(): ?float
    {
        return $this->coefficientDeMajoration;
    }

    public function setCoefficientDeMajoration(?float $coefficientDeMajoration): self
    {
        $this->coefficientDeMajoration = $coefficientDeMajoration;

        return $this;
    }

    public function getTauxTva(): ?float
    {
        return $this->taux_tva;
    }

    public function setTauxTva(?float $taux_tva): self
    {
        $this->taux_tva = $taux_tva;

        return $this;
    }

    public function getCoefficientDeCommission(): ?float
    {
        return $this->coefficientDeCommission;
    }

    public function setCoefficientDeCommission(?float $coefficientDeCommission): self
    {
        $this->coefficientDeCommission = $coefficientDeCommission;

        return $this;
    }

    public function getNumeroTva(): ?float
    {
        return $this->numeroTva;
    }

    public function setNumeroTva(?float $numeroTva): self
    {
        $this->numeroTva = $numeroTva;

        return $this;
    }

    public function getRib(): ?string
    {
        return $this->rib;
    }

    public function setRib(?string $rib): self
    {
        $this->rib = $rib;

        return $this;
    }

    public function getModeDeCollaboration(): ?string
    {
        return $this->modeDeCollaboration;
    }

    public function setModeDeCollaboration(?string $modeDeCollaboration): self
    {
        $this->modeDeCollaboration = $modeDeCollaboration;

        return $this;
    }

    public function getPeriodesaison(): ?string
    {
        return $this->periodesaison;
    }

    public function setPeriodesaison(?string $periodesaison): self
    {
        $this->periodesaison = $periodesaison;

        return $this;
    }

    public function getIdPld(): ?string
    {
        return $this->idPld;
    }

    public function setIdPld(?string $idPld): self
    {
        $this->idPld = $idPld;

        return $this;
    }

    public function getIdSellAndSign(): ?string
    {
        return $this->idSellAndSign;
    }

    public function setIdSellAndSign(?string $idSellAndSign): self
    {
        $this->idSellAndSign = $idSellAndSign;

        return $this;
    }

    /**
     * @return Collection|FicheSpecialite[]
     */
    public function getFicheSpecialites(): Collection
    {
        return $this->ficheSpecialites;
    }

    public function addFicheSpecialite(FicheSpecialite $ficheSpecialite): self
    {
        if (!$this->ficheSpecialites->contains($ficheSpecialite)) {
            $this->ficheSpecialites[] = $ficheSpecialite;
            $ficheSpecialite->setClient($this);
        }

        return $this;
    }

    public function removeFicheSpecialite(FicheSpecialite $ficheSpecialite): self
    {
        if ($this->ficheSpecialites->contains($ficheSpecialite)) {
            $this->ficheSpecialites->removeElement($ficheSpecialite);
            // set the owning side to null (unless already changed)
            if ($ficheSpecialite->getClient() === $this) {
                $ficheSpecialite->setClient(null);
            }
        }

        return $this;
    }

    public function getClientMajoration(): ?ClientMajoration
    {
        return $this->clientMajoration;
    }

    public function setClientMajoration(?ClientMajoration $clientMajoration): self
    {
        $this->clientMajoration = $clientMajoration;

        return $this;
    }

    public function getFicheClient(): ?FicheClient
    {
        return $this->ficheClient;
    }

    public function setFicheClient(?FicheClient $ficheClient): self
    {
        $this->ficheClient = $ficheClient;

        // set (or unset) the owning side of the relation if necessary
        $newClient = null === $ficheClient ? null : $this;
        if ($ficheClient->getClient() !== $newClient) {
            $ficheClient->setClient($newClient);
        }

        return $this;
    }

    /**
     * @return Collection|Recruteur[]
     */
    public function getRecruteurs(): Collection
    {
        return $this->recruteurs;
    }

    public function addRecruteur(Recruteur $recruteur): self
    {
        if (!$this->recruteurs->contains($recruteur)) {
            $this->recruteurs[] = $recruteur;
            $recruteur->addClient($this);
        }

        return $this;
    }

    public function removeRecruteur(Recruteur $recruteur): self
    {
        if ($this->recruteurs->contains($recruteur)) {
            $this->recruteurs->removeElement($recruteur);
            $recruteur->removeClient($this);
        }

        return $this;
    }

    public function getNaf(): ?string
    {
        return $this->naf;
    }

    public function setNaf(string $naf): self
    {
        $this->naf = $naf;

        return $this;
    }

    public  function  getSignataire(){
        foreach ($this->recruteurs->getValues() as $recruteur)
        {
         if ($recruteur->isSignataire())
         {
             return $recruteur;
         }
        }

    }
}
