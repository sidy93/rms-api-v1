<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReleveHeureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get",
 *      "post"={"method"="post", "controller"="App\Controller\ReleveHeureController"},
 *     },
 *     itemOperations={
 *      "get",
 *      "delete",
 *      "put"={"method"="put", "controller"="App\Controller\ReleveHeureController"},
 *     }
 * )
 * @ORM\Entity(repositoryClass=ReleveHeureRepository::class)
 */
class ReleveHeure
{
    use Timestamps;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rhPld;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaireInterim;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaireClient;

    /**
     * @ORM\ManyToOne(targetEntity=Renumeration::class, inversedBy="releveHeures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $remuneration;

    public function __construct()
    {
        $this->createdAt();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRhPld(): ?string
    {
        return $this->rhPld;
    }

    public function setRhPld(string $rhPld): self
    {
        $this->rhPld = $rhPld;

        return $this;
    }

    public function getCommentaireInterim(): ?string
    {
        return $this->commentaireInterim;
    }

    public function setCommentaireInterim(?string $commentaireInterim): self
    {
        $this->commentaireInterim = $commentaireInterim;

        return $this;
    }

    public function getCommentaireClient(): ?string
    {
        return $this->commentaireClient;
    }

    public function setCommentaireClient(?string $commentaireClient): self
    {
        $this->commentaireClient = $commentaireClient;

        return $this;
    }

    public function getRemuneration(): ?Renumeration
    {
        return $this->remuneration;
    }

    public function setRemuneration(?Renumeration $remuneration): self
    {
        $this->remuneration = $remuneration;

        return $this;
    }
}
