<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ClientMajorationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ClientMajorationRepository::class)
 */
class ClientMajoration
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=client::class, mappedBy="clientMajoration")
     */
    private $client;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $dimanche;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $samedi;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ferier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $vingtQuatre;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $trenteUn;

    public function __construct()
    {
        $this->client = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|client[]
     */
    public function getClient(): Collection
    {
        return $this->client;
    }

    public function addClient(client $client): self
    {
        if (!$this->client->contains($client)) {
            $this->client[] = $client;
            $client->setClientMajoration($this);
        }

        return $this;
    }

    public function removeClient(client $client): self
    {
        if ($this->client->contains($client)) {
            $this->client->removeElement($client);
            // set the owning side to null (unless already changed)
            if ($client->getClientMajoration() === $this) {
                $client->setClientMajoration(null);
            }
        }

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDimanche(): ?bool
    {
        return $this->dimanche;
    }

    public function setDimanche(?bool $dimanche): self
    {
        $this->dimanche = $dimanche;

        return $this;
    }

    public function getSamedi(): ?bool
    {
        return $this->samedi;
    }

    public function setSamedi(?bool $samedi): self
    {
        $this->samedi = $samedi;

        return $this;
    }

    public function getFerier(): ?bool
    {
        return $this->ferier;
    }

    public function setFerier(?bool $ferier): self
    {
        $this->ferier = $ferier;

        return $this;
    }

    public function getVingtQuatre(): ?bool
    {
        return $this->vingtQuatre;
    }

    public function setVingtQuatre(?bool $vingtQuatre): self
    {
        $this->vingtQuatre = $vingtQuatre;

        return $this;
    }

    public function getTrenteUn(): ?bool
    {
        return $this->trenteUn;
    }

    public function setTrenteUn(?bool $trenteUn): self
    {
        $this->trenteUn = $trenteUn;

        return $this;
    }
}
