<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SignatureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SignatureRepository::class)
 */
class Signature
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $contratId;

    /**
     * @ORM\Column(type="integer", options={"comment":" 0 = client; 1 = interim et 2 = autres docs"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $companyName;
     /**
     * @ORM\Column(type="string", length=100)
     */
    private $customerNumber;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $referenceMission;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $specialite;

    /**
     * @ORM\Column(type="integer")
     */
    private $relance = 0;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $civilitySignataire;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nomSignataire;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prenomSignataire;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $emailSignataire;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $idSignataire;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cellPhoneSignataire;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $remarque;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $statusTranasaction;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $transactionId;

    /**
     * @ORM\Column(type="integer",options={"comment":" 3 = Mail; 1 = sms et 2 = tactile"})
     */
    private $signatureMode;

    /**
     * @ORM\Column(type="string",length=150,nullable=true)
     */
    private $signatureDate;


    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $signatureStatus;



    /**
     * @ORM\ManyToOne(targetEntity=Contrat::class, inversedBy="signatures")
     */
    private $contrat_pld;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $dateDernierRelance;

    /**
     * @ORM\Column(type="date")
     */
    private $dateEnvoi;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $dateSignature;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getContratId()
    {
        return $this->contratId;
    }

    /**
     * @param mixed $contratId
     */
    public function setContratId($contratId): void
    {
        $this->contratId = $contratId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * @param mixed $customerNumber
     */
    public function setCustomerNumber($customerNumber): void
    {
        $this->customerNumber = $customerNumber;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1): void
    {
        $this->address1 = $address1;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getReferenceMission()
    {
        return $this->referenceMission;
    }

    /**
     * @param mixed $referenceMission
     */
    public function setReferenceMission($referenceMission): void
    {
        $this->referenceMission = $referenceMission;
    }

    /**
     * @return mixed
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param mixed $specialite
     */
    public function setSpecialite($specialite): void
    {
        $this->specialite = $specialite;
    }

    /**
     * @return int
     */
    public function getRelance(): int
    {
        return $this->relance;
    }

    /**
     * @param int $relance
     */
    public function setRelance(int $relance): void
    {
        $this->relance = $relance;
    }

    /**
     * @return mixed
     */
    public function getCivilitySignataire()
    {
        return $this->civilitySignataire;
    }

    /**
     * @param mixed $civilitySignataire
     */
    public function setCivilitySignataire($civilitySignataire): void
    {
        $this->civilitySignataire = $civilitySignataire;
    }

    /**
     * @return mixed
     */
    public function getNomSignataire()
    {
        return $this->nomSignataire;
    }

    /**
     * @param mixed $nomSignataire
     */
    public function setNomSignataire($nomSignataire): void
    {
        $this->nomSignataire = $nomSignataire;
    }

    /**
     * @return mixed
     */
    public function getPrenomSignataire()
    {
        return $this->prenomSignataire;
    }

    /**
     * @param mixed $prenomSignataire
     */
    public function setPrenomSignataire($prenomSignataire): void
    {
        $this->prenomSignataire = $prenomSignataire;
    }

    /**
     * @return mixed
     */
    public function getEmailSignataire()
    {
        return $this->emailSignataire;
    }

    /**
     * @param mixed $emailSignataire
     */
    public function setEmailSignataire($emailSignataire): void
    {
        $this->emailSignataire = $emailSignataire;
    }

    /**
     * @return mixed
     */
    public function getIdSignataire()
    {
        return $this->idSignataire;
    }

    /**
     * @param mixed $idSignataire
     */
    public function setIdSignataire($idSignataire): void
    {
        $this->idSignataire = $idSignataire;
    }

    /**
     * @return mixed
     */
    public function getCellPhoneSignataire()
    {
        return $this->cellPhoneSignataire;
    }

    /**
     * @param mixed $cellPhoneSignataire
     */
    public function setCellPhoneSignataire($cellPhoneSignataire): void
    {
        $this->cellPhoneSignataire = $cellPhoneSignataire;
    }

    /**
     * @return mixed
     */
    public function getStatusTranasaction()
    {
        return $this->statusTranasaction;
    }

    /**
     * @param mixed $statusTranasaction
     */
    public function setStatusTranasaction($statusTranasaction): void
    {
        $this->statusTranasaction = $statusTranasaction;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getSignatureMode()
    {
        return $this->signatureMode;
    }

    /**
     * @param mixed $signatureMode
     */
    public function setSignatureMode($signatureMode): void
    {
        $this->signatureMode = $signatureMode;
    }

    /**
     * @return mixed
     */
    public function getSignatureDate()
    {
        return $this->signatureDate;
    }

    /**
     * @param mixed $signatureDate
     */
    public function setSignatureDate($signatureDate): void
    {
        $this->signatureDate = $signatureDate;
    }

    /**
     * @return mixed
     */
    public function getSignatureStatus()
    {
        return $this->signatureStatus;
    }

    /**
     * @param mixed $signatureStatus
     */
    public function setSignatureStatus($signatureStatus): void
    {
        $this->signatureStatus = $signatureStatus;
    }

    /**
     * @return mixed
     */
    public function getContratPld()
    {
        return $this->contrat_pld;
    }

    /**
     * @param mixed $contrat_pld
     */
    public function setContratPld($contrat_pld): void
    {
        $this->contrat_pld = $contrat_pld;
    }

    /**
     * @return mixed
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * @param mixed $dateEnvoi
     */
    public function setDateEnvoi($dateEnvoi): void
    {
        $this->dateEnvoi = $dateEnvoi;
    }

    /**
     * @return mixed
     */
    public function getDateSignature()
    {
        return $this->dateSignature;
    }

    /**
     * @param mixed $dateSignature
     */
    public function setDateSignature($dateSignature): void
    {
        $this->dateSignature = $dateSignature;
    }

    /**
     * @return mixed
     */
    public function getDateDernierRelance()
    {
        return $this->dateDernierRelance;
    }

    /**
     * @param mixed $dateDernierRelance
     */
    public function setDateDernierRelance($dateDernierRelance): void
    {
        $this->dateDernierRelance = $dateDernierRelance;
    }

    /**
     * @return mixed
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * @param mixed $remarque
     */
    public function setRemarque($remarque): void
    {
        $this->remarque = $remarque;
    }




}
