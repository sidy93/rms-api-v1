<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SpecialiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(attributes={"pagination_enabled"=false},normalizationContext={"groups"={"specialite_read"}})
 * @ORM\Entity(repositoryClass=SpecialiteRepository::class)
 * @UniqueEntity("nomSpecialite")
 */
class Specialite
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"specialite_read","interim_qualification_subresource", "fs_read", "garde_read", "mission_read", "planning_read","candidature_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"specialite_read","interim_read", "mission_read", "planning_read","client_read",
      "interim_qualification_subresource", "garde_read", "offre_read","candidature_read"})

     */
    private $nomSpecialite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"specialite_read", "mission_read",
     *     "interim_read","interim_qualification_subresource", "offre_read","candidature_read"})

     */
    private $specialite;

    /**
     * @ORM\OneToMany(targetEntity=Qualification::class, mappedBy="specialite", orphanRemoval=true,
     *     cascade={"persist"})
     * @Groups({"specialite_read"})
     */
    private $qualifications;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"specialite_read", "mission_read",
     *     "interim_read","interim_qualification_subresource", "offre_read","candidature_read"})
     */
    private $abreviation;


    public function __construct()
    {
        $this->qualifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     * @Groups({"fs_read"})
     */
    public function getNomSpecialite(): ?string
    {
        return $this->nomSpecialite;
    }

    public function setNomSpecialite(string $nomSpecialite): self
    {
        $this->nomSpecialite = $nomSpecialite;

        return $this;
    }

    public function getSpecialite(): ?string
    {
        return $this->specialite;
    }

    public function setSpecialite(string $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }

    /**
     * @return Collection|Qualification[]
     */
    public function getQualifications(): Collection
    {
        return $this->qualifications;
    }

    public function addQualification(Qualification $qualification): self
    {
        if (!$this->qualifications->contains($qualification)) {
            $this->qualifications[] = $qualification;
            $qualification->setSpecialite($this);
        }

        return $this;
    }

    public function removeQualification(Qualification $qualification): self
    {
        if ($this->qualifications->contains($qualification)) {
            $this->qualifications->removeElement($qualification);
            // set the owning side to null (unless already changed)
            if ($qualification->getSpecialite() === $this) {
                $qualification->setSpecialite(null);
            }
        }

        return $this;
    }

    public function getAbreviation(): ?string
    {
        return $this->abreviation;
    }

    public function setAbreviation(?string $abreviation): self
    {
        $this->abreviation = $abreviation;

        return $this;
    }

}
