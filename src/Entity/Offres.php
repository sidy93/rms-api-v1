<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OffresRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"offre_read"}},
 *     denormalizationContext={"groups"={"offre_write"}}
 * )
 * @ORM\Entity(repositoryClass=OffresRepository::class)
 */
class Offres
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offre_read", "offre_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"offre_read", "offre_write"})
     */
    private $debut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"offre_read", "offre_write"})
     */
    private $fin;

    /**
     * @ORM\Column(type="string")
     * @Groups({"offre_read", "offre_write"})
     */
    private $rythme;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre_read", "offre_write"})
     */
    private $domaine;

    /**
     * @ORM\Column(type="float", length=255)
     * @Groups({"offre_read", "offre_write"})
     */
    private $salaire;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre_read", "offre_write"})
     */
    private $typeRemuneration;

    /**
     * @ORM\ManyToOne(targetEntity=FicheSpecialite::class, inversedBy="offres")
     * @Groups({"offre_read", "offre_write"})
     */
    private $ficheSpecialite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre_read", "offre_write"})
     */
    private $reference;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"offre_read", "offre_write"})
     */
    private $poste;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre_read", "offre_write"})
     */
    private $contrat;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"offre_read", "offre_write"})
     */
    private $enable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDebut(): ?string
    {
        return $this->debut->format('d-m-Y');
    }

    public function setDebut(DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?string
    {
        return $this->fin->format('d-m-Y');
    }

    public function setfin(?DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getRythme(): ?string
    {
        return $this->rythme;
    }

    public function setRythme(string $rythme): self
    {
        $this->rythme = $rythme;

        return $this;
    }

    public function getDomaine(): ?string
    {
        return $this->domaine;
    }

    public function setDomaine(string $domaine): self
    {
        $this->domaine = $domaine;

        return $this;
    }

    public function getSalaire(): ?string
    {
        return $this->salaire;
    }

    public function setSalaire(string $salaire): self
    {
        $this->salaire = $salaire;

        return $this;
    }

    public function getTypeRemuneration(): ?string
    {
        return $this->typeRemuneration;
    }

    public function setTypeRemuneration(string $typeRemuneration): self
    {
        $this->typeRemuneration = $typeRemuneration;

        return $this;
    }

    public function getFicheSpecialite(): ?FicheSpecialite
    {
        return $this->ficheSpecialite;
    }

    public function setFicheSpecialite(?FicheSpecialite $ficheSpecialite): self
    {
        $this->ficheSpecialite = $ficheSpecialite;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getPoste(): ?int
    {
        return $this->poste;
    }

    public function setPoste(int $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getContrat(): ?string
    {
        return $this->contrat;
    }

    public function setContrat(string $contrat): self
    {
        $this->contrat = $contrat;

        return $this;
    }

    public function getEnable(): ?int
    {
        return $this->enable;
    }

    public function setEnable(int $enable): self
    {
        $this->enable = $enable;

        return $this;
    }
}
