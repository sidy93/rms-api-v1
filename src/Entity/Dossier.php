<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DossierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get"={"method"="get"},
 *      "post"={"method"="post", "path"="/dossiers/{id}","controller"="App\Controller\DossierController"},
 *      "delete_item_dossier_by_id_user"={
 *          "method"="post",
 *          "path"="delete-element-dossiers/{id}",
 *          "controller"="App\Controller\DossierController",
 *          "swagger_context"={
 *              "summary"="test",
 *              "description"="test",
 *          }
 *      },
 *     },
 *     itemOperations={
 *      "put"={"method"="put"},
 *     "get"={"method"="get", "path"="/dossiers/{id}","controller"="App\Controller\DossierController"},
 *     }
 * )
 * @ORM\Entity(repositoryClass=DossierRepository::class)
 */
class Dossier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="dossier")
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $validitePieceIdentite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pieceName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carteVitaleName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carteGriseName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cvName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inscriptionName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $jDomicileName;


    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $remarques;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ribName;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $etat;

    /**
     * @ORM\OneToMany(targetEntity=ComplementDossier::class, mappedBy="dossier")
     */
    private $complementDossiers;

    public function __construct()
    {

        $this->complementDossiers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }





    public function getValiditePieceIdentite(): ?\DateTimeInterface
    {
        return $this->validitePieceIdentite;
    }

    public function setValiditePieceIdentite(?\DateTimeInterface $validitePieceIdentite): self
    {
        $this->validitePieceIdentite = $validitePieceIdentite;

        return $this;
    }

    public function getPieceName(): ?string
    {
        return $this->pieceName;
    }

    public function setPieceName(?string $pieceName): self
    {
        $this->pieceName = $pieceName;

        return $this;
    }

    public function getCarteVitaleName(): ?string
    {
        return $this->carteVitaleName;
    }

    public function setCarteVitaleName(?string $carteVitaleName): self
    {
        $this->carteVitaleName = $carteVitaleName;

        return $this;
    }

    public function getCarteGriseName(): ?string
    {
        return $this->carteGriseName;
    }

    public function setCarteGriseName(?string $carteGriseName): self
    {
        $this->carteGriseName = $carteGriseName;

        return $this;
    }

    public function getCvName(): ?string
    {
        return $this->cvName;
    }

    public function setCvName(?string $cvName): self
    {
        $this->cvName = $cvName;

        return $this;
    }

    public function getInscriptionName(): ?string
    {
        return $this->inscriptionName;
    }

    public function setInscriptionName(?string $inscriptionName): self
    {
        $this->inscriptionName = $inscriptionName;

        return $this;
    }

    public function getJDomicileName(): ?string
    {
        return $this->jDomicileName;
    }

    public function setJDomicileName(?string $jDomicileName): self
    {
        $this->jDomicileName = $jDomicileName;

        return $this;
    }

    public function getRibName(): ?string
    {
        return $this->ribName;
    }

    public function setRibName(?string $ribName): self
    {
        $this->ribName = $ribName;

        return $this;
    }

    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|ComplementDossier[]
     */
    public function getComplementDossiers(): Collection
    {
        return $this->complementDossiers;
    }

    public function addComplementDossier(ComplementDossier $complementDossier): self
    {
        if (!$this->complementDossiers->contains($complementDossier)) {
            $this->complementDossiers[] = $complementDossier;
            $complementDossier->setDossier($this);
        }

        return $this;
    }

    public function removeComplementDossier(ComplementDossier $complementDossier): self
    {
        if ($this->complementDossiers->contains($complementDossier)) {
            $this->complementDossiers->removeElement($complementDossier);
            // set the owning side to null (unless already changed)
            if ($complementDossier->getDossier() === $this) {
                $complementDossier->setDossier(null);
            }
        }

        return $this;
    }
    public function dossierMed() {
        $dossierMed = [];
        foreach ($this as $key => $value) {
            if ($key!='user' && $key!='id' && $key!='updatedAt' && $key!='validitePieceIdentite' && $key!='complementDossiers' && $key!='remarques')
                $dossierMed[$key] = $value;
        }
        return $dossierMed;
    }

    public function deleteItem($elemet) {
        foreach ($this as $key => $value) {
            if($key==$elemet){
                $this->$key = null;
            }
        }
    }
    public function getValueByItem($elemet){
        foreach ($this as $key => $value) {
            if($key==$elemet){
                return $value;
            }
        }
    }
    public function attributs() {
        $attributs = [];
        foreach ($this as $key => $value) {
            if ($key!='createAt' && $key!='id' && $key!='updateAt' && $key!='validitePieceIdentite' && $key!='complementDossiers')
                $attributs[] = $key;
        }
        return $attributs;
    }
    public function UpdateItem($elemet,$valeur) {
        foreach ($this as $key => $value) {
            if($key==$elemet){
                $this->$key = $valeur;
            }
        }
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemarques()
    {
        return $this->remarques;
    }

    /**
     * @param mixed $remarques
     */
    public function setRemarques($remarques): void
    {
        $this->remarques = $remarques;
    }


}
