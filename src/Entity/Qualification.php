<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\QualificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 *@ApiResource(
 * subresourceOperations={
 *      "api_interims_qualifications_get_subresource"={
 *         "method"="GET",
 *         "normalization_context"={"groups"={"interim_qualification_subresource"}
 *     }
 *     }
 *     }
 *
 * )
 * @ORM\Entity(repositoryClass=QualificationRepository::class)
 * @UniqueEntity("qualification")
 */
class Qualification
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"specialite_read", "fs_write", "interim_qualification_subresource", "fs_read", "mission_read", "planning_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Specialite::class, inversedBy="qualifications")
     * @Groups({"interim_read", "client_read",
     *     "interim_qualification_subresource", "fs_read", "garde_read", "offre_read", "mission_read", "planning_read","candidature_read"})
     */
    private $specialite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"specialite_read","interim_read","interim_qualification_subresource", "fs_read"})
     *
     */
    private $qualification;



    public function __construct()
    {
        $this->interims = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecialite(): ?Specialite
    {
        return $this->specialite;
    }

    public function setSpecialite(?Specialite $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }

    public function getQualification(): ?string
    {
        return $this->qualification;
    }

    public function setQualification(string $qualification): self
    {
        $this->qualification = $qualification;

        return $this;
    }
}
