<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FicheClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FicheClientRepository::class)
 * @ApiFilter(SearchFilter::class)
 */
class FicheClient
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity=Hebergement::class, mappedBy="ficheClient", orphanRemoval=true)
     * @Groups({"client_read"})
     */
    private $hebergement;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $situationStructure;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $distanceGare;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $distanceAeroport;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $infoPratiqueVille;

    /**
     * @ORM\Column(type="string")
     */
    private $hebergementInternat;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarqueHebergement;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $taxi;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $repas;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarqueRepas;

    /**
     * @ORM\OneToOne(targetEntity=Client::class, inversedBy="ficheClient", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $client;


    public function __construct()
    {
        $this->hebergement = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return Collection|Hebergement[]
     */
    public function getHebergement(): Collection
    {
        return $this->hebergement;
    }

    public function addHebergement(Hebergement $hebergement): self
    {
        if (!$this->hebergement->contains($hebergement)) {
            $this->hebergement[] = $hebergement;
            $hebergement->setFicheClient($this);
        }

        return $this;
    }

    public function removeHebergement(Hebergement $hebergement): self
    {
        if ($this->hebergement->contains($hebergement)) {
            $this->hebergement->removeElement($hebergement);
            // set the owning side to null (unless already changed)
            if ($hebergement->getFicheClient() === $this) {
                $hebergement->setFicheClient(null);
            }
        }

        return $this;
    }

    public function getSituationStructure(): ?string
    {
        return $this->situationStructure;
    }

    public function setSituationStructure(?string $situationStructure): self
    {
        $this->situationStructure = $situationStructure;

        return $this;
    }

    public function getDistanceGare(): ?int
    {
        return $this->distanceGare;
    }

    public function setDistanceGare(?int $distanceGare): self
    {
        $this->distanceGare = $distanceGare;

        return $this;
    }

    public function getDistanceAeroport(): ?int
    {
        return $this->distanceAeroport;
    }

    public function setDistanceAeroport(?int $distanceAeroport): self
    {
        $this->distanceAeroport = $distanceAeroport;

        return $this;
    }

    public function getInfoPratiqueVille(): ?string
    {
        return $this->infoPratiqueVille;
    }

    public function setInfoPratiqueVille(?string $infoPratiqueVille): self
    {
        $this->infoPratiqueVille = $infoPratiqueVille;

        return $this;
    }

    public function getHebergementInternat(): ?string
    {
        return $this->hebergementInternat;
    }

    public function setHebergementInternat(string $hebergementInternat): self
    {
        $this->hebergementInternat = $hebergementInternat;

        return $this;
    }

    public function getRemarqueHebergement(): ?string
    {
        return $this->remarqueHebergement;
    }

    public function setRemarqueHebergement(?string $remarqueHebergement): self
    {
        $this->remarqueHebergement = $remarqueHebergement;

        return $this;
    }

    public function getTaxi(): ?string
    {
        return $this->taxi;
    }

    public function setTaxi(?string $taxi): self
    {
        $this->taxi = $taxi;

        return $this;
    }

    public function getRepas(): ?string
    {
        return $this->repas;
    }

    public function setRepas(?string $repas): self
    {
        $this->repas = $repas;

        return $this;
    }

    public function getRemarqueRepas(): ?string
    {
        return $this->remarqueRepas;
    }

    public function setRemarqueRepas(?string $remarqueRepas): self
    {
        $this->remarqueRepas = $remarqueRepas;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

}
