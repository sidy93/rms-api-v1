<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PieceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *     collectionOperations={
 *      "get",
 *      "post"={"method"="post", "controller"="App\Controller\PieceController"},
 *      "saveFile"={"method"="post", "path"="/pieces/save-files", "controller"="App\Controller\PieceController"},
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={"justificatif":"exact"})
 * @ORM\Entity(repositoryClass=PieceRepository::class)
 */
class Piece
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Justificatif::class, inversedBy="pieces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $justificatif;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pieceFileName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $file;

    public function __construct()
    {
        $this->createdAt();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJustificatif(): ?Justificatif
    {
        return $this->justificatif;
    }

    public function setJustificatif(?Justificatif $justificatif): self
    {
        $this->justificatif = $justificatif;

        return $this;
    }

    public function getPieceFileName(): ?string
    {
        return $this->pieceFileName;
    }

    public function setPieceFileName(string $pieceFileName): self
    {
        $this->pieceFileName = $pieceFileName;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }
}
