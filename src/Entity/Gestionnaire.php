<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\GestionnaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"gi_read"}},
 *     denormalizationContext={"groups"={"gi_write"}},
 *     attributes={"order"={"id": "ASC"}},
 *     itemOperations={
 *      "get"={"method"="GET"},
 *      "delete"={"method"="delete"},
 *      "put"={"method"="put"},
 *     "deleteTacheGestionnaire"={"method"="delete","path"="/gestionnaires/{id}/taches","controller"="App\Controller\GestionnaireController"},
 *     }
 * )
 * @ORM\Entity(repositoryClass=GestionnaireRepository::class)
 */
class Gestionnaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"gi_read","tache_write", "tache_read"})
     *
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @Groups({"gi_read","gi_write", "tache_read"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"gi_read", "gi_write"})
     */
    private $idReferentiel;

    /**
     * @ORM\OneToMany(targetEntity=TacheGestionnaire::class, mappedBy="gestionnaire")
     * @Groups({"gi_read"})
     */

    private $tacheGestionnaires;



    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"gi_read", "gi_write"})
     */
    private $auteur;

    public function __construct()
    {
        $this->tacheGestionnaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIdReferentiel(): ?string
    {
        return $this->idReferentiel;
    }

    public function setIdReferentiel(?string $idReferentiel): self
    {
        $this->idReferentiel = $idReferentiel;

        return $this;
    }

    /**
     * @return Collection|TacheGestionnaire[]
     */
    public function getTacheGestionnaires(): Collection
    {
        return $this->tacheGestionnaires;
    }

    public function addTacheGestionnaire(TacheGestionnaire $tacheGestionnaire): self
    {
        if (!$this->tacheGestionnaires->contains($tacheGestionnaire)) {
            $this->tacheGestionnaires[] = $tacheGestionnaire;
            $tacheGestionnaire->setGestionnaire($this);
        }

        return $this;
    }

    public function removeTacheGestionnaire(TacheGestionnaire $tacheGestionnaire): self
    {
        if ($this->tacheGestionnaires->contains($tacheGestionnaire)) {
            $this->tacheGestionnaires->removeElement($tacheGestionnaire);
            // set the owning side to null (unless already changed)
//            if ($tacheGestionnaire->getGestionnaire() === $this) {
                $tacheGestionnaire->setGestionnaire(null);
//            }
        }

        return $this;
    }


    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }
}
