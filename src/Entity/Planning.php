<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlanningRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *     normalizationContext={"groups"={"planning_read"}},
 * )
 * @ORM\Entity(repositoryClass=PlanningRepository::class)
 */
class Planning
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"mission_read", "mission_write", "planning_read","candidature_read"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Candidature::class, mappedBy="planning")
     */
    private $candidature;

    /**
     * @ORM\Column(type="float")
     * @Groups({"mission_write", "mission_read", "planning_read","candidature_read"})
     */
    private $tarif;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"mission_read","candidature_read"})
     */
    private $majorer;

    /**
     * @ORM\Column(type="float")
     * @Groups({"mission_write", "mission_read", "planning_read","candidature_read"})
     */
    private $salaireNet;

    /**
     * @ORM\Column(type="date")
     * @Groups({"mission_write", "mission_read", "planning_read","candidature_read"})
     */
    private $date;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"mission_write", "mission_read", "planning_read","candidature_read"})
     */
    private $qualifications;

    /**
     * @ORM\ManyToOne(targetEntity=StatutPlanning::class, inversedBy="planning")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"mission_write", "mission_read", "planning_read", "candidature_read"})
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity=Mission::class, inversedBy="planning")
     * @Groups({"planning_read","candidature_read"})
     */
    private $mission;

    /**
     * @ORM\ManyToOne(targetEntity=TypeDeGarde::class, inversedBy="planning")
     * @Groups({"mission_write", "planning_read", "mission_read","candidature_read"})
     */
    private $typeDeGarde;

    /**
     * @ORM\Column(type="float")
     * @Groups({"mission_write", "mission_read", "planning_read"})
     */
    private $salaireBrute;

    public function __construct()
    {
        $this->candidature = new ArrayCollection();
        $this->createdAt();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Candidature[]
     */
    public function getCandidature(): Collection
    {
        return $this->candidature;
    }

    public function addCandidature(Candidature $candidature): self
    {
        if (!$this->candidature->contains($candidature)) {
            $this->candidature[] = $candidature;
            $candidature->setPlanning($this);
        }

        return $this;
    }

    public function removeCandidature(Candidature $candidature): self
    {
        if ($this->candidature->contains($candidature)) {
            $this->candidature->removeElement($candidature);
            // set the owning side to null (unless already changed)
            if ($candidature->getPlanning() === $this) {
                $candidature->setPlanning(null);
            }
        }

        return $this;
    }

    public function getTarif(): ?float
    {
        return $this->tarif;
    }

    public function setTarif(float $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getMajorer(): ?bool
    {
        return $this->majorer;
    }

    public function setMajorer(?bool $majorer): self
    {
        $this->majorer = $majorer;

        return $this;
    }

    public function getSalaireNet(): ?float
    {
        return $this->salaireNet;
    }

    public function setSalaireNet(float $salaireNet): self
    {
        $this->salaireNet = $salaireNet;

        return $this;
    }

    public function getQualifications()
    {
        return $this->qualifications;
    }

    public function setQualifications(?array $qualifications)
    {
        $this->qualifications = $qualifications;

        return $this;
    }

    public function getStatut(): ?StatutPlanning
    {
        return $this->statut;
    }

    public function setStatut(?StatutPlanning $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getMission(): ?Mission
    {
        return $this->mission;
    }

    public function setMission(?Mission $mission): self
    {
        $this->mission = $mission;

        return $this;
    }

    public function getTypeDeGarde(): ?TypeDeGarde
    {
        return $this->typeDeGarde;
    }

    public function setTypeDeGarde(?TypeDeGarde $typeDeGarde): self
    {
        $this->typeDeGarde = $typeDeGarde;

        return $this;
    }

    public function getSalaireBrute(): ?float
    {
        return $this->salaireBrute;
    }

    public function setSalaireBrute(float $salaireBrute): self
    {
        $this->salaireBrute = $salaireBrute;

        return $this;
    }

    /**
     * get data format
     * @Groups({"planning_read", "mission_read",})
     */
    public function getDateFormat()
    {
        return $this->getDate()->format("d-m-y");
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
