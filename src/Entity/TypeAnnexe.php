<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeAnnexeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"type_annexe_read"}},
 * )
 * @ORM\Entity(repositoryClass=TypeAnnexeRepository::class)
 */
class TypeAnnexe
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"type_annexe_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"type_annexe_read"})
     */
    private $libelle;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"type_annexe_read"})
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Annexe::class, mappedBy="typeAnnexe")
     * @Groups({"type_annexe_read"})
     */
    private $annexes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"type_annexe_read"})
     */
    private $url;

    public function __construct()
    {
        $this->annexes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Annexe[]
     */
    public function getAnnexes(): Collection
    {
        return $this->annexes;
    }

    public function addAnnex(Annexe $annex): self
    {
        if (!$this->annexes->contains($annex)) {
            $this->annexes[] = $annex;
            $annex->setTypeAnnexe($this);
        }

        return $this;
    }

    public function removeAnnex(Annexe $annex): self
    {
        if ($this->annexes->contains($annex)) {
            $this->annexes->removeElement($annex);
            // set the owning side to null (unless already changed)
            if ($annex->getTypeAnnexe() === $this) {
                $annex->setTypeAnnexe(null);
            }
        }

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
