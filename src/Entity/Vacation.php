<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\VacationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource()
 * @ORM\Entity(repositoryClass=VacationRepository::class)
 * @UniqueEntity("designation")
 */
class Vacation
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"garde_read", "planning_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"garde_read", "fs_read", "planning_read","candidature_read"})
     */
    private $designation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=TypeDeGarde::class, mappedBy="typeVacation", orphanRemoval=true)
     */
    private $typeDeGardes;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"garde_read"})
     */
    private $heureDebut;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"garde_read"})
     */
    private $heureFin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $abreviation;

    public function __construct()
    {
        $this->typeDeGardes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|TypeDeGarde[]
     */
    public function getTypeDeGardes(): Collection
    {
        return $this->typeDeGardes;
    }

    public function addTypeDeGarde(TypeDeGarde $typeDeGarde): self
    {
        if (!$this->typeDeGardes->contains($typeDeGarde)) {
            $this->typeDeGardes[] = $typeDeGarde;
            $typeDeGarde->setTypeVacation($this);
        }

        return $this;
    }

    public function removeTypeDeGarde(TypeDeGarde $typeDeGarde): self
    {
        if ($this->typeDeGardes->contains($typeDeGarde)) {
            $this->typeDeGardes->removeElement($typeDeGarde);
            // set the owning side to null (unless already changed)
            if ($typeDeGarde->getTypeVacation() === $this) {
                $typeDeGarde->setTypeVacation(null);
            }
        }

        return $this;
    }

    public function getHeureDebut()
    {
        return $this->heureDebut ? $this->heureDebut->format("H:i") : "null";
    }

    public function setHeureDebut(?\DateTimeInterface $heureDebut): self
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getHeureFin()
    {
        return $this->heureFin ? $this->heureFin->format("H:i") : "null";
    }

    public function setHeureFin(?\DateTimeInterface $heureFin): self
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    public function getAbreviation(): ?string
    {
        return $this->abreviation;
    }

    public function setAbreviation(?string $abreviation): self
    {
        $this->abreviation = $abreviation;

        return $this;
    }
}
