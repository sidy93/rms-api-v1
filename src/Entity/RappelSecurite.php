<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RappelSecuriteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource()
 * @ORM\Entity(repositoryClass=RappelSecuriteRepository::class)
 */
class RappelSecurite
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=MissionInterim::class, inversedBy="rappelSecurites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $missionInterim;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $conformiteMission;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $remarque;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMissionInterim(): ?MissionInterim
    {
        return $this->missionInterim;
    }

    public function setMissionInterim(?MissionInterim $missionInterim): self
    {
        $this->missionInterim = $missionInterim;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getConformiteMission()
    {
        return $this->conformiteMission;
    }

    /**
     * @param mixed $conformiteMission
     */
    public function setConformiteMission($conformiteMission): void
    {
        $this->conformiteMission = $conformiteMission;
    }

    /**
     * @return mixed
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * @param mixed $remarque
     */
    public function setRemarque($remarque): void
    {
        $this->remarque = $remarque;
    }


}
