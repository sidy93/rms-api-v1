<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Model\UpdateAttribut;
use App\Repository\InterimRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;

/** Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *      subresourceOperations={
 *        "prospection_interims_get_subresource"={"path"="/interims/{id}/prospections"}
 *     },
 *      normalizationContext={"groups"={"interim_read"}},

 *     itemOperations={
 *      "get"={"method"="GET"},
 *      "delete"={"method"="delete"},
 *      "put"={"method"="put"},
 *     "getSpecialite"={"method"="get","path"="/interims/{id}/specialites","controller"="App\Controller\SpecialiteInterimController"},
 *     "postSpecialite"={"method"="post","path"="/interims/updatespecialites/{id}","controller"="App\Controller\SpecialiteInterimController"},
 *     "getDisponibilite"={"method"="get","path"="/interims/{id}/disponibilite","controller"="App\Controller\DisponibiliteController"},
 *     "genererContrat"={"method"="post","path"="/interims/generer-contrat/{id}","controller"="App\Controller\ContratPldController"},
 *     "postDisponibilite"={"method"="post","path"="/interims/update-disponibilites/{id}","controller"="App\Controller\DisponibiliteController"},
 *     "getOffres"={"method"="get","path"="/interims/offres/{id}","controller"="App\Controller\OffresInterimController"}
 *
 *
 *     }
 *
 *     )
 * @ApiFilter(SearchFilter::class,properties={

 *     "user.nom":"partial",
 *     "portable":"partial",
 *     "idSellAndSign":"partial",
 *     "statut":"partial",
 *     "statut":"partial",
 *     "remarque":"partial",
 *     "auteur":"partial",
 *     "user.email":"partial",
 *     "user.prenom":"partial",
 *     "user.civilite":"partial",
 *     "user.adresse":"partial",
 *     "user.ville":"partial",
 *     "user.cp":"partial",
 *     "user.departement":"partial",
 *     "user.nationalite":"partial",
 *     "user.username":"partial",
 *     "qualifications.specialite.nomSpecialite":"partial"
 * })
 * @ApiFilter(BooleanFilter::class, properties={"actif","cible","inscription","user.isEnable","dossier.enable"})

 * @ApiFilter(DateFilter::class, properties={"createAt":DateFilter::EXCLUDE_NULL})
 *
 *
 * @ORM\Entity(repositoryClass=InterimRepository::class)
 */
class Interim
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"interim_read","candidature_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"interim_read"})
     */
    private $fixe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"interim_read"})
     */
    private $portable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"interim_read"})
     */
    private $numRpps;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"interim_read"})
     */
    private $inscription;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"interim_read"})
     */
    private $cible;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"interim_read"})
     */
    private $idSellAndSign;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"interim_read"})
     */
    private $statut;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"interim_read"})
     */
    private $remarque;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"interim_read"})
     */
    private $autreRemarques;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"interim_read"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"interim_read"})
     */
    private $coefIk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"interim_read"})
     */
    private $pcs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"interim_read"})
     */
    private $rayonIntervation;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"interim_read"})
     */
    private $preferenceDeplacement = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"interim_read"})
     */
    private $preferenceContact = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"interim_read"})
     */
    private $horaireJoignabilite = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"interim_read"})
     */
    private $disponibiliteAppel = [];

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"interim_read"})
     */
    private $auteur;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"interim_read"})
     */
    private $actif;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"interim_read","candidature_read"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Ressource::class)
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"interim_read"})
     */
    private $source;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"interim_read"})
     */
    private $idReglementAcompte;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"interim_read"})
     */
    private $idReglement;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"interim_read"})
     */
    private $idMoyenLocomotion;



    /**
     * @ORM\OneToMany(targetEntity=ProspectionInterim::class, mappedBy="interim",cascade={"remove"})
     * @ApiSubresource()
     */
    private $prospectionInterims;

    /**
     * @ORM\ManyToMany(targetEntity=Qualification::class, inversedBy="interims")
     * @Groups({"interim_read"})
     * @ApiSubresource()
     */
    private $qualifications;

    /**
     * @ORM\OneToMany(targetEntity=Disponibilite::class, mappedBy="interim",cascade={"remove"})
     * @ApiSubresource()
     */
    private $disponibilites;

    /**
     * @ORM\OneToMany(targetEntity=MissionInterim::class, mappedBy="interim", orphanRemoval=true)
     */
    private $missionInterims;

    /**
     * @ORM\OneToOne(targetEntity=Dossier::class, cascade={"persist", "remove"})
     */
    private $dossier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idPld;

    public function __construct()
    {
        $this->prospectionInterims = new ArrayCollection();
        $this->qualifications = new ArrayCollection();
        $this->disponibilites = new ArrayCollection();
        $this->missionInterims = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFixe(): ?string
    {
        return $this->fixe;
    }

    public function setFixe(?string $fixe): self
    {
        $this->fixe = $fixe;

        return $this;
    }

    public function getPortable(): ?string
    {
        return $this->portable;
    }

    public function setPortable(?string $portable): self
    {
        $this->portable = $portable;

        return $this;
    }

    public function getNumRpps(): ?string
    {
        return $this->numRpps;
    }

    public function setNumRpps(?string $numRpps): self
    {
        $this->numRpps = $numRpps;

        return $this;
    }

    public function getInscription(): ?bool
    {
        return $this->inscription;
    }

    public function setInscription(bool $inscription): self
    {
        $this->inscription = $inscription;

        return $this;
    }

    public function getCible(): ?bool
    {
        return $this->cible;
    }

    public function setCible(bool $cible): self
    {
        $this->cible = $cible;

        return $this;
    }

    public function getIdSellAndSign(): ?string
    {
        return $this->idSellAndSign;
    }

    public function setIdSellAndSign(?string $idSellAndSign): self
    {
        $this->idSellAndSign = $idSellAndSign;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getAutreRemarques(): ?string
    {
        return $this->autreRemarques;
    }

    public function setAutreRemarques(?string $autreRemarques): self
    {
        $this->autreRemarques = $autreRemarques;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCoefIk(): ?string
    {
        return $this->coefIk;
    }

    public function setCoefIk(?string $coefIk): self
    {
        $this->coefIk = $coefIk;

        return $this;
    }

    public function getPcs(): ?string
    {
        return $this->pcs;
    }

    public function setPcs(?string $pcs): self
    {
        $this->pcs = $pcs;

        return $this;
    }

    public function getRayonIntervation(): ?string
    {
        return $this->rayonIntervation;
    }

    public function setRayonIntervation(?string $rayonIntervation): self
    {
        $this->rayonIntervation = $rayonIntervation;

        return $this;
    }

    public function getPreferenceDeplacement(): ?array
    {
        return $this->preferenceDeplacement;
    }

    public function setPreferenceDeplacement(?array $preferenceDeplacement): self
    {
        $this->preferenceDeplacement = $preferenceDeplacement;

        return $this;
    }

    public function getPreferenceContact(): ?array
    {
        return $this->preferenceContact;
    }

    public function setPreferenceContact(?array $preferenceContact): self
    {
        $this->preferenceContact = $preferenceContact;

        return $this;
    }

    public function getHoraireJoignabilite(): ?array
    {
        return $this->horaireJoignabilite;
    }

    public function setHoraireJoignabilite(?array $horaireJoignabilite): self
    {
        $this->horaireJoignabilite = $horaireJoignabilite;

        return $this;
    }

    public function getDisponibiliteAppel(): ?array
    {
        return $this->disponibiliteAppel;
    }

    public function setDisponibiliteAppel(?array $disponibiliteAppel): self
    {
        $this->disponibiliteAppel = $disponibiliteAppel;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source): void
    {
        $this->source = $source;
    }







    public function getIdReglementAcompte(): ?int
    {
        return $this->idReglementAcompte;
    }

    public function setIdReglementAcompte(?int $idReglementAcompte): self
    {
        $this->idReglementAcompte = $idReglementAcompte;

        return $this;
    }

    public function getIdReglement(): ?int
    {
        return $this->idReglement;
    }

    public function setIdReglement(?int $idReglement): self
    {
        $this->idReglement = $idReglement;

        return $this;
    }

    public function getIdMoyenLocomotion(): ?int
    {
        return $this->idMoyenLocomotion;
    }

    public function setIdMoyenLocomotion(?int $idMoyenLocomotion): self
    {
        $this->idMoyenLocomotion = $idMoyenLocomotion;

        return $this;
    }



    /**
     * @return Collection|ProspectionInterim[]
     */
    public function getProspectionInterims(): Collection
    {
        return $this->prospectionInterims;
    }

    public function addProspectionInterim(ProspectionInterim $prospectionInterim): self
    {
        if (!$this->prospectionInterims->contains($prospectionInterim)) {
            $this->prospectionInterims[] = $prospectionInterim;
            $prospectionInterim->setInterim($this);
        }

        return $this;
    }

    public function removeProspectionInterim(ProspectionInterim $prospectionInterim): self
    {
        if ($this->prospectionInterims->contains($prospectionInterim)) {
            $this->prospectionInterims->removeElement($prospectionInterim);
            // set the owning side to null (unless already changed)
            if ($prospectionInterim->getInterim() === $this) {
                $prospectionInterim->setInterim(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Qualification[]
     */
    public function getQualifications(): Collection
    {
        return $this->qualifications;
    }

    public function addQualification(Qualification $qualification): self
    {
        $this->qualifications[] = $qualification;

        return $this;
    }

    public function removeQualification(Qualification $qualification): self
    {
        if ($this->qualifications->contains($qualification)) {
            $this->qualifications->removeElement($qualification);
        }

        return $this;
    }

    /**
     * @return Collection|Disponibilite[]
     */
    public function getDisponibilites(): Collection
    {
        return $this->disponibilites;
    }


    public function getCustomDisponibilite():array
    {
        $response = [];
        foreach ($this->getDisponibilites()->getValues()  as $dispo)
        {
            array_push($response,$dispo->getDate()->format(('Y-m-d')));
        }
        return  $response;
    }

    public function addDisponibilite(Disponibilite $disponibilite): self
    {
        if (!$this->disponibilites->contains($disponibilite)) {
            $this->disponibilites[] = $disponibilite;
            $disponibilite->setInterim($this);
        }

        return $this;
    }

    public function removeDisponibilite(Disponibilite $disponibilite): self
    {
        if ($this->disponibilites->contains($disponibilite)) {
            $this->disponibilites->removeElement($disponibilite);
            // set the owning side to null (unless already changed)
            if ($disponibilite->getInterim() === $this) {
                $disponibilite->setInterim(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MissionInterim[]
     */
    public function getMissionInterims(): Collection
    {
        return $this->missionInterims;
    }

    public function addMissionInterim(MissionInterim $missionInterim): self
    {
        if (!$this->missionInterims->contains($missionInterim)) {
            $this->missionInterims[] = $missionInterim;
            $missionInterim->setInterim($this);
        }

        return $this;
    }

    public function removeMissionInterim(MissionInterim $missionInterim): self
    {
        if ($this->missionInterims->contains($missionInterim)) {
            $this->missionInterims->removeElement($missionInterim);
            // set the owning side to null (unless already changed)
            if ($missionInterim->getInterim() === $this) {
                $missionInterim->setInterim(null);
            }
        }

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getQualificationSpecialite()

    {

        return $this->qualifications->first()->getSpecialite()->getNomspecialite();

    }

    public function getIdPld(): ?string
    {
        return $this->idPld;
    }

    public function setIdPld(string $idPld): self
    {
        $this->idPld = $idPld;

        return $this;
    }

}
