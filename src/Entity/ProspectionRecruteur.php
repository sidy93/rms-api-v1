<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProspectionRecruteurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProspectionRecruteurRepository::class)
 */
class ProspectionRecruteur
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Recruteur::class, inversedBy="prospectionRecruteurs")
     */
    private $recruteur;

    /**
     * @ORM\Column(type="text")
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=TypeProspection::class)
     */
    private $type;


    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateRapel;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $aRappeler;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $raisonSocial;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecruteur(): ?Recruteur
    {
        return $this->recruteur;
    }

    public function setRecruteur(?Recruteur $recruteur): self
    {
        $this->recruteur = $recruteur;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getType(): ?TypeProspection
    {
        return $this->type;
    }

    public function setType(?TypeProspection $type): self
    {
        $this->type = $type;

        return $this;
    }


    public function getDateRapel(): ?\DateTimeInterface
    {
        return $this->dateRapel;
    }

    public function setDateRapel(?\DateTimeInterface $dateRapel): self
    {
        $this->dateRapel = $dateRapel;

        return $this;
    }

    public function getARappeler(): ?bool
    {
        return $this->aRappeler;
    }

    public function setARappeler(?bool $aRappeler): self
    {
        $this->aRappeler = $aRappeler;

        return $this;
    }

    public function getRaisonSocial(): ?string
    {
        return $this->raisonSocial;
    }

    public function setRaisonSocial(string $raisonSocial): self
    {
        $this->raisonSocial = $raisonSocial;

        return $this;
    }
}
