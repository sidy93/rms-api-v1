<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RecruteurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *     normalizationContext={"groups"={"recruteur_read"}},
 *     denormalizationContext={"groups"={"recruteur_write"}}
 * )
 * @ORM\Entity(repositoryClass=RecruteurRepository::class)
 */
class Recruteur
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"recruteur_read", "recruteur_write", "client_read"})
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"recruteur_read", "recruteur_write", "client_read"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"recruteur_read", "recruteur_write", "client_read"})
     */
    private $fonction;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"recruteur_read", "recruteur_write", "client_read"})
     */
    private $signataire = false;
    /**
     * @ORM\OneToMany(targetEntity=ProspectionRecruteur::class, mappedBy="recruteur")
     */
    private $prospectionRecruteurs;

    /**
     * @ORM\ManyToMany(targetEntity=Client::class, inversedBy="recruteurs")
     * @Groups({"recruteur_read", "recruteur_write"})
     */
    private $client;

    public function __construct()
    {
        $this->prospectionRecruteurs = new ArrayCollection();
        $this->client = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFonction(): ?string
    {
        return $this->fonction;
    }

    public function setFonction(string $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * @return Collection|ProspectionRecruteur[]
     */
    public function getProspectionRecruteurs(): Collection
    {
        return $this->prospectionRecruteurs;
    }

    public function addProspectionRecruteur(ProspectionRecruteur $prospectionRecruteur): self
    {
        if (!$this->prospectionRecruteurs->contains($prospectionRecruteur)) {
            $this->prospectionRecruteurs[] = $prospectionRecruteur;
            $prospectionRecruteur->setRecruteur($this);
        }

        return $this;
    }

    public function removeProspectionRecruteur(ProspectionRecruteur $prospectionRecruteur): self
    {
        if ($this->prospectionRecruteurs->contains($prospectionRecruteur)) {
            $this->prospectionRecruteurs->removeElement($prospectionRecruteur);
            // set the owning side to null (unless already changed)
            if ($prospectionRecruteur->getRecruteur() === $this) {
                $prospectionRecruteur->setRecruteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClient(): Collection
    {
        return $this->client;
    }

    public function addClient(Client $client): self
    {
        if (!$this->client->contains($client)) {
            $this->client[] = $client;
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->client->contains($client)) {
            $this->client->removeElement($client);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isSignataire(): bool
    {
        return $this->signataire;
    }

    /**
     * @param bool $signataire
     */
    public function setSignataire(bool $signataire): void
    {
        $this->signataire = $signataire;
    }


}
