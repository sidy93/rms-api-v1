<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RenumerationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"remuneration_write"}},
 *     collectionOperations={
 *     "post",
 *      "get"={"method"="get", "controller"="App\Controller\RenumerationController"},
 *     "remuneration-contrat"={"method"="post","path"="/renumerations/contrat-pld","controller"="App\Controller\RenumerationController"},
 *     },
 *     itemOperations={
 *     "put"={"method"="PUT"},
 *      "get"={"method"="GET"},
 *      "getShowrefractor"={"method"="GET", "path"="/renumerations/show-refractor/{idInterim}", "controller"="App\Controller\RenumerationController"},
 *      "delete"={"method"="delete"},
 *     }
 * )
 * @ORM\Entity(repositoryClass=RenumerationRepository::class)
 */
class Renumeration
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"justificatif_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datePaiement;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datePiementFrais;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modePaiement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etatFrais;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $remarque;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $voiture;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantTrain;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantAvion;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montatntTaxi;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantBus;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantCovoiturage;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantParking;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantLocationVoiture;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantHotel;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $autre;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantTicket;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"remuneration_write"})
     */
    private $montantRepas;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $statut;

    public function __construct()
    {
        $this->montantAvion = 0;
        $this->montantBus = 0;
        $this->montantCovoiturage = 0;
        $this->montantHotel = 0;
        $this->montantRepas = 0;
        $this->montantTicket = 0;
        $this->montantTrain = 0;
        $this->voiture = 0;
        $this->montatntTaxi = 0;
        $this->montantParking = 0;
        $this->montantLocationVoiture = 0;
        $this->autre = 0;
        $this->createdAt();
        $this->releveHeures = new ArrayCollection();

    }
    public function attributs() {
        $attributs = [];
        foreach ($this as $key => $value) {
            $attributs[] = $key;
        }
        return $attributs;
    }
    public function refrator() {
        $tab = [];
        foreach ($this as $key => $value) {
            if($key != 'contrat')
                $tab[$key] = $value;
        }
        return $tab;
    }

    /**
     * @ORM\ManyToOne(targetEntity=Contrat::class, inversedBy="renumerations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"remuneration_write"})
     */
    private $contrat;

    /**
     * @ORM\OneToMany(targetEntity=ReleveHeure::class, mappedBy="remuneration")
     */
    private $releveHeures;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatePaiement(): ?\DateTimeInterface
    {
        return $this->datePaiement;
    }

    public function setDatePaiement(?\DateTimeInterface $datePaiement): self
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    public function getDatePiementFrais(): ?\DateTimeInterface
    {
        return $this->datePiementFrais;
    }

    public function setDatePiementFrais(?\DateTimeInterface $datePiementFrais): self
    {
        $this->datePiementFrais = $datePiementFrais;

        return $this;
    }

    public function getModePaiement(): ?string
    {
        return $this->modePaiement;
    }

    public function setModePaiement(?string $modePaiement): self
    {
        $this->modePaiement = $modePaiement;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getEtatFrais(): ?string
    {
        return $this->etatFrais;
    }

    public function setEtatFrais(?string $etatFrais): self
    {
        $this->etatFrais = $etatFrais;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getVoiture(): ?float
    {
        return $this->voiture;
    }

    public function setVoiture(?float $voiture): self
    {
        $this->voiture = $voiture;

        return $this;
    }

    public function getMontantTrain(): ?float
    {
        return $this->montantTrain;
    }

    public function setMontantTrain(?float $montantTrain): self
    {
        $this->montantTrain = $montantTrain;

        return $this;
    }

    public function getMontantAvion(): ?float
    {
        return $this->montantAvion;
    }

    public function setMontantAvion(?float $montantAvion): self
    {
        $this->montantAvion = $montantAvion;

        return $this;
    }

    public function getMontatntTaxi(): ?float
    {
        return $this->montatntTaxi;
    }

    public function setMontatntTaxi(?float $montatntTaxi): self
    {
        $this->montatntTaxi = $montatntTaxi;

        return $this;
    }

    public function getMontantBus(): ?float
    {
        return $this->montantBus;
    }

    public function setMontantBus(?float $montantBus): self
    {
        $this->montantBus = $montantBus;

        return $this;
    }

    public function getMontantCovoiturage(): ?float
    {
        return $this->montantCovoiturage;
    }

    public function setMontantCovoiturage(?float $montantCovoiturage): self
    {
        $this->montantCovoiturage = $montantCovoiturage;

        return $this;
    }

    public function getMontantParking(): ?float
    {
        return $this->montantParking;
    }

    public function setMontantParking(?float $montantParking): self
    {
        $this->montantParking = $montantParking;

        return $this;
    }

    public function getMontantLocationVoiture(): ?float
    {
        return $this->montantLocationVoiture;
    }

    public function setMontantLocationVoiture(?float $montantLocationVoiture): self
    {
        $this->montantLocationVoiture = $montantLocationVoiture;

        return $this;
    }

    public function getMontantHotel(): ?float
    {
        return $this->montantHotel;
    }

    public function setMontantHotel(?float $montantHotel): self
    {
        $this->montantHotel = $montantHotel;

        return $this;
    }

    public function getAutre(): ?float
    {
        return $this->autre;
    }

    public function setAutre(?float $autre): self
    {
        $this->autre = $autre;

        return $this;
    }

    public function getMontantTicket(): ?float
    {
        return $this->montantTicket;
    }

    public function setMontantTicket(?float $montantTicket): self
    {
        $this->montantTicket = $montantTicket;

        return $this;
    }

    public function getMontantRepas(): ?float
    {
        return $this->montantRepas;
    }

    public function setMontantRepas(?float $montantRepas): self
    {
        $this->montantRepas = $montantRepas;

        return $this;
    }

    public function getStatut(): ?bool
    {
        return $this->statut;
    }

    public function setStatut(?bool $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getContrat(): ?Contrat
    {
        return $this->contrat;
    }

    public function setContrat(?Contrat $contrat): self
    {
        $this->contrat = $contrat;

        return $this;
    }

    /**
     * @return Collection|ReleveHeure[]
     */
    public function getReleveHeures(): Collection
    {
        return $this->releveHeures;
    }

    public function addReleveHeure(ReleveHeure $releveHeure): self
    {
        if (!$this->releveHeures->contains($releveHeure)) {
            $this->releveHeures[] = $releveHeure;
            $releveHeure->setRemuneration($this);
        }

        return $this;
    }

    public function removeReleveHeure(ReleveHeure $releveHeure): self
    {
        if ($this->releveHeures->contains($releveHeure)) {
            $this->releveHeures->removeElement($releveHeure);
            // set the owning side to null (unless already changed)
            if ($releveHeure->getRemuneration() === $this) {
                $releveHeure->setRemuneration(null);
            }
        }

        return $this;
    }
}
