<?php


namespace App\Entity;


use DateTime as DateTimeAlias;
use Symfony\Component\Serializer\Annotation\Groups;

trait Timestamps
{
    /**
     * @ORM\Column(type="datetime")
     * @Groups({"client_read", "blog_read", "candidature_read",
     *      "user_read", "groupe_roles_read", "fs_read","interim_read","interim_prospection_subresource", "disponibilite_subresource", "actionGroupeRole_read", "gi_read", "ligne_read", "tache_write", "tache_read", "type_tache_read" })
     */
    private $createAt;
    /**
     * @ORM\Column(type="string", nullable = true)
     * @Groups({"client_read", "blog_read", "user_read", "groupe_roles_read", "fs_read","interim_read","interim_prospection_subresource", "disponibilite_subresource", "actionGroupeRole_read", "gi_read", "ligne_read", "tache_write", "tache_read", "type_tache_read" })

     */
    private $auteur;
    /**
     * @ORM\Column(type="datetime", nullable = true)
     * @Groups({"blog_read", "user_read","interim_read","interim_prospection_subresource", "candidature_read"})
     */
    private $updateAt;

    /**
     * @ORM\PrePersist()
     */
    public function createdAt()
    {
        $this->createAt = new DateTimeAlias("now");
        $this->updateAt = new DateTimeAlias("now");
    }

    /**
     * @ORM\PreUpdate()
     */
    public function updateAt()
    {
        $this->updateAt = new DateTimeAlias();
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->createAt->format('d-m-Y');
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @return mixed
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * @param mixed $auteur
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;
    }

    public function UpdateItem($elemet,$valeur) {
        foreach ($this as $key => $value) {
            if($key==$elemet){
                $this->$key = $valeur;
            }
        }
    }
}

