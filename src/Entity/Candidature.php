<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CandidatureRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;

/**
 * @ApiResource(
 *         collectionOperations={
 *         "post"={"method"="post","path"="/candidature","controller"="App\Controller\CandidatureController"},
 *          "get"={"method"="get","path"="/candidatures"}
 *     },
 *      normalizationContext={"groups"={"candidature_read"}},
 * )
 *
 * @ApiFilter(SearchFilter::class,properties={
 *     "missionInterims.interim.user.nom":"partial",
 *     "missionInterims.interim.user.email":"partial",
 *     "missionInterims.interim.user.prenom":"partial",
 *     "planning.mission.mois":"partial",
 *     "planning.mission.annee":"partial",
 *     "planning.mission.reference":"partial",
 *     "planning.typeDeGarde.typeVacation.designation":"partial",
 *     "statut.designation":"partial",
 *     "auteur":"partial",
 *     "planning.mission.ficheSpecialite.qualification.specialite.nomSpecialite":"partial",
 *     "planning.mission.ficheSpecialite.client.nomEtablissement":"partial"
 * })
 * @ApiFilter(BooleanFilter::class, properties={"enable"})

 * @ApiFilter(DateFilter::class, properties={"createAt":DateFilter::EXCLUDE_NULL,"planning.date":DateFilter::EXCLUDE_NULL})
 *
 * @ORM\Entity(repositoryClass=CandidatureRepository::class)
 */
class Candidature
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"candidature_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=MissionInterim::class, inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidature_read"})
     */
    private $missionInterims;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"candidature_read"})
     */
    private $enable;

    /**
     * @ORM\Column(type="smallint")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidature_read"})
     */
    private $relance = 0;

    /**
     * @ORM\ManyToOne(targetEntity=StatutCandidature::class, inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidature_read"})
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity=Planning::class, inversedBy="candidature")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidature_read"})
     */
    private $planning;

    /**
     * @ORM\ManyToOne(targetEntity=Contrat::class, inversedBy="candidatures")
     */
    private $contratPld;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMissionInterims(): ?MissionInterim
    {
        return $this->missionInterims;
    }

    public function setMissionInterims(?MissionInterim $missionInterims): self
    {
        $this->missionInterims = $missionInterims;

        return $this;
    }

    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    public function getStatut(): ?StatutCandidature
    {
        return $this->statut;
    }

    public function setStatut(?StatutCandidature $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getPlanning(): ?Planning
    {
        return $this->planning;
    }

    public function setPlanning(?Planning $planning): self
    {
        $this->planning = $planning;

        return $this;
    }

    public function getContratPld(): ?Contrat
    {
        return $this->contratPld;
    }

    public function setContratPld(?Contrat $contratPld): self
    {
        $this->contratPld = $contratPld;

        return $this;
    }

    /**
     * @return int
     */
    public function getRelance(): int
    {
        return $this->relance;
    }

    /**
     * @param int $relance
     */
    public function setRelance(int $relance): void
    {
        $this->relance = $relance;
    }


}
