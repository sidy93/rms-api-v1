<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContratRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ContratRepository::class)
 */
class Contrat
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"remuneration_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true, length=100)
     */
    private $idPld;

    /**
     * @ORM\Column(type="array")
     */
    private $urlContrat = [];

    /**
     * @ORM\ManyToOne(targetEntity=TypeContrat::class, inversedBy="contrat")
     */
    private $typeContrat;

    /**
     * @ORM\OneToMany(targetEntity=Signature::class, mappedBy="contrat_pld")
     */
    private $signatures;

    /**
     * @ORM\OneToMany(targetEntity=Renumeration::class, mappedBy="contrat", orphanRemoval=true)
     */
    private $renumerations;

    /**
     * @ORM\OneToMany(targetEntity=Candidature::class, mappedBy="contratPldId")
     */
    private $candidatures;

    public function __construct()
    {
        $this->createdAt();
        $this->signatures = new ArrayCollection();
        $this->renumerations = new ArrayCollection();
        $this->candidatures = new ArrayCollection();
    }

    public function refrator() {
        $tab = [];
        foreach ($this as $key => $value) {
            $tab[$key] = $value;
        }
        return $tab;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPld(): ?string
    {
        return $this->idPld;
    }

    public function setIdPld(string $idPld): self
    {
        $this->idPld = $idPld;

        return $this;
    }

    public function getUrlContrat(): ?array
    {
        return $this->urlContrat;
    }

    public function setUrlContrat(array $urlContrat): self
    {
        $this->urlContrat = $urlContrat;

        return $this;
    }

    public function getTypeContrat(): ?TypeContrat
    {
        return $this->typeContrat;
    }

    public function setTypeContrat(?TypeContrat $typeContrat): self
    {
        $this->typeContrat = $typeContrat;

        return $this;
    }

    /**
     * @return Collection|Signature[]
     */
    public function getSignatures(): Collection
    {
        return $this->signatures;
    }

    public function addSignature(Signature $signature): self
    {
        if (!$this->signatures->contains($signature)) {
            $this->signatures[] = $signature;
            $signature->setContrat($this);
        }

        return $this;
    }

    public function removeSignature(Signature $signature): self
    {
        if ($this->signatures->contains($signature)) {
            $this->signatures->removeElement($signature);
            // set the owning side to null (unless already changed)
            if ($signature->getContrat() === $this) {
                $signature->setContrat(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Renumeration[]
     */
    public function getRenumerations(): Collection
    {
        return $this->renumerations;
    }

    public function addRenumeration(Renumeration $renumeration): self
    {
        if (!$this->renumerations->contains($renumeration)) {
            $this->renumerations[] = $renumeration;
            $renumeration->setContrat($this);
        }

        return $this;
    }

    public function removeRenumeration(Renumeration $renumeration): self
    {
        if ($this->renumerations->contains($renumeration)) {
            $this->renumerations->removeElement($renumeration);
            // set the owning side to null (unless already changed)
            if ($renumeration->getContrat() === $this) {
                $renumeration->setContrat(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Candidature[]
     */
    public function getCandidatures(): Collection
    {
        return $this->candidatures;
    }

    public function addCandidature(Candidature $candidature): self
    {
        if (!$this->candidatures->contains($candidature)) {
            $this->candidatures[] = $candidature;
            $candidature->setContratPldId($this);
        }

        return $this;
    }

    public function removeCandidature(Candidature $candidature): self
    {
        if ($this->candidatures->contains($candidature)) {
            $this->candidatures->removeElement($candidature);
            // set the owning side to null (unless already changed)
            if ($candidature->getContratPldId() === $this) {
                $candidature->setContratPldId(null);
            }
        }

        return $this;
    }
}
