<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LigneRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  normalizationContext={"groups"={"ligne_read"}},
 * )
 * @ORM\Entity(repositoryClass=LigneRepository::class)
 */
class Ligne
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"ligne_read", "tache_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"ligne_read", "tache_read", "gi_read"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="array")
     * @Groups({"ligne_read", "tache_read"})
     */
    private $specialite = [];

    /**
     * @ORM\Column(type="text")
     * @Groups({"ligne_read", "tache_read"})
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=TypeLigne::class, inversedBy="lignes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"ligne_read", "tache_read", "gi_read"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=BoiteEmail::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"ligne_read", "tache_read", "gi_read"})
     */
    private $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getSpecialite(): ?array
    {
        return $this->specialite;
    }

    public function setSpecialite(array $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getType(): ?Typeligne
    {
        return $this->type;
    }

    public function setType(?Typeligne $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEmail(): ?BoiteEmail
    {
        return $this->email;
    }

    public function setEmail(?BoiteEmail $email): self
    {
        $this->email = $email;

        return $this;
    }
}
