<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AnnexeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"annexe_read"}},
 * )
 * @ORM\Entity(repositoryClass=AnnexeRepository::class)
 */
class Annexe
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"type_annexe_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"type_annexe_read"})
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"type_annexe_read"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=TypeAnnexe::class, inversedBy="annexes")
     */
    private $typeAnnexe;


    public function __construct()
    {
        $this->typeAnnexes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTypeAnnexe(): ?TypeAnnexe
    {
        return $this->typeAnnexe;
    }

    public function setTypeAnnexe(?TypeAnnexe $typeAnnexe): self
    {
        $this->typeAnnexe = $typeAnnexe;

        return $this;
    }

}
