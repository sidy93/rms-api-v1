<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeDeGardeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *     normalizationContext={"groups" = {"garde_read"}}
 * )
 * @ORM\Entity(repositoryClass=TypeDeGardeRepository::class)
 */
class TypeDeGarde
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"garde_read", "fs_read", "mission_write", "planning_read", "mission_read","candidature_read"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Planning::class, mappedBy="typeDeGarde")
     */
    private $planning;

    /**
     * @ORM\ManyToOne(targetEntity=FicheSpecialite::class, inversedBy="typeDeGardes")
     * @Groups({"garde_read"})
     */
    private $ficheSpecialite;

    /**
     * @ORM\Column(type="time")
     * @Groups({"garde_read", "fs_read", "planning_read", "mission_read","candidature_read"})
     */
    private $heureDebut;

    /**
     * @ORM\Column(type="time")
     * @Groups({"garde_read", "fs_read", "planning_read", "mission_read","candidature_read"})
     */
    private $heureFin;

    /**
     * @ORM\Column(type="float", length=100)
     * @Groups({"garde_read", "fs_read"})
     */
    private $tarifNormal;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     * @Groups({"garde_read"})
     */
    private $tarifSaison;

    /**
     * @ORM\Column(type="smallint", length=100, nullable=true)
     * @Groups({"garde_read", "fs_read"})
     */
    private $annee;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"garde_read"})
     */
    private $remarque;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     * @Groups({"garde_read"})
     */
    private $salaireNetNormal;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     * @Groups({"garde_read", "fs_read"})
     */
    private $salaireNet;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     * @Groups({"garde_read", "fs_read"})
     */
    private $salaireBrute;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     * @Groups({"garde_read"})
     */
    private $salaireBase;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     * @Groups({"garde_read", "fs_read",})
     */
    private $tauxReference;

    /**
     * @ORM\ManyToOne(targetEntity=Vacation::class, inversedBy="typeDeGardes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"garde_read", "fs_read", "planning_read","candidature_read"})
     */
    private $typeVacation;

    public function __construct()
    {
        $this->planning = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Planning[]
     */
    public function getPlanning(): Collection
    {
        return $this->planning;
    }

    public function addPlanning(Planning $planning): self
    {
        if (!$this->planning->contains($planning)) {
            $this->planning[] = $planning;
            $planning->setTypeDeGarde($this);
        }

        return $this;
    }

    public function removePlanning(Planning $planning): self
    {
        if ($this->planning->contains($planning)) {
            $this->planning->removeElement($planning);
            // set the owning side to null (unless already changed)
            if ($planning->getTypeDeGarde() === $this) {
                $planning->setTypeDeGarde(null);
            }
        }

        return $this;
    }

    public function getFicheSpecialite(): ?FicheSpecialite
    {
        return $this->ficheSpecialite;
    }

    public function setFicheSpecialite(?FicheSpecialite $ficheSpecialite): self
    {
        $this->ficheSpecialite = $ficheSpecialite;

        return $this;
    }

    public function getHeureDebut()
    {
        return $this->heureDebut->format("H:i");
    }

    public function setHeureDebut(\DateTimeInterface $heureDebut): self
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getHeureFin()
    {
        return $this->heureFin->format("H:i");
    }

    public function setHeureFin(\DateTimeInterface $heureFin): self
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    public function getTarifNormal(): ?string
    {
        return $this->tarifNormal;
    }

    public function setTarifNormal(string $tarifNormal): self
    {
        $this->tarifNormal = $tarifNormal;

        return $this;
    }

    public function getTarifSaison(): ?string
    {
        return $this->tarifSaison;
    }

    public function setTarifSaison(?string $tarifSaison): self
    {
        $this->tarifSaison = $tarifSaison;

        return $this;
    }

    public function getAnnee(): ?string
    {
        return $this->annee;
    }

    public function setAnnee(?string $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getSalaireNetNormal(): ?string
    {
        return $this->salaireNetNormal;
    }

    public function setSalaireNetNormal(?string $salaireNetNormal): self
    {
        $this->salaireNetNormal = $salaireNetNormal;

        return $this;
    }

    public function getSalaireNet(): ?string
    {
        return $this->salaireNet;
    }

    public function setSalaireNet(?string $salaireNet): self
    {
        $this->salaireNet = $salaireNet;

        return $this;
    }

    public function getSalaireBrute(): ?string
    {
        return $this->salaireBrute;
    }

    public function setSalaireBrute(?string $salaireBrute): self
    {
        $this->salaireBrute = $salaireBrute;

        return $this;
    }

    public function getSalaireBase(): ?string
    {
        return $this->salaireBase;
    }

    public function setSalaireBase(?string $salaireBase): self
    {
        $this->salaireBase = $salaireBase;

        return $this;
    }

    public function getTypeVacation(): ?Vacation
    {
        return $this->typeVacation;
    }

    public function setTypeVacation(?Vacation $typeVacation): self
    {
        $this->typeVacation = $typeVacation;

        return $this;
    }

    public function getTauxReference(): ?float
    {
        return $this->tauxReference;
    }

    public function setTauxReference(?float $tauxReference): self
    {
        $this->tauxReference = $tauxReference;

        return $this;
    }
}
