<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FicheSpecialiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\FicheSpecialiteController;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *     itemOperations={
 *     "get", "put", "delete",
 *     "post_publication"={
 *         "method"="post",
 *         "path"="/addQualificationsFs/{id}",
 *         "controller"= FicheSpecialiteController::class,
 *     }},
 *     normalizationContext={"groups"={"fs_read"}},
 *     denormalizationContext={"groups"={"fs_write"}}
 * )
 * @ApiFilter(SearchFilter::class)
 * @ORM\Entity(repositoryClass=FicheSpecialiteRepository::class)
 */
class FicheSpecialite
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"fs_read", "fs_write",
     *      "garde_read", "mission_write", "mission_read", "offre_read", "offre_write", "planning_read", "client_read", "candidature_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="ficheSpecialites")
     * @Groups({"mission_read", "fs_write", "garde_read", "planning_read","candidature_read"})
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity=Mission::class, mappedBy="ficheSpecialite")
     * @Groups({"fs_read"})
     */
    private $mission;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diplome;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $infoPratique;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $nombrePassageParAn;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $nombreLit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $nombreMedecinNuit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $nombreMedecinJour;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $coefTarification;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $coefSaison;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"fs_read", "fs_write"})
     */
    private $environemenTech;

    /**
     * @ORM\OneToMany(targetEntity=TypeDeGarde::class, mappedBy="ficheSpecialite")
     * @Groups({"fs_read"})
     */
    private $typeDeGardes;

    /**
     * @ORM\ManyToMany(targetEntity=Qualification::class)
     * @Groups({"fs_read", "fs_write",
     *      "garde_read", "offre_read", "mission_read", "planning_read", "client_read","candidature_read"})
     */
    private $qualification;

    /**
     * @ORM\Column(type="text")
     * @Groups({"fs_read", "fs_write"})
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Offres::class, mappedBy="ficheSpecialite")
     */
    private $offres;

    public function __construct()
    {
        $this->mission = new ArrayCollection();
        $this->typeDeGardes = new ArrayCollection();
        $this->qualification = new ArrayCollection();
        $this->offres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|Mission[]
     */
    public function getMission(): Collection
    {
        return $this->mission;
    }

    public function addMission(Mission $mission): self
    {
        if (!$this->mission->contains($mission)) {
            $this->mission[] = $mission;
            $mission->setFicheSpecialite($this);
        }

        return $this;
    }

    public function removeMission(Mission $mission): self
    {
        if ($this->mission->contains($mission)) {
            $this->mission->removeElement($mission);
            // set the owning side to null (unless already changed)
            if ($mission->getFicheSpecialite() === $this) {
                $mission->setFicheSpecialite(null);
            }
        }

        return $this;
    }

    public function getDiplome(): ?string
    {
        return $this->diplome;
    }

    public function setDiplome(?string $diplome): self
    {
        $this->diplome = $diplome;

        return $this;
    }

    public function getInfoPratique(): ?string
    {
        return $this->infoPratique;
    }

    public function setInfoPratique(?string $infoPratique): self
    {
        $this->infoPratique = $infoPratique;

        return $this;
    }

    public function getNombrePassageParAn(): ?int
    {
        return $this->nombrePassageParAn;
    }

    public function setNombrePassageParAn(?int $nombrePassageParAn): self
    {
        $this->nombrePassageParAn = $nombrePassageParAn;

        return $this;
    }

    public function getNombreLit(): ?int
    {
        return $this->nombreLit;
    }

    public function setNombreLit(?int $nombreLit): self
    {
        $this->nombreLit = $nombreLit;

        return $this;
    }

    public function getNombreMedecinNuit(): ?int
    {
        return $this->nombreMedecinNuit;
    }

    public function setNombreMedecinNuit(?int $nombreMedecinNuit): self
    {
        $this->nombreMedecinNuit = $nombreMedecinNuit;

        return $this;
    }

    public function getNombreMedecinJour(): ?int
    {
        return $this->nombreMedecinJour;
    }

    public function setNombreMedecinJour(?int $nombreMedecinJour): self
    {
        $this->nombreMedecinJour = $nombreMedecinJour;

        return $this;
    }

    public function getCoefTarification(): ?float
    {
        return $this->coefTarification;
    }

    public function setCoefTarification(?float $coefTarification): self
    {
        $this->coefTarification = $coefTarification;

        return $this;
    }

    public function getCoefSaison(): ?float
    {
        return $this->coefSaison;
    }

    public function setCoefSaison(?float $coefSaison): self
    {
        $this->coefSaison = $coefSaison;

        return $this;
    }

    public function getEnvironemenTech(): ?string
    {
        return $this->environemenTech;
    }

    public function setEnvironemenTech(?string $environemenTech): self
    {
        $this->environemenTech = $environemenTech;

        return $this;
    }

    /**
     * @return Collection|TypeDeGarde[]
     */
    public function getTypeDeGardes(): Collection
    {
        return $this->typeDeGardes;
    }

    public function addTypeDeGarde(TypeDeGarde $typeDeGarde): self
    {
        if (!$this->typeDeGardes->contains($typeDeGarde)) {
            $this->typeDeGardes[] = $typeDeGarde;
            $typeDeGarde->setFicheSpecialite($this);
        }

        return $this;
    }

    public function removeTypeDeGarde(TypeDeGarde $typeDeGarde): self
    {
        if ($this->typeDeGardes->contains($typeDeGarde)) {
            $this->typeDeGardes->removeElement($typeDeGarde);
            // set the owning side to null (unless already changed)
            if ($typeDeGarde->getFicheSpecialite() === $this) {
                $typeDeGarde->setFicheSpecialite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Qualification[]
     */
    public function getQualification(): Collection
    {
        return $this->qualification;
    }

    public function addQualification(Qualification $qualification): self
    {
        if (!$this->qualification->contains($qualification)) {
            $this->qualification[] = $qualification;
        }

        return $this;
    }

    public function removeQualification(Qualification $qualification): self
    {
        if ($this->qualification->contains($qualification)) {
            $this->qualification->removeElement($qualification);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Offres[]
     */
    public function getOffres(): Collection
    {
        return $this->offres;
    }

    public function addOffre(Offres $offre): self
    {
        if (!$this->offres->contains($offre)) {
            $this->offres[] = $offre;
            $offre->setFicheSpecialite($this);
        }

        return $this;
    }

    public function removeOffre(Offres $offre): self
    {
        if ($this->offres->contains($offre)) {
            $this->offres->removeElement($offre);
            // set the owning side to null (unless already changed)
            if ($offre->getFicheSpecialite() === $this) {
                $offre->setFicheSpecialite(null);
            }
        }

        return $this;
    }


}
