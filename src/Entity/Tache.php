<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TacheRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"tache_read"}},
 *     denormalizationContext={"groups"={"tache_write"}},
 *     attributes={"order"={"id": "ASC"}}
 * )
 * @ORM\Entity(repositoryClass=TacheRepository::class)
 */
class Tache
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"tache_read", "gi_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $nom;

    /**
     * @ORM\Column(type="time")
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $heureDebut;

    /**
     * @ORM\Column(type="time")
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $heureFin;

    /**
     * @ORM\Column(type="date")
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $dateFin;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $delaisRespecter;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $etat;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=TypeTache::class, inversedBy="taches")
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Ligne::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"tache_read","tache_write", "gi_read"})
     */
    private $ligne;

    /**
     * @ORM\OneToMany(targetEntity=TacheGestionnaire::class, mappedBy="tache", orphanRemoval=true, cascade={"persist"})
     * @Groups({"tache_read","tache_write"})
     */
    private $tacheGestionnaires;

    public function __construct()
    {
        $this->tacheGestionnaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getHeureDebut(): ?\DateTimeInterface
    {
        return $this->heureDebut;
    }

    public function setHeureDebut(\DateTimeInterface $heureDebut): self
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getHeureFin(): ?\DateTimeInterface
    {
        return $this->heureFin;
    }

    public function setHeureFin(\DateTimeInterface $heureFin): self
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getDelaisRespecter(): ?bool
    {
        return $this->delaisRespecter;
    }

    public function setDelaisRespecter(?bool $delaisRespecter): self
    {
        $this->delaisRespecter = $delaisRespecter;

        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(?int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?TypeTache
    {
        return $this->type;
    }

    public function setType(?TypeTache $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLigne(): ?Ligne
    {
        return $this->ligne;
    }

    public function setLigne(?Ligne $ligne): self
    {
        $this->ligne = $ligne;

        return $this;
    }

    /**
     * @return Collection|TacheGestionnaire[]
     */
    public function getTacheGestionnaires(): Collection
    {
        return $this->tacheGestionnaires;
    }

    public function addTacheGestionnaire(TacheGestionnaire $tacheGestionnaire): self
    {
        if (!$this->tacheGestionnaires->contains($tacheGestionnaire)) {
            $this->tacheGestionnaires[] = $tacheGestionnaire;
            $tacheGestionnaire->setTache($this);
        }

        return $this;
    }

    public function removeTacheGestionnaire(TacheGestionnaire $tacheGestionnaire): self
    {
        if ($this->tacheGestionnaires->contains($tacheGestionnaire)) {
            $this->tacheGestionnaires->removeElement($tacheGestionnaire);
            // set the owning side to null (unless already changed)
            if ($tacheGestionnaire->getTache() === $this) {
                $tacheGestionnaire->setTache(null);
            }
        }

        return $this;
    }
}
