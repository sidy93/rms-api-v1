<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\HebergementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource()
 * @ORM\Entity(repositoryClass=HebergementRepository::class)
 */

class Hebergement
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"client_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client_read"})
     */
    private $lieu;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"client_read"})
     */
    private $remarque;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client_read"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"client_read"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"client_read"})
     */
    private $nombreEtoile;

    /**
     * @ORM\OneToMany(targetEntity=MissionInterim::class, mappedBy="hebergement")
     */
    private $missionInterims;

    /**
     * @ORM\ManyToOne(targetEntity=FicheClient::class, inversedBy="hebergement")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ficheClient;

    public function __construct()
    {
        $this->missionInterims = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getNombreEtoile(): ?int
    {
        return $this->nombreEtoile;
    }

    public function setNombreEtoile(?int $nombreEtoile): self
    {
        $this->nombreEtoile = $nombreEtoile;

        return $this;
    }

    /**
     * @return Collection|MissionInterim[]
     */
    public function getMissionInterims(): Collection
    {
        return $this->missionInterims;
    }

    public function addMissionInterim(MissionInterim $missionInterim): self
    {
        if (!$this->missionInterims->contains($missionInterim)) {
            $this->missionInterims[] = $missionInterim;
            $missionInterim->setHebergement($this);
        }

        return $this;
    }

    public function removeMissionInterim(MissionInterim $missionInterim): self
    {
        if ($this->missionInterims->contains($missionInterim)) {
            $this->missionInterims->removeElement($missionInterim);
            // set the owning side to null (unless already changed)
            if ($missionInterim->getHebergement() === $this) {
                $missionInterim->setHebergement(null);
            }
        }

        return $this;
    }

    public function getFicheClient(): ?FicheClient
    {
        return $this->ficheClient;
    }

    public function setFicheClient(?FicheClient $ficheClient): self
    {
        $this->ficheClient = $ficheClient;

        return $this;
    }
}
