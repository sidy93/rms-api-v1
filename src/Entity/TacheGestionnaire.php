<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TacheGestionnaireRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"tache_gestionnaire_read"}},
 * )
 * @ORM\Entity(repositoryClass=TacheGestionnaireRepository::class)
 */
class TacheGestionnaire
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"tache_read","tache_write", "tache_gestionnaire_read", "gi_read"})
     *
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Tache::class, inversedBy="tacheGestionnaires")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"gi_read"})
     */
    private $tache;

    /**
     * @ORM\ManyToOne(targetEntity=Gestionnaire::class, inversedBy="tacheGestionnaires")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"tache_read","tache_write", "tache_gestionnaire_read"})
     */
    private $gestionnaire;

    public function __construct()
    {
        $this->createdAt();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTache(): ?Tache
    {
        return $this->tache;
    }

    public function setTache(?Tache $tache): self
    {
        $this->tache = $tache;

        return $this;
    }

    public function getGestionnaire(): ?Gestionnaire
    {
        return $this->gestionnaire;
    }

    public function setGestionnaire(?Gestionnaire $gestionnaire): self
    {
        $this->gestionnaire = $gestionnaire;

        return $this;
    }
}
