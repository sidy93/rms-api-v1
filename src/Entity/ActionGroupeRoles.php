<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ActionGroupeRolesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *  * @ApiResource(
 *     normalizationContext={"groups"={"actionGroupeRole_read"}},
 *     attributes={"order"={"id": "ASC"}}
 * )
 * @ORM\Entity(repositoryClass=ActionGroupeRolesRepository::class)
 */
class ActionGroupeRoles
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"actionGroupeRole_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"actionGroupeRole_read"})
     */
    private $edit;

    /**
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Groups({"actionGroupeRole_read"})
     */
    private $libelle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEdit(): ?bool
    {
        return $this->edit;
    }

    public function setEdit(bool $edit): self
    {
        $this->edit = $edit;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }
}
