<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ComplementDossierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ComplementDossierRepository::class)
 */
class ComplementDossier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Dossier::class, inversedBy="complementDossiers")
     */
    private $dossier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $diplomeFileName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $diplomeName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getDiplomeFileName(): ?string
    {
        return $this->diplomeFileName;
    }

    public function setDiplomeFileName(string $diplomeFileName): self
    {
        $this->diplomeFileName = $diplomeFileName;

        return $this;
    }

    public function getDiplomeName(): ?string
    {
        return $this->diplomeName;
    }

    public function setDiplomeName(string $diplomeName): self
    {
        $this->diplomeName = $diplomeName;

        return $this;
    }

    public function complementsDossierMed() {
        $dossierMed = [];
        foreach ($this as $key => $value) {
            if ($key!='dossier' && $key!='id')
                $dossierMed[$key] = $value;
        }
        return $dossierMed;
    }
    public function deleteItem($elemet) {
        foreach ($this as $key => $value) {
            if($key==$elemet){
                $this->$key = null;
            }
        }
    }
    public function UpdateItem($elemet,$valeur) {
        foreach ($this as $key => $value) {
            if($key==$elemet){
                $this->$key = $valeur;
            }
        }
    }
}
