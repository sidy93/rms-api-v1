<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MissionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *     normalizationContext={"groups"={"mission_read"}},
 *     denormalizationContext={"groups"={"mission_write"}}
 * )
 * @ApiFilter(SearchFilter::class)
 * @ORM\Entity(repositoryClass=MissionRepository::class)
 */
class Mission
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"fs_read", "mission_read", "mission_write", "planning_read", "candidature_read"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Planning::class, mappedBy="mission", cascade={"persist", "remove"})
     * @Groups({"mission_read", "mission_write"})
     */
    private $planning;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"fs_read", "mission_read", "mission_write", "planning_read", "candidature_read"})
     */
    private $mois;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"fs_read", "mission_read", "mission_write"})
     */
    private $jtc;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"fs_read", "mission_read", "mission_write", "planning_read","candidature_read"})
     */
    private $annee;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"mission_read", "mission_write"})
     */
    private $diplomeMinimumRequis;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Groups({"mission_read", "mission_write"})
     */
    private $requisMedecinInscrit;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"fs_read", "mission_read", "mission_write", "candidature_read"})
     */
    private $enable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"mission_read", "mission_write"})
     */
    private $majoration;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"mission_read", "mission_write"})
     */
    private $remarque;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"fs_read", "mission_read", "mission_write", "candidature_read"})
     */
    private $reference;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"mission_read", "mission_write"})
     */
    private $diplome;

    /**
     * @ORM\ManyToOne(targetEntity=FicheSpecialite::class, inversedBy="mission")
     * @Groups({"mission_read", "mission_write", "planning_read","candidature_read"})
     */
    private $ficheSpecialite;

    public function __construct()
    {
        $this->planning = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Planning[]
     */
    public function getPlanning(): Collection
    {
        return $this->planning;
    }

    public function addPlanning(Planning $planning): self
    {
        if (!$this->planning->contains($planning)) {
            $this->planning[] = $planning;
            $planning->setMission($this);
        }

        return $this;
    }

    public function removePlanning(Planning $planning): self
    {
        if ($this->planning->contains($planning)) {
            $this->planning->removeElement($planning);
            // set the owning side to null (unless already changed)
            if ($planning->getMission() === $this) {
                $planning->setMission(null);
            }
        }

        return $this;
    }

    public function getMois(): ?string
    {
        return $this->mois;
    }

    public function setMois(string $mois): self
    {
        $this->mois = $mois;

        return $this;
    }

    public function getJtc(): ?string
    {
        return $this->jtc;
    }

    public function setJtc(string $jtc): self
    {
        $this->jtc = $jtc;

        return $this;
    }

    public function getAnnee(): ?string
    {
        return $this->annee;
    }

    public function setAnnee(string $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getDiplomeMinimumRequis(): ?string
    {
        return $this->diplomeMinimumRequis;
    }

    public function setDiplomeMinimumRequis(?string $diplomeMinimumRequis): self
    {
        $this->diplomeMinimumRequis = $diplomeMinimumRequis;

        return $this;
    }

    public function getRequisMedecinInscrit(): ?bool
    {
        return $this->requisMedecinInscrit;
    }

    public function setRequisMedecinInscrit(?bool $requisMedecinInscrit): self
    {
        $this->requisMedecinInscrit = $requisMedecinInscrit;

        return $this;
    }

    public function getEnable(): ?int
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    public function getMajoration(): ?bool
    {
        return $this->majoration;
    }

    public function setMajoration(bool $majoration): self
    {
        $this->majoration = $majoration;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDiplome(): ?string
    {
        return $this->diplome;
    }

    public function setDiplome(?string $diplome): self
    {
        $this->diplome = $diplome;

        return $this;
    }

    public function getFicheSpecialite(): ?FicheSpecialite
    {
        return $this->ficheSpecialite;
    }

    public function setFicheSpecialite(?FicheSpecialite $ficheSpecialite): self
    {
        $this->ficheSpecialite = $ficheSpecialite;

        return $this;
    }

    /**
     * @return int
     * @Groups({"fs_read"})
     */
    public function getNombrePlanning()
    {
        return sizeof($this->planning);
    }
}
