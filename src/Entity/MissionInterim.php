<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MissionInterimRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      collectionOperations={
 *         "get" = {"method"="get", "path"="/mission_interims", "controller"="App\Controller\MissionInterimController"},
 *         "post"
 *     }
 * )
 *@ApiFilter(SearchFilter::class,properties={

 *     "interim.user.nom":"partial",
 *     "interim.user.prenom":"partial",
 *     "candidatures.planning.mission.mois":"partial",
 *     "candidatures.planning.mission.annee":"partial",
 *     "candidatures.planning.mission.reference":"partial",
 *     "candidatures.planning.mission.ficheSpecialite.qualification.specialite.nomSpecialite":"partial"
 * })
 * @ApiFilter(DateFilter::class, properties={"createAt":DateFilter::EXCLUDE_NULL,"candidatures.planning.date:DateFilter::EXCLUDE_NULL"})
 * @ORM\Entity(repositoryClass=MissionInterimRepository::class)
 */
class MissionInterim
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"candidature_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Interim::class, inversedBy="missionInterims")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidature_read"})
     */
    private $interim;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"candidature_read"})
     */
    private $remarques;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"candidature_read"})
     */
    private $commentaires;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"candidature_read"})
     */
    private $conditionMedecin;

    /**
     * @ORM\ManyToOne(targetEntity=Hebergement::class, inversedBy="missionInterims")
     */
    private $hebergement;

    /**
     * @ORM\OneToMany(targetEntity=RappelSecurite::class, mappedBy="missionInterim", orphanRemoval=true)
     */
    private $rappelSecurites;

    /**
     * @ORM\OneToMany(targetEntity=Candidature::class, mappedBy="missionInterims", orphanRemoval=true)
     */
    private $candidatures;

    public function __construct()
    {
        $this->rappelSecurites = new ArrayCollection();
        $this->candidatures = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInterim(): ?Interim
    {
        return $this->interim;
    }

    public function setInterim(?Interim $interim): self
    {
        $this->interim = $interim;

        return $this;
    }

    public function getRemarques(): ?string
    {
        return $this->remarques;
    }

    public function setRemarques(?string $remarques): self
    {
        $this->remarques = $remarques;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(?string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getConditionMedecin(): ?string
    {
        return $this->conditionMedecin;
    }

    public function setConditionMedecin(?string $conditionMedecin): self
    {
        $this->conditionMedecin = $conditionMedecin;

        return $this;
    }

    public function getHebergement(): ?Hebergement
    {
        return $this->hebergement;
    }

    public function setHebergement(?Hebergement $hebergement): self
    {
        $this->hebergement = $hebergement;

        return $this;
    }

    /**
     * @return Collection|RappelSecurite[]
     */
    public function getRappelSecurites(): Collection
    {
        return $this->rappelSecurites;
    }

    public function addRappelSecurite(RappelSecurite $rappelSecurite): self
    {
        if (!$this->rappelSecurites->contains($rappelSecurite)) {
            $this->rappelSecurites[] = $rappelSecurite;
            $rappelSecurite->setMissionInterim($this);
        }

        return $this;
    }

    public function removeRappelSecurite(RappelSecurite $rappelSecurite): self
    {
        if ($this->rappelSecurites->contains($rappelSecurite)) {
            $this->rappelSecurites->removeElement($rappelSecurite);
            // set the owning side to null (unless already changed)
            if ($rappelSecurite->getMissionInterim() === $this) {
                $rappelSecurite->setMissionInterim(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Candidature[]
     */
    public function getCandidatures(): Collection
    {
        return $this->candidatures;
    }

    public function addCandidature(Candidature $candidature): self
    {
        if (!$this->candidatures->contains($candidature)) {
            $this->candidatures[] = $candidature;
            $candidature->setMissionInterims($this);
        }

        return $this;
    }

    public function removeCandidature(Candidature $candidature): self
    {
        if ($this->candidatures->contains($candidature)) {
            $this->candidatures->removeElement($candidature);
            // set the owning side to null (unless already changed)
            if ($candidature->getMissionInterims() === $this) {
                $candidature->setMissionInterims(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function  getSpecialiteMission() :?string{
        return $this->candidatures[0]
             ->getPlanning()
             ->getMission()
             ->getFicheSpecialite()
             ->getQualification()[0]
             ->getSpecialite()->getNomSpecialite();
    }


}
