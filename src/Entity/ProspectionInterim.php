<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProspectionInterimRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *
 * attributes={"pagination_items_per_page"=5},
 *
 *    subresourceOperations={
 *      "api_interims_prospection_interims_get_subresource"={
 *         "method"="GET",
 *         "normalization_context"={"groups"={"interim_prospection_subresource"}}
 *     }
 *     },
 *     normalizationContext={"groups"={"prospection_read"}},

 * )
 * @ORM\Entity(repositoryClass=ProspectionInterimRepository::class)
 */
class ProspectionInterim
{
   use Timestamps;
    /** 
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"interim_prospection_subresource","prospection_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Interim::class, inversedBy="prospectionInterims")
     */
    private $interim;

    /**
     * @ORM\Column(type="text")
     * @Groups({"interim_prospection_subresource","prospection_read"})
     */
    private $commentaire;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"interim_prospection_subresource","prospection_read"})
     */
    private $dateRapell;

    /**
     * @ORM\ManyToOne(targetEntity=TypeProspection::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"interim_prospection_subresource","prospection_read"})
     */
    private $type;


    /**
     * ProspectionInterim constructor.
     */
    public function __construct()
    {
        $this->createdAt();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInterim(): ?Interim
    {
        return $this->interim;
    }

    public function setInterim(?Interim $Interim): self
    {
        $this->interim = $Interim;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }



    public function getDateRapell(): ?\DateTimeInterface
    {
        return $this->dateRapell;
    }

    public function setDateRapell(?\DateTimeInterface $dateRapell): self
    {
        $this->dateRapell = $dateRapell;

        return $this;
    }

    public function getType(): ?TypeProspection
    {
        return $this->type;
    }

    public function setType(?TypeProspection $type): self
    {
        $this->type = $type;

        return $this;
    }
}
