<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"user_read"}},
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("pseudo")
 *
 */
class User implements UserInterface
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user_read","interim_read","recruteur_read", "recruteur_write","gi_read", "gi_write","candidature_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="le mail est obligatoire")
     * @Assert\Email(message="le mail saisi est incorrect")
     * @Groups({"user_read","interim_read", "user_write","gi_write", "gi_read","recruteur_read", "recruteur_write", "client_read"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"gi_write", "gi_read"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="le mot de passe est obligatoire")
     * @Assert\Length(min="8", minMessage="le mot de passe doit faire minimum 8 lettres")
     * @Groups({"recruteur_read", "recruteur_write","gi_write"})
     */
    private $password;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotBlank(message="la civilite est obligatoire")
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read"})
     */
    private $civilite;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read"})
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read", "client_read"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read", "client_read"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confirmationToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read","interim_read", "gi_write", "gi_read"})
     */
    private $image;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read"})
     */
    private $isEnable = true;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read"})
     */
    private $datenaissance;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="le nom est obligatoire")
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read","candidature_read", "client_read"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="la prenom est obligatoire")
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read","candidature_read", "client_read"})
     */
    private $prenom;

    /**
     * @ORM\ManyToMany(targetEntity=GroupeRoles::class)
     * @Groups({"user_read", "gi_write", "gi_read"})
     */
    private $groupeRoles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write"})
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write"})
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write"})
     */
    private $departement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write"})
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write"})
     */
    private $idPaysNaissance;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write"})
     */
    private $lieuNaissance;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read","interim_read", "recruteur_read", "recruteur_write","gi_write", "gi_read"})
     */
    private $nationalite;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dossier", cascade={"persist"}, mappedBy="user")
     */
    private $dossier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $num_secu;

    public function __construct()
    {
        $this->createdAt();
        $this->groupeRoles = new ArrayCollection();
        $this->updateAt();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse ? $this->adresse : "Pas d'adresse";
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param mixed $civilite
     */
    public function setCivilite($civilite): void
    {
        $this->civilite = $civilite;
    }

    /**
     * @return bool
     */
    public function getIsEnable()
    {
        return $this->isEnable;
    }

    /**
     * @param $isEnable
     */
    public function setIsEnable($isEnable): void
    {
        $this->isEnable = $isEnable;
    }

    public function getDatenaissance(): ?string
    {
        return $this->datenaissance ? $this->datenaissance->format("Y-m-d") : $this->datenaissance;
    }

    public function setDatenaissance(?DateTimeInterface $datenaissance): self
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection|groupeRoles[]
     */
    public function getGroupeRoles(): Collection
    {
        return $this->groupeRoles;
    }

    public function addGroupeRole(groupeRoles $groupeRole): self
    {
        if (!$this->groupeRoles->contains($groupeRole)) {
            $this->groupeRoles[] = $groupeRole;
        }

        return $this;
    }

    public function removeGroupeRole(groupeRoles $groupeRole): self
    {
        if ($this->groupeRoles->contains($groupeRole)) {
            $this->groupeRoles->removeElement($groupeRole);
        }

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getIdPaysNaissance(): ?string
    {
        return $this->idPaysNaissance;
    }

    public function setIdPaysNaissance(?string $idPaysNaissance): self
    {
        $this->idPaysNaissance = $idPaysNaissance;

        return $this;
    }

    public function getLieuNaissance(): ?string
    {
        return $this->lieuNaissance;
    }

    public function setLieuNaissance(?string $lieuNaissance): self
    {
        $this->lieuNaissance = $lieuNaissance;

        return $this;
    }

    public function getNationalite(): ?string
    {
        return $this->nationalite;
    }

    public function setNationalite(?string $nationalite): self
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    public function getDossier(): ?Dossier
    {
        return $this->dossier;
    }

    public function setDossier(?Dossier $dossier): self
    {
        $this->dossier = $dossier;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $dossier ? null : $this;
        if ($dossier->getUser() !== $newUser) {
            $dossier->setUser($newUser);
        }

        return $this;
    }

    /**
     * @return string
     * @Groups({"recruteur_read"})
     */
    public function getCiviliteString()
    {
        return $this->civilite ? 'Monsieur' : 'Madame';
    }

    public function getNumSecu(): ?string
    {
        return $this->num_secu;
    }

    public function setNumSecu(?string $num_secu): self
    {
        $this->num_secu = $num_secu;

        return $this;
    }
}
