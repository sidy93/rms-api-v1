<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\JustificatifRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"justificatif_write"}},
 * )
 * @ORM\Entity(repositoryClass=JustificatifRepository::class)
 */
class Justificatif
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"justificatif_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"justificatif_write"})
     */
    private $adresseInterim;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"justificatif_write"})
     */
    private $adresseHopital;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"justificatif_write"})
     */
    private $nombreTrajet;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"justificatif_write"})
     */
    private $nombreRelance;

    /**
     * @ORM\Column(type="float", length=100, nullable=true)
     * @Groups({"justificatif_write"})
     */
    private $coefIK;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"justificatif_write"})
     */
    private $nombreKm;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"justificatif_write"})
     */
    private $enable;

    /**
     * @ORM\OneToMany(targetEntity=Piece::class, mappedBy="justificatif", orphanRemoval=true)
     */
    private $pieces;

    /**
     * @ORM\OneToOne(targetEntity=Renumeration::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"justificatif_write"})
     */
    private $remuneration;

    public function __construct()
    {
        $this->pieces = new ArrayCollection();
        $this->createdAt();
        $this->enable = false;
    }

    public function attributs() {
        $attributs = [];
        foreach ($this as $key => $value) {
            $attributs[] = $key;
        }
        return $attributs;
    }
    public function UpdateItem($elemet,$valeur) {
        foreach ($this as $key => $value) {
            if($key==$elemet){
                $this->$key = $valeur;
            }
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdresseInterim(): ?string
    {
        return $this->adresseInterim;
    }

    public function setAdresseInterim(?string $adresseInterim): self
    {
        $this->adresseInterim = $adresseInterim;

        return $this;
    }

    public function getAdresseHopital(): ?string
    {
        return $this->adresseHopital;
    }

    public function setAdresseHopital(?string $adresseHopital): self
    {
        $this->adresseHopital = $adresseHopital;

        return $this;
    }

    public function getNombreTrajet(): ?float
    {
        return $this->nombreTrajet;
    }

    public function setNombreTrajet(?float $nombreTrajet): self
    {
        $this->nombreTrajet = $nombreTrajet;

        return $this;
    }

    public function getNombreRelance(): ?int
    {
        return $this->nombreRelance;
    }

    public function setNombreRelance(?int $nombreRelance): self
    {
        $this->nombreRelance = $nombreRelance;

        return $this;
    }

    public function getCoefIK(): ?float
    {
        return $this->coefIK;
    }

    public function setCoefIK(?float $coefIK): self
    {
        $this->coefIK = $coefIK;

        return $this;
    }

    public function getNombreKm(): ?float
    {
        return $this->nombreKm;
    }

    public function setNombreKm(?float $nombreKm): self
    {
        $this->nombreKm = $nombreKm;

        return $this;
    }

    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * @return Collection|Piece[]
     */
    public function getPieces(): Collection
    {
        return $this->pieces;
    }

    public function addPiece(Piece $piece): self
    {
        if (!$this->pieces->contains($piece)) {
            $this->pieces[] = $piece;
            $piece->setJustificatif($this);
        }

        return $this;
    }

    public function removePiece(Piece $piece): self
    {
        if ($this->pieces->contains($piece)) {
            $this->pieces->removeElement($piece);
            // set the owning side to null (unless already changed)
            if ($piece->getJustificatif() === $this) {
                $piece->setJustificatif(null);
            }
        }

        return $this;
    }

    public function getRemuneration(): ?Renumeration
    {
        return $this->remuneration;
    }

    public function setRemuneration(Renumeration $remuneration): self
    {
        $this->remuneration = $remuneration;

        return $this;
    }


}
