<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DisponibiliteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Auteur Sidy Mohamed Diallo
 * @ApiResource(
 *  normalizationContext={"groups"={"disponibilite_read"}},
 * )
 * @ORM\Entity(repositoryClass=DisponibiliteRepository::class)
 */
class Disponibilite
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"interim_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"interim_read"})
     */
    private $date;


    /**
     * @ORM\ManyToOne(targetEntity=Interim::class, inversedBy="disponibilites")
     *
     */
    private $interim;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }



    public function getInterim(): ?Interim
    {
        return $this->interim;
    }

    public function setInterim(?Interim $interim): self
    {
        $this->interim = $interim;

        return $this;
    }
}
