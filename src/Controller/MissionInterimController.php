<?php


namespace App\Controller;

use App\Entity\Candidature;
use App\Entity\Client;
use App\Entity\Contrat;
use App\Entity\FicheSpecialite;
use App\Entity\MissionInterim;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MissionInterimController
{


    /**
     * @var EntityManagerInterface
     */
    private $em;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param Request $request
     */
    public function  __invoke(Request $request){}

    /**
     * @Route("api/mission_interims", name ="listes_mission_interims", methods={"get"})
     */
    public function listeMissionInterimAction(Request $request)


    {
        $response = [];

        $missionInterims = $this->em->getRepository(MissionInterim::class)->findAllMissisonInterim($request->query->all());


        if($missionInterims){
            foreach ($missionInterims as $value){
                $clientAndVacation = $this->returnClientAndVacationByIdMI($value['id']);
                $missionInterim = array_merge($value, $clientAndVacation);
                array_push($response, $missionInterim);
            }
        }
        return new JsonResponse($response);
    }
    public function returnClientAndVacationByIdMI($idMI)
    {
        $response = [
            'candidatures' => [],
            'raison_sociale' => '',
            'idClient' => '',
            'ref_mission' => 0,
            'recruteursCH' => []
        ];
        $missionInterims = $this->em->getRepository(MissionInterim::class)->find($idMI);
        $candidatures = $missionInterims->getCandidatures()->getValues();
        $response['candidatures'] = $this->returnCandidatureRefractor($candidatures);
        $data = $this->em->getRepository(Candidature::class)->getCLientByIdCandidature($candidatures[0]->getId());
        if($data){
            $response['raison_sociale'] = $data['raison_sociale'];
            $response['ref_mission'] = $data['ref_mission'];
            $response['recruteursCH'] = $this->returnREcruteursByClient($data['idClient']);
            $response['idClient'] = $data['idClient'];
            $response['specialite'] = $this->returnSpecialiteByFicheSpecialite(intval($data['ficheSpecialite']))['specialite'];
            $response['nomSpecialite'] = $this->returnSpecialiteByFicheSpecialite(intval($data['ficheSpecialite']))['nomSpecialite'];
            $response['abreviationSpecialite'] = $this->returnSpecialiteByFicheSpecialite(intval($data['ficheSpecialite']))['abreviationSpecialite'];
        }
        return $response;
    }
    public function returnCandidatureRefractor($candidatures)
    {
        $response = [];
        foreach ($candidatures as $candidature){
            $data = $this->em->getRepository(Candidature::class)->getCandidatureRefractor($candidature->getId());
            if($data['contrat_pld']){
                $data['contrat_pld'] = $this->returnContratRafractorById($data['contrat_pld']);
            }
            array_push($response, $data);
        }
        return $response;
    }
    public function returnREcruteursByClient($idClient)
    {
        $response = [];
        $client = $this->em->getRepository(Client::class)->find(intval($idClient));
        foreach ($client->getRecruteurs()->getValues() as $recruteur){
            $recruteurRefractor =  [
              'id' => $recruteur->getId(),
              'idUser' => $recruteur->getUser()->getId(),
              'nom' => $recruteur->getUser()->getNom(),
              'prenom' => $recruteur->getUser()->getPrenom(),
              'tel' => $recruteur->getUser()->getTelephone(),
              'email' => $recruteur->getUser()->getEmail(),
            ];
            array_push($response, $recruteurRefractor);
        }
        return $response;
    }

    public function returnContratRafractorById($id)
    {
        $response = [
            'id' => '',
            'idPld' => '',
            'url' => '',
            'cei' => false,
            'csi' => false,
            'cec' => false,
            'csc' => false,
        ];
        $contrat = $this->em->getRepository(Contrat::class)->find($id);
        if($contrat){
            $response['id'] = $contrat->getId();
            $response['idPld'] = $contrat->getIdPld();
            $response['url'] = $contrat->getUrlContrat();
            foreach ($contrat->getSignatures()->getValues() as $sellAndSign){
                switch ($sellAndSign->getType()){
                    case 0:
                        // Ch
                        $sellAndSign->getDateEnvoi() ? $response['cec'] = true : $response['cec'] = false;
                        $sellAndSign->getDateSignature() ? $response['csc'] = true : $response['csc'] = false;
                        break;
                    case 1:
                        // Interim
                        $sellAndSign->getDateEnvoi() ? $response['cei'] = true : $response['cei'] = false;
                        $sellAndSign->getDateSignature() ? $response['csi'] = true : $response['csi'] = false;
                        break;
                }
            }
        }

        return $response;
    }

    public function returnSpecialiteByFicheSpecialite($id){
        $reponse = [
            'specialite' => '',
            'nomSpecialite' => '',
        ];
        $ficheSpecialite = $this->em->getRepository(FicheSpecialite::class)->find($id);
        if($ficheSpecialite){
            $reponse['specialite'] = $ficheSpecialite->getQualification()->getValues()[0]->getSpecialite()->getSpecialite();
            $reponse['nomSpecialite'] = $ficheSpecialite->getQualification()->getValues()[0]->getSpecialite()->getNomspecialite();
            $reponse['abreviationSpecialite'] = $ficheSpecialite->getQualification()->getValues()[0]->getSpecialite()->getAbreviation();
        }
        return $reponse;
    }
}
