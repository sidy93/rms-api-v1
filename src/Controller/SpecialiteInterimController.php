<?php


namespace App\Controller;


use App\Entity\Interim;

use App\Entity\Qualification;
use App\Events\HistoriqueSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Service\SendinblueService;
use App\Service\Utiles;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class SpecialiteInterimController
 * @package App\Controller
 */
class SpecialiteInterimController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    private $eventHistory;
    private $params;

    /**
     * SpecialiteInterimController constructor.
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $params
     */
    public function __construct(EntityManagerInterface $entityManager, HistoriqueSubscriber $eventHistory, ParameterBagInterface $params)
    {
        $this->em = $entityManager;
        $this->eventHistory = $eventHistory;
        $this->params = $params;
    }

    /**
     * @param Interim $data
     * @param Request $request
     * @param SendinblueService $mail
     * @return string|JsonResponse
     */
    public function __invoke(Interim $data, Request $request, SendinblueService $mail)
    {

        if ($request->isMethod("GET")) {
            $response = [];
            $qualifications = [];
            try {
                if ($data) {
                    foreach ($data->getQualifications()->getValues() as $qalif) {
                        $qualifications[$qalif->getSpecialite()->getNomSpecialite()] [] =

                            [
                                "id" => $qalif->getId(),
                                "qualification" => $qalif->getQualification(),
                                "creat_at" => $qalif->getCreateAt()
                            ];
                    }

                    foreach ($qualifications as $key => $q) {
                        $response [] = [
                            "specialite" => $key,
                            "qualifications" => $q
                        ];
                    }

                }

            } catch (Exception $exception) {
                return $exception->getMessage();
            }

            return new JsonResponse($response);
        } else if ($request->isMethod("post")) {

            $roles = $this->eventHistory->getRoles($this->eventHistory->getSecurity()->getUser()->getRoles());
            $nomComplet = $this->eventHistory->getSecurity()->getUser()->getNom() . ' ' . $this->eventHistory->getSecurity()->getUser()->getPrenom();
            $idUser = $this->eventHistory->getSecurity()->getUser()->getId();

            $erreur = null;
            $succes = null;

            $qualifPost = json_decode($request->getContent());
            try {
                $qualifMail = [];
                $specialite = null;
                $action = null;
                $methode = null;

                foreach ($qualifPost->qualifs as $qualification) {
                    $qualif = $this->em->getRepository(Qualification::class)->find($qualification);
                    $specialite = $qualif->getSpecialite()->getNomSpecialite();
                    array_push($qualifMail, $qualif->getQualification());
                    if ($qualif) {
                        $itemQualif = $qualif->getQualification();
                        if ($qualifPost->add === true) {
                            $data->addQualification($qualif);

                            $succes = "mis a jour effectueé";

                            $action ="Ajout de nouvelle qualification $itemQualif";
                            $methode = "post";
                        } else {

                            $data->removeQualification($qualif);
                            $succes = "suppression effectueé";
                            $action ="supression de la qualification $itemQualif";
                            $methode = "put";

                        }

                        $historyItem = [
                            'cible' => $data->getUser()->getNom() . ' ' . $data->getUser()->getPrenom(),
                            'element' => $action,
                            'cible_id' => $data->getId(),
                            'table'=>"Qualification"
                        ];

                        $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, $methode);

                    }
                    $this->em->persist($data);
                }
                $this->em->flush();
                if ($qualifPost->mail == true) {
                    $template = "emails/erp/interim/creation_specialite.html.twig";
                    $mail->sendNotificationMail(
                        $data->getUser()->getEmail(),
                        $data->getUser()->getNom(),
                        false,
                        $specialite,
                        $template,
                        $qualifMail,
                        "Mis a jour specialité",
                        "update specialite");
                }

                $this->em->flush();

            } catch (Exception $exception) {
                $erreur = $exception->getMessage();
            }

            return new JsonResponse(
                [
                    "erreur" => $erreur,
                    "success" => $succes
                ]
            );

        }
    }
}
