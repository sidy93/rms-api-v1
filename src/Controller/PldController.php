<?php

namespace App\Controller;

use App\Entity\Interim;
use App\Entity\Specialite;
use App\Entity\User;
use App\Service\Utiles;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class PldController extends AbstractController
{

    public function __construct()
    {
        $this->httpClient = new Client([
            'timeout'  => 30.0,
        ]);
    }

    /**
     * @Route("api/pld/qualifications",
     * name="pld_list_qualifications")
      */
    public function listQualificationsPldAction()
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'QUALIFICATION', Utiles::httpOptionsPld(null,null));
        if($http->getBody())
            $response = $http->getBody()->getContents();
        return new JsonResponse($response);
    }

    /**
     * @Route("api/pld/caracteristiquePoste",
     * name="pld_list_caracteristiquePoste")
      */
    public function listCaracteristiquePostePldAction()
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'caracteristiquePoste', Utiles::httpOptionsPld(null,null));
        if($http->getBody())
            $response = $http->getBody()->getContents();

        return new JsonResponse($response);
    }

    /**
     * @Route("api/pld/commerciaux",
     * name="pld_list_commerciaux")
      */
    public function listCommerciauxPldAction()
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'COMMERCIAL', Utiles::httpOptionsPld(null,null));
        if($http->getBody())
            $response = $http->getBody()->getContents();

        return New JsonResponse($response);
    }
    /**
     * @Route("api/pld/primes",
     * name="pld_list_primes")
     */
    public function listPrimesPldAction()
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'PRIME', Utiles::httpOptionsPld(null,null));
        if($http->getBody()){
            foreach (json_decode($http->getBody()->getContents()) as $key => $value){
                $response[$key]= [
                    "Dossier" => $value->Dossier,
                    "IdPrime" => $value->IdPrime,
                    "Designation" => $value->Designation,
                    "DesignationFacture" => $value->DesignationFacture,
                    "JTC" => $value->JTC,
                    "Condition" => $value->Condition,
                    "IdRubriqueImposable" => $value->IdRubriqueImposable,
                    "IdRubriqueNonImposable" => $value->IdRubriqueNonImposable,
                    "Commentaire" => $value->Commentaire,
                ];
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/pld/primes/{idClient}",
     * name="pld_list_primes_by_client")
      */
    public function listPrimesByClientPldAction($idClient)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository(Client::class)->find($idClient);
        $response = [];
        $response_reformat = [];
        if($client){
            $idPld = $client->getIdPld();
            $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'PRIME', Utiles::httpOptionsPld(null,null));
            if($http->getBody())
                $response = $http->getBody()->getContents();

            foreach (json_decode($response) as $key => $value){
                if (strstr($value->Designation, $idPld) || strstr($value->Designation, '0000000')) {
                    $response_reformat[$key]= [
                        "Dossier" => $value->Dossier,
                        "IdPrime" => $value->IdPrime,
                        "Designation" => $value->Designation,
                        "DesignationFacture" => $value->DesignationFacture,
                        "JTC" => $value->JTC,
                        "Condition" => $value->Condition,
                        "IdRubriqueImposable" => $value->IdRubriqueImposable,
                        "IdRubriqueNonImposable" => $value->IdRubriqueNonImposable,
                        "Commentaire" => $value->Commentaire,
                    ];
                }
            }
        }
        return $response_reformat;
    }
    /**
     * @Route("/pld/Client/{idClient}",
     * name="pld_add_or_update_Client")
      */
    public function ClientPldAction($idClient)
    {
        $response = [];
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository(Client::class)->find($idClient);
        if($client){
            $body['SIRET'] = $client->getSiret();
            $body['NAF'] = $client->getNaf();
            $body['TVAIntra'] = $client->getNumeroDeTva();
            $body['RaisonSociale'] = strlen($client->getRaisonSociale())> 34 ? substr($client->getRaisonSociale(),0,33) : $client->getRaisonSociale();
            $body['RaisonSocialeSuite'] = strlen($client->getRaisonSociale())> 34 ? substr($client->getRaisonSociale(),34) : null;
            $body['AdresseRue'] = strlen($client->getAdresse())> 34 ? substr($client->getAdresse(),0,33) : $client->getAdresse();
            $body['AdresseSuite'] =  strlen($client->getAdresse())> 34 ? substr($client->getAdresse(),34) : null;
            $body['AdresseCodePostal'] = $client->getCp();
            $body['AdresseVille'] = $client->getVille();
            $body['AdresseCodeINSEE'] = "";
            $body['AdressePays'] = $client->getAdressePays();
            $body['Groupe'] = "";
            $body['GrandCompte'] = "";
            $body['Telephone1'] = $client->getTel();
            $body['Telephone2'] = "";
            $body['Telephone3'] = "";
            $body['Fax'] = $client->getFax();
            $body['Email'] = $client->getEmail();
            $body['EmailFacture'] = "";
            $body['BanqueIBAN'] = "";
            $body['BanqueBIC'] = "";
            $body['BanqueDomiciliation'] = "";
            $body['FactureNom'] = "";
            $body['FactureNomSuite'] = "";
            $body['FactureAdresseRue'] = "";
            $body['FactureAdresseSuite'] = "";
            $body['FactureAdresseCodePostal'] = "";
            $body['FactureAdresseVille'] = "";
            $body['FactureAdressePays'] = "";
            $body['IdFormeJuridique'] = 0;
            $body['TxtFormeJuridique'] = "";
            $body['IdActivite'] = 0;
            $body['TxtActivite'] = "";
            $body['IdActiviteFact'] = 0;
            $body['TxtActiviteFact'] = "";
            $body['IdEffectif'] = 0;
            $body['TxtEffectif'] = "";
            $body['IdCommercial1'] = "";
            $body['TxtCommercial1'] = "";
            $body['IdCommercial2'] = "";
            $body['TxtCommercial2'] = "";
            $body['IdModeReglement'] = 0;
            $body['TxtModeReglement'] = "";
            $body['IdEcheance'] = 0;
            $body['TxtEcheance'] = "";
            $body['IdDevise'] = 0;
            $body['TxtDevise'] = 0;
            $body['IdFactureCycle'] = 0;
            $body['TxtFactureCycle'] = "";
            $body['IdFactureGroupe1'] = 0;
            $body['TxtFactureGroupe1'] = "";
            $body['FactureSousTotal1'] = false;
            $body['IdFactureGroupe2'] = 0;
            $body['TxtFactureGroupe2'] = "";
            $body['FactureSousTotal2'] = false;
            $body['IdFactureGroupe3'] = 0;
            $body['TxtFactureGroupe3'] = "";
            $body['IdRemiseSpeciale'] = 0;
            $body['TxtRemiseSpeciale'] = "";
            $body['RemiseTaux'] = 0;
            $body['NbFacture'] = 0;
            $body['CreditCode'] = "0";
            $body['CreditMontant'] = 0;
            $body['CreditCompl'] = 0;
            $body['CreditDateObt'] = "";
            $body['CreditDateEch'] = "";
            $body['CreditDateDem'] = "";
            $body['CreditMontantDem'] = 0;
            $body['CreditDateRefus'] = "";
            $body['CreditOrg'] = "";
            $body['NumeroFacto'] = "";
            $body['CreditCommentaire'] = "";
            $body['IdConsignePoste1'] = 0;
            $body['TxtConsignePoste1'] = "";
            $body['IdConsignePoste2'] = 0;
            $body['TxtConsignePoste2'] = "";
            $body['IdConsignePoste3'] = 0;
            $body['TxtConsignePoste3'] = "";
            $body['IdConsigneReference'] = 0;
            $body['TxtConsigneReference'] = "";
            $body['IdConsigneRemuneration'] = 0;
            $body['TxtConsigneRemuneration'] = "";
            $body['IdConsigneFacturation'] = 0;
            $body['TxtConsigneFacturation'] = "";
            $body['IdAdresseVM'] = 0;
            $body['TxtAdresseVM'] = "";
            $body['Annotations'] = "";
            $body['PenseBete'] = "";
            $body['Horaires'] = "";
            $body['HoraireAMDebut'] = "";
            $body['HoraireAMFin'] = "";
            $body['HorairePMDebut'] = "";
            $body['HorairePMFin'] = "";
            $body['HoraireDebutPremier'] = "";
            $body['NbHeureHebdo'] = 0;
            $body['NbHeureLundi'] = 0;
            $body['NbHeureMardi'] = 0;
            $body['NbHeureMercredi'] = 0;
            $body['NbHeureJeudi'] = 0;
            $body['NbHeureVendredi'] = 0;
            $body['NbHeureSamedi'] = 0;
            $body['NbHeureDimanche'] = 0;
            $body['NbHeureFerie'] = 0;
            $body['CoeffHeures'] = 0;
            $body['CoeffPrimes'] = 0;
            $body['CoeffIndemnites'] = 0;
            $body['CoeffGestion'] = 0;
            $body['CoeffNbDecimale'] = 0;
            $body['EstDemat'] = "N";
            $body['DematEligible'] = 0;
            $body['DematNom'] = "";
            $body['DematPrenom'] = "";
            $body['DematMail'] = "";
            $body['DematRefuse'] = "";
            $body['DematContrat'] = "";
            $body['DematFacture'] = "";
            $body['DateHeureMAJ'] = "";
            $body['DateheureCRE'] = "";
            $body['TabPrimes'] = [];
            $body['TabQualifications'] = [];
            $body['TabChantiers'] = [];
            $recruteurs = [];
            $recruteur_clients = $em->getRepository("RMSApiBundle:RecruteurClient")->findBy(array('client' => $idClient));
            if($recruteur_clients){
                foreach ($recruteur_clients as $r) {
                    $recruteurs[] = [
                        'Civilite' => $r->getRecruteur()->getUser()->getCivilite(),
                        'Nom' => $r->getRecruteur()->getUser()->getPrenom() . " " . $r->getRecruteur()->getUser()->getNom(),
                        'Fonctions' => $r->getRecruteur()->getFonction(),
                        'Telephone1' => $r->getRecruteur()->getUser()->getTelephone(),
                        'Telephone2' => "",
                        'Fax' => "",
                        'Email' => $r->getRecruteur()->getUser()->getEmail(),
                        'Reference' => "",
                        'Commentaire' => "",
                        'PourContrat' => "",
                        'PourProlong' => "",
                        'PourFacture' => "",
                        'PourRH' => "",
                        'PourProposition' => "",
                        'PourRelance' => "",
                        'PourRegistre' => "",
                        'PourStat' => "",
                        'PourPrisePoste' => "",
                        'PourDemat' => "",
                        'Suppression' => "",

                    ];
                }
            }
            $body['TabInterlocuteurs'] = $recruteurs;

            $http =  $this->httpClient->request('POST', Utiles::baseUrlPld().'Client', Utiles::httpOptionsPld( null, json_encode($body)));
            if($http->getBody())
                $response = $http->getBody()->getContents();
        }


        return $response;
    }

    /**
     * @Route("/pld/interim/{idInterim}",
     * name="pld_add_or_update_interim")
      */
    public function InterimPldAction($idInterim, Request $request)
    {
        $response = [];
        $em = $this->getDoctrine()->getManager();
        $interim = $em->getRepository(Interim::class)->find($idInterim);
        if($interim){
            $body['TypeInterimaire'] = 2;
            $body['Statut'] = 1;
            $body['Civilite'] = $interim->getUser()->getCivilite();
            $body['Nom'] = strlen($interim->getUser()->getNom())> 34 ? substr($interim->getUser()->getNom(),0,33) : $interim->getUser()->getNom();
            $body['NomSuite'] = strlen($interim->getUser()->getNom())> 34 ? substr($interim->getUser()->getNom(),34) : null;
            $body['Prenom'] = $interim->getUser()->getPrenom();
            $body['AdresseRue'] = strlen($interim->getUser()->getAdresse())> 34 ? substr($interim->getUser()->getAdresse(),0,33) : $interim->getUser()->getAdresse();
            $body['AdresseSuite'] =  strlen($interim->getUser()->getAdresse())> 34 ? substr($interim->getUser()->getAdresse(),34) : null;
            $body['AdresseCodePostal'] = $interim->getUser()->getCp();
            $body['AdresseVille'] = $interim->getUser()->getVille();
            $body['AdresseCodeINSEE'] = "";
            $body['AdressePays'] = "";
            $body['NumeroSecu'] = $interim->getUser()->getNumSecu();
            $body['GrandCompte'] = "";
            $body['DateNaissance'] = $interim->getUser()->getDateNaissance();
            $body['DepartNaissance'] = "";
            $body['LieuNaissance'] = $interim->getUser()->getLieuNaissance();
            $body['PaysNaissance'] =  "";
            $body['IdPaysNaissance'] =  $interim->getUser()->getIdPaysNaissance();
            $body['TxtPaysNaissance'] =  "";
            $body['NomNaissance'] =  "";
            $body['SituationFamiliale'] =  0;
            $body['NbEnfants'] =  0;
            $body['Sexe'] =  0;
            $body['IdNationalite'] = $interim->getUser()->getIdNationalite();
            $body['TxtNationalite'] = "";
            $body['Telephone1'] = $interim->getUser()->getTelephone();
            $body['Telephone2'] = "";
            $body['Telephone3'] = "";
            $body['Email'] = $interim->getUser()->getEmail();
            $body['IdReglementAcompte'] = $interim->getIdReglementAcompte();
            $body['TxtReglementeAcompte'] = "";
            $body['IdReglementPaie'] = $interim->getIdReglement();
            $body['TxtReglementePaie'] = "";
            $body['DateVisiteMedicale'] = "";
            $body['ValiditeVisiteMedicale'] = 0;
            $body['BanqueIBAN'] = "";
            $body['BanqueBIC'] = "";
            $body['BanqueDomiciliation'] = "";
            $body['BanqueTitulaire'] = "";
            $body['IdCategoriePaie'] = 1;
            $body['TxtCategoriePaie'] = "Interimair";
            $body['IdRecruteur1'] = "";
            $body['TxtRecruteur1'] = "";
            $body['IdRecruteur2'] = "";
            $body['TxtRecruteur2'] = "";
            $body['IdMoyenLocomotion'] = $interim->getIdMoyenLocomotion();
            $body['TxtMoyenLocomotion'] = "";
            $body['IdZoneGeographique'] = "";
            $body['TxtZoneGeographique'] = "";
            $body['Annotation'] = "";
            $body['Alerte'] = "";
            $body['Profession'] = "";
            $body['PCS'] = $interim->getPcs();
            $body['CVFichierNom'] = "";
            $body['CVFichierImage'] = "";
            $body['CV2FichierNom'] = "";
            $body['CV2FichierImage'] = "";
            $body['CV3FichierNom'] = "";
            $body['CV3FichierImage'] = "";
            $body['PhotoFichierNom'] = "";
            $body['PhotoFichierImage'] = "";
            $body['RIBFichierNom'] = "";
            $body['RIBFichierImage'] = "";
            $body['VMFichierNom'] = "";
            $body['VMFichierImage'] = "";
            $body['EstVIP'] = "";
            $body['EstDemat'] = "";
            $body['DematEligible'] = "";
            $body['DematNom'] = "";
            $body['DematPrenom'] = "";
            $body['DematMail'] = "";
            $body['DematRefuse'] = "";
            $body['DematContrat'] = "";
            $body['DematBulletin'] = "";
            $body['TabQualifications'] = [];
            $body['TabCartes'] = [];
            $body['TabDispoGene'] = [];
            $body['TabDispoJour'] = [];
            $body['TabAdequat'] = [];
            $body['TabHabilit'] = [];

            $http =  $this->httpClient->request('POST', Utiles::baseUrlPld().'Interimaire', Utiles::httpOptionsPld( null, json_encode($body)));
            if($http->getBody()){
                $response = $http->getBody()->getContents();
                if($response && json_decode($response)->Code){
                    $idInterimPld = json_decode($response)->ID;
                    $user = $interim->getUser();
                    $user->setIdPld($idInterimPld);
                    $em->persist($user);
                    $em->flush();

                    // Historique ==========================================================
                    $userService = $this->container->get('user.service');
                    $user = ($userService->GetUser($request));
                    $dataHistory = [
                        'content' => 'Engistrement de lintermaire sur pld',
                        'action' => 'enregistrement',
                        'entities' => '',
                        'reference' => $interim->getId(),
                        'cibles' => $interim->getUser()->getNom().' '.$interim->getUser()->getPrenom(),
                    ];
                    $utilesService = $this->container->get('utiles.service');
                    $utilesService->addHistory($dataHistory, $user);
                    // Fin de lhistorique ===================================================
                }
            }
        }

        return $response;
    }


    /**
     * @Route("/pld/contrat/{id}",
     * name="pld_find_contrat")
      */
    public function findContratPldAction($id)
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'Contrat', Utiles::httpOptionsPld( $id, null));
        if($http->getBody()){
            $response = json_decode($http->getBody()->getContents())[0];
        }
        return $response;
    }

    /**
     * @Route("/pld/client/{id}",
     * name="pld_find_client")
      */
    public function findClientPldAction($id)
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'Client', Utiles::httpOptionsPld( $id, null));
        if($http->getBody()){
            $response = json_decode($http->getBody()->getContents())[0];
        }
        return json_encode($response);
    }

    /**
     * @Route("/pld/interimaire/{id}",
     * name="pld_find_Interim")
      */
    public function findInterimPldAction($id)
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'Interimaire', Utiles::httpOptionsPld( $id, null));
        if($http->getBody()){
            $response = json_decode($http->getBody()->getContents())[0];
        }
        return json_encode($response);
    }

    /**
     * @Route("/pld/rh/{id}",
     * name="pld_find_rh")
      */
    public function findRhPldAction($id)
    {
        $response = [];
        $http =  $this->httpClient->request('GET', Utiles::baseUrlPld().'RH', Utiles::httpOptionsPld( $id, null));
        if($http->getBody()){
            $response = json_decode($http->getBody()->getContents())[0];
        }
        return $response;
    }

    /**
     * @Route("/pld/edit-rh",
     * name="pld_create_rh")
      */
    public function editRhAction($rh)
    {
        $response = null;
        $http =  $this->httpClient->request('POST', Utiles::baseUrlPld().'RH', Utiles::httpOptionsPld( null, json_encode($rh)));
        if($http->getBody()){
            $response = json_decode($http->getBody()->getContents());
        }
        return $response;
    }
    /**
     * @Route("/pld/generer-rh/{id}",
     * name="pld_generer_rh")
     */
    public function genererRhPldAction($id, Request $request)
    {
        header('Access-Control-Allow-Origin: *');


        $response = new Response();
        $em = $this->getDoctrine()->getManager();
        $rhPld = $this->findRhPldAction(intval($id));
        if($rhPld){
            $contratPld = json_decode($this->findContratPldAction($rhPld->NumeroContrat));
            $user = $em->getRepository(User::class)->findOneBy(['idPld' => $rhPld->IdInterimaire]);
//            dump($rhPld);die();
            $data = [
                'interim' => [
                    'nom' => $user->getNom() ,
                    'prenom' => $user->getPrenom(),
                    'photo' => $user->getPhoto(),
                    'qualification' => $contratPld->TxtQualification
                ],
                'client' => [
                    'rs' => strtolower($contratPld->RaisonSociale),
                    'service' => strtolower($contratPld->TxtCaracteristiquePoste),
                    'adresse' => strtolower($contratPld->AdresseRue),
                    'ville' => strtolower($contratPld->AdresseVille),
                ],
            ];

            // Configure Dompdf according to your needs
            $pdfOptions = new Options(array('enable_remote' => true));

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('@RMSApi/RH/rh.html.twig', ['data' => $data]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser (force download)
            $dompdf->stream("mypdf.pdf", [
                "Attachment" => false
            ]);

        }
        return $response;
    }

    /**
     * @Route("/pld/createContrat",
     * name="pld_create_contrat")
      */
    public function ContratPldAction($contratPld)
    {
        $erreur = false;
        $response = null;
//        $qualifications = json_decode($this->listQualificationPldAction());
//        if(empty($body->contrat->IdContrat)){
//            foreach ( $qualifications as $qualification){
//                if(strstr($this->stripAccents(strtolower($qualification->Designation)), strtolower($this->stripAccents($this->specialiteErp($body->contrat->IdQualification)->getSpecialite())))){
//                    $erreur = false;
//                    $body->contrat->IdQualification = $qualification->IdQualification;
//                    break 1;
//                }
//                else{
//                    $erreur =  true;
//                    $response = "La spécialité de ERP (".$this->specialiteErp($body->contrat->IdQualification)->getSpecialite().") n'existe pas sur la liste des qualifications PLD !";
//                }
//            }
//        }
        if(!$erreur){
            $http =  $this->httpClient->request('POST', Utiles::baseUrlPld().'Contrat', Utiles::httpOptionsPld( null, json_encode($contratPld) ));
            if($http->getBody()){
                $response = json_decode($http->getBody()->getContents());
            }
        }

        return $response;
    }

    /**
     * @Route(path="/pld/idInterimPld-to-erp/{offset}",name="get_idInterimPld_To_Erp")
     */
    public function idInterimPldToErpAction($offset)
    {
        $em = $this->getDoctrine()->getManager();
        $limit = 100 + $offset;
        $response = [];
        for($i = $offset; $i < $limit; $i++){
            $cpt = 0;
            $idPld = "0000000";
            if($i < 10){
                $idPld = "000000". $i;
            }
            else if($i < 100){
                $idPld = "00000". $i;
            }
            else if($i < 1000){
                $idPld = "0000". $i;
            }
            else if ($i < 10000){
                $idPld = "000". $i;
            }
            try {
                $interimPld = json_decode($this->findInterimPldAction($idPld));
                if($interimPld){
                    $email = $interimPld->Email;
                    $nom = $interimPld->Nom;
                    $prenom = $interimPld->Prenom;

                    $interimErpExiste = $em->getRepository(User::class)->findUserLike($email,$nom,$prenom);
                    if($interimErpExiste){
                        $interimErpExiste = (object)$interimErpExiste;
                        $interimErp = $em->getRepository(User::class)->find($interimErpExiste->id);
                        $interimErp->setIdPld($idPld);
                        $em->persist($interimErp);
                        $response = 'ok';

                    }
                }
            }
            catch (\Exception $exception){}
        }
        $em->flush();
        return new JsonResponse($response);
    }

    /**
     * @Route(path="/pld/idClientPld-to-erp/{offset}",name="get_idClientPld_To_Erp")
     */
    public function idClientPldToErpAction($offset)
    {
        $em = $this->getDoctrine()->getManager();
        $limit = 100 + $offset;
        $response = [];
        for($i = $offset; $i < $limit; $i++){
            $idPld = "0000000";
            if($i < 10){
                $idPld = "000000". $i;
            }
            else if($i < 100){
                $idPld = "00000". $i;
            }
            else if($i < 1000){
                $idPld = "0000". $i;
            }
            else if ($i < 10000){
                $idPld = "000". $i;
            }
            try {
                $clientPld = json_decode($this->findClientPldAction($idPld));
                if($clientPld){
                    $raisonSociale = $clientPld->RaisonSociale;
                    $email = $clientPld->Email;
                    $telephone = $clientPld->Telephone1;
                    $clientErpExiste = $em->getRepository(Client::class)->findClientLike($raisonSociale);
                    if($clientErpExiste){
                        $clientErpExiste = (object)$clientErpExiste;
                        $clientErp = $em->getRepository(Client::class)->find($clientErpExiste->id);
                        $clientErp->setIdPld($idPld);
                        $em->persist($clientErp);
                        $response = "ok";
                    }
                }
            }
            catch (\Exception $exception){}
        }
        $em->flush();
        return new JsonResponse($response);
    }
    function stripAccents($str){

        $url = $str;
        $url = preg_replace('#Ç#', 'C', $url);
        $url = preg_replace('#ç#', 'c', $url);
        $url = preg_replace('#è|é|ê|ë#', 'e', $url);
        $url = preg_replace('#È|É|Ê|Ë#', 'E', $url);
        $url = preg_replace('#à|á|â|ã|ä|å#', 'a', $url);
        $url = preg_replace('#@|À|Á|Â|Ã|Ä|Å#', 'A', $url);
        $url = preg_replace('#ì|í|î|ï#', 'i', $url);
        $url = preg_replace('#Ì|Í|Î|Ï#', 'I', $url);
        $url = preg_replace('#ð|ò|ó|ô|õ|ö#', 'o', $url);
        $url = preg_replace('#Ò|Ó|Ô|Õ|Ö#', 'O', $url);
        $url = preg_replace('#ù|ú|û|ü#', 'u', $url);
        $url = preg_replace('#Ù|Ú|Û|Ü#', 'U', $url);
        $url = preg_replace('#ý|ÿ#', 'y', $url);
        $url = preg_replace('#Ý#', 'Y', $url);

        return ($url);
    }

    public function specialiteErp($string){
        $response = null;
        $em = $this->getDoctrine()->getManager();
        $specialite = $em->getRepository(Specialite::class)->findOneBy(['nomspecialite' => $string]);
        if($specialite)
            $response = $specialite;
        return $response;

    }
}
