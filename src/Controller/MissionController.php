<?php


namespace App\Controller;


use App\Entity\Mission;
use App\Entity\Offres;
use App\Entity\Planning;
use App\Entity\StatutPlanning;
use App\Entity\TypeDeGarde;
use App\Events\HistoriqueSubscriber;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MissionController
{
    private $eventHistory;

    /**
     * MissionController constructor.
     */
    public function __construct(HistoriqueSubscriber $eventHistory)
    {
        $this->eventHistory = $eventHistory;
    }

    /**
     * @Route(path="api/getLastIdMission",name="get_last_id_mission")
     */
    public function getIdMissionAction(Request $request, EntityManagerInterface $manager)
    {
        $data = [];
        $lastIdMission = 0;
        $lastIdOffre = 0;
        $mission = $manager->getRepository(Mission::class)->findAll();
        if (sizeof($mission) > 0) {
            $sizeMission = sizeof($mission) - 1;
            $lastIdMission = $mission[$sizeMission]->getId();
        } else {
            $lastIdMission = 0;
        }

        $offre = $manager->getRepository(Offres::class)->findAll();
        if (sizeof($offre) > 0) {
            $sizeOffres = sizeof($offre) - 1;
            $lastIdOffre = $offre[$sizeOffres]->getId();
        } else {
            $lastIdOffre = 0;
        }
        return new JsonResponse(['lastIdMission' => $lastIdMission, 'lastIdOffre' => $lastIdOffre]);
    }

    /**
     * @Route(path="api/listeMission/{idClient}",name="get_liste_des_mission")
     */
    public function getListeMissionAction($idClient, Request $request, EntityManagerInterface $manager)
    {
        $data = [];
        // recuperation de la liste des mission interin suivant lid du client
        $missions = $manager->getRepository(Mission::class)->getListMissionForClient($idClient);
        foreach ($missions as $key => $value) {
            $plannings = $manager->getRepository(Planning::class)->getPlanningByMissionId(intval($value['id']));
            $planningsAP = $manager->getRepository(Planning::class)->getPlanningByMissionIdAndStatut(intval($value['id']), "A Pourvoir");
            $planningsP = $manager->getRepository(Planning::class)->getPlanningByMissionIdAndStatut(intval($value['id']), "Pourvue");
            $planningsAutre = $manager->getRepository(Planning::class)->getPlanningByMissionIdAndStatutNonPourvueORAPourvoir(intval($value['id']));
            $planningsGroupByDate = $manager->getRepository(Planning::class)->getGroupByDatePlanningByMissionById(intval($missions[$key]['id']));
            foreach ($plannings as $keyP => $valuep) {
                $plannings[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            foreach ($planningsAP as $keyP => $valuep) {
                $planningsAP[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            foreach ($planningsP as $keyP => $valuep) {
                $planningsP[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            foreach ($planningsAutre as $keyP => $valuep) {
                $planningsAutre[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            $missions[$key]['plannings'] = $plannings;
            $missions[$key]['dateGroup'] = $planningsGroupByDate;
            $missions[$key]['planningsAP'] = $planningsAP;
            $missions[$key]['planningsP'] = $planningsP;
            $missions[$key]['planningsAutre'] = $planningsAutre;
        }
        // ======================================== fin de la recuperation des mission interin du client ==========================================
        // recuperation de la liste des offres de mission suivant lid du client
        $offres = $manager->getRepository(Mission::class)->getOffresMissionForClient($idClient);
        // ============================== fin de la recuperation de la liste des offres de missions ===============================================
        $data['offres'] = $offres;
        $data['missions'] = $missions;
        return new JsonResponse($data);
    }

    /**
     * permet deffectuer divers modification a savoir ajout modification suppression
     * @Route(path="api/updateMissionAndPlanning/{idClient}/{idMission}",name="update_missions_and_planning_by_id")
     */
    public function updateMissionAndPlanningction($idClient, $idMission, Request $request, EntityManagerInterface $manager)
    {
        try {
            $missions = '';
            if($this->eventHistory->getSecurity()->getUser()){
                $roles = $this->eventHistory->getRoles($this->eventHistory->getSecurity()->getUser()->getRoles());
                $nomComplet = $this->eventHistory->getSecurity()->getUser()->getNom() . ' ' . $this->eventHistory->getSecurity()->getUser()->getPrenom();
                $idUser = $this->eventHistory->getSecurity()->getUser()->getId();
                $missions = [];
                $data = json_decode($request->getContent());
                $mission = $manager->getRepository(Mission::class)->find($idMission);
                if ($mission instanceof Mission) {
                    $etablissement = $mission->getFicheSpecialite()->getClient()->getNomEtablissement();
                    $specialite = strtolower($mission->getFicheSpecialite()->getQualification()->getValues()[0]->getSpecialite()->getNomSpecialite());
                    $annee = $mission->getAnnee();
                    $planningAdd = $data->planningAdd;
                    $planningIdRemove = $data->planningIdRemove;
                    $planningUpdate = $data->planningUpdate;
                    $groupeDateDeleted = $data->groupeDateDeleted;
                    // SUPPRESSION DUN ENSEMBLE DE DATES
                    if (sizeof($groupeDateDeleted) > 0) {
                        foreach ($groupeDateDeleted as $item) {
                            $supPlannings = $manager->getRepository(Planning::class)->getDatePlanningByMissionIdAndGroupeDate($idMission, $item);
                            foreach ($supPlannings as $sup) {
                                $supPlan = $manager->getRepository(Planning::class)->find($sup['id']);
                                $date = $supPlan->getDate()->format('d-m-Y');
                                $statut = $supPlan->getTypeDeGarde()->getTypeVacation()->getDesignation();
                                $historyItem = [
                                    'cible' => $mission->getReference(),
                                    'element' => "Suppression du planning $date, $statut, CH $etablissement ($specialite $annee)",
                                    'cible_id' => $supPlan->getId(),
                                    'table' => 'mission',
                                ];
                                $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                                $manager->remove($supPlan);
                            }
                        }
                    }
                    // ajout de nouveau planning
                    foreach ($planningAdd as $key => $value) {
                        if ($value) {
                            $typeDeGarde = $manager->getRepository(TypeDeGarde::class)->find($value->typeDeGarde->id);
                            $statut = $manager->getRepository(StatutPlanning::class)->find($value->statut->id);
                            $planningAdd[$key]->mission->id = intval($idMission);
                            $planning = new Planning();
                            $planning->createdAt();
//            $planning->setDate((new DateTime($value->date))->modify('+1 day'));
                            $planning->setDate((new DateTime($value->date)));
                            $planning->setTarif($value->tarif);
                            $planning->setSalaireBrute($value->salaireBrute);
                            $planning->setSalaireNet($value->salaireNet);
                            $planning->setQualifications($value->qualifications);
                            $planning->setMajorer(0);
                            $planning->setMission($mission);
                            $planning->setTypeDeGarde($typeDeGarde);
                            $planning->setStatut($statut);
                            $manager->persist($planning);
                            // creation de lhistoire
                            $date = $planning->getDate()->format('d-m-Y');
                            $statut_ = $planning->getTypeDeGarde()->getTypeVacation()->getDesignation();
                            $historyItem = [
                                'cible' => $mission->getReference(),
                                'element' => "Ajout de planning $date, $statut_, CH $etablissement ($specialite $annee)",
                                'cible_id' => 0,
                                'table' => 'mission',
                            ];
                            $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                        }
                    }
                    // modification de planning
                    foreach ($planningUpdate as $key => $value) {
                        $planningUpdated = $manager->getRepository(Planning::class)->find($value->id);
                        $typeDeGarde = $manager->getRepository(TypeDeGarde::class)->find($value->typeDeGarde->id);
                        $statut = $manager->getRepository(StatutPlanning::class)->find($value->statut->id);
                        $tarifContent= "";
                        if($value->tarif != $planningUpdated->getTarif()){
                            $tarifContent = ', tarif '.$planningUpdated->getTarif().'->'.$value->tarif.',';
                        }
                        $planningUpdated->updateAt();
                        $planningUpdated->setTarif($value->tarif);
                        $planningUpdated->setSalaireBrute($value->salaireBrute);
                        $planningUpdated->setSalaireNet($value->salaireNet);
//            $planningUpdated->setQualifications($value->qualifications);
                        $planningUpdated->setMission($mission);
                        $planningUpdated->setTypeDeGarde($typeDeGarde);
                        $planningUpdated->setStatut($statut);
                        // creation de lhistoire
                        $date = $planningUpdated->getDate()->format('d-m-Y');
                        $statut_ = $planningUpdated->getTypeDeGarde()->getTypeVacation()->getDesignation();
                        $historyItem = [
                            'cible' => $mission->getReference(),
                            'element' => "$date $tarifContent $statut_, CH $etablissement ($specialite $annee)",
                            'cible_id' => 0,
                            'table' => 'mission',
                        ];
                        $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                    }
                    // suppresion de planning
                    foreach ($planningIdRemove as $item) {
                        if ($item) {
                            $planningRemoved = $manager->getRepository(Planning::class)->find(intval($item));
                            if ($planningRemoved instanceof Planning) {
                                // creation de lhistoire
                                $date = $planningRemoved->getDate()->format('d-m-Y');
                                $statut_ = $planningRemoved->getTypeDeGarde()->getTypeVacation()->getDesignation();
                                $historyItem = [
                                    'cible' => $mission->getReference(),
                                    'element' => "Suppression du planning $date, $statut_, CH $etablissement ($specialite $annee)",
                                    'cible_id' => 0,
                                    'table' => 'mission',
                                ];
                                $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                                $manager->remove($planningRemoved);
                            }
                        }
                    }
                    $manager->flush();
                    $missions = $this->getMissionByIdAction($idClient, $mission->getReference(), $manager, true);
                }
            }
            return new JsonResponse($missions);
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * recuperation des mission suivant les id du client et de la mission
     * @Route(path="api/getMissionByIdsAndReference/{idClient}/{reference}",name="get_mission_by_id_and_reference")
     */
    public function getMissionByIdAction($idClient, $reference, EntityManagerInterface $manager, $test = false)
    {
        $refExplode = explode("-", $reference);
        // on recherche les mission par interim
        if ($refExplode[0] === 'RMS') {
            $missions = $manager->getRepository(Mission::class)->getMissionByIdClientAndReference($reference, $idClient);
            $missions = $missions[0];
            $plannings = $manager->getRepository(Planning::class)->getPlanningByMissionId(intval($missions['id']));
            $planningsAP = $manager->getRepository(Planning::class)->getPlanningByMissionIdAndStatut(intval($missions['id']), "A Pourvoir");
            $planningsP = $manager->getRepository(Planning::class)->getPlanningByMissionIdAndStatut(intval($missions['id']), "Pourvue");
            $planningsAutre = $manager->getRepository(Planning::class)->getPlanningByMissionIdAndStatutNonPourvueORAPourvoir(intval($missions['id']));
            $planningsGroupByDate = $manager->getRepository(Planning::class)->getGroupByDatePlanningByMissionById(intval($missions['id']));
            foreach ($plannings as $keyP => $valuep) {
                $plannings[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            foreach ($planningsAP as $keyP => $valuep) {
                $planningsAP[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            foreach ($planningsP as $keyP => $valuep) {
                $planningsP[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            foreach ($planningsAutre as $keyP => $valuep) {
                $planningsAutre[$keyP]['qualifications'] = unserialize($valuep['qualifications']);
            }
            $missions['plannings'] = $plannings;
            $missions['planningsAP'] = $planningsAP;
            $missions['planningsP'] = $planningsP;
            $missions['planningsAutre'] = $planningsAutre;
            if (sizeof($missions) > 0) {
                $missions['dateGroup'] = $planningsGroupByDate;
            }
            if ($test) {
                return $missions;
            } else {
                return new JsonResponse($missions);
            }
        } else {
            $missions = $manager->getRepository(Mission::class)->getOffreByIdClientAndReference($reference, $idClient);
            $missions = $missions[0];
            if ($test) {
                return $missions;
            } else {
                return new JsonResponse($missions);
            }
        }
    }
}
