<?php


namespace App\Controller;


use App\Entity\Candidature;
use App\Entity\Dossier;
use App\Entity\Interim;
use App\Entity\Mission;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function GuzzleHttp\json_decode;

class UpdateOneAttributController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route(path="api/update-boolean",name="update_password_by_admin", methods={"POST"})
     */

    public  function update (Request  $request)
    {

            $erreur = null;
            $succes = null;
            $response = null;


            try {
                $contentRequest = json_decode($request->getContent());



              $model = $this->getEntityClassName($contentRequest->entity);

              if ($model){
                  foreach ($contentRequest->ids as $id)
                  {


                      $entity = $this->em->getRepository($model)->find($id);

                      if ($entity)
                      {
                        $entity->UpdateItem($contentRequest->attribut,$contentRequest->value);
                        $this->em->persist($entity);
                      }
                  }
                  $this->em->flush();
              }
              else {
                   $erreur = "model non trouvé";
              }

            }
            catch (\Exception $exception)
            {
                $erreur = $exception->getMessage();
            }

            return  new JsonResponse(
                [
                  "erreur"=>$erreur
                ]
            );


    }


   public function  getEntityClassName($data){
     $model = null;
       switch ($data) {
           case "mission":
               $model = Mission::class;
               break;
           case "interim":
               $model = Interim::class;
               break;
           case "candidature":
               $model = Candidature::class;
               break;
           case "dossier":
               $model = Dossier::class;
               break;
           case "user":
               $model = User::class;
               break;
    }

    return $model;
}
}