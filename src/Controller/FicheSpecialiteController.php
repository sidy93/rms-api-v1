<?php

namespace App\Controller;

use App\Entity\FicheSpecialite;
use App\Entity\Qualification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class FicheSpecialiteController extends AbstractController
{
    public function __invoke(FicheSpecialite $data, Request $request, EntityManagerInterface $em)
    {
        $ids = json_decode($request->getContent())->id;
        $update = json_decode($request->getContent())->update;
        if($update){
            // on vide tout dabord puis on recree
            foreach ($data->getQualification()->getValues() as $value){
                $qualification = $em->getRepository(Qualification::class)->find(intval($value->getId()));
                $data->removeQualification($qualification);
            }
            // on recree
            // on cree directement
            foreach ($ids as $value){
                $qualificationtpm = $em->getRepository(Qualification::class)->find(intval($value));
                $data->addQualification($qualificationtpm);
            }
        }else{
            // on cree directement
            foreach ($ids as $value){
                $qualification = $em->getRepository(Qualification::class)->find(intval($value));
                $data->addQualification($qualification);
            }
        }
        $em->flush();
        return new JsonResponse("ok");
    }
}
