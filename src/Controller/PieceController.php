<?php


namespace App\Controller;
use App\Entity\Interim;
use App\Entity\Justificatif;
use App\Entity\Piece;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PieceController extends AbstractController
{
    private $em;
    private $params;
    public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->em = $entityManager;
    }


    /**
     * @Route("api/pieces", name ="add_or_pieces", methods={"post"})
     */
    public function editAction(Request $request)
    {
        $response = [];
        $data = json_decode($request->getContent());
        foreach ($data as $item){
            $justificatif = $this->em->getRepository(Justificatif::class)->find($data[0]->justificatif);
            if($justificatif){
                $piece = $this->em->getRepository(Piece::class)->findOneBy(['pieceFileName' => $item->pieceFileName, 'type' => $item->type]);
                if(!$piece){
                    $piece = new Piece();
                }
                $piece->setPieceFileName($item->pieceFileName);
                $piece->setJustificatif($justificatif);
                $piece->setType($item->type);
                $this->em->persist($piece);
                $this->em->flush();
            }
        }
        return new JsonResponse($data);
    }

    /**
     * @Route("api/pieces/save-files/{idInterim}", name ="pieces_save_files_by_interim", methods={"post"})
     */
    public function saveFileAction(Request $request, $idInterim)
    {
        $response = [];
        $interim = $this->em->getRepository(Interim::class)->find($idInterim);
        if($interim){
            $host = $request->getSchemeAndHttpHost();
            $folderInterimaires = $this->params->get("folder_files") . "/Dr_" . $interim->getUser()->getNom() . "_" . $interim->getUser()->getId() . "/Justificatifs";
            $files = $request->files;
            foreach ($files as $key => $file) {
                $key = str_replace('_', ' ', $key);
                $response = $key;
                $piece = $this->em->getRepository(Piece::class)->findOneBy(['pieceFileName' => $key]);
                if($piece){
                    $filename = $file->getClientOriginalName();
                    $piece->setFile($host . '/' . $folderInterimaires . '/' . $filename);
                    $file->move($folderInterimaires . '/', $filename);
                    $this->em->persist($piece);
                }
            }
            $this->em->flush();
        }
        return new JsonResponse($response);
    }
}
