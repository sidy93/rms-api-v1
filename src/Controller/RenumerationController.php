<?php


namespace App\Controller;
use App\Entity\Candidature;
use App\Entity\Contrat;
use App\Entity\Justificatif;
use App\Entity\Piece;
use App\Entity\ReleveHeure;
use App\Entity\Renumeration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class RenumerationController extends AbstractController
{
    private $em;
    private $params;
    public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->em = $entityManager;
    }
    public function returnJustificatifByRenmuneration($renumeration)
    {
        $response = [
            'adresseInterim' => '',
            'adresseClient' => '',
            'coef_ik' => 0,
            'trajet' => 0,
            'km' => 0,
            'relance' => 0,
            'pieces' => []
        ];
        $justificatif = $this->em->getRepository(Justificatif::class)->findOneBy(['remuneration' => $renumeration->getId()]);
        if ($justificatif){
            $pieces = $this->em->getRepository(Piece::class)->findBy(['justificatif' => $justificatif->getId()]);

            $response['adresseInterim'] = $justificatif->getAdresseInterim() ? $justificatif->getAdresseInterim() : 'Pas d\'adresse';
            $response['adresseClient'] = $justificatif->getAdresseHopital() ? $justificatif->getAdresseHopital() : 'Pas d\'adresse';
            $response['coef_ik'] = $justificatif->getCoefIK() ? $justificatif->getCoefIK() : 0;
            $response['trajet'] = $justificatif->getNombreTrajet() ? $justificatif->getNombreTrajet() : 0;
            $response['km'] = $justificatif->getNombreKm() ? $justificatif->getNombreKm() : 0;
            $response['relance'] = $justificatif->getNombreRelance() ? $justificatif->getNombreRelance() : 0;
            $response['createAt'] = $justificatif->getCreateAt();
            $response['id'] = $justificatif->getId();
        }
        else {
            $interimAndClient = $this->em->getRepository(Renumeration::class)->findInterimAndClientByIdContrat($renumeration->getContrat()->getIdPld());
            if($interimAndClient){
                $response['adresseInterim'] = $interimAndClient['adresseInterim'] ? $interimAndClient['adresseInterim'] : 'Pas d\'adresse';
                $response['adresseClient'] = $interimAndClient['adresseClient'] ? $interimAndClient['adresseClient'] : 'Pas d\'adresse';
                $response['coef_ik'] = $interimAndClient['coef_ik'] ? $interimAndClient['coef_ik'] : 0;
            }
        }
        return $response;
    }

    public function returnNbreContratByidInterim($idInterim)
    {
        return sizeof($this->em->getRepository(Renumeration::class)->returnNbreContratByidInterim($idInterim));
    }
    public function returnNbreRhByContrat($iRenum)
    {
        $typeRh = ['PRESTATION', 'FRAIS'];
        $response = [];
        $remuneration = $this->em->getRepository(Renumeration::class)->find($iRenum);
        $rhs = $this->em->getRepository(ReleveHeure::class)->findBy(['remuneration' => intval($remuneration->getId())]);
        if($rhs){
            foreach ($rhs as $rh){
                try {
                    $rhPld = (new PldController())->findRhPldAction($rh->getRhPld());
                } catch (\Exception $exception){
                    $rhPld = "Le Relevé d'heures n°" .$rh->getRhPld(). " a été sur Pld.";
                }
                array_push($response, [
                    'id' => $rh->getId(),
                    'pld' => $rhPld,
                    'commentaireInterim' => $rh->getCommentaireInterim(),
                    'commentaireClient' => $rh->getCommentaireClient(),
                    'url' => '',]);
            }
        } else {





        }

        return $response;
    }
    public function returnEtatSignatureContratGlobal($idContrat)
    {
        $response = [
            'cec' => 0,
            'csc' => 0,
            'cei' => 0,
            'csi' => 0,
        ];

        return $response;
    }
    public function returnEtatSignatureContrat($idContrat)
    {
        $response = [
            'cec' => null,
            'csc' => null,
            'cei' => null,
            'csi' => null,
        ];
        $contrat = $this->em->getRepository(Contrat::class)->findOneBy(['idPld' => $idContrat]);
        if($contrat){
            foreach ($contrat->getSignatures()->getValues() as $sellAndSign){
                switch ($sellAndSign->getType()){
                    case 0:
                        // Ch
                        $sellAndSign->getDateEnvoi() ? $response['cec'] = $sellAndSign->getDateEnvoi()->format('d-m-Y') : $response['cec'] = null;
                        $sellAndSign->getDateSignature() ? $response['csc'] = $sellAndSign->getDateSignature()->format('d-m-Y') : $response['csc'] = null;
                        break;
                    case 1:
                        // Interim
                        $sellAndSign->getDateEnvoi() ? $response['cei'] = $sellAndSign->getDateEnvoi()->format('d-m-Y') : $response['cei'] = null;
                        $sellAndSign->getDateSignature() ? $response['csi'] = $sellAndSign->getDateSignature()->format('d-m-Y') : $response['csi'] = null;
                        break;
                }
            }
        }
        return $response;
    }
    public function returnCandidatureByContratErp($idContratERP)
    {
        $response = [];
        $candidatures = $this->em->getRepository(Candidature::class)->findBy(['contratPld' => $idContratERP]);
        if($candidatures){
            $response = $candidatures;
        }
        return $response;
    }
    public function returnSalaireContrat($idInterim)
    {
        $response = [
            'presta' => 0,
            'frais' => 0,
            'tarif' => 0,
            'prestafrais' => 0,
        ];

        return $response;
    }
    /**
 * @Route("api/renumerations", name ="listes_renumerations", methods={"get"})
 */
    public function listeAction()
    {
        $response = [];
        $renumerations = $this->em->getRepository(Renumeration::class)->getAll();
        if(sizeof($renumerations) > 0){
            foreach ($renumerations as $renumeration) {
                $renumeration['contrat'] = $this->returnNbreContratByidInterim($renumeration['idInterim']);
                $renumeration['rh'] = $this->returnNbreRhByContrat($renumeration['id']);
                $renumeration['sellAndSign'] = $this->returnEtatSignatureContratGlobal($renumeration['idInterim']);
                $renumeration['commission'] = $this->returnSalaireContrat($renumeration['idInterim']);
                array_push($response, $renumeration);
            }
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("api/renumerations/show-refractor/{idInterim}", name ="show_refractor_details_renumeration", methods={"get"})
     */
    public function showRefractorAction(Request $request, $idInterim)
    {
        $response = [];
        $details = $this->em->getRepository(Renumeration::class)->returnNbreContratByidInterim($idInterim);
        if($details){
            foreach ($details as $item){
                $item['sellAndSign'] = $this->returnEtatSignatureContrat($item['idContratPld']);
                $item['totalVacation'] = sizeof($this->returnCandidatureByContratErp($item['idContratErp']));
                array_push($response, $item);
            }
        }
        return new JsonResponse($response);
    }
    /**
     * @Route("api/renumerations/contrat-pld", name ="show_remuneration_contrat", methods={"post"})
     */
    public function remunerationByContratAction(Request $request)
    {
        $response = [];
        $idContratPld = json_decode($request->getContent())->idContratPld;
        $contrat = $this->em->getRepository(Contrat::class)->findOneBy(['idPld' => $idContratPld]);
        if($contrat){
            $remuneration = $this->em->getRepository(Renumeration::class)->findOneBy(['contrat' => $contrat->getId()]);
            if($remuneration){
                $planning = [];
                $response = $remuneration->refrator();
//                try {
//                    $contratPld = (new PldController())->findContratPldAction($contrat->getIdPld());
//                } catch (\Exception $exception){
//                    $contratPld = "Le contrat n°" .$contrat->getIdPld(). " a été sur Pld.";
//                }
                $response['rh'] = $this->returnNbreRhByContrat($remuneration->getId());
                $response['justificatif'] = $this->returnJustificatifByRenmuneration($remuneration);
                $response['contratPld'] = [];
                $candidatures = $this->em->getRepository(Candidature::class)->findBy(['contratPld' => $contrat->getId()]);
                foreach ($candidatures as $candidature){

                    array_push($planning, [
                        'date' => ['date' => $candidature->getPlanning()->getDate()->format("Y-m-d"), 'semaine' =>$candidature->getPlanning()->getDate()->format("W")],
                        'vacation' => $candidature->getPlanning()->getTypeDeGarde()->getTypeVacation()->getDesignation(),
                        'abreviationVacation' => $candidature->getPlanning()->getTypeDeGarde()->getTypeVacation()->getAbreviation(),
                        'debut' => $candidature->getPlanning()->getTypeDeGarde()->getHeureDebut(),
                        'fin' => $candidature->getPlanning()->getTypeDeGarde()->getHeureFin(),
                        'net' => $candidature->getPlanning()->getSalaireNet(),
                        'brute' => $candidature->getPlanning()->getSalaireBrute(),
                        'tarif' => $candidature->getPlanning()->getTarif(),
                    ]);
                }
                $response['planning'] =  $planning;
                $response['idContratErp'] =  $contrat->getId();
//                $response['planningByWeek'] =  $planning;
                $interimAndClient = $this->em->getRepository(Renumeration::class)->findInterimAndClientByIdContrat($contrat->getIdPld());
                if($interimAndClient){
                    $response['interimAndClient'] = $interimAndClient;
                }
            }
        }
        return new JsonResponse($response);
    }


}
