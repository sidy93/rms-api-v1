<?php


namespace App\Controller;


use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class ClientHebergementController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ClientHebergementController constructor.
     * @param EntityManagerInterface $entityManager
     */

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param Client $data
     *
     */
    public function __invoke(Client $data)
    {
        $response = [];
          if ($data->getFicheClient())
        {
            if ($data->getFicheClient()->getHebergement()->count() > 0)
            {
                $response = $data->getFicheClient()->getHebergement()->getValues();
            }
        }



          return  $response;


    }



}