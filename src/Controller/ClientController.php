<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Mission;
use App\Entity\Candidature;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param Request $request
     */
    public function __invoke(Request $request, $data)
    {
        $missionForClient = [];
        if ($request->isMethod("get")) {
            $missionForClient = $this->em->getRepository(Mission::class)->getMissionsForClient($data->getId());
            foreach ($missionForClient as $key => $mission) {
                // nombre de candidatures suivant le statut de candidatures
                $nombreCandidatures = $this->em->getRepository(Candidature::class)->getNombreCandidaturesByStatutOnMission(intval($mission['id']));
                $missionForClient[$key]['stats'] = $nombreCandidatures;
                // recuperation des interimaire ayant postuler sur cette mission
                $interims = $this->em->getRepository(Candidature::class)->getCandidatsOnMission(intval($mission['id']));
                foreach ($interims as $keyItem => $item) {
                    $candidatures = $this->em->getRepository(Candidature::class)->getCandidaturesInterimOnMissionByIdMissionAndIdInterim(intval($mission['id']), $item['interimId']);
                    $interims[$keyItem]['candidatures'] = $candidatures;
                }
                $missionForClient[$key]['interims'] = $interims;
            }
            return new JsonResponse($missionForClient);
        }
    }



}
