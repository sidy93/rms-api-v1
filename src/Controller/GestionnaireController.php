<?php


namespace App\Controller;

use App\Entity\Gestionnaire;
use App\Entity\Tache;
use App\Entity\TacheGestionnaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class GestionnaireController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * GestionnaireController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param Gestionnaire $gestionnaire
     */
    public function  __invoke(Gestionnaire $gestionnaire,Request $request)
    {
        $response = 'erreur';
        if ($request->isMethod("DELETE")) {
            $response = [];
            $idTacheGestionnaire = intval($request->query->all()['idTache']);
            $tacheGestionnaire = $this->em->getRepository(TacheGestionnaire::class)->find($idTacheGestionnaire);
            if($tacheGestionnaire){
                $this->em->remove($tacheGestionnaire);
                $this->em->persist($gestionnaire);
                $this->em->flush();
                $response = 'ok';
            }
            return new JsonResponse($response);
        }
    }
}
