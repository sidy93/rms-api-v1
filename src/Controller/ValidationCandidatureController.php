<?php


namespace App\Controller;


use App\Entity\Candidature;
use App\Entity\MissionInterim;
use App\Entity\Planning;
use App\Entity\StatutCandidature;
use App\Entity\StatutPlanning;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function GuzzleHttp\Psr7\str;

class ValidationCandidatureController
{
    public $if_erreur = '';
    public $test_vacation = 0;
    // Validation
    public $pourvu = 'Pourvue';
    public $enAttente = 'En Attente';
    public $perdue = 'Perdue';
    public $apourvoir = 'A Pourvoir';
    public $nonRetenue = 'Non Retenue';
    public $validee = 'Validee';
    public $annuleeCh = 'Annulee CH';
    public $annuleeMedAvant = 'AnnuleeMedAvantValidation';
    public $annuleeMedApres = 'AnnuleeMedApresValidation';
    public $nonEffectuee = 'Non Effectuee';
    public $annuler_i = 'Annulee_I';
    public $annuler_C_apresCandidature = 'AnnuleeChApresValidation';
    public $annuler_C_avantCandidature = 'AnnuleeChAvantValidation';
    public $msg = [];
    public $planningsTraiter = [];
    public $candidaturesTraiter = [];
    public $dataRecu = [];
    public $idCandidatureChoisie = [];
    public $mission = [];
    public $array_nuit = [
        'nuit', 'nuit jt', 'nuit ht', 'astreinte',
        'astreinte jt', 'astreinte ht'
    ];
    // Nuit et Astreinte
    public $array_deminuit = [
        'demi-nuit', 'demi-nuit jt', 'demi-nuit ht', 'astreinte',
        'astreinte jt', 'astreinte ht'
    ];
    // Nuit et Astreinte
    public $array_journee = [
        'journée', 'journée jt',
        'journée ht', 'journée + demi-nuit ht',
        'journée + astreinte', 'journée + astreinte jt', 'journée + astreinte ht'
    ];
    public $array_demijournee = [
        'demi journée',
        'demi journée jt', 'demi journée ht',
        'journée + demi-nuit', 'journée + demi-nuit jt'
    ];
    public $array_h24 = [
        '24 h', 'h24', '24 h jt', '24 h ht'
    ];
    public $array_astreinte = [
        'journée + astreinte', 'journée + astreinte jt', 'journée + astreinte ht'
    ];
    /**
     * @var EntityManagerInterface
     */
    private $em;
    private $pourvuePlanning;
    private $aPourvoirPlanning;

    private $valideeCandidature;
    private $enAttenteCandidature;
    private $nonRetenueCandidature;

    private $annulerMedAvantValidation;
    private $annulerMedApresValidation;
    private $annulerChAvantValidation;
    private $annulerChApresValidation;
    private $nonEffectueCandidature;
    private $perduePlanning;
    private $perdueCandidature;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;

        $this->pourvuePlanning = $this->em->getRepository(StatutPlanning::class)->findBy(
            array('designation' => $this->pourvu)
        );
        $this->pourvuePlanning = $this->pourvuePlanning ? $this->pourvuePlanning[0] : null;
        $this->aPourvoirPlanning = $this->em->getRepository(StatutPlanning::class)->findBy(
            array('designation' => $this->apourvoir)
        );
        $this->aPourvoirPlanning = $this->aPourvoirPlanning ? $this->aPourvoirPlanning[0] : null;
        $this->enAttenteCandidature = $this->em->getRepository(StatutCandidature::class)->findBy(
            array('designation' => $this->enAttente)
        );
        $this->enAttenteCandidature = $this->enAttenteCandidature ? $this->enAttenteCandidature[0] : null;
        $this->valideeCandidature = $this->em->getRepository(StatutCandidature::class)->findBy(
            array('designation' => $this->validee)
        );
        $this->valideeCandidature = $this->valideeCandidature ? $this->valideeCandidature[0] : null;
        $this->nonRetenueCandidature = $this->em->getRepository(StatutCandidature::class)->findBy(
            array('designation' => $this->nonRetenue)
        );
        $this->nonRetenueCandidature = $this->nonRetenueCandidature ? $this->nonRetenueCandidature[0] : null;
        $this->annulerMedAvantValidation = $this->em->getRepository(StatutCandidature::class)->findBy(
            array('designation' => $this->annulerMedAvantValidation)
        );
        $this->annulerMedAvantValidation = $this->annulerMedAvantValidation ? $this->annulerMedAvantValidation[0] : null;
        $this->annulerMedApresValidation = $this->em->getRepository(StatutCandidature::class)->findBy(
            array('designation' => $this->annulerMedApresValidation)
        );
        $this->annulerMedApresValidation = $this->annulerMedApresValidation ? $this->annulerMedApresValidation[0] : null;
        $this->perdueCandidature = $this->em->getRepository(StatutCandidature::class)->findBy(
            array('designation' => $this->perdue)
        );
        $this->perdueCandidature = $this->perdueCandidature ? $this->perdueCandidature[0] : null;
        $this->perduePlanning = $this->em->getRepository(StatutPlanning::class)->findBy(
            array('designation' => $this->perdue)
        );
        $this->perduePlanning = $this->perduePlanning ? $this->perduePlanning[0] : null;
        $this->annulerChAvantValidation = $this->em->getRepository(StatutPlanning::class)->findBy(
            array('designation' => $this->annuler_C_avantCandidature)
        );
        $this->annulerChAvantValidation = $this->annulerChAvantValidation ? $this->annulerChAvantValidation[0] : null;
        $this->annulerChApresValidation = $this->em->getRepository(StatutPlanning::class)->findBy(
            array('designation' => $this->annulerMedApresValidation)
        );
        $this->annulerChApresValidation = $this->annulerChApresValidation ? $this->annulerChApresValidation[0] : null;
        $this->nonEffectueCandidature = $this->em->getRepository(StatutCandidature::class)->findBy(
            array('designation' => $this->nonEffectuee)
        );
        $this->nonEffectueCandidature = $this->nonEffectueCandidature ? $this->nonEffectueCandidature[0] : null;
    }


    /**
     * recuperation des historiques
     * @Route("api/validationCandidature", name = "Validation_des_candidatures")
     */
    public function getValidationCandidatureAction(Request $request)
    {
        $content = [];
        $data = json_decode($request->getContent());

        try {
            foreach ($data as $item) {
                if ($item) {
                    $this->verificationBeforeValidation($item, $data);
                    if ($this->if_erreur) {
                        $erreur = $this->if_erreur;
                        break 1;
                    } else {
                        // si seulement toutes les conditions sont remplies
                        if (strtolower($item->designation) == strtolower($this->validee)) {
                            $rep = $this->casValidation($item);
                            if ($rep != null) {
                                $erreur = $rep;
                                break 1;
                            }
                        } elseif (strtolower($item->designation) == strtolower($this->nonRetenue)) {
                            $rep = $this->casNonRetenu($item);
                            if ($rep != null) {
                                $erreur = $rep;
                                break 1;
                            }
                        } elseif (strtolower($item->designation) == strtolower($this->annuleeMedAvant)) {
                            $rep = $this->casAnnuler($item);
                            if ($rep != null) {
                                $erreur = $rep;
                                break 1;
                            }

                        } elseif (strtolower($item->designation) == strtolower($this->nonEffectuee)) {
                            $rep = $this->casNonEffectuer($item);
                            if ($rep != null) {
                                $erreur = $rep;
                                break;
                            }
                        } elseif (strtolower($item->designation) == strtolower($this->enAttente)) {
                            $rep = $this->casEnAttente($item);
                            if ($rep != null) {
                                $erreur = $rep;
                                break;
                            }
                        }
                    }
                }
            }
            $content['data']['candidatures'] = $this->candidaturesTraiter;
            $content['data']['items'] = array_merge($data);
            $content['data']['plannings'] = $this->planningsTraiter;
            if ($this->if_erreur == "") {
                $missionInterim = $this->em->getRepository(MissionInterim::class)->find(intval($data[0]->missionMedecinId));
                $missionInterim->setCommentaires($data[0]->comments);
                $this->em->persist($missionInterim);
                $content['data']['success'] = "Validation réussie";
                $content['data']['erreur'] = "";
            } else {
                $content['data']['success'] = "";
                $content['data']['erreur'] = $this->if_erreur;
            }
//            dd($content);
            $this->em->flush();
        } catch (Exception $exception) {
         //   dd($exception);
            $content['data']['erreur'] = $exception->getMessage();
        }
        return new JsonResponse($content);
    }

    // Verification avant la validation
    function verificationBeforeValidation($element_choisi, $liste_des_items)
    {
        $interim_id = intval($element_choisi->interimId);
        $candidature_id = intval($element_choisi->candidatureId);
        $current_date = new DateTime($element_choisi->dateNonFormat);
        $date_moin_un = (new DateTime($element_choisi->dateNonFormat))->modify('-1 day');
        $date_plus_un = (new DateTime($element_choisi->dateNonFormat))->modify('+1 day');
        if (strtolower($element_choisi->designation) == strtolower($this->validee)) {
            // JOURNEE et Astreinte
            if (in_array(mb_strtolower($element_choisi->garde, 'UTF-8'), array_merge($this->array_journee))) {
                // verifier si le jour j il na pas une validation sur la plateforme "J J"
                $array_garde = array_merge($this->array_journee, $this->array_demijournee, $this->array_h24);
                $valider_pour_interim_le_mm_jour = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $current_date, $array_garde);
                // verifier si le jour j - 1 il na pas une validation sur la plateforme "J -1"
                $array_garde_moins = array_merge($this->array_h24, $this->array_nuit);
                $validate_date_moins_un_for_interim = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $date_moin_un, $array_garde_moins);
                if (sizeof($valider_pour_interim_le_mm_jour) > 0) {
                    // on verifie si linterimaire na pas une validation le meme jour en journee, demi journee ou h24
                    $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $valider_pour_interim_le_mm_jour[0]['id_candidature']);
                    if (!$continue) {
                        $this->if_erreur = 'l\'interimaire a une validation le même jour au CH de :' . $valider_pour_interim_le_mm_jour[0]["Nom_E"];
                    }
                }
                //verifie si linterimaire na pas recu une validation  la veille en nuit ou h24
                if (sizeof($validate_date_moins_un_for_interim) > 0) {
                    $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $validate_date_moins_un_for_interim[0]['id_candidature']);
                    if (!$continue) {
                        $this->if_erreur = 'l\'interimaire a une validation la veille en nuit ou H24 au  CH :' . $validate_date_moins_un_for_interim[0]["Nom_E"];
                    }
                }
            } else
                // DEMI JOURNEE
                if (in_array(mb_strtolower($element_choisi->garde, 'UTF-8'), array_merge($this->array_demijournee))) {
                    //  j j
                    $array_garde = array_merge($this->array_journee, $this->array_demijournee, $this->array_h24);
                    $valider_pour_interim_le_mm_jour = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $current_date, $array_garde);
                    // J - 1
                    $array_garde_moins = array_merge($this->array_h24, $this->array_nuit);
                    $validate_date_moins_un_for_interim = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $date_moin_un, $array_garde_moins);
                    if (sizeof($valider_pour_interim_le_mm_jour) > 0) {
                        // on verifie si linterimaire na pas une validation le meme jour en journee, demi journee ou h24
                        $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $valider_pour_interim_le_mm_jour[0]['id_candidature']);
                        if (!$continue) {
                            $this->if_erreur = 'l\'interimaire a une validation le même jour au CH de :' . $valider_pour_interim_le_mm_jour[0]["Nom_E"];
                        }
                    }
                    //verifie si linterimaire na pas recu une validation  la veille en nuit ou h24
                    if (sizeof($validate_date_moins_un_for_interim) > 0) {
                        $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $validate_date_moins_un_for_interim[0]['id_candidature']);
                        if (!$continue) {
                            $this->if_erreur = 'l\'interimaire a une validation la veille en nuit ou H24 au  CH :' . $validate_date_moins_un_for_interim[0]["Nom_E"];
                        }
                    }
                } else
                    // NUIT
                    if (in_array(mb_strtolower($element_choisi->garde, 'UTF-8'), array_merge($this->array_nuit))) {
                        // verifie si linterimaire na pas deja une validation le meme jour en nuit
                        $array_garde = array_merge($this->array_nuit, $this->array_h24, $this->array_deminuit);
                        $valider_pour_interim_le_mm_jour = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $current_date, $array_garde);
                        // J + 1
                        $array_garde_plus = array_merge($this->array_journee, $this->array_h24, $this->array_demijournee);
                        $validate_date_plus_un_for_interim = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $date_plus_un, $array_garde_plus);

                        if (sizeof($valider_pour_interim_le_mm_jour) > 0) {
                            // on verifie si linterimaire na pas une validation le meme jour en journee, demi journee ou h24
                            $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $valider_pour_interim_le_mm_jour[0]['id_candidature']);
                            if (!$continue) {
                                $this->if_erreur = 'l\'interimaire a une validation le même jour au CH de :' . $valider_pour_interim_le_mm_jour[0]["Nom_E"];
                            }
                        }
                        //verifie si linterimaire na pas recu une validation  le lendemein en journee ou h24
                        if (sizeof($validate_date_plus_un_for_interim) > 0) {
                            $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $validate_date_plus_un_for_interim[0]['id_candidature']);
                            if (!$continue) {
                                $this->if_erreur = 'l\'interimaire a une validation le lendemain en journee ou H24 au  CH :' . $validate_date_plus_un_for_interim[0]["Nom_E"];
                            }
                        }
                    } else
                        // DEMI NUIT
                        if (in_array(mb_strtolower($element_choisi->garde, 'UTF-8'), array_merge($this->array_deminuit))) {
                            // JJ
                            // verifie si linterimaire na pas deja une validation le meme jour en nuit ou demi nuit
                            $array_garde = array_merge($this->array_nuit, $this->array_h24, $this->array_deminuit);
                            $valider_pour_interim_le_mm_jour = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $current_date, $array_garde);
                            if (sizeof($valider_pour_interim_le_mm_jour) > 0) {
                                $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $valider_pour_interim_le_mm_jour[0]['id_candidature']);
                                if (!$continue) {
                                    // on verifie si linterimaire na pas une validation le meme jour en journee, demi journee ou h24
                                    $this->if_erreur = 'l\'interimaire a une validation le même jour au CH de :' . $valider_pour_interim_le_mm_jour[0]["Nom_E"];
                                }
                            }
                        } else
                            // 24 H
                            if (in_array(mb_strtolower($element_choisi->garde, 'UTF-8'), array_merge($this->array_h24))) {
                                // verifie si linterimaire na pas deja une validation le meme jour
                                $array_garde_jj = array_merge($this->array_nuit, $this->array_deminuit, $this->array_h24, $this->array_demijournee, $this->array_journee);
                                $valider_pour_interim_le_mm_jour = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $current_date, $array_garde_jj);
                                // verifie si linterimaire a une validation sur J + 1
                                $array_garde_plus = array_merge($this->array_journee, $this->array_demijournee, $this->array_h24);
                                $validate_date_plus_un_for_interim = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $date_plus_un, $array_garde_plus);
                                // verifie si linterimaire a une validation sur J - 1
                                $array_garde_moins = array_merge($this->array_nuit, $this->array_h24);
                                $validate_moins_un_for_interim = $this->onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $date_moin_un, $array_garde_moins);
                                // j j
                                if (sizeof($valider_pour_interim_le_mm_jour) > 0) {
                                    // on verifie si linterimaire na pas une validation le meme jour en journee, demi journee ou h24
                                    $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $valider_pour_interim_le_mm_jour[0]['id_candidature']);
                                    if (!$continue) {
                                        $this->if_erreur = 'l\'interimaire a une validation le même jour au CH de :' . $valider_pour_interim_le_mm_jour[0]["Nom_E"];
                                    }
                                }
                                // J + 1
                                if (sizeof($validate_date_plus_un_for_interim) > 0) {
                                    $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $validate_date_plus_un_for_interim[0]['id_candidature']);
                                    if (!$continue) {
                                        $this->if_erreur = 'l\'interimaire a une validation le lendemain en journee ou H24 au  CH :' . $validate_date_plus_un_for_interim[0]["Nom_E"];
                                    }
                                }
                                // J - 1
                                if (sizeof($validate_moins_un_for_interim) > 0) {
                                    $continue = $this->VerfieCandidaturePasEvoluer($liste_des_items, $validate_moins_un_for_interim[0]['id_candidature']);
                                    if (!$continue) {
                                        $this->if_erreur = 'l\'interimaire a une validation la veille en nuit ou H24 au  CH :' . $validate_moins_un_for_interim[0]["Nom_E"];
                                    }
                                }
                            }
        }
    }

    // verifier si la candidature na pas evoluer
    public function onGetCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $date, $groupeStatuts)
    {
        $tmp = $this->em->getRepository(Candidature::class)
            ->getCandidatureOnDateWithLibelleForInterim($interim_id, $candidature_id, $date, $groupeStatuts);
        return $tmp;
    }

    // verifie si la canddidature na toujours pas evoluer
    // Valider en base mais on veut modifier depuis la vue
    public function VerfieCandidaturePasEvoluer($items_selectionnees, $id_candidature_validee)
    {
        foreach ($items_selectionnees as $item) {
            if ($id_candidature_validee == $item->candidatureId) {
                if (strtolower($item->designation) != strtolower($this->validee)) {
                    // si sa retourne true donc la date validee a evoluer on peut donc validee le couple envoyer
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    //    Validation
    function casValidation($itemDate)
    {
        $erreur = null;
        try {
            $interim_id = $itemDate->interimId;
            $mission_medecin_id = $itemDate->missionMedecinId;
            $candidature = $this->em->getRepository(Candidature::class)->find($itemDate->candidatureId);
            $planning = $this->em->getRepository(Planning::class)->find($itemDate->planningId);
            $LibellletypeDeGarde = $itemDate->garde;
            //============================================================================================
            //recuperation des candidatures semblables de l'interim sur les differents CH a la meme date
            $candidatureSameNonRetenu = $this->em->getRepository(Candidature::class)
                ->getSameCandidaturesforDateAndLibelleGarde($interim_id, $planning->getDate(),
                    $LibellletypeDeGarde, $itemDate->candidatureId);
            //recuperation de l'ensembles des candidatures sur cette date de planning
            $candidaturesOnThisDatePlanning = $this->em->getRepository(Candidature::class)->findBy(
                array('planning' => $itemDate->planningId)
            );
            //============================================================================================
            $candidature->setStatut($this->valideeCandidature);
//                $candidature->setAuteurValidation($itemDate->auteurValidation);
//                $candidature->setUpdateAt(new DateTime('now'));
            $planning->setStatut($this->pourvuePlanning);
            // recuperation des plannings traités
            $this->setPlanningsTraiter($planning->getId(), $planning->getStatut()->getDesignation());
            foreach ($candidatureSameNonRetenu as $item) {
                $this->setNonRetenuSameCandidature($item, $this->nonRetenueCandidature);
            }
            foreach ($candidaturesOnThisDatePlanning as $item) {
                if (intval($itemDate->candidatureId) != $item->getId()) {
                    $this->setNonRetenuSameCandidature($item, $this->nonRetenueCandidature);
                }
            }
            $this->em->persist($candidature, $planning);
        } catch (\Exception $exception) {
            $erreur = $exception->getMessage();
        }
        return $erreur;
    }

    public function setPlanningsTraiter($idPlan, $statut)
    {
        $tmp = [
            'idPlanning' => $idPlan,
            'statut' => $statut
        ];
        array_push(
            $this->planningsTraiter, $tmp
        );
    }

    public function setNonRetenuSameCandidature($item, $statut)
    {
        if ((strtolower($item->getStatut()->getDesignation()) == strtolower($this->validee)) ||
            (strtolower($item->getStatut()->getDesignation()) == strtolower($this->enAttente))) {
            $item->setStatut($statut);
            // $item->setAuteurValidation($itemDate->auteurValidation);
            // $item->setUpdateAt(new DateTime('now'));
            // on set les candidatures traiter
            $this->setCandidaturesTraiter($item->getId(), $item->getStatut()->getDesignation());
            $this->em->persist($item);
        }
    }

    public function setCandidaturesTraiter($idCandidatures, $statut)
    {
        $this->planningsTraiter[] = [
            'idCandidature' => $idCandidatures,
            'statut' => $statut
        ];
    }

    function casNonRetenu($itemDate)
    {
        $erreur = null;
        try {
            // recuperation de la mission medecin adequate
            $mission_medecin = $this->em->getRepository(MissionInterim::class)->find($itemDate->missionMedecinId);
            $candidature = $this->em->getRepository(Candidature::class)->find($itemDate->candidatureId);
            $planning = $this->em->getRepository(Planning::class)->find($itemDate->planningId);
            if (!isset($erreur)) {
                $candidature->setStatut($this->nonRetenueCandidature);
                if (strtolower($candidature->getStatut()->getDesignation()) == strtolower($this->validee)) {
                    if ($planning->getDate() > new DateTime('now')) {
                        $planning->setStatut($this->aPourvoirPlanning);
                        // on remet les autres candidatures en situation en attention si la date de planning nest pas passe
                        //recuperation de l'ensembles des candidatures sur cette date de planning
                        $candidaturesOnThisDatePlanning = $this->em->getRepository(Candidature::class)->findBy(
                            array('planning' => $itemDate->planningId)
                        );
                        // on remet les candidature sur situation en attente
                        foreach ($candidaturesOnThisDatePlanning as $item) {
                            if ($itemDate->candidatureId != $item->getId()) {
                                $item->$candidature->setStatut($this->enAttenteCandidature);
                                $this->em->persist($item);
                            }
                        }
                        $this->em->persist($planning);
                    }
                }
//                $candidature->setAuteurValidation($itemDate->auteurValidation);
//                $candidature->setUpdateAt(new \DateTime('now'));
                $this->em->persist($candidature);
            }
        } catch (\Exception $exception) {
            $erreur = $exception->getMessage();
        }
        return $erreur;
    }

    function casAnnuler($itemDate)
    {
        $erruer = null;
        try {
            $candidature = $this->em->getRepository(Candidature::class)->find($itemDate->candidatureId);
            $planning = $this->em->getRepository(Planning::class)->find($itemDate->planningId);
            if (!isset($erruer)) {
                if (strtolower($itemDate->designation) == strtolower($this->annuleeMedApres)) {
                    // annulation de linterim apres validation du client
                    $candidature->setStatut($this->annulerMedApresValidation);
                    //$candidature->setAuteurValidation($itemDate->auteurValidation);
                    // $candidature->setStatutCandidature($this->nonRetenue);
                    if ($planning->getDate() > (new DateTime('now'))) {
                        $planning->setStatut($this->aPourvoirPlanning);
                        //recuperation de l'ensembles des candidatures sur cette date de planning
                        $candidaturesOnThisDatePlanning = $this->em->getRepository(Candidature::class)->findBy(
                            array('planning' => $itemDate->planningId)
                        );
                        // on remet les candidature sur situation en attente
                        foreach ($candidaturesOnThisDatePlanning as $item) {
                            if ($itemDate->candidatureId != $item->getId() && ($item->getPlanning()->getDate > new DateTime('now'))) {
                                $item->setStatut($this->enAttenteCandidature);
                                $this->em->persist($item);
                            }
                        }
                    } else {
                        $planning->setStatut($this->perduePlanning);
                    }
                    $this->em->persist($candidature, $planning);
                } else if (strtolower($itemDate->designation) == strtolower($this->annuleeMedAvant)) {
                    // annulation de linterim apres validation du client
                    $candidature->setStatut($this->annulerMedApresValidation);
                    $this->em->persist($candidature, $planning);
                } else if (strtolower($itemDate->designation) == strtolower($this->annuler_C_avantCandidature)) {
                    // annulation de linterim apres validation du client
                    $candidature->setStatut($this->annulerMedApresValidation);
                    //$candidature->setAuteurValidation($itemDate->auteurValidation);
                    // $candidature->setStatutCandidature($this->nonRetenue);
                    if ($planning->getDate() > (new DateTime('now'))) {
                        $planning->setStatut($this->aPourvoirPlanning);
                        //recuperation de l'ensembles des candidatures sur cette date de planning
                        $candidaturesOnThisDatePlanning = $this->em->getRepository(Candidature::class)->findBy(
                            array('planning' => $itemDate->planningId)
                        );
                        // on remet les candidature sur situation en attente
                        foreach ($candidaturesOnThisDatePlanning as $item) {
                            if ($itemDate->candidatureId != $item->getId() && ($item->getPlanning()->getDate > new DateTime('now'))) {
                                $item->setStatut($this->enAttenteCandidature);
                                $this->em->persist($item);
                            }
                        }
                    } else {
                        $planning->setStatut($this->perduePlanning);
                    }
                    $this->em->persist($candidature, $planning);
                }
            }
        } catch (\Exception $exception) {
            $erruer = Utiles::messageErrorServer();
        }

        return $erruer;


    }

    function casNonEffectuer($itemDate)
    {
        $erreur = null;
        try {
            $candidature = $this->em->getRepository(Candidature::class)->find($itemDate->candidatureId);
            $planning = $this->em->getRepository(Planning::class)->find($itemDate->planningId);
            if (!isset($erreur)) {
                if (strtolower($itemDate->designation) == strtolower($this->nonEffectuee)) {
//                     if ($itemDate->statutCandidature == $this->validee) {
                    $candidature->setStatut($this->nonEffectueCandidature);
//                    $candidature->setAuteurValidation($itemDate->auteurValidation);
//                    $candidature->setUpdateAt(new DateTime('now'));
                    $this->em->persist($candidature);
                }
            }
        } catch (\Exception $exception) {
            $erreur = $exception->getMessage();
        }

        return $erreur;

    }

    function casEnAttente($itemDate)
    {
        $erreur = null;
        try {
            // recuperation de la mission medecin adequate
            $candidature = $this->em->getRepository(Candidature::class)->find($itemDate->candidatureId);
            $planning = $this->em->getRepository(Planning::class)->find($itemDate->planningId);
            if (!isset($erreur)) {
                if (strtolower($itemDate->designation) == strtolower($this->enAttente)) {
                    $candidature->setStatut($this->enAttenteCandidature);
                    if (strtolower($planning->getStatut()->getDesignation()) === strtolower($this->pourvu))
                        $planning->setStatut($this->aPourvoirPlanning);
                    // $candidature->setAuteurValidation($itemDate->auteurValidation);
                    // $candidature->setUpdateAt(new \DateTime('now'));
                    $this->em->persist($candidature);
                }
            }
        } catch (\Exception $exception) {
            $erreur = $exception->getMessage();
        }
        return $erreur;
    }

    public  function  sendEmail($items)
    {

        try {

            if ($contentRequest->mail && $newCandidature )
            {

                $template =  "emails/erp/interim/confirmation_candidature.html.twig";
                $this->mail->sendNotificationMail(
                    $interim->getUser()->getEmail(),
                    $interim->getUser()->getNom(),
                    false,
                    null,
                    $template,
                    $arrayMailValue,
                    "Nouvelle candidature",
                    "new candidature");
            }
        }catch (\Exception $exception)
        {
            $erreur = $exception->getMessage();
        }
    }

}
