<?php

namespace App\Controller;

use App\Entity\Qualification;
use App\Entity\Specialite;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Json;

class QualificationController extends AbstractController
{
    /**
     * invoke il faut specifier id
     * @Route("/api/addQualifications", name="add_qualification")
     */
    public function addQualifications(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $data = (json_decode($request->getContent()))->qualifications;
        // on recupere la liste des element a supprimer
        $removeData = (json_decode($request->getContent()))->removeItems;
        foreach ($data as $item) {
            // si id = 0 on procede a une creation
            if ($item->id == 0) {
                $qualif = new Qualification();
                $specialite = $entityManager->getRepository(Specialite::class)->find(intval($item->specialite));
                $qualif->setSpecialite($specialite);
                $qualif->setQualification($item->qualification);
                $qualif->createdAt();
                $entityManager->persist($qualif);
            } else {
                // si id = 0 on procede a une modification a recuperant la qualification
                $qualif = $entityManager->getRepository(Qualification::class)->find(intval($item->id));
                $qualif->setQualification($item->qualification);
            }
        }
        // on supprime si ya des element a supprimer uniquement lors de la modification
        if(isset($removeData[0])){
            foreach ($removeData as $item){
                $qualif = $entityManager->getRepository(Qualification::class)->find($item);
                $entityManager->remove($qualif);
            }
        }
        $entityManager->flush();
        return new JsonResponse('ok');
    }
}
