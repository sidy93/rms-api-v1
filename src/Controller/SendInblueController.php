<?php

namespace App\Controller;

use App\Model\SenderMail;
use App\Service\SendinblueService;
use App\Service\Utiles;
use GuzzleHttp\Client as GuzzleHttpClient;
use JMS\Serializer\Serializer;
use JMS\SerializerBundle\JMSSerializerBundle;
use SendinBlue\Client\Api\AccountApi;
use SendinBlue\Client\Configuration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;


/**
 * Class SendInblueController
 * @package App\Controller
 * @Route("api/mail")
 */
class SendInblueController extends AbstractController
{
    /**
     * @var SendinblueService
     */
     private  $mailService;



     /**
    /**
     * SendInblueController constructor.
     */
    public function __construct(SendinblueService $mail)
    {
        $this->mailService = $mail;




    }

    /**
     * @Route("/transaction", name="mail_transaction")
     * @param SendinblueService $mail
     */
    public function sendTransacEmail(Request $request ,SendinblueService  $mail)
    {
        $erreur = null;

        try {

            $data = json_decode($request->getContent());

            $sender = Utiles::sender($data->sender->email, $data->sender->name);


            $body = [
                'url' => $this->getParameter("base_url_site"),
                'type' => $data->type,
                'template' => "emails/transaction.html.twig",
                'message'=>$data->message,

            ];
            $tags = "mdd transaction";
            foreach ($data->to as $to) {

                $body["nom"]=$to->name;
                $htmlContent = $this->mailService->htmlContents($body);

                $mail->notificationMail($sender,[$to],$data->subject,$htmlContent,$tags,$data->cc,$data->replyto);
            }


        }
        catch (\Exception $exception)
        {
            $erreur= $exception->getMessage();
        }

      return new JsonResponse(
          [
              "erreur"=>$erreur


          ]
      );

    }


    /**
     * @Route("/fiche-poste", name="mail_fiche_poste")
     * @param SendinblueService $mail
     */
    public function sendDemandeFichePosteEmail(Request $request ,SendinblueService  $mail)
    {
        $erreur = null;

        try {

            $data = json_decode($request->getContent());

            foreach ($data->client as $item)
            {
                $template =  "emails/erp/client/fiche_de_poste.html.twig";

                $mail->sendNotificationMail(
                    $item->email,
                   $item->name,
                    true,
                    null,
                    $template,
                    $data->specialite,
                    "DEMANDE FICHE DE PROFIL DE POSTE",
                    "Demande fiche Poste");
            }

        }
        catch (\Exception $exception)
        {
            $erreur= $exception->getMessage();
        }

        return new JsonResponse(
            [
                "erreur"=>$erreur


            ]
        );

    }

    /**
     *  activité de messagerie transactionnelle agrégée sur une période de temps
     * @Route("/report-mail", name="mail_report")
     *
     */
    public  function  getAggregatedSmtpReport(Request $request)

    {
        $erreur = null;
        $response = null;

        $data = json_decode($request->getContent());
        try {

            $response = $this->mailService->gregatedReport(null,null,1,null);



        }catch (\Exception $exception)
        {
            $erreur = $exception->getMessage();
        }

        return new JsonResponse(
            [
                'erreur'=>$erreur,
                'response'=>json_encode($response)
            ]
        );
    }


    /**
     *  recuperation des contacts
     * @Route("/report-contact", name="mail_contact")
     *
     */
    public  function  getContacts(Request $request)

    {
        $erreur = null;
        $response = null;

        $data = json_decode($request->getContent());
        try {

            $response = $this->mailService->getContacts(null,null,null);


        }catch (\Exception $exception)
        {
            $erreur = $exception->getMessage();
        }

        return new JsonResponse(
            [
                'erreur'=>$erreur,
                'response'=>json_encode($response)
            ]
        );
    }


    /**
     *  recuperation des donnees sur de campagnes
     * @Route("/campagnes", name="mail_campagne")
     *
     */
    public  function dasboard(Request $request)
    {
        $data = json_decode($request->getContent());
        $erreur = null;
        $response = [];

        try {
            $response["contacts"] = $this->mailService->getContacts(null,null,null);
            $response["campagnes"] = $this->mailService->getEmailCampaigns(null,null,null,null,null,null);
        }catch (\Exception $exception)
        {
            $erreur = $exception->getMessage();
        }

        return new  JsonResponse(
            [
                "data"=>json_encode($response),
                "erreur"=>$erreur
            ]
        );

    }

}
