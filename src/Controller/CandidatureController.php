<?php


namespace App\Controller;


use App\Entity\Candidature;
use App\Entity\Interim;
use App\Entity\Mission;
use App\Entity\MissionInterim;
use App\Entity\Planning;
use App\Entity\StatutCandidature;
use App\Service\SendinblueService;
use App\Service\Utiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CandidatureController
{



    private $em;
    private $mail;


    public function __construct(EntityManagerInterface $entityManager,SendinblueService  $mail)
    {
        $this->em = $entityManager;
        $this->mail = $mail;
    }

    /**
     * @param Request $request
     */
    public function __invoke(Request $request)
    {

        if ($request->isMethod("post")) {
            $erreur = null;
            $succes = null;
            $response = null;
            $newCandidature = false;
            try {
                if ($request->getContent()) {
                    $contentRequest = json_decode($request->getContent());



                    $interim = $this->em->getRepository(Interim::class)->find($contentRequest->interim);

                    $statut = $this->em->getRepository(StatutCandidature::class)->findOneBy([
                        "designation" => "En Attente"
                    ]);

                    $arrayMailValue = [];

                    if ($statut === null) {

                        $erreur = "veillez renseigneer le statut candidature  en Attente sur admin config";
                    }

                    if ($interim && $statut) {
                        $missionInterim = new MissionInterim();
                        foreach ($contentRequest->candidatures as $item) {

                            //  rettur un tableau de donnée si le medecin a deja postuler sur cette mission
                            $postuler = $this->em->getRepository(MissionInterim::class)->getMissionInterimWithPlanning($item->planning_id, $interim->getId());


                            if (count($postuler) > 0) {
                                $missionInterim = $this->em->getRepository(MissionInterim::class)->find($postuler[0]["id"]);
                            }


//                            $missionInterim->setInterim($interim);
//                            $missionInterim->setCommentaires($contentRequest->commentaire);
//                            $missionInterim->createdAt();
//                            $this->em->persist($missionInterim);


                                $missionInterim->setInterim($interim);
                                $missionInterim->setCommentaires($contentRequest->commentaire);
                                $missionInterim->setAuteur($contentRequest->auteur);
                                $missionInterim->createdAt();
                                $this->em->persist($missionInterim);




                            $planning = $this->em->getRepository(Planning::class)->find($item->planning_id);



                            // verification si la candidature n'existe pas
                            $candidatureExiste = $this->em->getRepository(Candidature::class)->findOneBy(
                                [
                                    "planning" => $planning,
                                    "missionInterims" => $missionInterim
                                ]
                            );


                            if (!$candidatureExiste) {

                                $candidature = new Candidature();

                                $candidature->setStatut($statut);
                                $candidature->setPlanning($planning);
                                $candidature->setMissionInterims($missionInterim);
                                $candidature->setEnable($item->enable);
                                $candidature->setAuteur($contentRequest->auteur);
                                $candidature->createdAt();
                                $this->em->persist($candidature);
                                $this->em->flush();
                                array_push($arrayMailValue,$planning);
                                $newCandidature = true;


                            }


                        }


                    }

                    try {

                        if ($contentRequest->mail && $newCandidature )
                        {

                            $template =  "emails/erp/interim/confirmation_candidature.html.twig";
                            $this->mail->sendNotificationMail(
                                $interim->getUser()->getEmail(),
                                $interim->getUser()->getNom(),
                                false,
                                null,
                                $template,
                                $arrayMailValue,
                                "Nouvelle candidature",
                                "new candidature");
                        }
                    }catch (\Exception $exception)
                    {
                        $erreur = $exception->getMessage();
                    }




                }

            }catch (\Exception $exception) {

                $erreur = Utiles::messageErrorServer() . $exception->getMessage();
            }

            return new  JsonResponse(
                [
                    "erreur" => $erreur,
                    "mail"=> $contentRequest->mail,
                    "new"=> $newCandidature
                ]
            );
        }

    }
}
