<?php


namespace App\Controller;

use App\Entity\Candidature;
use App\Entity\Client;
use App\Entity\Contrat;
use App\Entity\Entreprise;
use App\Entity\Interim;
use App\Entity\Justificatif;
use App\Entity\Piece;
use App\Entity\ReleveHeure;
use App\Entity\Renumeration;
use App\Entity\Signature;
use App\Entity\Specialite;
use App\Entity\TypeAnnexe;
use App\Entity\Vacation;
use App\Service\Utiles;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ContratPldController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $contratPldObject = [
        'IdContrat' => '',
        'NumeroAvenant' => '',
        'IdClient' => '',
        'IdInterimaire' => '',
        'IdCommercial' => '',
        'TypeContrat' => 1,
        'TypeRecours' => 2,
        'DateDebut' => '',
        'DateFin' => '',
        'DateDebutSouplesse' => '',
        'DateFinSouplesse' => '',
        'DateFinMission' => '',
        'IdRecours' => '',
        'TxtRecours' => '',
        'TxtJustificatif' => '',
        'TxtCaracteristiquePoste' => '',
        'IdConsignePoste' => 0,
        'TxtConsignePoste' => '',
        'IdConsigneReference' => 0,
        'TxtConsigneReference' => '',
        'IdConsigneRemuneration' => 0,
        'TxtConsigneRemuneration' => '',
        'IdConsigneFacturation' => 0,
        'TxtConsigneFacturation' => '',
        'TxtConsigneQualification' => '',
        'IdQualification' => '',
        'Niveau' => '',
        'Profession' => '',
        'PCS' => '',
        'CodeDelegationGestion' => 1,
        'CoeffHeures' => 0,
        'CoeffPrimes' => 0,
        'CoeffIndemnites' => 1,
        'TauxHorairePaye' => 0,
        'TauxHoraireFacture' => 0,
        'AdresseRue' => '',
        'AdresseSuite' => '',
        'AdresseCodePostal' => '',
        'AdresseVille' => '',
        'AdresseCodeINSEE' => '',
        'AdressePays' => '',
        'Interlocuteur' => '',
        'MoyenAcces' => '',
        'EstChezClient' => 'O',
        'EstEnChantier' => 'N',
        'EstSansTaxeTransport' => 'N',
        'EstMissionPlante' => 'N',
        'EstNonDemarre' => 'N',
        'EstNonLieu' => 'N',
        'EstTermineNuit' => 'N',
        'EstPosteARisque' => 'N',
        'EstTermePrecis' => 'O',
        'EstDureeMini' => 'N',
        'EstAvecSouplesse' => 'N',
        'NbJoursEssai' => 2,
        'NbJoursOuvre' => 0,
        'NbHeureHebdo' => 0,
        'SalaireReference' => '',
        'Reference1' => '',
        'Reference2' => '',
        'CodeAnalytique1' => '',
        'CodeAnalytique2' => 0,
        'Horaires' => '',
        'HoraireAMDebut' => '',
        'HoraireAMFin' => '',
        'HorairePMDebut' => '',
        'HorairePMFin' => '',
        'HoraireDebutPremier' => '',
        'PeriodeNonTrav' => '',
        'EstTravailleLundi' => 'O',
        'EstTravailleMardi' => 'O',
        'EstTravailleMercredi' => 'O',
        'EstTravailleJeudi' => 'O',
        'EstTravailleVendredi' => 'O',
        'EstTravailleSamedi' => '',
        'EstTravailleDimanche' => '',
        'EstTravailleFerie' => '',
        'IdAdresseVM' => 0,
        'TxtAdresseVM' => '',
        'IdZoneTransport' => 1,
        'TxtZoneTransport' => 'Transp T1',
        'PenibiliteNC' => '',
        'IdAffaire' => '',
        'TabPrimes' => [],
        'TabPenibilites' => [],
        'PrimeClientAuto' => '',
        'PrimeQualificationAuto' => ''
    ];
    private $em;
    private $params;
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->em = $entityManager;
    }

    /**
     * @Route("api/contrat-pld/{id}", name ="delete_contrat", methods={"post"})
     */
    public function deleteContratAction(Contrat $contrat)
    {
        $response = [
            'success' => '',
            'erreur' => '',
        ];
        $candidatures = $this->em->getRepository(Candidature::class)->findBy(['contratPld' => $contrat->getId()]);
        $sellAndSign = $this->em->getRepository(Signature::class)->findBy(['contrat' => $contrat->getId()]);
        $remuneration = $this->em->getRepository(Renumeration::class)->findOneBy(['contrat' => $contrat->getId()]);
        if($sellAndSign){
            foreach ($sellAndSign as $value){
                $this->em->remove($value);
            }
        }
        if($candidatures){
            foreach ($candidatures as $candidature) {
                $candidature->setContratPld(null);
                $this->em->persist($candidature);
            }
        }
        if($remuneration){
            $rhs = $this->em->getRepository(ReleveHeure::class)->findBy(['remuneration' => $remuneration->getId()]);
            $justificatif = $this->em->getRepository(Justificatif::class)->findOneBy(['remuneration' => $remuneration->getId()]);
            if($rhs){
                foreach ($rhs as $rh) {
                    $this->em->remove($rh);
                }
            }
            if($justificatif){
                $pieces = $this->em->getRepository(Piece::class)->findBy(['justificatif' => $justificatif->getId()]);
                if($pieces){
                    foreach ($rhs as $rh) {
                        $this->em->remove($rh);
                    }
                }
                $this->em->remove($justificatif);
            }
            $this->em->remove($remuneration);
        }
        $this->em->remove($contrat);
        $this->em->flush();
        return new JsonResponse($contrat->getIdPld());
    }

    /**
     * @Route("api/contrat-pld/{id}", name ="get_contrat_by_id", methods={"get"})
     */
    public function getContratbyIdAction(Contrat $contrat)
    {
        $response = [
            'success' => '',
            'erreur' => '',
            'data' => [],
        ];
        if($contrat->getIdPld()){
            $contratPld = (new PldController())->findContratPldAction($contrat->getIdPld());
            $response['data'] = $contratPld;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("api/avenant-pld", name ="creation_avenant_pld", methods={"post"})
     */
    public function creationAvenantPldAction(Request $request)
    {
        $response = [
            'success' => '',
            'erreur' => '',
            'data' => [],
        ];
        $data = json_decode($request->getContent());
        $interim = $this->em->getRepository(Interim::class)->find($data->idInterim);
        $client = $this->em->getRepository(Client::class)->find($data->idClient);
        $data->contratPld->DateDebut = $this->returnDateDebutAnFinByCandidatureIds($data->candidaturesId)['debutPld'];
        $data->contratPld->DateFin = $this->returnDateDebutAnFinByCandidatureIds($data->candidaturesId)['finPld'];
        if($data->contratPld->IdContrat){
            $contratExiste = $this->em->getRepository(Contrat::class)->findOneBy(['idPld' => $data->contratPld->IdContrat]);
            if($contratExiste){
                $this->deleteContratAction($contratExiste);
            }
        }
        $contratPldResultat = (new PldController())->ContratPldAction($data->contratPld);
        if($contratPldResultat->Code) {
            $host = $request->getSchemeAndHttpHost();
            if($interim){
                $this->genererLesContratsInterimAndClient($interim, $client, $data, $host, $contratPldResultat);
                $this->em->flush();
            }
            $response['success'] = 'Contrat crée avec succès sur Pld';
        }


        return new JsonResponse($response);
    }

    /**
     * @Route("api/contrat-pld", name ="generer_contrat_by_interim", methods={"post"})
     */
    public function genererContratAction(Request $request)
    {
        $response = [
            'success' => [],
            'erreur' => '',
        ];
        $data = json_decode($request->getContent());
        $interim = $this->em->getRepository(Interim::class)->find($data->idInterim);
        $client = $this->em->getRepository(Client::class)->find($data->idClient);
        if($interim && $client){
            $this->contratPldObject['IdContrat'] = $data->data_pld->IdContrat;
            $this->contratPldObject['NumeroAvenant'] = $data->data_pld->NumeroAvenant;
            $this->contratPldObject['NumeroAvenant'] = $data->data_pld->NumeroAvenant;
            $this->contratPldObject['TypeContrat'] = $data->data_pld->TypeContrat;
            $this->contratPldObject['IdInterimaire'] = '3887';
            $this->contratPldObject['IdClient'] = '0000294';
            $this->contratPldObject['PCS'] = $data->data_pld->Pcs;
            $this->contratPldObject['Interlocuteur'] = $data->data_pld->Interlocuteur;
            $this->contratPldObject['IdQualification'] = $data->data_pld->IdQualification;
            $this->contratPldObject['IdRecours'] = $data->data_pld->IdRecours;
            $this->contratPldObject['TypeRecours'] = $data->data_pld->TypeRecours;
            $this->contratPldObject['TxtJustificatif'] = $data->data_pld->justificatif;
            $this->contratPldObject['NbJoursEssai'] = $data->data_pld->nbJoursEssai;
            $this->contratPldObject['Horaires'] = $data->data_pld->horaires;
            $this->contratPldObject['TxtConsignePoste'] = $data->data_pld->txtConsignePoste;
            $this->contratPldObject['TxtConsigneRemuneration'] = "Salaire net avant Impôt: " . $this->returnSalaireByCandidatureIds($data->candidaturesId) . "€";
            $this->contratPldObject['MoyenAcces'] = $data->data_pld->moyenAcces;
            $this->contratPldObject['HoraireAMDebut'] = $data->data_pld->heureDebut;
            $this->contratPldObject['HoraireAMFin'] = $data->data_pld->heureFin;
//            (intval($data->data_pld->heureDebut) >= 0 && intval($data->data_pld->heureDebut) < 1200) ? $this->contratPldObject['HoraireAMDebut'] = $data->data_pld->heureDebut : $this->contratPldObject['HorairePMDebut'] = $data->data_pld->heureDebut;
//            (intval($data->data_pld->heureFin) >= 0 && intval($data->data_pld->heureFin) <= 1200) ? $this->contratPldObject['HoraireAMFin'] = $data->data_pld->heureFin : $this->contratPldObject['HorairePMFin'] = $data->data_pld->heureFin;
            $this->contratPldObject['DateDebut'] = $this->returnDateDebutAnFinByCandidatureIds($data->candidaturesId)['debutPld'];
            $this->contratPldObject['DateFin'] = $this->returnDateDebutAnFinByCandidatureIds($data->candidaturesId)['finPld'];
            $this->contratPldObject['DateFinMission'] = $this->returnDateDebutAnFinByCandidatureIds($data->candidaturesId)['finPld'];
            $this->contratPldObject['TxtCaracteristiquePoste'] = $data->data_pld->caracteristiquePoste;
            $this->contratPldObject['TabPrimes'] = $data->data_pld->primes;
            if($data->contratPld && intval($data->data_pld->TypeContrat) != 4){
                $contratExiste = $this->em->getRepository(Contrat::class)->findOneBy(['idPld' => $data->contratPld]);
                if($contratExiste){
                    $this->deleteContratAction($contratExiste);
                }
            }
            $contratPldResultat = (new PldController())->ContratPldAction($this->contratPldObject);
            if($contratPldResultat->Code){
                array_push($response['success'], 'Contrat crée avec succès sur Pld: '. $contratPldResultat->ID);
                $host = $request->getSchemeAndHttpHost();
                if($interim){
                    $this->genererLesContratsInterimAndClient($interim, $client, $data, $host, $contratPldResultat);
                    $this->em->flush();
                }
            }
        }
        return new JsonResponse($response);
    }
    /**
     * @Route("api/contrats-idPld", name ="contrat_by_id_pld", methods={"post"})
     */
    public function getContratByIdPldPldAction(Request $request)
    {
        $response = [];
        $idPld = json_decode($request->getContent());
        $contratPld = (new PldController())->findContratPldAction($idPld);
        if($contratPld){
            $response = $contratPld;
        }
        return new JsonResponse($response);
    }
    public function returnSalaireByCandidatureIds($ids)
    {
        $response = 0;
        foreach ($ids as $key =>  $id) {
            $candidature = $this->em->getRepository(Candidature::class)->find(intval($id));
            $response += $candidature->getPlanning()->getSalaireNet();
        }
        return $response;
    }
    public function returnDateDebutAnFinByCandidatureIds($ids){
        $reponse = [
            'debutPld' => '',
            'finPld' => '',
            'debut' => '',
            'fin' => ''
        ];
        foreach ($ids as $key =>  $id) {
            $candidature = $this->em->getRepository(Candidature::class)->find(intval($id));
            if($key == 0){
                $reponse['debut'] = $candidature->getPlanning()->getDate()->format('d/m/Y');
                $reponse['fin'] = $candidature->getPlanning()->getDate()->format('d/m/Y');
                $reponse['debutPld'] = $candidature->getPlanning()->getDate()->format('Ymd');
                $reponse['finPld'] = $candidature->getPlanning()->getDate()->format('Ymd');
            } else {
                if (strtotime($reponse['debut']) > strtotime($candidature->getPlanning()->getDate()->format('d/m/Y'))){
                    $reponse['debut'] = $candidature->getPlanning()->getDate()->format('d/m/Y');
                    $reponse['debutPld'] = $candidature->getPlanning()->getDate()->format('Ymd');
                }
                if (strtotime($reponse['fin']) < strtotime($candidature->getPlanning()->getDate()->format('d/m/Y'))){
                    $reponse['fin'] = $candidature->getPlanning()->getDate()->format('d/m/Y');
                    $reponse['finPld'] = $candidature->getPlanning()->getDate()->format('Ymd');
                }
            }
        }
        return $reponse;
    }

    public function genererLesContratsInterimAndClient($interim, $client, $data, $host, $contratPldResultat){
        $typeAnnexes = $this->em->getRepository(TypeAnnexe::class)->findAll();
        $entreprise = $this->em->getRepository(Entreprise::class)->find(1);
        $contratPld = (new PldController())->findContratPldAction($contratPldResultat->ID);
        $contratPld->semaine = date('W', strtotime($contratPld->DateDebut));
        if($typeAnnexes && $entreprise && $contratPld) {
            $contratERP = new Contrat();
            $contratERP->setIdPld($contratPldResultat->ID);

            $pathInterimairesContrat = $this->params->get("folder_files") . "/Dr_" . $interim->getUser()->getNom() . "_" . $interim->getUser()->getId() . "/Contrats/" . $data->ref_mission . '/'. $contratPld->IdContrat;
            if (!file_exists($pathInterimairesContrat)) {
                mkdir($pathInterimairesContrat, 0777, true);
            }
            $urlContrat = [];
            foreach ($typeAnnexes as $value) {
                $nomFichier = null;
                $annexe = [];
                if($value->getLibelle() == 'Contrat de mise à disposition' && sizeof($value->getAnnexes()->getValues()) > 0) {
                    $nomFichier = 'contrat_mise_a_disposition';
                }
                if($value->getLibelle() == 'Contrat de mission temporaire' && sizeof($value->getAnnexes()->getValues()) > 0 ){
                    $nomFichier = 'contrat_mission_temporaire';
                }
                if($nomFichier !== null){
                    $pdfFilepath = $pathInterimairesContrat . '/' . $nomFichier .'.pdf';
                    array_push($urlContrat, $host.'/'.$pdfFilepath);
                    $contratERP->setUrlContrat($urlContrat);
                    $pdfOptions = new Options();
                    $pdfOptions->set('defaultFont', 'Arial');
                    $pdfOptions->set('isRemoteEnabled', TRUE);


                    $dompdf = new Dompdf($pdfOptions);

                    foreach ($contratPld->TabPrimes as $prime){
                        if(strpos(strtolower($prime->TxtPrime), 'heures') === false){
                            $abreviationTab = explode('-', strtolower($prime->TxtPrime));
                            if(sizeof($abreviationTab) > 1){
                                $abreviatonTypeVacation = $abreviationTab[0];
                                $abreviatonSpecialite = $abreviationTab[1];
                                $typeVacationByPrime = $this->em->getRepository(Vacation::class)->findOneBy(['abreviation' => $abreviatonTypeVacation]);
                                $specialiteByPrime = $this->em->getRepository(Specialite::class)->findOneBy(['abreviation' => $abreviatonSpecialite]);
                                $typeVacationByPrime ? $prime->TxtPrime = $typeVacationByPrime->getDesignation() : $prime->TxtPrime = $abreviatonTypeVacation;
                                $specialiteByPrime ? $prime->TxtPrime = $prime->TxtPrime." ".$specialiteByPrime->getNomspecialite() : $prime->TxtPrime =$prime->TxtPrime." ".$abreviatonSpecialite;
                            }
                        }
                    }

                    $html = $this->renderView('contrats/'. $nomFichier .'.html.twig', [
                        'entreprise' => $entreprise,
                        'typeAnnexe' => $value,
                        'client' => $client,
                        'dateDebut' => $this->returnDateDebutAnFinByCandidatureIds($data->candidaturesId)['debut'],
                        'dateFin' => $this->returnDateDebutAnFinByCandidatureIds($data->candidaturesId)['fin'],
                        'interimaire' => $interim,
                        'contratPld' => $contratPld,
                        'params' => $data->data_pld ? $data->data_pld : [],
                        'txtConsignePoste' => $contratPld->TxtConsignePoste ? explode(';' , $contratPld->TxtConsignePoste) : [],
                        'salaireErp' => $this->returnSalaireByCandidatureIds($data->candidaturesId)
                    ]);
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4', 'portrait');

                    $dompdf->render();
                    $output = $dompdf->output();
                    file_put_contents($pdfFilepath, $output);
                }
            }

            $this->em->persist($contratERP);
            $this->updateCandidatureForContratIdPld($data->candidaturesId, $contratERP);
        }
    }
    public function updateCandidatureForContratIdPld($candidaturesId, $contrat){
        $renumeration = new Renumeration();
        $nom = $this->getUser()->getNom();
        $prenom = $this->getUser()->getPrenom();
        $renumeration->setContrat($contrat);
        $renumeration->setAuteur($prenom." ".$nom);
        $this->em->persist($renumeration);
        foreach ($candidaturesId as $id) {
            $candidature = $this->em->getRepository(Candidature::class)->find(intval($id));
            if($candidature){
                $candidature->setContratPld($contrat);
                $this->em->persist($candidature);
            }
        }
    }

    /**
     * @Route("api/update-annexes/{id}", name ="update_annexes_by_entreprise", methods={"post"})
     */
    public function genererAnnexeAction(Request $request, Entreprise $entreprise)
    {
        $response = [
            'success' => '',
            'erreur' => '',
        ];
        if($entreprise){
            $typeAnnexes = $this->em->getRepository(TypeAnnexe::class)->findAll();
            $raisonSociale = $entreprise->getRaisonSociale();
            $uploadDirAnnexes = 'uploads/Entreprise/annexes';
            if (file_exists($uploadDirAnnexes)) {
                Utiles::rmdir_recursive($uploadDirAnnexes);
            }
            mkdir($uploadDirAnnexes, 0777, true);
            $host = $request->getSchemeAndHttpHost();
            if($typeAnnexes){
                foreach ($typeAnnexes as $value) {
                    $nomFichier = null;
                    $annexe = [];
                    if($value->getLibelle() == 'Contrat de mise à disposition' && sizeof($value->getAnnexes()->getValues()) > 0) {
                        $nomFichier = 'annexe_contrat_mise_a_disposition';
                    }
                    if($value->getLibelle() == 'Contrat de mission temporaire' && sizeof($value->getAnnexes()->getValues()) > 0 ){
                        $nomFichier = 'annexe_contrat_mission_temporaire';
                    }
                    if($nomFichier !== null){

                        $urlAnnexe = $host . '/' . $uploadDirAnnexes . '/' . $nomFichier .'.pdf';
                        $pdfFilepath = $uploadDirAnnexes . '/' . $nomFichier .'.pdf';

                        $pdfOptions = new Options();
                        $pdfOptions->set('defaultFont', 'Arial');
                        $pdfOptions->set('isRemoteEnabled', TRUE);


                        $dompdf = new Dompdf($pdfOptions);;

                        $html = $this->renderView('contrats/annexes/'. $nomFichier .'.html.twig', ['typeAnnexe' => $value]);
                        $dompdf->loadHtml($html);
                        $dompdf->setPaper('A4', 'portrait');

                        $dompdf->render();
                        $output = $dompdf->output();
                        file_put_contents($pdfFilepath, $output);
                        $value->setUrl($urlAnnexe);
                        $this->em->persist($value);
                    }
                    else {
                        $value->setUrl('');
                        $this->em->persist($value);
                    }
                }
                $this->em->flush();
            }

        }
        return new JsonResponse($response);
    }

}
