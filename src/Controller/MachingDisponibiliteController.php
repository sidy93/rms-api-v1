<?php


namespace App\Controller;


use App\Entity\Interim;
use App\Entity\Mission;
use App\Entity\Planning;
use App\Service\Utiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MachingDisponibiliteController
 * @package App\Controller
 * @Route("/api")
 */
class MachingDisponibiliteController
{


    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MachingDisponibiliteController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }


    /**
     * @Route(path="/maching-disponibilites",name="get_maching_disponibilite")
     * @param Request $request
     * @return JsonResponse
     */

    public  function  getMissionByDateDispo(Request  $request)
    {
        if ($request->isMethod("post")) {
            $erreur = null;
            $succes = null;
            $response = null;
            try {
                $contentRequest = json_decode($request->getContent());


                $response = $this->em->getRepository(Mission::class)->getMissionByArrayDatesDispo($contentRequest->date);



            }catch (\Exception $exception)
            {
                $erreur = $exception->getMessage();
            }


            return new  JsonResponse(
                [
                    "data"=>$response,
                    "erreur"=>$erreur,

                ]
            );

        }
    }


    /**
     * @Route(path="/maching-qualification/{qualif}/{interim}",name="get_maching_qualification")
     * @param $id
     * @return JsonResponse
     */

    public  function  getMissionByQualification($qualif,$interim, Utiles $utiles)
    {

            $erreur = null;
            $succes = null;
            $response = null;
            try {



                $missions = $this->em->getRepository(Mission::class)->getMissionAdPlanningAPourvoir((int)$qualif);

                $data =  $utiles->refractor_json_offre($this->em,$missions,(int)$interim);





            }catch (\Exception $exception)
            {
                $erreur = $exception->getMessage();
            }


            return new  JsonResponse(
                [
                    "data"=>$data,
                    "erreur"=>$erreur,

                ]
            );

        }

}