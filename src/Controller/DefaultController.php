<?php


namespace App\Controller;

use App\Entity\Entreprise;
use App\Entity\GroupeRoles;
use App\Entity\User;
use App\Events\HistoriqueSubscriber;
use App\Events\sendMailSubscriber;
use App\Repository\UserRepository;
use App\Service\Utiles;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWSProvider\JWSProviderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class DefaultController extends AbstractController
{
    private $hs;
    private $JWTTokenManager;
    private $jwsProvider;
    private $encoder;

    public function __construct(HistoriqueSubscriber $hs, JWSProviderInterface $jwsProvider,
                                JWTTokenManagerInterface $JWTTokenManager, JWTEncoderInterface $encoder)
    {
        $this->hs = $hs;
        $this->JWTTokenManager = $JWTTokenManager;
        $this->jwsProvider = $jwsProvider;
        $this->encoder = $encoder;
    }

    /**
     * action a faire avec le cron afin de mettre a jour automatiquement les adresses
     * recuperation des communes https://geo.api.gouv.fr/ et les sauvegarder dans un fichier commune
     * @Route("api/creation_commune", name = "creation_commune_and_vile")
     */
    public function CommuneAction()
    {
        $filesystem = new Filesystem();
        $datan = [];
        $httpClient = HttpClient::create();
        // on recupere la liste des departement de https://geo.api.gouv.fr/ afin de boucler
        $departements = $httpClient->request('GET', 'https://geo.api.gouv.fr/departements');
        $departements = json_decode($departements->getContent());
        // on parcour afin de recuperer tous les commune
        foreach ($departements as $departement) {
            $communes = $httpClient->request('GET',
                "https://geo.api.gouv.fr/departements/$departement->code/communes");

            $communes = json_decode($communes->getContent());
            foreach ($communes as $commune) {
                $nomDept = $departement->nom;
                $nomCommune = $commune->nom;
                foreach ($commune->codesPostaux as $item) {
                    $data[] = [
                        'cp' => $item,
                        'code' => $commune->code,
                        'commune' => $nomCommune,
                        'departement' => $nomDept,
                        'numero_departement' => $departement->code,
                        'context' => $commune->code . ' ' . $item . ' ' . $nomCommune . ' ' . $nomDept
                    ];
                }
            }
        }
        $fichier = 'commune.json';
        $isFichierExist = file_exists($fichier);
        if (!$isFichierExist) {
            $filesystem->touch($fichier);
        }

        $file = fopen($fichier, 'w+');
        fwrite($file, json_encode($data));
        fclose($file);
        return New JsonResponse('ok');
    }

    /**
     * @Route("api/getCommunes", name ="recuperation_des_communes_sur_le_fichier")
     */
    public function getCommunesAction()
    {
//jjh
        $communes = [];
        $fichier = 'commune.json';
        if (file_exists($fichier)) {
            $contenue_fichier = file_get_contents($fichier) != null ? file_get_contents($fichier) : [];
            $communes = json_decode($contenue_fichier, true);
        }
        return new JsonResponse($communes);
    }

    /**
     * @Route("api/setImageUser/{id}", name ="setImage_User", methods={"POST", "HEAD"})
     */
    public function setImageUserAction(Request $request, User $user, EntityManagerInterface $entityManager)
    {
        $response = [
            'data' => '',
            'msg' => 'Echec de l\' enregistrement de la photo !'
        ];
        if ($user) {
            $groupeRoles = $user->getGroupeRoles()->getValues();
            foreach ($groupeRoles as $groupeRole) {
                $libelle = strtolower($groupeRole->getLibelle());
                if (strpos($libelle, 'interim')) {
                    $uploadDir = 'Uploads/Interimaires/Dr_';
                    break;
                } else if (strpos($libelle, 'recruteur')) {
                    $uploadDir = 'Uploads/Recruteurs/';
                    break;
                } else {
                    $uploadDir = 'Uploads/Gestionnaires/';
                    break;
                }
            }
            $nomId = strtoupper($user->getNom() . '_' . $user->getId());
            $uploadDir = $uploadDir . $nomId . '/';
            $host = 'http://' . $request->server->get('HTTP_HOST');
            $file = $request->files->get('file');
            if (empty($file)) {
                return new Response("No file specified",
                    Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'text/plain']);
            }
            $filename = $file->getClientOriginalName();
            $file->move($uploadDir, $filename);

            $user->setImage($host . '/' . $uploadDir . $file->getClientOriginalName());
            $entityManager->flush();
            $response = [
                'data' => $request->server,
                'msg' => 'Ajout de la photo avec succées!'
            ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("api/setLogoEntreprise/{id}", name ="set_logo_entreprise", methods={"POST"})
     */
    public function setLogoEntrepriseAction(Request $request, Entreprise $entreprise, EntityManagerInterface $entityManager)
    {
        $response = [
            'data' => '',
            'msg' => 'Echec de l\' enregistrement du logo !'
        ];
        if ($entreprise) {
            $uploadDir = 'uploads/Entreprise/';
            $raisonSociale = $entreprise->getRaisonSociale();
            $host = $request->getSchemeAndHttpHost();
            $file = $request->files->get('logo');
            if (empty($file)) {
                return new Response("No file specified",
                    Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'text/plain']);
            }
            $filename = $file->getClientOriginalName();
            $file->move($uploadDir, $filename);

            $entreprise->setLogo($host . '/' . $uploadDir . $filename);
            $entityManager->flush();
            $response = [
                'data' => $request->server,
                'msg' => 'Logo ajouté avec succées !'
            ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("api/activation", name ="activation_compte", methods={"POST", "HEAD"})
     */
    public function activationCompteAction(Request $request, EntityManagerInterface $entityManager, UserRepository $userRepository)
    {

        $cle = ($request->getContent());
        $token = $this->jwsProvider->load($cle);
        $email = $token->getPayload()['username'];
        $user = $userRepository->findBy(array('email' => $email))[0];
        $user->setIsEnable(true);
        $user->setConfirmationToken("");
        $entityManager->flush();
        return new JsonResponse("success");
    }

    /**
     * @Route(path="api/list-tables",name="liste_des_tables", methods={"GET"})
     *
     */
    public function listTableAction(Connection $connection)
    {
        // Retourne la liste des tables de la base de données sur les auelles on veut effectuer les restrictions
        $response = [];
        $sql = "SELECT table_name nom from INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'apirms_v1'
                AND table_name != 'migration_versions'
                AND table_name != 'user_groupe_roles'";
        $tables = $connection->fetchAll($sql);
        if ($tables) {
            // Historiaue n'est pas une table dans notre BD alors on la l'ajoute dans la liste des tables a retourner
            array_push($tables, ['nom' => 'historique']);
            $response = $tables;
        }
        return new JsonResponse($response);
    }

    /**
     * @Route(path="api/add_roles",name="ajouter_roles", methods={"POST"})
     */
    public function addRoleOnUserAction(Request $request, EntityManagerInterface $entityManager)
    {
        // Permet d'ajouter le groupe de roles aux users
        $data = json_decode($request->getContent());
        if ($data) {
            // On recupere les users en fonction des ids
            foreach ($data->idUsers as $idUser) {
                $user = $entityManager->getRepository(User::class)->find($idUser);
                if ($user) {
                    // Si le user a un ou plusieur groupe de roles on le(s) supprime(s)
                    if (sizeof($user->getGroupeRoles()) > 0) {
                        foreach ($user->getGroupeRoles() as $groupeRole) {
                            $user->removeGroupeRole($groupeRole);
                        }
                    }
                    // On fait la mise a jour des groupes de roles
                    foreach ($data->roles as $idGroupeRole) {
                        $groupeRole = $entityManager->getRepository(GroupeRoles::class)->find($idGroupeRole);
                        if ($groupeRole) {
                            $user->addGroupeRole($groupeRole);
                        }

                    }
                }

            }
            $entityManager->flush();
        }
        $response = $data;
        return new JsonResponse($response);
    }

    /**
     * @Route(path="api/get_currentUser",name="recuper_current_user", methods={"GET"})
     */
    public function getCurrentUserAction(UserInterface $user)
    {
        return new JsonResponse($this->JWTTokenManager->create($user));
    }

    /**
     * @Route(path="api/update-password-by-admin",name="update_password_by_admin", methods={"POST"})
     */
    public function updatePasswordByAdminAction(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $em, sendMailSubscriber $sendMailSubscriber)
    {
        $data = json_decode($request->getContent());
        $response = "erreur";
        if ($data) {
            foreach ($data as $id) {
                $user = $em->getRepository(User::class)->find($id);
                if ($user) {
                    $email = $user->getEmail();
                    $password = $this->generatePassword(8);
                    $hash = $encoder->encodePassword($user, $password);
                    $user->setPassword($hash);
                    $user->setEnabled(1);
                    $em->flush();
                    $subject = "DEMANDE DE REINITIALISATION MDP";
                    $data = ['user' => $user, 'pwd' => $password];
                    $template = "emails/demande_reinitialisation_mdp.html.twig";
                    $sendMailSubscriber->send($subject, $email, $template, $data);
                    $response = "ok";
                }
            }
        }

//        return new Response("<body>$response</body>");
        return new Response(json_encode($response));
    }

    public function generatePassword($size)
    {
        $characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $password = "";
        for ($i = 0; $i < $size; $i++) {
            $password .= ($i % 2) ? strtoupper($characters[array_rand($characters)]) : $characters[array_rand($characters)];
        }

        return $password;

    }


}
