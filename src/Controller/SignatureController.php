<?php


namespace App\Controller;

use App\Entity\Candidature;
use App\Entity\Client;
use App\Entity\Contrat;
use App\Entity\Hebergement;
use App\Entity\Interim;
use App\Entity\MissionInterim;
use App\Entity\Recruteur;
use App\Entity\Signature;
use App\Service\Utiles;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Twig\Environment;

/**
 * Class SignatureController
 * @package App\Controller
 * @Route("api/signature")
 */
class SignatureController extends AbstractController
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $httpClient;
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * SignatureController constructor.
     */
    public function __construct(KernelInterface $kernel, ParameterBagInterface $params)
    {
        $this->httpClient = new \GuzzleHttp\Client([

            'timeout' => 30.0,
        ]);

        $this->kernel = $kernel;
        $this->params = $params;
    }

    /**
     * presentation contrat pour signature
     * @param Request $request
     * @param Environment $twig
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/create", name ="create_signature", methods={"post"})
     */
    public function createContrat(Request $request, Environment $twig)
    {

        $erreur = null;
        $success = null;
        $response = null;

        $signataireSellAndSign = null;
        $id_contrat = null;


        try {
            $em = $this->getDoctrine()->getManager();
            $data = json_decode($request->getContent());


            $statusCode = null;
            $user = null;

            $signature = new Signature();

            if (!$data->id_ch)
                throw new Exception("donneés invalides");

            $ch = $em->getRepository(Client::class)->find($data->id_ch);
            if (!$ch)
                throw new Exception("client  invalide");


            $clientSellAndSign = null;


            if ($ch->getIdSellAndSign() == null) {
                $recruteur = $ch->getSignataire();
                $number = Utiles::generateNumberClientSellAndSign(10, $ch->getId());

                $clientSellAndSign = $this->createClientSellAndSign($recruteur, $ch, $number);


                if ($clientSellAndSign) {
                    $clientSellAndSign = json_decode($clientSellAndSign);
                    $ch->setIdSellAndSign($clientSellAndSign->number);
                    $em->persist($ch);

                }


            } else {
                $clientSellAndSign = $this->getClientSellAndSignByNumber($ch->getIdSellAndSign());
                $clientSellAndSign = json_decode($clientSellAndSign);
            }

            $signature->setCustomerNumber($clientSellAndSign->number);
            $signature->setCompanyName($clientSellAndSign->companyName);
            $signature->setAddress1($clientSellAndSign->address1);
            $signature->setCity($clientSellAndSign->city);
            $em->persist($signature);


            $user = null;

            $mission_medecin = $em->getRepository(MissionInterim::class)->find((int)$data->mission_medecin);
            if ($data->interim == true) {
                $interim = $em->getRepository(Interim::class)->find($data->id_interim);
                if (!$interim)
                    throw new Exception("Aucun interimaire trouvé");
                $user = $interim->getUser();


                if (!$mission_medecin)
                    throw new Exception("Mission interim non trouvée");
                if ($data->hebergement) {
                    $hebergement = $em->getRepository(Hebergement::class)->find($data->hebergement->id);

                    $mission_medecin->setHebergement($hebergement);
                    $em->persist($mission_medecin);

                    $template_value = Utiles::arrayValueForTemplate([
                        'ch',
                        'lieu',
                        'etoile',
                        'adresse',
                        'telephone',
                        'remarque'
                    ],
                        [
                            $ch->getRaisonSocial(),
                            $data->hebergement->lieu,
                            $data->hebergement->nombreEtoile,
                            $data->hebergement->adresse,
                            $data->hebergement->telephone,
                            $data->hebergement->remarque,


                        ]);
                } else {
                    $template_value = Utiles::arrayValueForTemplate(['ch'], [$ch->getRaisonSocial()]);
                }
                $message_body = $twig->render("emails/message_body_interim_sell_and_sign.html.twig", $template_value);
            } else {
                $recruteur = $em->getRepository(Recruteur::class)->find((int)$data->id_recruteur);
                if (!$recruteur)
                    throw new Exception("Veillez ajouter un signature pour le client");
                $user = $recruteur->getUser();
                $template_value = Utiles::arrayValueForTemplate(['nom',], [$user->getNom()]);
                $message_body = $twig->render("emails/message_body_sell_and_sign.html.twig", $template_value);
            }

            $signataireSellAndSign = json_decode($this->getOrCreateContractorSellAndSign($user, $clientSellAndSign->number));

            if ($signataireSellAndSign) {


                $signature->setCellPhoneSignataire($user->getTelephone());

                $message_title = "ENVOI CONTRAT MISSION" . "-" . $data->id_pld . "-" . $data->reference_mission;

                $contrat = json_decode($this->createContratSellAndSign($clientSellAndSign->number, $message_body, $message_title));
                $signature->setFilename($contrat->filename);

                if (!$contrat)
                    throw new Exception("Echec creation contrat sur sell  & Sign");

                $contrat_pld = $em->getRepository(Contrat::class)->findOneBy(["idPld" => $data->id_pld]);

                if (!$contrat_pld)
                    throw new Exception("contrat pld invalide");


                $url = $contrat_pld->getUrlContrat();

                $pdf_contrat = json_decode($this->UploadPdfContrat($contrat->id, $data->interim, $url[0]));


                if (!$pdf_contrat)
                    throw new Exception("Echec Chargement document a faire signé");

                $ad_signataire = json_decode($this->adSignataireToContrat($contrat->id, $signataireSellAndSign->id, $data->mode_signature));

                $signature->setSignatureDate($ad_signataire->signatureDate);
                $signature->setSignatureStatus($ad_signataire->signatureStatus);

                $em->persist($signature);


                if (!$ad_signataire)
                    throw new Exception("erreur ! aucun signataire trouvé");


                $s = json_decode($this->sendContratToSign($contrat->id));


                if ($s->status == "OPEN") {


                    $contrat_pld = $em->getRepository(Contrat::class)->findOneBy(
                        [
                            "idPld" => $data->id_pld
                        ]);


                    $contrat_existe = $em->getRepository(Signature::class)->findOneBy(
                        [
                            "contrat_pld" => $contrat_pld,
                            "type" => $data->apartenance
                        ]);


                    if ($contrat_existe != null) {
                        $em->remove($contrat_existe);


                    }
                    $signature->setSpecialite($mission_medecin->getSpecialiteMission());
                    $signature->setStatusTranasaction($s->status);
                    $signature->setReferenceMission($data->reference_mission);
                    $signature->setContratId($contrat->id);
                    $signature->setTransactionId($s->transaction_id);
                    $signature->setSignatureMode($data->mode_signature);
                    $signature->setIdSignataire($signataireSellAndSign->id);
                    $signature->setCivilitySignataire($signataireSellAndSign->civility);
                    $signature->setEmailSignataire($signataireSellAndSign->email);
                    $signature->setNomSignataire($signataireSellAndSign->lastname);
                    $signature->setNomSignataire($signataireSellAndSign->lastname);
                    $signature->setPrenomSignataire($signataireSellAndSign->firstname);
                    $signature->setContratPld($contrat_pld);
                    $signature->setType($data->apartenance);
                    $signature->setDateEnvoi((new \DateTime("now")));
                    $signature->createdAt();
                    $em->persist($signature);


                    $success = 'le contrat a bien été envoyer en signature';
                } else {
                    $erreur = " une erreur est survenu lors de l'envoi du contrat";
                }


            } else {
                $erreur = "erreur chargement contrat a faire signé";
            }


            if (!isset($erreur)) {
                $em->flush();
                $cible = '';

                if ($data->interim) {
                    $cible = $interim->getUser()->getNom() . ' ' . $interim->getUser()->getPrenom();
                } else {
                    $cible = $ch->getNomEtablissement();
                }

                //   $this->addHistory($request,$cible,"signature contrat ",$contratBd->getContratId(),"presentation contrat pour signature");


            }

        } catch (\Exception $exception) {
            $erreur = $exception->getMessage();
        }

        return new  JsonResponse(
            [
                "success" => $success,
                "erreur" => $erreur,

            ]
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/statut", name ="statut_signature", methods={"post"})
     */
    public function showContratIdByIdpldAndApartenance(Request $request)
    {
        $erreur = null;
        $success = null;
        $response = null;
        $contrat = null;


        try {


            $data = json_decode($request->getContent());

            $em = $this->getDoctrine()->getManager();


            $contrat_pld = $em->getRepository(Contrat::class)->findOneBy(
                [
                    "idPld" => $data->id_pld
                ]

            );


            if (!$contrat_pld)
                throw new Exception("contrat  non trouvée veillez ressayer ou verifier si le contrat a été generer, consulter le service IT si le probleme persiste");


            $contrat = $em->getRepository(Signature::class)->findOneBy(
                [
                    'contrat_pld' => $contrat_pld,
                    'type' => $data->apartenance
                ]
            );


            if ($contrat)
                $response = $this->getDataContrat($contrat->getContratId());


        } catch (\Exception $exception) {
            $erreur = $exception->getMessage();
            $data = null;
        }

        return new  JsonResponse(
            [
                "erreur" => $erreur,
                "success" => $success,
                "data" => $response
            ]
        );

    }

    public function getDataContrat($contrat)
    {
        $response = null;

        try {


            $transaction = json_decode($this->getTransactionContrat($contrat));
            $response["transaction"] = $transaction;

            $etatContrat = json_decode($this->getStatutContrat($contrat));
            $response["etatContrat"] = $etatContrat;

            $response["signataire"] = json_decode($this->getSignataireById($etatContrat->elements[0]->contractorId));

            $response["client"] = json_decode($this->getClientByNumber($transaction->customerNumber));
        } catch (Exception $exception) {
            throw new Exception("identifiant contrat invalide");
        }

        return $response;

    }

    /**
     * @param $id
     * /* fucnction en chantier
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/docs/{id}", name ="docs_signature", methods={"get"})
     */
    public function getDocumentPdf($id)
    {
        $erreur = null;
        $response = null;


        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign2() . 'do', [

            'query' =>
                [

                    'm' => "getCurrentDocumentForContract",
                    'id' => $id,
                    "responseType" => 'blob',
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],


        ]);


        return new BinaryFileResponse($response->getBody());
    }


    /**
     * relancer contrat pour signature
     * @param Request $request
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/relance", name ="docs_signature", methods={"post"})
     */

    public function relanceContrat(Request $request)
    {
        $erreur = null;
        $success = null;
        $response = null;

        try {
            $data = json_decode($request->getContent());

            $body = [
                'contract_id' => $data->contract_id,
                'all' => true,
                "prevalidation_mode" => false,
            ];

            $em = $this->getDoctrine()->getManager();
            $contrat = $em->getRepository(Signature::class)->findOneBy(
                [
                    "contratId" => $data->contract_id
                ]
            );

            if (!$contrat)
                throw new Exception("aucun contrat avec cet idtentifiant" . $data->contrat_id . "trouvé");

            $contrat->setRelance($contrat->getRelance() + 1);
            $contrat->setDateDernierRelance(new \DateTime("now"));
            $em->persist($contrat);


            $response = $this->httpClient->request('POST', "https://cloud.sellandsign.com/calinda/hub/selling/do", [

                'query' =>
                    [
                        'm' => "animateContractorsForContract",
                        'j_token' => urldecode(Utiles::tokenSellSign())
                    ],

                "body" => json_encode($body)


            ]);


            if ($response->getStatusCode() === 200) {
                $success = "le  contrat" . " " . $data->contract_id . " " . " a bien été relancé";
                $em->flush();
            }
        } catch (Exception $exception) {
            $erreur = $exception->getMessage();
        }


        return new  JsonResponse(
            [
                "erreur" => $erreur,
                "success" => $success,
                "data" => $response
            ]
        );
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/annuler", name ="annuler_signature", methods={"post"})
     */
    public function annulerContrat(Request $request)
    {
        $erreur = null;
        $data = null;
        $success = null;

        try {
            $em = $this->getDoctrine()->getManager();


            $data = json_decode($request->getContent());

            if (!isset($erreur)) {

                $response = $this->httpClient->request('POST', 'https://cloud.sellandsign.com/calinda/hub/selling/do', [

                    'query' =>
                        [

                            'm' => "cancelContract",
                            'id' => $data->id,
                            'j_token' => urldecode(Utiles::tokenSellSign())

                        ],


                ]);

                if (json_decode($response->getBody()->getContents())) {
                    $contrat = $em->getRepository(Signature::class)->findOneBy(
                        [
                            "contratId" => $data->id
                        ]
                    );

                    if (!$contrat)
                        throw new Exception("une erreure est survenue lors de l'annulation du contrat, veillez ressayer");
                    if ($contrat) {
                        $contrat->setStatusTranasaction("ABANDONED");
                        $em->persist($contrat);
                        $em->flush();
                        $success = "le  contrat." . $data->id . " " . "a bien été annuler";

                        //  $this->addHistory($request,$contrat->getType(),"annuler signature contrat ",$data["contrat_id"],"Annulation  contrat presenter pour signature");
                    }


                }

            }
        } catch (\Exception $exception) {
            $erreur = Utiles::messageErrorServer() . " " . $exception->getMessage();
        }


        return new  JsonResponse(
            [

                "erreur" => $erreur,
                "success" => $success
            ]
        );
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @Route("/validation", name ="validation_signature", methods={"post"})
     */

    public function validationContrat(Request $request)
    {
        $erreur = null;
        $data = null;
        $success = null;

        try {
            $em = $this->getDoctrine()->getManager();

            $data = json_decode($request->getContent());
            $contrat = $em->getRepository(Signature::class)->findOneBy(
                [
                    "contratId" => $data->contrat_id
                ]
            );

            if ($contrat == null)
                throw new Exception("aucun contrat avec cet idtentifiant" . $data->contrat_id . "trouvé");


            $response = $this->httpClient->request('GET', 'https://cloud.sellandsign.com/calinda/hub/selling/do', [

                'query' =>
                    [

                        'm' => "closeTransaction",
                        'transactionId' => $data->transactionId,
                        'j_token' => urldecode(Utiles::tokenSellSign())

                    ],

            ]);

            if (json_decode($response->getBody()->getContents())) {
                $contrat->setDateSignature(new \DateTime('now'));
                $em->persist($contrat);
                $em->flush();

                //   $this->addHistory($request,$contrat->getType(),"valider signature contrat ",$contrat->getContratId(),"Validation  contrat presenter pour signature");


                $success = "le  contrat." . $data->contrat_id . " " . "a bien été valider";


            }


        } catch (\Exception $exception) {
            $erreur = Utiles::messageErrorServer() . " " . $exception->getMessage();
        }


        return new  JsonResponse(
            [

                "erreur" => $erreur,
                "success" => $success
            ]
        );
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("/suppression/{id}", name ="delete_signature_contrat", methods={"delete"})
     */
    public function  deleteContrat($id)
    {
        $erreur = null;
        $data = null;
        $success = null;

        try
        {
            $em = $this->getDoctrine()->getManager();


            $contrat = $em->getRepository(Signature::class)->find($id);


            if ($contrat == null)
                throw new Exception("aucun contrat trouvé");

               // $this->addHistory($request,$contrat->getType(),"suppresion contrat ",$contrat->getContratId(),"suppression  contrat presenter pour signature");

                $em->remove($contrat);
                $em->flush();
                $success = "le  contrat a bien été supprimer";


        }catch (\Exception $exception)
        {
            $erreur = $exception->getMessage();
        }


        return  new  JsonResponse(
            [

                "erreur"=>$erreur,
                "success"=>$success
            ]
        );
    }

    /** return la liste des clients sell and sign
     * @param Request $request
     * @return JsonResponse
     * @Route("/clients", name ="list_clients", methods={"get"})
     */

    public function listClient(Request $request)
    {
        $erreur = null;
        $data = null;
        $success = null;

        try {


            if (count($request->query->all()) === 0)
                throw new Exception("Parametre invalide");


            $params = $request->query->all();
            $params["j_token"] = urldecode(Utiles::tokenSellSign());
            $params["action"] = "searchCustomers";


            $data = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'customer/list', [
                'query' => $params
            ])->getBody()->getContents();


        } catch (\Exception $exception) {
            $erreur = Utiles::messageErrorServer() . " " . $exception->getMessage();
        }


        return new  JsonResponse(
            [

                "erreur" => $erreur,
                "data" => json_decode($data)
            ]
        );
    }

    /** return la liste des contrats sell and sign
     * @param Request $request
     * @return JsonResponse
     * @Route("/contrats", name ="list_contrats", methods={"post"})
     */
    public function listContrat(Request $request)
    {
        $erreur = null;
        $response = null;
        $totalSize = 0;


        try {
            $data = json_decode($request->getContent(), true);



            $em = $this->getDoctrine()->getManager();

            $response = $em->getRepository(Signature::class)->findAllContratSignature($data);




            foreach ($response as $value)
            {

                $transaction = json_decode($this->getTransactionContrat($value["contratId"]));

                $contrat = $em->getRepository(Signature::class)->findOneBy([
                    'contratId'=>$value["contratId"]
                ]);

                if (!$contrat)
                    throw new Exception("Contrat non trouvé");

                $contrat->setTransactionId($transaction->transactionId);
                $contrat->setStatusTranasaction($transaction->status);

                $em->persist($contrat);

                $em->flush();


            }


           $response= $em->getRepository(Signature::class)->findAllContratSignature($data);
            $totalSize =  count($response);







        } catch (Exception $exception) {
            $erreur = $exception->getMessage();
        }


        return new JsonResponse(
            [
                "erreur" => $erreur,
                "data" => $response,
                "totalSize" => $totalSize
            ]
        );

    }


    /** return les infos d'un contrat presenté pour signature par rapport a un id pld envoyer en post
     * @param Request $request
     * @return JsonResponse
     * @Route("/contrat-sign-pld", name ="list_sign_pld", methods={"get"})
     */
    public function getContratSignatureByIdPld(Request $request)
    {

       $erreur = null;
       $response = null;

        try {
            $data = $request->query->all();

            if (count($data) === 0)
                throw new Exception("parametre invalide");

            $em = $this->getDoctrine()->getManager();

            $contratPld = $em->getRepository(Contrat::class)->findOneBy(["idPld"=>$data["id_contrat_pld"]]);


            if (!$contratPld)
                throw new Exception("Contrat non trouvé");

            foreach ($contratPld->getSignatures()->getValues() as $contrat){
                     if ($contrat->getType() === (integer)$data["type"])
                     {
                         $response = [
                             "date_envoi_contrat"=>$contrat->getDateEnvoi(),
                             "etat_contrat"=>$contrat->getStatusTranasaction(),
                             "date_relance_contrat"=>$contrat->getDateDernierRelance()
                         ];
                         break;
                     }
               }


        } catch (Exception $exception) {
            $erreur = $exception->getMessage();
        }


        return new JsonResponse(
            [
                "erreur" => $erreur,
                 "data"=>$response


            ]
        );

    }


    /** presentation d'autre document pour signature
     * @param Request $request
     * @return JsonResponse
     * @Route("/other-contrats-create", name ="other_contrats-create", methods={"post"})
     */
    public function createOtherContrat(Request $request, SluggerInterface $slugger)
    {
        $erreur = null;
        $client = [];
        $signataire = [];


        try {


        $data = $request->query->all();

         // return new Response(json_decode($data));
        $file = $request->files->get("files");

        try {
            $folderContrat = $this->params->get("other_contrat_folder");

            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

            if ($file->guessExtension() != "pdf")
                throw new Exception("format document invalide, seul les fichiers au format pdf sont accepté ");


            $file->move($folderContrat, $newFilename);
        } catch (Exception $exception) {
            throw new Exception("Erreur chargement du fichier");
        }


        $donnees = Utiles::formatDataSendPackToSign($data);
        $client = $donnees["client"];
        $signataire = $donnees["signataire"];

        $urlFile = $folderContrat . '/' . $newFilename;


        $em = $this->getDoctrine()->getManager();

        $customerBd = $em->getRepository(Signature::class)->findOneBy(["companyName" => $data["company_name"]]);

        if ($customerBd) {
            $client["number"] = $customerBd->getCustomerNumber();
        } else {
            $clientSellAndSign = json_decode($this->createClientSellAndSign(null, null, null, $client));
            $client["number"] = $clientSellAndSign->number;
        }


        if (!$clientSellAndSign) {
            throw new  Exception("Echec creation client sur sellAndSign");
        }

       ;

        $contrat = json_decode($this->sendPacketSign($client["number"], $signataire, $urlFile, $originalFilename));

        if ($contrat->contract_status === "OPEN")
        {
            $newSignature = new  Signature();
            $newSignature->setCustomerNumber($contrat->customer_number);
            $newSignature->setStatusTranasaction($contrat->contract_status);
            $newSignature->setContratId($contrat->contract_id);
            $newSignature->setCompanyName($data["company_name"]);
            $newSignature->setType(2);
            $newSignature->setSignatureMode(3);
            $newSignature->setFilename($newFilename);
            $newSignature->setCity($data["city"]);
            $newSignature->setAddress1( $data["address_1"]);
            $newSignature->setCellPhoneSignataire($signataire["cell_phone"]);
            $newSignature->setPrenomSignataire($signataire["firstname"]);
            $newSignature->setNomSignataire($signataire["lastname"]);
            $newSignature->setEmailSignataire($signataire["email"]);
            $newSignature->setCivilitySignataire($signataire["civility"]);
            $newSignature->setDateEnvoi((new \DateTime("now")));
            $newSignature->createdAt();

            $em->persist($newSignature);
            $em->flush();



        }


        } catch (Exception $exception) {
            $erreur = $exception->getMessage();
        }


        return new JsonResponse(
            [
                "erreur" => $erreur,

            ]
        );

    }


    /**
     * recuperation de la transaction d'un contrat
     * @param $id_contrat
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTransactionContrat($id_contrat)
    {

        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'contract/read', [

            'query' =>
                [
                    'action' => "getContract",
                    'contract_id' => $id_contrat,
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],


        ]);


        return $response->getBody()->getContents();


    }

    /**
     * recuperation du statut du contrat
     * @param $id_contrat
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStatutContrat($id_contrat)
    {

        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'contractorsforcontract/list', [

            'query' =>
                [
                    'action' => "getContractorsAbout",
                    'contract_id' => $id_contrat,
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],


        ]);


        return $response->getBody()->getContents();


    }

    /** recuperation signataire contrat
     * @param $id_signataire
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSignataireById($id_signataire)
    {

        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'contractor/create', [

            'query' =>
                [
                    'action' => "getOrCreateContractor",
                    'id' => $id_signataire,
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],


        ]);

        return $response->getBody()->getContents();


    }

    /**
     * recuperarion client sell in sign
     * @param $number
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getClientByNumber($number)
    {

        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'customer/read', [

            'query' =>
                [

                    'action' => "getCustomer",
                    'number' => $number,
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],


        ]);

        return $response->getBody()->getContents();


    }

    /**
     * creation d'un client sur sellandsign
     * @param $recruteur
     * @param $ch
     * @param $number
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createClientSellAndSign($recruteur, $ch, $number, $data = null)
    {
        $response = null;
        if ($recruteur != null) {
            $email = explode(",", $ch->getEmail());
            $query = [
                'action' => "selectOrCreateCustomer",
                'number' => $number,
                'customer_code' => "RMS123",
                'company_name' => $ch->getRaisonSocial(),
                'address_1' => $ch->getAdresse(),
                'address_2' => $ch->getComplementAdresse(),
                'postal_code' => $ch->getCp(),
                'city' => $ch->getVille(),
                'country' => "FR",
                'civility' => $recruteur->getUser()->getCivilite(),
                'firstname' => $recruteur->getUser()->getPrenom(),
                'lastname' => $recruteur->getUser()->getNom(),
                'email' => $email[0],
                'cell_phone' => $recruteur->getUser()->getTelephone(),
                'job_title' => $recruteur->getFonction(),
                'j_token' => urldecode(Utiles::tokenSellSign())

            ];
        }

        if ($data)
            $query = $data;


        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'customer/update', [

            'query' => $query


        ]);


        return $response->getBody()->getContents();
    }

    /**
     * recuperation d'un client
     * @param $number
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getClientSellAndSignByNumber($number)
    {

        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'customer/read', [

            'query' =>
                [
                    'number' => $number,
                    'action' => "getCustomer",
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],


        ]);

        return $response->getBody()->getContents();
    }

    /**
     * creation ou recuperation d'un signataire
     * @param $user
     * @param $customer_numer
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOrCreateContractorSellAndSign($user, $customer_numer, $data = null)
    {
        $response = null;
        if ($user) {
            $query = [
                'action' => "getOrCreateContractor",
                'customer_number' => $customer_numer,
                'id' => -1,
                'address_1' => $user->getAdresse(),
                'address_2' => '',
                'postal_code' => $user->getCp(),
                'city' => $user->getVille(),
                'country' => "FR",
                'civility' => $user->getCivilite(),
                'firstname' => $user->getPrenom(),
                'lastname' => $user->getNom(),
                'email' => $user->getEmail(),
                'cell_phone ' => $user->getTelephone(),
                'j_token' => urldecode(Utiles::tokenSellSign())

            ];
        }
        if ($data)
            $query = $data;
        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'contractor/create', [

            'query' => $query

        ]);


        return $response->getBody()->getContents();
    }

    /**
     * presentation contrat pour signature
     * @param $customer_numer
     * @param $message_body
     * @param $message_title
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createContratSellAndSign($customer_numer, $message_body, $message_title)
    {
        $response = null;
        if ($customer_numer != null) {
            $date = new \DateTime("now");

            $currentMilliSecond = (int)(microtime(true) * 1000);


            // 'contract_definition_id'=> 14409,
            //'vendor_email'=> "fo.reseaumedicalservicesprod@calindasoftware.com",
            $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'contract/create', [

                'query' =>
                    [
                        'customer_number' => $customer_numer,
                        'action' => "createContract",
                        'date' => $currentMilliSecond,
                        'vendor_email' => "fo.reseaumedicalservicesprod@calindasoftware.com",
                        'closed' => false,
                        'contract_definition_id' => 9342,
                        'message_title' => $message_title,
                        'message_body' => $message_body,
                        'filename' => "",
                        'keep_on_move' => false,
                        'transaction_id' => "",
                        'customer_entity_id' => -1,
                        'j_token' => urldecode(Utiles::tokenSellSign())

                    ],


            ]);


        }

        return $response->getBody()->getContents();
    }

    /**
     * upload document a faire signé
     * @param $id_contrat
     * @param $interim
     * @param $contrat_pld
     * @return string|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function UploadPdfContrat($id_contrat, $interim, $contrat_pld)
    {
        $file = null;
        $response = null;


        $url = explode("/", $contrat_pld);


        $file = $this->kernel->getProjectDir() . "/public/" . $url[3] . '/' . $url[4] . '/' . $url[5] . '/' . $url[6] . '/' . $url[7] . '/' . $url[8];

        $name = $url[8];


        if ($file != null) {
            $response = $this->httpClient->request('POST', 'https://cloud.sellandsign.com/calinda/hub/selling/do', [

                'query' =>
                    [
                        'm' => "uploadContract",
                        'id' => $id_contrat,
                        'j_token' => urldecode(Utiles::tokenSellSign())

                    ],

                'multipart' => [
                    [
                        'name' => $name,
                        'contents' => fopen($file, 'r'),
                        'filename' => $name,
                        'headers' => [
                            'Content-type' => 'application/pdf'
                        ]

                    ]
                ]


            ]);


            $response = $response->getBody()->getContents();
        }

        return $response;


    }

    /**
     * ajout du signataire de contrat
     * @param $id_contrat
     * @param $id_signataire
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function adSignataireToContrat($id_contrat, $id_signataire, $mode)
    {

        $response = $this->httpClient->request('GET', Utiles::baseUrlSellAndSign() . 'contractorsforcontract/insert', [

            'query' =>
                [
                    'action' => "addContractorTo",
                    'contract_id' => $id_contrat,
                    'contractor_id' => $id_signataire,
                    'signature_mode' => $mode,
                    'signature_status' => "NONE",
                    'signature_id' => "",
                    'signature_date' => "",
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],


        ]);


        return $response->getBody()->getContents();


    }

    /**
     * mise en signature du contrat
     * @param $id_contrat
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendContratToSign($id_contrat)
    {


        try {
            $response = $this->httpClient->request('GET', 'https://cloud.sellandsign.com/calinda/hub/selling/do', [

                'query' =>
                    [
                        'm' => "contractReady",
                        'c_id' => $id_contrat,
                        'j_token' => urldecode(Utiles::tokenSellSign())

                    ]


            ]);


            return $response->getBody()->getContents();
        } catch (Exception $exception) {
            throw new Exception("information signataire invalide,Veillez verifier si le nom ou prenom ne contient pas des caractères spéciaux");
        }


    }

    public function getSignataireOtherDocs($clientNumber, $signataire, $file_name, $mail_objet)
    {

        $response = [
            "customer" => [
                "number" => $clientNumber,
                "mode" => 3,
                "contractor_id" => -1,
                "vendor" => "vendor_reseaumedicalservicesprod@calindasoftware.com",
                "fields" => [
                    [
                        "key" => "firstname",
                        "value" => $signataire["firstname"],

                    ],
                    [
                        "key" => "lastname",
                        "value" => $signataire["lastname"]
                    ],
                    [
                        "key" => "civility",
                        "value" => $signataire["civility"] // MONSIEUR ou MADAME
                    ],
                    [
                        "key" => "email",
                        "value" => $signataire["email"],
                    ],
                    [
                        "key" => "cellPhone",
                        "value" => $signataire["cell_phone"]
                    ]


                ]


            ],
            "contractors" => [],
            "contract" => [
                "contract_definition_id" => 9342,
                "pdf_file_path" => $file_name, // le nom du fichier pdf tel qu'il est donné dans le paquet http
                "contract_id" => -1,
                "message_title" => $mail_objet,
                "message_body" => "Vous êtes signataire du contrat  ci-joint"
            ],

            "contract_properties" => [
                [
                    "key" => "internal_contract_id", // une clé pour votre identifiant interne du contrat, la valeur pourra vous être donné lors du retour du fichier signé
                    "value" => "", // la valeur de la clé
                    "to_fill_by_user" => 0
                ],
                [
                    "key" => "callback:url",
                    "value" => ""
                ]
            ],

            "files" => [],
            "options" => [],

            "to_sign" => 1

        ];

        return json_encode($response);

    }

    public function sendPacketSign($clientNumer, $signataire, $urlFile, $fileName)

    {

        $response = $this->httpClient->request('POST', 'https://cloud.sellandsign.com/calinda/hub/selling/do', [


            'query' =>
                [
                    'm' => "sendCommandPacket",
                    'j_token' => urldecode(Utiles::tokenSellSign())

                ],

            'multipart' => [
                [
                    'name' => 'adhoc_light.sellsign',
                    'contents' => $this->getSignataireOtherDocs($clientNumer, $signataire, $fileName, "Signature docs"),
                    'filename' => 'adhoc_light.sellsign',
                    'headers' => [
                        'Content-type' => 'application/json'
                    ]
                ],
                [
                    'name' => $fileName,
                    'contents' => fopen($urlFile, 'r'),
                    'filename' => $fileName,
                    'headers' => [
                        'Content-type' => 'application/pdf'
                    ]
                ]
            ]
        ]);


        return $response->getBody()->getContents();
    }
}