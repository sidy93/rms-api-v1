<?php


namespace App\Controller;


use App\Entity\Disponibilite;
use App\Entity\Interim;
use App\Events\HistoriqueSubscriber;
use App\Service\SendinblueService;
use App\Service\Utiles;
use ContainerDjgv594\getConsole_Command_AboutService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DisponibiliteController
{

    private $mois = array(
        "Janvier", "Février", 'Mars', "Avril", "Mai", "Juin", "Juillet", "Août", "septembre", "Octobre", "Novembre", "Décembre"

    );

    /**
     * @var EntityManagerInterface
     */
    private $em;
    private $mail;
    private $eventHistory;

    public function __construct(EntityManagerInterface $entityManager, SendinblueService $mail, HistoriqueSubscriber $eventHistory)
    {
        $this->em = $entityManager;
        $this->mail = $mail;
        $this->eventHistory = $eventHistory;
    }

    /**
     * @param Interim $data
     */
    public function __invoke(Interim $data, Request $request)
    {

        if ($request->isMethod("get")) {
            $erreur = null;
            $succes = null;
            $response = null;


            try {
                $periode = array();


                foreach ($data->getDisponibilites() as $disponibilite) {


                    if ($disponibilite->getDate() < new  DateTime("now")) {
                        $this->em->remove($disponibilite);
                        $this->em->flush();
                        continue;
                    }

                    $number = (int)$disponibilite->getDate()->format('n');

                    array_push($periode, $this->mois[$number - 1] . '-' . $disponibilite->getDate()->format("Y"));


                    $response  [$this->mois[$number - 1] . '-' . $disponibilite->getDate()->format("Y")] [] = [


                        $disponibilite->getDate()->format("Y-m-d")


                    ];


                }


            } catch (Exception $exception) {
                $erreur = $exception->getMessage();
            }

            return new JsonResponse(
                [
                    "data" => $response,
                    "periode" => $periode,
                    "erreur" => $erreur,
                    "success" => $succes
                ]
            );

        } else if ($request->isMethod("post")) {
            $erreur = null;
            $response = null;
            $succes = null;

            try {

                $contentRequest = json_decode($request->getContent());

                $newData = array_diff($contentRequest->date, $data->getCustomDisponibilite());
                $deleteData = array_diff($data->getCustomDisponibilite(), $contentRequest->date);
                if ($contentRequest->delete == true) {
                    $this->updateDisponibliteByInterim($contentRequest->date, $data, true);
                } else {
                    if (count($deleteData) > 0 && $contentRequest->edit === true) {

                        $this->updateDisponibliteByInterim($deleteData, $data, true);
                    }
                    if (count($newData) > 0) {
                        $this->updateDisponibliteByInterim($newData, $data, false, $contentRequest->mail);
                    }

                }


                $succes = "Mis a jour effectuer avec success";
            } catch (Exception $exception) {
                $erreur = Utiles::messageErrorServer() . $exception->getMessage();
            }

            return new  JsonResponse(
                [
                    "success" => $succes,
                    "erreur" => $erreur,
                    "data" => $response
                ]
            );

        }


    }


    public function updateDisponibliteByInterim($disponiblites, $interim, $delete, $mail = false)
    {
        $roles = $this->eventHistory->getRoles($this->eventHistory->getSecurity()->getUser()->getRoles());
        $nomComplet = $interim->getUser()->getNom() . ' ' . $interim->getUser()->getPrenom();
        $idUser = $interim->getUser()->getId();
        foreach ($disponiblites as $d) {
            $dispoExiste = $this->em->getRepository(Disponibilite::class)->findOneBy(
                array(
                    "interim" => $interim,
                    "date" => new DateTime($d)
                )
            );
            if ($delete && $dispoExiste) {
                $historyItem = [
                    'cible' => $nomComplet,
                    'element' => "suppression de la disponibilité $d",
                    'cible_id' => $interim->getUser()->getId()
                ];
                $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                $this->em->remove($dispoExiste);

            } else if (!$dispoExiste) {
                $d = (new DateTime($d))->format('d-m-Y');
                $historyItem = [
                    'cible' => $nomComplet,
                    'element' => "nouvelle disponibilité $d",
                    'cible_id' => $interim->getUser()->getId()
                ];
                $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                $disponibilite = new  Disponibilite();
                $disponibilite->setDate(new DateTime($d));
                $disponibilite->createdAt();
                $disponibilite->setInterim($interim);
                $this->em->persist($disponibilite);

            }


        }

        if ($mail == true) {
            $template = "emails/erp/interim/confirmation_disponibilte.html.twig";
            $this->mail->sendNotificationMail(
                $interim->getUser()->getEmail(),
                $interim->getUser()->getNom(),
                false,
                null,
                $template,
                [],
                "Mis a jour disponibilité",
                "update dispo");
        }
        $this->em->flush();


    }

    /**
     * @Route("api/disponibilites_list", name ="listes_disponibilites_interims", methods={"get"})
     */
    public function  list(Request $request)
    {
        $erreur = null;
        $data = null;
        try {

            $data = $this->em->getRepository(Disponibilite::class)->findAllVisibleQueryDisponibilite($request->query->all());


        } catch (Exception $exception) {
            $erreur = $exception->getMessage();
        }

        return new JsonResponse([
            "data" => $data,
            "erreur" => $erreur
        ]);

    }
}
