<?php


namespace App\Controller;

use App\Entity\Gestionnaire;
use App\Entity\GroupeRoles;
use App\Entity\User;
use App\Service\Utiles;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class UserController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * SpecialiteInterimController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("api/users/delete", name ="delete_users")
     */
    public function deleteUsersAction(Request $request)
    {
        $userId = json_decode($request->getContent());
        $response = [
            'success' => [],
            'erreur' => [],
        ];
        foreach ($userId as $id){
            $user = $this->em->getRepository(User::class)->find($id);
            if($user){
                $email = $user->getEmail();
                $nomComplet = $user->getPrenom() . ' ' . $user->getNom();
                if (sizeof($user->getGroupeRoles()) == 0){
                    $response['erreur'] = 'user simple';

                }
                else if($user->getGroupeRoles() && Utiles::isRecruteur($user)){
                    $response['erreur'] = 'recruteur';

                }
                else if($user->getGroupeRoles() && Utiles::isInterimaire($user)){
                    $response['erreur'] = 'interim';

                }
                else if($user->getGroupeRoles() && !Utiles::isRecruteur($user) && !Utiles::isInterimaire($user)){
                    if(!$this->deleteGiByUser($user)){
                        array_push($response['erreur'], $nomComplet);
                    }
                    else {
                        array_push($response['success'], $nomComplet);
                    }
                }

            } else{
                $response['erreur'] = $user;
            }
        }
        return new JsonResponse($response);
    }
    /**
     * @Route("api/users/reinitialisation-pwd", name ="reinitialisation_pwd")
     */
    public function reinitialisationPwdAction(Request $request)
    {
        $userId = json_decode($request->getContent());
        $response = [
            'success' => [],
            'erreur' => [],
        ];
        foreach ($userId as $id){
            $user = $this->em->getRepository(User::class)->find($id);
            if($user){
                $email = $user->getEmail();
                $nomComplet = $user->getPrenom() . ' ' . $user->getNom() . ' (' . $email . ')';
                $password = Utiles::generatePassword(8);
                $subject = "DEMANDE DE REINITIALISATION MDP";
                $template_value["pwd"] = $password;
                array_push($response['success'], $nomComplet);
            } else{
                $response['erreur'] = "Aucun compte trouvé !";
            }
        }
        return new JsonResponse($response);
    }
    /**
     * @Route("api/users/update-groupe-roles", name ="update_groupe_roles")
     */
    public function updateGroupeRolesAction(Request $request)
    {
        $response = [
            'success' => [],
            'erreur' => [],
        ];
        $data = json_decode($request->getContent());
        $usersId = $data->usersId;
        $idGroupeRole = $data->groupeRoles;
        $groupeRole = $this->em->getRepository(GroupeRoles::class)->find($idGroupeRole);
        foreach ($usersId as $id) {
            $user = $this->em->getRepository(User::class)->find($id);
            if ($user) {
                sizeof($user->getGroupeRoles()) > 0 ? $user->removeGroupeRole($user->getGroupeRoles()[0]) : null;
                $user->addGroupeRole($groupeRole);
                $this->em->persist($user);
            }
        }
        $this->em->flush();
        return new JsonResponse($response);
    }

    public function deleteGiByUser($user){
        $rep = false;
        $gi = $this->em->getRepository(Gestionnaire::class)->findOneBy(['user' => $user->getId()]);
        try {
            $this->em->remove($gi);
            $rep = true;
            $this->em->flush();
        } catch (\Exception $exception){
            $rep = false;
        }
        return $rep;
    }
    public function deleteInterimaireByUser($user){}
    public function deleteRecruteurByUser($user){}
    public function deleteUser($user){}
}
