<?php

namespace App\Controller;

use App\Events\HistoriqueSubscriber;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HistoriqueController extends AbstractController
{
    private $eventHistory;

    /**
     * HistoriqueController constructor.
     */
    public function __construct(HistoriqueSubscriber $eventHistory)
    {
        $this->eventHistory = $eventHistory;
    }

    /**
     * @Route("/historique", name="historique")
     */
    public function index()
    {
        return $this->render('historique/index.html.twig', [
            'controller_name' => 'HistoriqueController',
        ]);
    }

    /**
     * @Route("api/historiques", name="historiques")
     */
    public function historiquesAction(Request $request)
    {
        $filesystem = new Filesystem();
        $annee = date('Y');
        $jour = date("l");
        $mois = date("F");
        // le mois on le formate avec la fonction (monthOrDayFrench) en specifiant true si cest le mois ou false si cest le jour
        $mois = $this->eventHistory->monthOrDayFrench($mois, 1);
        // nomenclature du ficher de base
        $fichier = 'historique/' . $annee . '/' . $mois . '/' . $this->eventHistory->monthOrDayFrench($jour, 0) . '_' . date('d') . '.json';

        if ($filesystem->exists($fichier)) {
            $data = file_get_contents($fichier);
            if(is_array(json_decode($data))){
                $data = json_decode($data) ? json_decode($data) : 'erreur';
            }else{
                $data = 'erreur !!! l\'historique est soit vide ou mal renseigné';
            };
        } else {
            $data = [];
        }
//        dd($data);
        return new JsonResponse($data);
    }

    /**
     * @Route("/historiques/getHistoryByPeriode",name="historiques_periodes")
     */
    public function historiquesByPerideAction(Request $request)
    {
        $utilesService = $this->container->get('utiles.service');
        $filesystem = new Filesystem();
        $dateModif = "";
        $tmp = json_decode($request->getContent());
        $dateDebut = new DateTime($tmp[0]);
        date_modify($dateDebut, '-1 day');
        $dateFin = new DateTime($tmp[1]);
        $arrayData = [];
        // on parcours lensemble des dates avec leurs fichiers correspondants
        do {
            $dateModif = date_modify($dateDebut, '+1 day');
            $annee = $dateModif->format('Y');
            $jour = ($dateModif->format('l'));
            $mois = ($dateModif->format('F'));
            // le mois on le formate avec la fonction (monthOrDayFrench) en specifiant true si cest le mois ou false si cest le jour
            $mois = $this->eventHistory->monthOrDayFrench($mois, 1);
            // nomenclature du ficher de base
            $fichier = 'historique/' . $annee . '/' . $mois . '/' . $this->eventHistory->monthOrDayFrench($jour, 0) . '_' . $dateModif->format('d') . '.json';
            if ($filesystem->exists($fichier)) {
                $data = json_decode(file_get_contents($fichier));
                if ($data) {
                    $arrayData = array_merge($arrayData, $data);
                }
            }
        } while ($dateFin > $dateModif);
        return new JsonResponse($arrayData);
    }
}
