<?php


namespace App\Controller;
use App\Entity\Candidature;
use App\Entity\Contrat;
use App\Entity\Justificatif;
use App\Entity\Renumeration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class JustificatifController extends AbstractController
{
    private $em;
    private $params;
    public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->em = $entityManager;
    }


    /**
     * @Route("api/renumerations/show-refractor/{idInterim}", name ="show_refractor_details_renumeration", methods={"get"})
     */
    public function showRefractorAction(Request $request, $idInterim)
    {
        $response = [];
        $details = $this->em->getRepository(Renumeration::class)->returnNbreContratByidInterim($idInterim);
        if($details){
            foreach ($details as $item){
                $item['sellAndSign'] = $this->returnEtatSignatureContrat($item['idContratPld']);
                $item['totalVacation'] = sizeof($this->returnCandidatureByContratErp($item['idContratErp']));
                array_push($response, $item);
            }
        }
        return new JsonResponse($response);
    }
}
