<?php


namespace App\Controller;


use App\Entity\FicheSpecialite;
use App\Entity\Interim;
use App\Entity\Mission;
use App\Service\Utiles;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OffresInterimController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param Interim $data
     * @param Request $request
     * @param Utiles $utiles
     * @return JsonResponse
     */
    public function __invoke(Interim $data, Request $request,Utiles $utiles)
    {

        if ($request->isMethod("get")) {

            $erreur = null;
            $succes = null;
            $response = null;


            try {


                 $qualifications = [];

                foreach ($data->getQualifications() as $qualification)
                {


                    array_push($qualifications,$qualification->getId());
                }



               $missions = $this->em->getRepository(Mission::class)->getMissionAdPlanningAPourvoir($qualifications);






                $data =  $utiles->refractor_json_offre($this->em,$missions,$data->getId());


            }
            catch (\Exception $exception)
            {

                $erreur = $exception->getMessage();
            }

            return new JsonResponse(
                [
                    "erreur" => $erreur,
                    "data" => $data
                ]
            );

        }


    }


}
