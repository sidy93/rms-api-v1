<?php


namespace App\Controller;

use App\Entity\ComplementDossier;
use App\Entity\Dossier;
use App\Entity\Gestionnaire;
use App\Entity\Interim;
use App\Entity\Tache;
use App\Entity\TacheGestionnaire;
use App\Entity\User;
use App\Events\HistoriqueSubscriber;
use App\Service\SendinblueService;
use App\Service\Utiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class DossierController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    private $params;
    private $eventHistory;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager, HistoriqueSubscriber $eventHistory)
    {
        $this->params = $params;
        $this->em = $entityManager;
        $this->eventHistory = $eventHistory;
    }

    /**
     * @param Dossier $dossier
     */
    public function __invoke(Dossier $dossier, Request $request)
    {
    }

    /**
     * @Route("api/dossiers/{id}", name ="show_dossier_by_id_user", methods={"get"})
     */
    public function showAction(Int $id, Request $request)
    {
        $userId = json_decode($request->getContent());
        $response = [
            'success' => [],
            'erreur' => [],
        ];
        $complementsDossier = [];
        $dossier = $this->em->getRepository(Dossier::class)->findOneBy(['user' => $id]);
        if ($dossier) {
            $complements = $this->em->getRepository(ComplementDossier::class)->findBy(['dossier' => $dossier->getId()]);
            if ($complements) {
                foreach ($complements as $complement) {
                    array_push($complementsDossier, $complement->complementsDossierMed());
                }
            }
            $response['data'] = ['dossier' => true, 'items' => $dossier->dossierMed(), 'complements' => $complementsDossier];
        } else {
            $dossier = new Dossier();
            $response['data'] = ['dossier' => false, 'items' => $dossier->attributs()];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("api/delete-element-dossiers/{id}", name ="delete_item_dossier_by_id_user", methods={"post"})
     */
    public function deleteAction(Int $id, Request $request)
    {
        $roles = $this->eventHistory->getRoles($this->eventHistory->getSecurity()->getUser()->getRoles());
        $idUser = $id;
        $userInterim = $this->em->getRepository(Interim::class)->findBy(array('user' => $idUser))[0];
        $nomComplet = $userInterim->getUser()->getNom() . ' ' . $userInterim->getUser()->getPrenom();
        $userId = json_decode($request->getContent());
        $response = [
            'success' => '',
            'erreur' => '',
        ];
        $host = $request->getSchemeAndHttpHost();

        $dossier = $this->em->getRepository(Dossier::class)->findOneBy(['user' => $id]);
        if ($dossier) {
            try {
                $item = $request->getContent();
                $itemTmp = $request->getContent();
                if (strpos($item, '_complement_dossier')) {
                    $complementDossier = $this->em->getRepository(ComplementDossier::class)->findOneBy(['dossier' => $dossier->getId(), 'diplomeName' => chop($item, '_complement_dossier')]);
                    if ($complementDossier) {
                        try {
                            $file = str_replace($host . '/', "", $complementDossier->getDiplomeFileName());
                            $file ? unlink($file) : null;
                        } catch (\Exception $exception) {
                        }
                        $historyItem = [
                            'cible' => $nomComplet,
                            'element' => "Suppression d'element du complément du dossier - $itemTmp",
                            'cible_id' => $idUser,
                            'table' => 'Dossier',
                        ];
                        $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                        $this->em->remove($complementDossier);
                    }
                } else {
                    try {
                        $file = str_replace($host . '/', "", $dossier->getValueByItem($item));
                        $file ? unlink($file) : null;
                    } catch (\Exception $exception) {
                    }
                    $historyItem = [
                        'cible' => $nomComplet,
                        'element' => "Suppression d'element du complément du dossier - $itemTmp",
                        'cible_id' => $idUser,
                        'table' => 'Dossier',
                    ];
                    $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                    $dossier->deleteItem($item);
                    $this->em->persist($dossier);
                }
                $response['success'] = "la suppression est effective !";
                $this->em->flush();
            } catch (\Exception $exception) {
                $msg = $exception->getMessage();
                $response['erreur'] = "Echec de la suppression, $msg !";
            }
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("api/dossiers/{id}", name ="save_dossier", methods={"post"})
     */
    public function saveDossierAction($id, Request $request, SendinblueService  $mail)
    {
        $response = [
            'success' => '',
            'erreur' => '',
        ];

        $user = $this->em->getRepository(User::class)->find($id);

        if ($user) {
            $roles = $this->eventHistory->getRoles($this->eventHistory->getSecurity()->getUser()->getRoles());

            $nomComplet = $user->getNom() . ' ' . $user->getPrenom();
            $idUser = $this->eventHistory->getSecurity()->getUser()->getId();
            $data = json_decode($request->getContent());

            $host = $request->getSchemeAndHttpHost();
            $files = $request->files;
            $folderInterimaires = $this->params->get("folder_files") . "/Dr_" . $user->getNom() . "_" . $user->getId() . "/Dossier/";
            $dossierMed = $this->em->getRepository(Dossier::class)->findOneBy(['user' => $user->getId()]);
            if ($dossierMed) {
                $dossier = $dossierMed;
            } else {
                $dossier = new Dossier();
                $dossier->setUser($user);
            }
            try {
                if ($files) {
                    foreach ($files as $key => $file) {
                        $filename = $file->getClientOriginalName();
                        $keyTmp = $key;
                        if (strpos($key, '_complement_dossier')) {
                            $complementDossier = $this->em->getRepository(ComplementDossier::class)->findOneBy(['dossier' => $dossier->getId(), 'diplomeName' => $filename]);
                            if ($complementDossier) {
                                $complementDossier->setDiplomeFileName($host . '/' . $folderInterimaires . 'diplomes/' . $filename);
                            } else {
                                $complementDossier = new ComplementDossier();
                                $complementDossier->setDossier($dossier);
                                $complementDossier->setDiplomeName($filename);
                                $complementDossier->setDiplomeFileName($host . '/' . $folderInterimaires . 'diplomes/' . $filename);
                            }
                            $file->move($folderInterimaires . 'diplomes/', $filename);
                            $historyItem = [
                                'cible' => $nomComplet,
                                'element' => "Ajout d'elements du complément du dossier - $keyTmp",
                                'cible_id' => $idUser,
                                'table' => 'complement_dossier',
                            ];
                            $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                            $this->em->persist($complementDossier);
                        } else {
                            $historyItem = [
                                'cible' => $nomComplet,
                                'element' => "Ajout d'elements du dossier - $keyTmp",
                                'cible_id' => $idUser,
                                'table' => 'Dossier',
                            ];
                            $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                            $dossier->UpdateItem($key, $host . '/' . $folderInterimaires . '' . $filename);
                            $file->move($folderInterimaires, $filename);
                        }
                    }
                    $dossier->setEnable(false);
                    $dossier->setEtat(false);
                    $this->em->persist($dossier);

                    $response['success'] = "Vous avez mis à jour le dossier du Dr " . $user->getNom() . " avec succès !";

                }
                if ($data) {
                    $dossier->setEtat($data->value);
                    $dossier->setRemarques($data->remarques);
                    $historyItem = [
                        'cible' => $nomComplet,
                        'element' => "Vous avez activé le dossier du Dr" . $user->getNom(),
                        'cible_id' => $idUser,
                        'table'=>"Dossier"
                    ];
                        if ($data->mail)
                        {
                            $template =  "emails/erp/interim/validation_dossier.html.twig";
                            $mail->sendNotificationMail(
                                $user->getEmail(),
                                $user->getNom(),
                                false,
                                null,
                                $template,
                                [],
                                "VALIDATION DE VOTRE DOSSIER RMS",
                                "activation dossier");
                        }


                    $this->eventHistory->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, 'put');
                    $this->em->persist($dossier);
                    $data->value ? $response['success'] = "Vous avez activé le dossier du Dr " . $user->getNom() . " avec succès !" : $response['success'] = "Vous avez désactivé le dossier du Dr " . $user->getNom() . " avec succès !";
                }
                $this->em->flush();
            } catch (\Exception $exception) {
                $response['erreur'] = $exception->getMessage();

            }
        }
        return new JsonResponse($response);
    }

}
