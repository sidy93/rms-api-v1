<?php


namespace App\Controller;
use App\Entity\Interim;
use App\Entity\Justificatif;
use App\Entity\Piece;
use App\Entity\ReleveHeure;
use App\Entity\Renumeration;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ReleveHeureController extends AbstractController
{
    private $em;
    private $params;
    public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->em = $entityManager;
    }


    /**
     * @Route("api/releve_heures", name ="add_releve_dheure", methods={"post"})
     */
    public function newAction(Request $request)
    {
        $response = [];
        $data = json_decode($request->getContent());
        foreach ($data->rh as $rh){
            $contratPldResultat = (new PldController())->editRhAction($rh);
            if($contratPldResultat->Code){
                $rh = $this->em->getRepository(ReleveHeure::class)->findOneBy(['rhPld' => $contratPldResultat->ID]);
                if(!$rh){
                    $rh = new ReleveHeure();
                    $remuneration = $this->em->getRepository(Renumeration::class)->find($data->others_data->id);
                    $rh->setRemuneration($remuneration);
                    $rh->setRhPld($contratPldResultat->ID);
                    $this->em->persist($rh);
                    $this->em->flush();
                }
                $response = $contratPldResultat;
            }
        }
        return new JsonResponse($response);
    }
}
