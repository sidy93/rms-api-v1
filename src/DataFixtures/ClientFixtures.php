<?php

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ClientFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker= Factory::create('fr_FR');

        for ($i=0; $i<=100 ; $i++)
        {
            $client = new Client();
            $client->setNomEtablissement($faker->company);
            $client->setRaisonSocial($faker->company);
            $client->setType($faker->numberBetween(0,1));
            $client->setActiver($faker->numberBetween(0,1));
            $client->setAdresse($faker->address);
            $client->setComplementAdresse($faker->address);
            $client->setCible(false);
            $client->setCp($faker->postcode);
            $client->setEmail('test');
            $client->setNaf($faker->email);
            $client->setDepartement($faker->city);
            $client->setVille($faker->city);
            $client->setTelephone($faker->phoneNumber);
            $client->createdAt($faker->dateTimeBetween('-6 months'));

            $manager->persist($client);

           


        }
        $manager->flush();
    }

}
