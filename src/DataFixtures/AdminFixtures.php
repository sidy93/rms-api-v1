<?php

namespace App\DataFixtures;

use App\Entity\Ressource;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AdminFixtures extends Fixture
{
    private  $ressource = array(
        array('id' => '1','nom' => 'HOSPIMEDIA','prenom' => '','raison_social' => 'HOSPIMEDIA','url' => 'https://www.staffsante.fr/','type' => '0','telephone' => ''),
        array('id' => '2','nom' => 'DIRECTEMPLOI','prenom' => '','raison_social' => 'DIRECTEMPLOI','url' => 'https://https://www.directemploi.com/','type' => '0','telephone' => ''),
        array('id' => '3','nom' => 'DOC112','prenom' => '','raison_social' => 'DOC112','url' => 'https://http://www.doc112.com/','type' => '0','telephone' => ''),
        array('id' => '4','nom' => 'ANNONCES MEDICALES','prenom' => '','raison_social' => 'ANNONCES MEDICALES','url' => 'https://https://www.annonces-medicales.com/','type' => '0','telephone' => ''),
        array('id' => '5','nom' => 'COLLEGUE','prenom' => '','raison_social' => 'COLLEGUE','url' => 'https://','type' => '0','telephone' => ''),
        array('id' => '6','nom' => 'SITE','prenom' => '','raison_social' => 'SITE','url' => 'https://https://reseaumedical.fr/','type' => '0','telephone' => ''),
        array('id' => '7','nom' => 'CLIENT','prenom' => '','raison_social' => 'CLIENT','url' => 'https://','type' => '0','telephone' => ''),
        array('id' => '8','nom' => 'STAFFSANTE','prenom' => '','raison_social' => 'STAFFSANTE','url' => 'https://https://www.staffsante.fr/','type' => '0','telephone' => '')
    );
    public function load(ObjectManager $manager)
    {


       for ($i=0; $i<count($this->ressource); $i++)
       {
           $source = new Ressource();
           $source->setType($this->ressource[$i]["type"]);
           $source->setTelephone($this->ressource[$i]["telephone"]);
           $source->setRaisonSocial($this->ressource[$i]["raison_social"]);
           $source->setNom($this->ressource[$i]["nom"]);
           $source->setPrenom($this->ressource[$i]["prenom"]);
           $source->setUrl($this->ressource[$i]["url"]);

           $manager->persist($source);
       }

        $manager->flush();
    }
}
