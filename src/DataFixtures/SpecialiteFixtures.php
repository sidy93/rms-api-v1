<?php

namespace App\DataFixtures;

use App\Entity\Qualification;
use App\Entity\Specialite;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SpecialiteFixtures extends Fixture
{
private  $qualifications = array(
    array('id' => '1','specialite_id' => '1','qualification' => 'SAU','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '4','specialite_id' => '3','qualification' => 'REANIMATION','id' => '3','nomspecialite' => 'REANIMATION','specialite' => 'REANIMATEUR'),
    array('id' => '8','specialite_id' => '2','qualification' => 'BLOC OPERATOIRE:OBSTETRIQUE(ANESTHESIE PERIDURALE)','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '9','specialite_id' => '2','qualification' => 'BLOC OPERATOIRE:ORL','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '10','specialite_id' => '2','qualification' => 'BLOC OPERATOIRE:OPHTALMOLOGIE','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '11','specialite_id' => '2','qualification' => 'ANESTHESIE GENERALE','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '12','specialite_id' => '2','qualification' => 'ANESTHESIE LOCO REGIONALES','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '13','specialite_id' => '2','qualification' => 'REANIMATION','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '14','specialite_id' => '2','qualification' => 'GESTION SSPI','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '15','specialite_id' => '2','qualification' => 'PRESCRIPTIONS POST OPERATOIRES','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '16','specialite_id' => '2','qualification' => 'CONSULTATIONS(Service Chirurgie, Urgences)','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '18','specialite_id' => '4','qualification' => 'GERIATRIE','id' => '4','nomspecialite' => 'GERIATRIE','specialite' => 'GERIATRE'),
    array('id' => '19','specialite_id' => '5','qualification' => 'cardiologie','id' => '5','nomspecialite' => 'CARDIOLOGIE','specialite' => 'CARDIOLOGUE'),
    array('id' => '20','specialite_id' => '7','qualification' => 'Pneumologie','id' => '7','nomspecialite' => 'PNEUMOLOGIE','specialite' => 'PNEUMOLOGUE'),
    array('id' => '22','specialite_id' => '9','qualification' => 'PEDIATRIE POLYVALENTE','id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
    array('id' => '23','specialite_id' => '9','qualification' => 'NEONATOLOGIE','id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
    array('id' => '24','specialite_id' => '9','qualification' => 'Conditionnement nouveau né avant transfert niveau III','id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
    array('id' => '25','specialite_id' => '9','qualification' => ' NIVEAU 1','id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
    array('id' => '27','specialite_id' => '9','qualification' => 'NIVEAU 2A','id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
    array('id' => '28','specialite_id' => '9','qualification' => 'NIVEAU 2B','id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
    array('id' => '29','specialite_id' => '9','qualification' => 'NIVEAU  3','id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
    array('id' => '31','specialite_id' => '11','qualification' => 'ALCOOLOGIE','id' => '11','nomspecialite' => 'ALCOOLOGIE','specialite' => 'ALCOOLOGUE'),
    array('id' => '32','specialite_id' => '12','qualification' => 'Radiologie Conventionnelle','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '33','specialite_id' => '12','qualification' => 'Echographie','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '34','specialite_id' => '12','qualification' => 'Senologie','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '35','specialite_id' => '12','qualification' => 'Scanner','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '36','specialite_id' => '12','qualification' => 'IRM','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '37','specialite_id' => '12','qualification' => 'Interventionnelle','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '38','specialite_id' => '12','qualification' => 'Cardiaques en coupe','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '39','specialite_id' => '12','qualification' => 'Echographie de contraste','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '40','specialite_id' => '12','qualification' => 'Mamographie','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '41','specialite_id' => '12','qualification' => 'Forcomed','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '42','specialite_id' => '13','qualification' => 'MEDECINE GENERALE','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '43','specialite_id' => '14','qualification' => 'rhumatologie','id' => '14','nomspecialite' => 'RHUMATOLOGIE','specialite' => 'RHUMATOLOGUE'),
    array('id' => '44','specialite_id' => '15','qualification' => 'Medecine polyvalente','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '45','specialite_id' => '16','qualification' => 'GASTROENTEROLOGIE','id' => '16','nomspecialite' => 'GASTROENTEROLOGIE','specialite' => 'GASTROENTEROLOGUE'),
    array('id' => '46','specialite_id' => '17','qualification' => 'MEDECINE INTERNE','id' => '17','nomspecialite' => 'MEDECINE INTERNE','specialite' => 'MEDECIN INTERNISTE'),
    array('id' => '47','specialite_id' => '18','qualification' => 'GYNECOLOGIE OBSTETRIQUE','id' => '18','nomspecialite' => 'GYNECOLOGIE OBSTETRIQUE','specialite' => 'GYNECOLOGUE OBSTETRICIEN'),
    array('id' => '48','specialite_id' => '19','qualification' => 'CHIRURGIE ORTHOPEDIQUE','id' => '19','nomspecialite' => 'CHIRURGIE ORTHOPEDIQUE','specialite' => 'CHIRURGIEN ORTHOPEDISTE'),
    array('id' => '49','specialite_id' => '20','qualification' => 'ORTHOPEDIE','id' => '20','nomspecialite' => 'ORTHOPEDIE','specialite' => 'ORTHOPEDISTE'),
    array('id' => '50','specialite_id' => '21','qualification' => 'CHIRURGIE VISCERALE','id' => '21','nomspecialite' => 'CHIRURGIE VISCERALE','specialite' => 'CHIRURGIEN VISCERALE'),
    array('id' => '51','specialite_id' => '22','qualification' => 'BIOLOGIE','id' => '22','nomspecialite' => 'BIOLOGIE','specialite' => 'BIOLOGISTE'),
    array('id' => '52','specialite_id' => '23','qualification' => 'PSYCHIATRIE','id' => '23','nomspecialite' => 'PSYCHIATRIE','specialite' => 'PSYCHIATRE'),
    array('id' => '53','specialite_id' => '24','qualification' => 'CHIRURGIE DISGESTIVE','id' => '24','nomspecialite' => 'CHIRURGIE DISGESTIVE','specialite' => 'CHIRURGIEN DIGESTIF'),
    array('id' => '54','specialite_id' => '25','qualification' => 'CHIRURGIE GENERALE','id' => '25','nomspecialite' => 'CHIRURGIE GENERALE','specialite' => 'CHIRURGIEN GENERALISTE'),
    array('id' => '55','specialite_id' => '26','qualification' => 'NEPHROLOGIE','id' => '26','nomspecialite' => 'NEPHROLOGIE','specialite' => 'NEPHROLOGUE'),
    array('id' => '57','specialite_id' => '28','qualification' => 'ONCOLOGIE','id' => '28','nomspecialite' => 'ONCOLOGIE','specialite' => 'ONCOLOGUE'),
    array('id' => '58','specialite_id' => '29','qualification' => 'OPHTALMOLOGIE','id' => '29','nomspecialite' => 'OPHTALMOLOGIE','specialite' => 'OPHTALMOLOGUE'),
    array('id' => '59','specialite_id' => '30','qualification' => 'ENDOCRINOLOGIE','id' => '30','nomspecialite' => 'ENDOCRINOLOGIE','specialite' => 'ENDOCRINOLOGUE'),
    array('id' => '61','specialite_id' => '32','qualification' => 'ADDICTOLOGIE','id' => '32','nomspecialite' => 'ADDICTOLOGIE','specialite' => 'ADDICTOLOGUE'),
    array('id' => '62','specialite_id' => '33','qualification' => 'ORL','id' => '33','nomspecialite' => 'ORL','specialite' => 'ORL (OTO RHINO LARYNGOLOGISTE)'),
    array('id' => '63','specialite_id' => '34','qualification' => 'HEMATOLOGIE','id' => '34','nomspecialite' => 'HEMATOLOGIE','specialite' => 'HEMATOLOGUE'),
    array('id' => '64','specialite_id' => '35','qualification' => 'CHIRURGIE VASCULAIRE','id' => '35','nomspecialite' => 'CHIRURGIE VASCULAIRE','specialite' => 'CHIRURGIE VASCULAIRE'),
    array('id' => '65','specialite_id' => '36','qualification' => 'GERONTOLOGIE','id' => '36','nomspecialite' => 'GERONTOLOGIE','specialite' => 'GÉRONTOLOGUE'),
    array('id' => '66','specialite_id' => '37','qualification' => 'MEDECIN DU TRAVAIL','id' => '37','nomspecialite' => 'MEDECIN DU TRAVAIL','specialite' => 'MEDECINE DU TRAVAIL'),
    array('id' => '67','specialite_id' => '38','qualification' => 'MPR','id' => '38','nomspecialite' => 'MPR','specialite' => 'MEDECIN PHYSIQUE ET READAPTATION'),
    array('id' => '69','specialite_id' => '40','qualification' => 'UROLOGIE','id' => '40','nomspecialite' => 'NEUROCHIRURGIE','specialite' => 'NEUROCHIRURGIEN'),
    array('id' => '70','specialite_id' => '1','qualification' => 'SCC','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '71','specialite_id' => '48','qualification' => 'pharmacie','id' => '48','nomspecialite' => 'pharmacie','specialite' => 'pharmacie'),
    array('id' => '72','specialite_id' => '54','qualification' => 'PEDOPSYCHIATRIE','id' => '54','nomspecialite' => 'PEDOPSYCHIATRIE','specialite' => 'PEDOPSYCHIATRE'),
    array('id' => '73','specialite_id' => '1','qualification' => 'SAMU','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '74','specialite_id' => '1','qualification' => 'CNP-SMUR','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '75','specialite_id' => '1','qualification' => 'POST-URGENCES','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '76','specialite_id' => '67','qualification' => 'GYNECOLOGIE MEDICALE','id' => '67','nomspecialite' => 'GYNECOLOGIE MEDICALE','specialite' => 'GYNECOLOGUE MEDICALE'),
    array('id' => '93','specialite_id' => '1','qualification' => 'UHCD - SAU','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '108','specialite_id' => '59','qualification' => 'DIABETOLOGUE','id' => '59','nomspecialite' => 'DIABETOLOGIE','specialite' => 'DIABETOLOGUE'),
    array('id' => '111','specialite_id' => '1','qualification' => 'UHCD','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '113','specialite_id' => '13','qualification' => 'UHCD','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '116','specialite_id' => '13','qualification' => 'MG-Oncologie','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '118','specialite_id' => '13','qualification' => 'HEMATOLOGIE','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '126','specialite_id' => '13','qualification' => 'Diabétologie','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '128','specialite_id' => '13','qualification' => 'Gériatrie','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '131','specialite_id' => '13','qualification' => 'Gastro-entérologie','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '132','specialite_id' => '13','qualification' => 'SSR','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '133','specialite_id' => '1','qualification' => 'UHCD - SAU - SMUR','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '143','specialite_id' => '1','qualification' => 'Régulation','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '144','specialite_id' => '1','qualification' => 'Déchocage','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '145','specialite_id' => '2','qualification' => 'Hépato-gastro-entérologie','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '147','specialite_id' => '13','qualification' => 'Interne','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '148','specialite_id' => '74','qualification' => 'SOPHROLOGIE','id' => '74','nomspecialite' => 'SOPHROLOGIE','specialite' => 'SOPHROLOGIE'),
    array('id' => '149','specialite_id' => '12','qualification' => 'Radiologie polyvalente','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '150','specialite_id' => '12','qualification' => 'Radiologie Thoracique','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '151','specialite_id' => '12','qualification' => 'Radiologie Abdominale','id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
    array('id' => '154','specialite_id' => '13','qualification' => 'Endocrinologie','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '155','specialite_id' => '13','qualification' => 'Oncologie ','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '157','specialite_id' => '13','qualification' => 'Neurologie ','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '159','specialite_id' => '13','qualification' => ' Pneumologie','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '162','specialite_id' => '15','qualification' => 'Hématologie','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '163','specialite_id' => '15','qualification' => 'Endocrinologie','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '164','specialite_id' => '15','qualification' => 'Oncologie ','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '165','specialite_id' => '15','qualification' => 'Diabétologie ','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '166','specialite_id' => '15','qualification' => 'Neurologie ','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '167','specialite_id' => '15','qualification' => 'Médecine interne','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '168','specialite_id' => '15','qualification' => 'Pneumologie','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '169','specialite_id' => '15','qualification' => 'Gastro-entérologie','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '170','specialite_id' => '15','qualification' => 'Cardiologie','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '171','specialite_id' => '15','qualification' => 'Gériatrie ','id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
    array('id' => '172','specialite_id' => '4','qualification' => 'SSR','id' => '4','nomspecialite' => 'GERIATRIE','specialite' => 'GERIATRE'),
    array('id' => '173','specialite_id' => '4','qualification' => 'EHPAD','id' => '4','nomspecialite' => 'GERIATRIE','specialite' => 'GERIATRE'),
    array('id' => '174','specialite_id' => '4','qualification' => 'USLD','id' => '4','nomspecialite' => 'GERIATRIE','specialite' => 'GERIATRE'),
    array('id' => '175','specialite_id' => '4','qualification' => 'Court Sejour Gériatrique','id' => '4','nomspecialite' => 'GERIATRIE','specialite' => 'GERIATRE'),
    array('id' => '176','specialite_id' => '4','qualification' => 'Long Séjour Geriatrique','id' => '4','nomspecialite' => 'GERIATRIE','specialite' => 'GERIATRE'),
    array('id' => '184','specialite_id' => '82','qualification' => 'test','id' => '82','nomspecialite' => 'test','specialite' => 'test'),
    array('id' => '185','specialite_id' => '82','qualification' => 'dsdsds','id' => '82','nomspecialite' => 'test','specialite' => 'test'),
    array('id' => '186','specialite_id' => '13','qualification' => 'Médecine gériatrique','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '187','specialite_id' => '13','qualification' => 'SSR gériatrique + USSA','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '188','specialite_id' => '13','qualification' => 'USLD','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '189','specialite_id' => '13','qualification' => 'Médecine polyvalente','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
    array('id' => '190','specialite_id' => '2','qualification' => 'ANESTHESIE','id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
    array('id' => '191','specialite_id' => '1','qualification' => 'SAU','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '192','specialite_id' => '1','qualification' => 'SAU','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '193','specialite_id' => '1','qualification' => 'SAU','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '194','specialite_id' => '1','qualification' => 'SAU - SMUR','id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
    array('id' => '195','specialite_id' => '83','qualification' => 'AIDE SOIGNANT','id' => '83','nomspecialite' => 'AIDE SOIGNANT','specialite' => 'AIDE SOIGNANT'),
    array('id' => '196','specialite_id' => '84','qualification' => 'MÉDECINE NUCLÉAIRE','id' => '84','nomspecialite' => 'MÉDECINE NUCLÉAIRE','specialite' => 'MÉDECINE NUCLÉAIRE'),
    array('id' => '197','specialite_id' => '13','qualification' => 'Court Séjour Gériatrique ','id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE')
);

private $specialite = array(
array('id' => '1','nomspecialite' => 'URGENCES','specialite' => 'URGENTISTE'),
array('id' => '2','nomspecialite' => 'ANESTHESIE','specialite' => 'ANESTHESISTE'),
array('id' => '3','nomspecialite' => 'REANIMATION','specialite' => 'REANIMATEUR'),
array('id' => '4','nomspecialite' => 'GERIATRIE','specialite' => 'GERIATRE'),
array('id' => '5','nomspecialite' => 'CARDIOLOGIE','specialite' => 'CARDIOLOGUE'),
array('id' => '7','nomspecialite' => 'PNEUMOLOGIE','specialite' => 'PNEUMOLOGUE'),
array('id' => '8','nomspecialite' => 'RADIOTHERAPIE','specialite' => 'RADIOTHÉRAPEUTE'),
array('id' => '9','nomspecialite' => 'PEDIATRIE','specialite' => 'PEDIATRE'),
array('id' => '10','nomspecialite' => 'ODONTOLOGIE','specialite' => 'ODONTOLOGISTE'),
array('id' => '11','nomspecialite' => 'ALCOOLOGIE','specialite' => 'ALCOOLOGUE'),
array('id' => '12','nomspecialite' => 'RADIOLOGIE','specialite' => 'RADIOLOGUE'),
array('id' => '13','nomspecialite' => 'MEDECINE GENERALE','specialite' => 'GENERALISTE'),
array('id' => '14','nomspecialite' => 'RHUMATOLOGIE','specialite' => 'RHUMATOLOGUE'),
array('id' => '15','nomspecialite' => 'MEDECINE POLYVALENTE','specialite' => 'POLYVALENT'),
array('id' => '16','nomspecialite' => 'GASTROENTEROLOGIE','specialite' => 'GASTROENTEROLOGUE'),
array('id' => '17','nomspecialite' => 'MEDECINE INTERNE','specialite' => 'MEDECIN INTERNISTE'),
array('id' => '18','nomspecialite' => 'GYNECOLOGIE OBSTETRIQUE','specialite' => 'GYNECOLOGUE OBSTETRICIEN'),
array('id' => '19','nomspecialite' => 'CHIRURGIE ORTHOPEDIQUE','specialite' => 'CHIRURGIEN ORTHOPEDISTE'),
array('id' => '20','nomspecialite' => 'ORTHOPEDIE','specialite' => 'ORTHOPEDISTE'),
array('id' => '21','nomspecialite' => 'CHIRURGIE VISCERALE','specialite' => 'CHIRURGIEN VISCERALE'),
array('id' => '22','nomspecialite' => 'BIOLOGIE','specialite' => 'BIOLOGISTE'),
array('id' => '23','nomspecialite' => 'PSYCHIATRIE','specialite' => 'PSYCHIATRE'),
array('id' => '24','nomspecialite' => 'CHIRURGIE DISGESTIVE','specialite' => 'CHIRURGIEN DIGESTIF'),
array('id' => '25','nomspecialite' => 'CHIRURGIE GENERALE','specialite' => 'CHIRURGIEN GENERALISTE'),
array('id' => '26','nomspecialite' => 'NEPHROLOGIE','specialite' => 'NEPHROLOGUE'),
array('id' => '27','nomspecialite' => 'NEUROLOGIE','specialite' => 'NEUROLOGUE'),
array('id' => '28','nomspecialite' => 'ONCOLOGIE','specialite' => 'ONCOLOGUE'),
array('id' => '29','nomspecialite' => 'OPHTALMOLOGIE','specialite' => 'OPHTALMOLOGUE'),
array('id' => '30','nomspecialite' => 'ENDOCRINOLOGIE','specialite' => 'ENDOCRINOLOGUE'),
array('id' => '31','nomspecialite' => 'ANGIOLOGIE','specialite' => 'ANGIOLOGISTE'),
array('id' => '32','nomspecialite' => 'ADDICTOLOGIE','specialite' => 'ADDICTOLOGUE'),
array('id' => '33','nomspecialite' => 'ORL','specialite' => 'ORL (OTO RHINO LARYNGOLOGISTE)'),
array('id' => '34','nomspecialite' => 'HEMATOLOGIE','specialite' => 'HEMATOLOGUE'),
array('id' => '35','nomspecialite' => 'CHIRURGIE VASCULAIRE','specialite' => 'CHIRURGIE VASCULAIRE'),
array('id' => '36','nomspecialite' => 'GERONTOLOGIE','specialite' => 'GÉRONTOLOGUE'),
array('id' => '37','nomspecialite' => 'MEDECIN DU TRAVAIL','specialite' => 'MEDECINE DU TRAVAIL'),
array('id' => '38','nomspecialite' => 'MPR','specialite' => 'MEDECIN PHYSIQUE ET READAPTATION'),
array('id' => '40','nomspecialite' => 'NEUROCHIRURGIE','specialite' => 'NEUROCHIRURGIEN'),
array('id' => '42','nomspecialite' => 'UROLOGIE','specialite' => 'UROLOGUE'),
array('id' => '46','nomspecialite' => 'KINESITHERAPIE','specialite' => 'KINESITHERAPEUTE'),
array('id' => '48','nomspecialite' => 'pharmacie','specialite' => 'pharmacie'),
array('id' => '54','nomspecialite' => 'PEDOPSYCHIATRIE','specialite' => 'PEDOPSYCHIATRE'),
array('id' => '55','nomspecialite' => 'GEROTONTOPSYCHIATRIE','specialite' => 'GEROTONTOPSYCHIATRIE'),
array('id' => '58','nomspecialite' => 'CHIRURGIE FACIALE','specialite' => 'CHIRURGIEN'),
array('id' => '59','nomspecialite' => 'DIABETOLOGIE','specialite' => 'DIABETOLOGUE'),
array('id' => '67','nomspecialite' => 'GYNECOLOGIE MEDICALE','specialite' => 'GYNECOLOGUE MEDICALE'),
array('id' => '72','nomspecialite' => 'KINESITHERAPEUTE','specialite' => 'KINESITHERAPIE'),
array('id' => '82','nomspecialite' => 'test','specialite' => 'test'),
array('id' => '74','nomspecialite' => 'SOPHROLOGIE','specialite' => 'SOPHROLOGIE'),
array('id' => '83','nomspecialite' => 'AIDE SOIGNANT','specialite' => 'AIDE SOIGNANT'),
array('id' => '84','nomspecialite' => 'MÉDECINE NUCLÉAIRE','specialite' => 'MÉDECINE NUCLÉAIRE')
);

    public function load(ObjectManager $manager)
    {


        for ($i=0; $i<count($this->specialite) ; $i++)
        {

                $specialite = new  Specialite();
                $specialite->setNomSpecialite($this->specialite[$i]["nomspecialite"]);
                $specialite->setSpecialite($this->specialite[$i]["specialite"]);
                $specialite->createdAt();
                $manager->persist($specialite);

            for ($j=0; $j<count($this->qualifications) ; $j++) {
                if ($this->qualifications[$j]["specialite_id"] === $this->specialite[$i]["id"])
                {
                    $qualification = new  Qualification();
                    $qualification->setQualification($this->qualifications[$j]["qualification"]);
                    $qualification->setSpecialite($specialite);
                    $qualification->createdAt();
                    $manager->persist($qualification);
                }

            }







        }

        $manager->flush();
    }
}
