<?php

namespace App\DataFixtures;

use App\Entity\Remarque;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RemarqueFixtures extends Fixture
{

private $remarque = array(
array('id' => '1','designation' => 'Le médecin arrête temporairement les remplacements','type' => '0'),
array('id' => '2','designation' => 'Le médecin arrête définitivement les remplacements','type' => '0'),
array('id' => '3','designation' => 'Le médecin ne souhaite plus collaborer avec RMS','type' => '0'),
array('id' => '4','designation' => 'Le médecin n’est pas satisfait de sa collaboration avec RMS','type' => '0'),
array('id' => '5','designation' => 'Le médecin souhaite recevoir les offres d’une ou des localité (s) précise (s)','type' => '0'),
array('id' => '6','designation' => 'Le médecin souhaite être contacté à des horaires précises ','type' => '0'),
array('id' => '7','designation' => 'Le médecin change de spécialité','type' => '0'),
array('id' => '8','designation' => 'Le médecin ne souhaite plus aller dans un CH','type' => '0'),
array('id' => '9','designation' => 'Le médecin ne souhaite plus recevoir ses E-mail par L\\\'ERP','type' => '0')
);
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i<count($this->remarque) ; $i++)
        {
            $remarque = new  Remarque();
            $remarque->setDesignation($this->remarque[$i]["designation"]);
            $remarque->setAppartenance($this->remarque[$i]["type"]);
            $remarque->createdAt();

            $manager->persist($remarque);
        }

        $manager->flush();
    }
}
