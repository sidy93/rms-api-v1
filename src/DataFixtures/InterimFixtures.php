<?php

namespace App\DataFixtures;

use App\Entity\Disponibilite;
use App\Entity\Interim;
use App\Entity\ProspectionInterim;
use App\Entity\Qualification;
use App\Entity\Specialite;
use App\Entity\TypeProspection;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\UserPassportInterface;



class InterimFixtures extends Fixture
{
    private  $type_message_prospection = array(
        array('id' => '1','designation' => 'A Rappeller'),
        array('id' => '2','designation' => 'le Télelephone sonne jusqu’au répondeur'),
        array('id' => '3','designation' => 'voir commentaire'),
        array('id' => '4','designation' => 'Messagerie'),
        array('id' => '6','designation' => 'MAUVAIS NUMERO'),
        array('id' => '10','designation' => 'disponibilités voir commentaire'),
        array('id' => '13','designation' => 'l’appel tombe directement sur le répondeur plusieurs fois de suite'),
        array('id' => '22','designation' => 'message de l’opérateur indiquant que le Numéro est inexistant'),
        array('id' => '155','designation' => 'Nous tombons sur une autre personne'),
        array('id' => '1771','designation' => 'enverra ses disponibilité par mail'),
        array('id' => '1841','designation' => 'le Télelephone sonne occupé'),
        array('id' => '90163','designation' => 'CH NON INTERESSE'),
        array('id' => '90187','designation' => 'INJOIGNABLE'),
        array('id' => '90188','designation' => 'JOINT'),
        array('id' => '90189','designation' => 'JOINT + MAIL'),
        array('id' => '90190','designation' => 'INJOIGNABLE + MAIL'),
        array('id' => '90191','designation' => 'ACCUSE DE RÉCEPTION'),
        array('id' => '90192','designation' => 'CONFORMITE'),
        array('id' => '90193','designation' => 'STANDARD'),
        array('id' => '90194','designation' => 'ENVOI PROPO COMMERCIALE'),
        array('id' => '90195','designation' => 'ENVOI PLAQUETTE COMMERCIALE'),
        array('id' => '90196','designation' => 'CH A RAPPELER'),
        array('id' => '90197','designation' => 'RAPPEL SÉCURITÉ'),
        array('id' => '90198','designation' => 'NON PROSPECTE')
    );

    /**
     * @var UserPasswordEncoderInterface
     */
    private  $encoder;

    /**
     * InterimFixtures constructor.
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $faker= Factory::create('fr_FR');


        for ($i=0; $i<=30 ; $i++)
        {

            $user = new User();

            $user->setNom($faker->lastName);
            $user->setPrenom($faker->firstName);
            $user->setEmail($faker->email);
            $user->setTelephone($faker->phoneNumber);
            $user->setAdresse($faker->address);
            $user->setIsEnable($faker->boolean);
            $user->setPassword($this->encoder->encodePassword($user,"Passer2020"));
            $user->setPseudo($faker->userName);
            $user->setDatenaissance($faker->dateTimeBetween('-30 years'));
            $user->setRoles(array('ROLE_INTERIM'));
            $user->setCivilite($faker->numberBetween(0,1));
            $user->createdAt();
            $manager->persist($user);

            $interim = new  Interim();
            $interim->setCible($faker->numberBetween(0,1));
            $interim->setActif($faker->boolean);
            $interim->setAuteur($faker->name());
            $interim->setAutreRemarques($faker->text);
            $interim->setFixe($faker->buildingNumber);
            $interim->setDescription($faker->text);
            $interim->setInscription($faker->boolean);
            $interim->setNumRpps($faker->creditCardNumber);
            $interim->setUser($user);
            $interim->createdAt();


            $manager->persist($interim);

            for ($d=1; $d<= 50 ; $d++)
            {

                $diponibilite = new  Disponibilite();
                $diponibilite->createdAt();
                $diponibilite->setDate($faker->dateTimeBetween('-1 years'));

                $diponibilite->setInterim($interim);
                $manager->persist($diponibilite);

            }



            for ($t=0; $t<count($this->type_message_prospection) ; $t++)
            {



                $type = new  TypeProspection();
                $type->setDesignation($this->type_message_prospection[$t]["designation"]);
                $type->setDescription($faker->text);
                $type->createdAt();



                $manager->persist($type);

                for ($p=0; $p<=10 ; $p++)
                {

                    $prospection = new  ProspectionInterim();
                    $prospection->createdAt();
                    $prospection->setDateRapell($faker->dateTimeBetween('-1 years'));
                    $prospection->setCommentaire($faker->text);
                    $prospection->setType($type);

                    $prospection->setInterim($interim);

                    $manager->persist($prospection);

                }



            }




        }
        $manager->flush();
    }

}
