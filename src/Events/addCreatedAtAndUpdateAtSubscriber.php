<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Gestionnaire;
use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class addCreatedAtAndUpdateAtSubscriber implements EventSubscriberInterface
{
    private $security;
    private $em;

    public function __construct(Security $security, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->em = $em;
    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUpdateOrCreated', EventPriorities::PRE_WRITE]
        ];
        // TODO: Implement getSubscribedEvents() method.
    }
    public function setUpdateOrCreated(ViewEvent $event)
    {
        $verbs = ["PUT", "DELETE", "PATCH"];
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if ($method === 'POST') {
            $entity->setAuteur($this->security->getUser()->getUsername());
            if(! ($entity instanceof Gestionnaire))
                $entity->createdAt();
        } else if(in_array($method, $verbs)) {
//            if(! ($entity instanceof Gestionnaire))
//                $entity->updateAt();
        }
    }
}
