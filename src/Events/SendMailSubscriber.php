<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\FicheSpecialite;
use App\Entity\User;
use App\Service\SendinblueService;
use App\Service\Utiles;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWSProvider\JWSProviderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

class SendMailSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $templating;
    private $container;
    private $from = 'stageinfo73s@gmail.com';
    private $alertTo = 'stageinfo73s@gmail.com';
    private $JWTTokenManager;
    private $jwsProvider;
    private $http = "http://localhost:4200/activation?";
    private $equipe = "";
    private $contact = "";
    private $security;
    private $plateforme = "";

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var SendinblueService
     */
    private $mail;

    public function __construct(Swift_Mailer $mailer,
                                Security $security,
                                Environment $templating,
                                JWSProviderInterface $jwsProvider,
                                JWTTokenManagerInterface $JWTTokenManager,
                                ContainerInterface $container,
                                ParameterBagInterface $params,
                                SendinblueService $mail)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->container = $container;
        $this->JWTTokenManager = $JWTTokenManager;
        $this->jwsProvider = $jwsProvider;
        $this->security = $security;
        $this->params = $params;
        $this->mail = $mail;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW =>
                ['sendMail', EventPriorities::POST_WRITE],


        ];
        // TODO: Implement getSubscribedEvents() method.
    }

    public function getPassword(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        dd($user);
    }

    public function sendMail(ViewEvent $event)
    {
        $jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if ($user instanceof User && $method === 'POST') {
            // Génération aléatoire d'une clé
            $cle = $this->JWTTokenManager->create($user);
            $token = $this->jwsProvider->load($cle);
            $data = [
                'url' => $this->params->get("base_url_site"),
                'type' => false,
                'template' => "emails/erp/interim/creation_compte.html.twig",
                'nom' => $user->getNom(),
                'civilite' => "Mr"
            ];
            $sender = Utiles::sender($this->params->get("sender_email"), $this->params->get("sender_name"));
            if ($this->security->getUser() && in_array("ROLE_ADMIN", $this->security->getUser()->getRoles())) {

                 $to = Utiles::toMail($user->getEmail(), $user->getNom());
                 $htmlContent = $this->mail->htmlContents($data);

                // mail de creation du compte
                 $this->mail->notificationMail($sender,$to,"creation compte",$htmlContent);


            } else {

                $data["template"] = "emails/erp/interne/interim/new_interim.html.twig";
                $to = Utiles::toMail($this->params->get("interne_email"), $this->params->get("interne_name"));
                $htmlContent = $this->mail->htmlContents($data);

                // alert de creation de compte en interne
                $this->mail->notificationMail($sender,$to,"creation compte",$htmlContent);


            }  
        }
    }





}
