<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Client;
use App\Entity\FicheClient;
use App\Entity\FicheSpecialite;
use App\Entity\Gestionnaire;
use App\Entity\GroupeRoles;
use App\Entity\Interim;
use App\Entity\Mission;
use App\Entity\Offres;
use App\Entity\ProspectionInterim;
use App\Entity\Recruteur;
use App\Entity\Specialite;
use App\Entity\TypeDeGarde;
use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class HistoriqueSubscriber implements EventSubscriberInterface
{
    private $encoder;
    private $security;
    private $em;

    public function __construct(UserPasswordEncoderInterface $encoder, Security $security, EntityManagerInterface $em)
    {
        $this->encoder = $encoder;
        $this->security = $security;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['historique', EventPriorities::POST_WRITE]
        ];
        // TODO: Implement getSubscribedEvents() method.
    }

    public function historique(ViewEvent $event)
    {
        $event_current = $event->getControllerResult();
        $historyItem = [];
        $request_uri = $event->getRequest()->getRequestUri();
        $request_uri = $event->getRequest()->getRequestUri();
        $tmpUri = substr($request_uri, 5);
        $indexUri = strpos($tmpUri, '?');
        if ($indexUri) {
            $cible = substr($tmpUri, 0, $indexUri);
            $cible_id = substr($tmpUri, 0, $indexUri);
        } else {
            $cible = $tmpUri;
            $cible_id = $tmpUri;
        }
        $method = $event->getRequest()->getMethod();
//        if (strtolower($method) !== 'get' && $event_current) {
//            if ($this->security->getUser()) {
//                $roles = $this->getRoles($this->security->getUser()->getRoles());
//                $idUser = $this->security->getUser()->getId();
//                $nomComplet = $this->security->getUser()->getNom() . ' ' . $this->security->getUser()->getPrenom();
//                // verifier si cest le client ================================================================================
//                // et cree son historique
//                if ($event_current instanceof Client || $event->getRequest()->attributes->get('data') instanceof Client) {
//                    //verifier si cest une activation ou pas
//                    $historyItem = $this->createHistoryForClient($event_current, $event, $method);
//                } // =========================================================================================================
//                // creation de lhistorique de fiches de specialites
//                else if ($event_current instanceof FicheSpecialite) {
//                    $historyItem = $this->createHistoryForFicheSpecialite($event_current, $event, $method);
//                } // =========================================================================================================
//                // creation de lhistorique de types de gardes
//                else if ($event_current instanceof TypeDeGarde) {
//                    $historyItem = $this->createHistoryForTypeDeGarde($event_current, $event, $method);
//                } // =========================================================================================================
//                // creation de lhistorique fiche client
//                else if ($event_current instanceof FicheClient) {
//                    $historyItem = $this->createHistoryForFicheCLient($event_current, $event, $method);
//                } // =========================================================================================================
//                // creation de lhistorique de mission
//                else if ($event_current instanceof Mission) {
//                    $historyItem = $this->createHistoryForMission($event_current, $event, $method);
//                } // =========================================================================================================
//                // creation de lhistorique de mission
//                else if ($event_current instanceof Offres) {
//                    $historyItem = $this->createHistoryOffre($event_current, $event, $method);
//                } // =========================================================================================================
//                else if ($event_current instanceof Recruteur) {
//                    $historyItem = $this->createHistoryForRecruteurAndInterim($event_current, $event, $method, 'recruteur');
//                } else if ($event_current instanceof Interim) {
//                    $historyItem = $this->createHistoryForRecruteurAndInterim($event_current, $event, $method, 'interim');
//                } // creation de lhistorique de mission
//                else if ($event_current instanceof ProspectionInterim) {
//                    $historyItem = $this->createHistoryForProspectionInterim($event_current, $event, $method);
//                } // =========================================================================================================
//                // creation de lhistorique de mission
//                else if ($event_current instanceof Gestionnaire) {
//                    $historyItem = $this->createHistoryForGestionnaire($event_current, $event, $method);
//                } // =========================================================================================================
//                else {
//                    dd($event_current);
//                }
//            } else {
//                $element = " ";
//                $nomComplet = '';
//                $idUser = '';
//                $roles = 'anonyme';
//            }
//            if ($historyItem) {
//                $this->setDataForHistory($nomComplet, $historyItem, $idUser, $roles, $method);
//            }
//        }
    }

    public function getRoles($roles)
    {
        if (in_array('ROLE_ADMIN', $roles)) {
            return 'admin';
        } else if (in_array('ROLE_INTERIM', $roles)) {
            return 'interim';
        } else if (in_array('ROLE_RECRUTEUR', $roles)) {
            return 'recruteur';
        } else if (in_array('ROLE_COMMERCIAL', $roles)) {
            return 'commercial';
        } else if (in_array('ROLE_SUPERVISEUR', $roles)) {
            return 'superviseur';
        } else if (in_array('ROLE_COMPTABLE', $roles)) {
            return 'comptable';
        } else {
            return 'anonyme';
        }
    }

    // identifier le role dans le array des roles

    public function createHistoryForClient($event_current, $event, $method)
    {
        if ((isset(json_decode($event->getRequest()->getContent())->activer))
            && (!(isset(json_decode($event->getRequest()->getContent())->nomEtablissement)))) {
            if ((json_decode($event->getRequest()->getContent())->activer) === 1) {
                $element = "Activation du CH " . $event_current->getNomEtablissement();
            }
            if ((json_decode($event->getRequest()->getContent())->activer) === 0) {
                $element = "Désactivation du CH " . $event_current->getNomEtablissement();
            }
        } else {
            if (strtolower($method) === 'delete') {
                $element = "Suppression du CH " . $event->getRequest()->attributes->get('data')->getNomEtablissement();
            } else if (strtolower($method) === 'put') {
                $element = "Modification du CH " . $event_current->getNomEtablissement();
            } else if (strtolower($method) === 'post') {
                $element = "Création du CH " . $event_current->getNomEtablissement();
            }
        }
        if ($event->getRequest()->attributes->get('data')->getNomEtablissement()) {
            $explode_tmp = explode("/", $event->getRequest()->getRequestUri());
        }
        $cible = $event_current ? $event_current->getNomEtablissement() : $event->getRequest()->attributes->get('data')->getNomEtablissement();
        $cible_id = $event_current ? $event_current->getId() : $explode_tmp[sizeof($explode_tmp) - 1];
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'client'];
    }

    //    Partie concernant la gestion des historiques

    public function createHistoryForFicheSpecialite($event_current, $event, $method)
    {
        if (sizeof($event_current->getQualification()->getValues()) > 0) {
            $specialite = $event_current->getQualification()->getValues()[0]->getSpecialite()->getNomSpecialite();
            $specialite_id = $event_current->getQualification()->getValues()[0]->getSpecialite()->getId();
        } else {
            $tmpSpecialite = $this->em->getRepository(Specialite::class)->find(json_decode($event->getRequest()->getContent())->speciliate);
            $specialite = $tmpSpecialite->getNomSpecialite();
            $specialite_id = $tmpSpecialite->getId();
        }
        $idFiche = $event_current->getId();
        $etablissement = $event_current->getClient()->getNomEtablissement();
        if (strtolower($method) === 'delete') {
        } else if (strtolower($method) === 'put') {
            $element = "Modification fiche de spécialité $specialite [ $idFiche ] pour le CH $etablissement";
        } else if (strtolower($method) === 'post') {
            $element = "Création fiche de spécialité $specialite [ $idFiche ] pour le CH $etablissement";
        }
        $cible = $specialite;
        $cible_id = $specialite_id;
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'fiche spécialité'];
    }

    //    Partie concernant la gestion des historiques

    public function createHistoryForTypeDeGarde($event_current, $event, $method)
    {
        $designationGarde = $event_current->getTypeVacation()->getDesignation();
        $specialite = $event_current->getFicheSpecialite()->getQualification()->getValues()[0]->getSpecialite()->getNomSpecialite();
        $etablissement = $event_current->getFicheSpecialite()->getClient()->getNomEtablissement();
        $idGarde = $event_current->getId();
        if (strtolower($method) === 'delete') {
        } else if (strtolower($method) === 'put') {
            $element = "Modification garde ($designationGarde) pour le ch $etablissement (fiche: $specialite)";
        } else if (strtolower($method) === 'post') {
            $element = "Création garde ($designationGarde) pour le ch $etablissement (fiche: $specialite)";
        }
        $cible = "type de garde";
        $cible_id = $idGarde;
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'type de garde'];
    }

    public function createHistoryForFicheCLient($event_current, $event, $method)
    {
        $etablissement = $event_current->getClient()->getNomEtablissement();
        $idItem = $event_current->getId();
        if (strtolower($method) === 'delete') {
        } else if (strtolower($method) === 'put') {
            $element = "Modification, Fiche client, CH $etablissement";
        } else if (strtolower($method) === 'post') {
            $element = "Création, Fiche client, CH $etablissement";
        }
        $cible = "Fiche Client";
        $cible_id = $idItem;
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'fiche client'];
    }

    public function createHistoryForMission($event_current, $event, $method)
    {
//        dd($event_current);
        $enable = (json_decode($event->getRequest()->getContent()))->enable ? (json_decode($event->getRequest()->getContent()))->enable : null;
        if (isset($enable)) {
            $statut = $enable === 1 ? 'Activation' : 'Desactivation';
            $reference = $event_current->getMois() . ' ' . $event_current->getAnnee();
            $element = "$statut de la mission $reference";
        } else {

        }
//        $request = json_decode($event->getRequest()->getContent());
//        $nbrePlannings = sizeof($event_current->getPlanning()->getValues());
//        $etablissement = $event_current->getFicheSpecialite()->getClient()->getNomEtablissement();
//        $idItem = $event_current->getId();
//        if (strtolower($method) === 'delete') {
//        } else if (strtolower($method) === 'put') {
//            $element = "Modification mission $request->contrat par $request->typeMission $request->mois $request->annee pour le ch $etablissement";
//        } else if (strtolower($method) === 'post') {
//            $element = "Création mission $request->contrat par $request->typeMission $request->mois $request->annee (plannings -> $nbrePlannings) pour le ch $etablissement";
//        }
        $cible = $event_current->getReference();
        $cible_id = $event_current->getId();
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'mission'];
    }

    public function createHistoryOffre($event_current, $event, $method)
    {
        $specialite = $event_current->getFicheSpecialite()->getQualification()->getValues()[0]->getSpecialite()->getNomSpecialite();
        $request = json_decode($event->getRequest()->getContent());
        $requestContent = json_decode($event->getRequest()->getContent());
        $mission = $requestContent->contrat . " $specialite, " . $request->mois . " - " . $request->annee . " ( " . $request->debut . " / " . $request->fin . " )";
        $etablissement = $event_current->getFicheSpecialite()->getClient()->getNomEtablissement();
        $idItem = $event_current->getId();
        if (strtolower($method) === 'delete') {
        } else if (strtolower($method) === 'put') {
            $element = "Modification offre de mission $mission, $etablissement";
        } else if (strtolower($method) === 'post') {
            $element = "Offre de mission $mission, $etablissement";
        }
        $cible = $event_current->getReference();
        $cible_id = $event_current->getId();
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'offres'];
    }

    public function createHistoryForRecruteurAndInterim($event_current, $event, $method, $item)
    {
        if (strtolower($method) === 'delete') {
        } else if (strtolower($method) === 'put') {
            $element = "modification du $item";
        } else if (strtolower($method) === 'post') {
            $element = "Création du $item";
        }
        $cible = $event_current->getUser()->getNom() . ' ' . $event_current->getUser()->getPrenom();
        $cible_id = $event_current->getId() ? $event_current->getId() : 0;
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => $item];
    }

    public function createHistoryForProspectionInterim($event_current, $event, $method)
    {
        $comment = $event_current->getCommentaire();
        $type = $event_current->getCommentaire();
        $interim = $event_current->getInterim()->getUser()->getNom() . " " . $event_current->getInterim()->getUser()->getPrenom();
        $type = $event_current->getType()->getDesignation();
        $idInterim = $event_current->getInterim()->getUser()->getId();
        if (strtolower($method) === 'delete') {
        } else if (strtolower($method) === 'put') {
            $element = "$type : $comment";
        } else if (strtolower($method) === 'post') {
            $element = "$type : $comment";
        }
        $cible = $interim;
        $cible_id = $idInterim;
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'prospection interim'];
    }

    public function createHistoryForGestionnaire($event_current, $event, $method)
    {
        $item = $event_current->getUser()->getNom() . " " . $event_current->getUser()->getPrenom();
        $idInterim = $event_current->getUser()->getId();
        if (strtolower($method) === 'delete') {
        } else if (strtolower($method) === 'put') {
            $element = "Modification du gestionnaire : $item";
        } else if (strtolower($method) === 'post') {
            $element = "Creation du gestionnaire : $item";
        }
        $cible = $item;
        $cible_id = $event_current->getId();
        return ['element' => $element, 'cible' => $cible, 'cible_id' => $cible_id, 'table' => 'gestionnaire'];
    }

    public function setDataForHistory($nomComplet = null, $historyItem = null, $idUser = null, $roles = null, $method = null, $cicle = null, $cible_id = null)
    {
        $array_methods['GET'] = ["key" => "Récupération", "value" => "recupération de la liste des " . $historyItem['cible']];
        $array_methods['PUT'] = ["key" => "Modification", "value" => $historyItem['element']];
        $array_methods['DELETE'] = ["key" => "Suppression", "value" => "suppression de " . $historyItem['cible']];
        $array_methods['POST'] = ["key" => "Création", "value" => $historyItem['element']];
        $auteur = $this->security->getUser()->getNom() . ' ' . $this->security->getUser()->getPrenom();
        $auteur_id = $this->security->getUser()->getId();

        // creation de l'historique
        $history = [
            'auteur' => $auteur,
            'auteur_id' => $auteur_id,
            'date' => (new DateTime('now'))->format('d-m-Y H:i'),
            'heure' => (new DateTime('now'))->format('H:i'),
            'roles' => $roles,
            'table' => $historyItem['table'],
            'description' => $array_methods[strtoupper($method)]['value'],
            'method' => $array_methods[strtoupper($method)]['key'],
            'cible' => $historyItem['cible'],
            'cible_id' => $historyItem['cible_id'],
        ];
//        dd($history);
        $this->addHistory($history);
    }

    // Permet de mettre les mois et les jours en francais

    public function addHistory($object)
    {
        $request = new Request();
        $filesystem = new Filesystem();
        $contenue_fichier = [];
        $tmp = $contenue_fichier;
        $annee = date('Y');
        $jour = date("l");
        $mois = date("F");
        // le mois on le formate avec la fonction (monthOrDayFrench) en specifiant true si cest le mois ou false si cest le jour
        $mois = $this->monthOrDayFrench($mois, 1);
        // verifie si le dossier de lannee nexiste pas
        $isAnneeExist = $filesystem->exists('historique/' . $annee);
        // on prend le soin de cree le nom du fichier jour_de_semaine + date + .json
        $fichier = 'historique/' . $annee . '/' . $mois . '/' . $this->monthOrDayFrench($jour, 0) . '_' . date('d') . '.json';
        if (!$isAnneeExist)
            $filesystem->mkdir('historique/' . $annee);

        $isMoisExist = $filesystem->exists('historique/' . $annee . '/' . $mois);
        if (!$isMoisExist)
            $filesystem->mkdir('historique/' . $annee . '/' . $mois);

        $isFichierExist = file_exists($fichier);
        if (!$isFichierExist)
            $filesystem->touch($fichier);
        else
            $contenue_fichier = file_get_contents($fichier) != null ? file_get_contents($fichier) : [];

        if (gettype($contenue_fichier) != 'array')
            $tmp = json_decode($contenue_fichier);

        $object = (object)($object);
        $roles = $object->roles;
        $dataHistory = [
            'auteur_id' => $object->auteur_id,
            'auteur' => $object->auteur,
            'date' => (new DateTime('now'))->format('d-m-Y'),
            'heure' => (new DateTime('now'))->format('H:i'),
            'role' => $roles,
            'description' => $object->description,
            'action' => $object->method,
            'table' => $object->table,
            'cible' => $object->cible,
            'cible_id' => $object->cible_id,
        ];
        $object_tmp = json_encode($dataHistory);
        $object_tmp = json_decode($object_tmp);
//        array_push($tmp, $object_tmp);
        $file = fopen($fichier, 'w+');
        fwrite($file, json_encode($tmp));
        fclose($file);
    }

    public function monthOrDayFrench($el, $isMonth)
    {
        $el = strtolower($el);


        $months = [
            'january' => 'janvier',
            'february' => 'février',
            'march' => 'mars',
            'april' => 'avril',
            'may' => 'mai',
            'june' => 'juin',
            'july' => 'juillet',
            'august' => 'août',
            'september' => 'septembre',
            'october' => 'octobre',
            'november' => 'novembre',
            'december' => 'décembre'
        ];


        $days = [
            'monday' => 'lundi',
            'tuesday' => 'mardi',
            'wednesday' => 'mercredi',
            'thursday' => 'jeudi',
            'friday' => 'vendredi',
            'saturday' => 'samedi',
            'sunday' => 'dimanche'
        ];
        if ($isMonth) {

            return $months[$el];
        } else {
            return $days[$el];
        }
    }

    public function getSecurity()
    {
        return $this->security;
    }
}
