<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\FicheSpecialite;
use App\Entity\Specialite;
use App\Service\SendinblueService;
use App\Service\Utiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class FicheSpecialiteMailSubscriber implements EventSubscriberInterface
{
    private $mail;
    private $params;
    private $em;

    public function __construct(ParameterBagInterface $params, SendinblueService $mail,EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->mail = $mail;
        $this->em = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW =>

                ['sendMail', EventPriorities::POST_WRITE],

        ];
        // TODO: Implement getSubscribedEvents() method.
    }



    public function sendMail(ViewEvent $event)
    {

        $ficheSpecialite= $event->getControllerResult();

        $method = $event->getRequest()->getMethod();


        if ($ficheSpecialite instanceof FicheSpecialite && $method === 'POST') {
            $contentRequest = json_decode($event->getRequest()->getContent());

                if($contentRequest->mail)
                {
                    if ($ficheSpecialite->getClient()->getRecruteurs()->count() > 0 )
                    {
                        $specialite = $this->em->getRepository(Specialite::class)->find($contentRequest->speciliate);
                        foreach ($ficheSpecialite->getClient()->getRecruteurs() as $recruteur)
                        {
                            $template = "emails/erp/client/new_fiche-specialite.html.twig";

                            $this->mail->sendNotificationMail(
                                $recruteur->getUser()->getEmail(),
                                $recruteur->getUser()->getNom(),
                                true,
                                $specialite->getNomSpecialite(),
                                $template,
                                [],
                                "DEMANDE DE DEVIS " . $ficheSpecialite->getClient()->getNomEtablissement() . ' ' . $specialite->getNomSpecialite(),
                                "demande de devis");
                        }

                    }
                }




        }
    }



}