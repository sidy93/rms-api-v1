<?php


namespace App\Events;



use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JwtCreatedSubscriber
{
    public function updateJwtData(JWTCreatedEvent $createdEvent){
        $user = $createdEvent->getUser();
        $data = $createdEvent->getData();

        // on ajoute delements dans le token
        $data['id'] = $user->getId();
        $data['nom'] = $user->getNom();
        $data['prenom'] = $user->getPrenom();
        $data['image'] = $user->getImage();
        // on regroupe les differents roles de lutilisateur sur un tableau
        $tmp = [];
        foreach ($user->getGroupeRoles()->getValues() as $item){
            foreach ($item->getRoles() as $role){
                array_push($tmp, $role);
            }
        }
        $data['groupeRoles'] = $tmp;
        $createdEvent->setData($data);
    }
}
