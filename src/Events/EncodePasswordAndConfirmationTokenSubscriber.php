<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\GroupeRoles;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EncodePasswordAndConfirmationTokenSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager)
    {
        $this->encoder = $encoder;
        $this->em = $entityManager;
    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['encodePassword', EventPriorities::PRE_WRITE]
        ];

    }

    public function encodePassword(ViewEvent $event)
    {

        $user = $event->getControllerResult();

        $method = $event->getRequest()->getMethod();

        if($user instanceof User && $method==='POST'){
            if(sizeof($user->getRoles())){
                foreach ($user->getRoles() as $idroles){
                    $groupeRoles = $this->em->getRepository(GroupeRoles::class)->find($idroles);
                    $groupeRoles ? $user->addGroupeRole($groupeRoles) : null;
                }
            }
            // Génération aléatoire d'une clé
            $cle = md5(microtime(TRUE)*100000);
            $hash = $this->encoder->encodePassword($user, "passer");
            $user->setPassword($hash);

        }
    }
}
