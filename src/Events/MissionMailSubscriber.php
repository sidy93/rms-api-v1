<?php


namespace App\Events;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\FicheSpecialite;
use App\Entity\Mission;
use App\Entity\Specialite;
use App\Service\SendinblueService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class MissionMailSubscriber implements EventSubscriberInterface
{
    private $mail;
    private $params;
    private $em;

    public function __construct(ParameterBagInterface $params, SendinblueService $mail,EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->mail = $mail;
        $this->em = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW =>

                ['sendMail', EventPriorities::PRE_WRITE],

        ];
        // TODO: Implement getSubscribedEvents() method.
    }



    public function sendMail(ViewEvent $event)
    {

        $mission = $event->getControllerResult();

        $method = $event->getRequest()->getMethod();


        if ($mission instanceof Mission && $method === 'POST') {
            $contentRequest = json_decode($event->getRequest()->getContent());


            if($contentRequest->mail)
            {
                if ($mission->getFicheSpecialite()->getClient()->getRecruteurs()->count() > 0 )
                {
                    $fiche = $this->em->getRepository(FicheSpecialite::class)->find($contentRequest->ficheSpecialite->id);
                    $specialite =  $fiche->getQualification()->first()->getSpecialite()->getNomSpecialite();


                    foreach ($mission->getFicheSpecialite()->getClient()->getRecruteurs() as $recruteur)
                    {
                        $template = "emails/erp/client/nouvelle_mission.html.twig";

                        $this->mail->sendNotificationMail(
                            $recruteur->getUser()->getEmail(),
                            $recruteur->getUser()->getNom(),
                            true,
                           $specialite.' '.$mission->getMois().' '.$mission->getAnnee().' '.$mission->getReference(),
                            $template,
                            [],
                            "NOUVELLE MISE A JOURS PLANNING " . $mission->getReference() ,
                            "nouvelle mission");
                    }

                }
            }


        }
    }



}