<?php


namespace App\Service;


use App\Entity\FicheSpecialite;
use App\Entity\Mission;
use Dompdf\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class Utiles
{
    /**
     * @param $email
     * @param $name
     * @return mixed
     */
    public  static  function  toMail($email,$name)
    {
        $to [] =  [
            "email"=> $email,
            "name"=> $name
        ];

        return $to;
    }

    /**
     * @param $mail
     * @param $name
     * @return array
     */
    public  static  function  sender($mail,$name)
    {

        return  [
            "name"=>$name,
            "email"=> $mail,
        ];
    }

    public static function isUniqueColum($em, array $columnName, $value, $objet)
    {
        for ($i = 0; $i < count($columnName); $i++) {
            $query = $em->getRepository($objet)->findBy(
                array(
                    $columnName[$i] => $value[$i]
                )
            );
        }
        return $query;
    }

    public static function mailFromAlerteInterne()
    {
        return "interim@reseaumedical.fr";
    }

    public static function credentialsPld()
    {
        return base64_encode('Rmstestws:Ulj0V9ZerGst');
//        return base64_encode('Rms:w4cX');
    }

    public  static  function libelleStatutPlanning(){
        return ["Pourvue", "Non Pourvue","A Pourvoir","Annuler Par le CH","Autres","Perdue"];
    }


    public static function tokenSellSign()
    {
      //  return "RMSPRODU%7CciRgF7qIorbN3%2F4GP%2FrmjsF0tgM21nCvCg8%2F0irZHDg%3D";
       // return "2139fU%7CzNz1ip6IraLTIRqeYdqxC0udSukv8DgbkbYgki2IHaWU5Nb2t8sdIA%3D%3D";
        return "RESMEDSE%7CrO%2BIL%2Fg%2BchM7r0cNlMo8BTtTcaHX7vuB%2BSCjb47fzl0%3D";

    }

    public static function baseUrlSellAndSign()
    {
        return "https://cloud.sellandsign.com/calinda/hub/selling/model/";
    }

    public  static function baseUrlSellAndSign2 (){
        return "https://cloud.sellandsign.com/calinda/hub/selling/";
    }

    public static function baseUrlPld()
    {
        return "http://reseau-medical.com:8085/api/Tempo/";
//        return "https://rms.mysynapse.fr/api/Tempo/";
    }

    public static function httpOptionsPld($id, $body)
    {
        $httpOptions = [
            'headers' => [
                'Authorization' => 'Basic ' . self::credentialsPld(),
                "Content-Type" => "application/json"
            ],
            'query' => [
                'Dossier' => 'RMS01',
                'ID' => $id
            ],
            'body' => $body
        ];
        return $httpOptions;
    }

    public static function baseUrlSendInblue()
    {
        return "https://api.sendinblue.com/v3/";
    }

    public static function baseUrlApiVille()
    {
        return "https://geo.api.gouv.fr";
    }

    public static function keyApiSendInblue()
    {
        return "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";
    }

    public  static  function  getMoth(){
        return
            ['Janvier', 'Février', 'Mars', 'Avril',
                'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre',
                'Décembre'];
    }

    public  static  function libelleStatutCandidature(){
        return ["Validee","En Attente", "Non Retenue","Annulee Med","Non Effectuee"];
    }
    public  static  function statutCandidature(){
        return [
            'validee',
            'en attente',
            'non retenue',
            'annulee med',
            'annulee ch',
            'non effectuee',
            'annulee_c_apresvalidation',
            'annulee_c_avantvalidation',
        ];
    }
    public  static  function  mailCCRms()
    {
        return "interim@reseaumedical.fr";
    }

    public static  function  mailFrom()
    {
        return "interim@reseaumedical.fr";
    }
    public static  function  mailFromCompta()
    {
        return "compta@reseaumedical.fr";
    }

    public static function mailreplyto()
    {
        return "contact@reseaumedical.fr";
    }

    public static function apiBaseUrl()
    {
        return "127.0.0.1/api-master/web/app_dev.php/auth/";
    }

    public static function mailNameFromAlerteInterne()
    {
        return "Interim";
    }
    public static function mailNameFrom()
    {
        return "RMS";
    }
    public static function mailInterneTo()
    {
        return "interim@reseaumedical";
    }

    public static function arrayValueForTemplate($columnName, $colonne_value)
    {
        $query = [];
        for ($i = 0; $i < count($columnName); $i++) {
            $query[$columnName[$i]] = $colonne_value[$i];
        }
        return $query;
    }


    public static function getEmailAndName($items)
    {

        $query = [];
        foreach ($items as $item) {


            $query[$item['email']] = $item["nom"];

        }
        return $query;
    }

    public static function toForEmail($columnName, $colonne_value)
    {

        $query = [];
        for ($i = 0; $i < count($columnName); $i++) {
            $query[$columnName[$i]] = $colonne_value[$i];
        }
        return $query;
    }

    public static function getEmailFromAndCcAndreplyto($em, $columnName, $objet, $colonne_value)
    {
        $email_content = [];
        for ($i = 0; $i < count($colonne_value); $i++) {
            $query = $em->getRepository($objet)->findOneBy(
                array(
                    $columnName => $colonne_value[$i]
                )
            );
            if ($i == 0) {
                $email_content ['from'] = [$query->getEmail(), $query->getName()];
            } elseif ($i == 1) {
                $email_content ['cc'] = [$query->getEmail() => $query->getName()];
            } elseif ($i == 2) {
                $email_content ['replyto'] = [$query->getEmail() => $query->getName()];
            }


        }
        return $email_content;
    }

//    public static function customObjet(array $columnName, $value, $objet)
//    {
//        foreach ($recruteur_client as $i => $cr) {
//            $recruteurs[] = [
//                $columnName[$i] => $cr->getRecruteur()->getId(),
//
//            ];
//        }
//    }


    public static function generatePassword($size)
    {
        $characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $password = "";
        for ($i = 0; $i < $size; $i++) {
            $password .= ($i % 2) ? strtoupper($characters[array_rand($characters)]) : $characters[array_rand($characters)];
        }

        return $password;

    }


    public static function generateNumberClientSellAndSign($size, $id_client)
    {
        $characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        $number = "RMS" . $id_client;
        for ($i = 0; $i < $size; $i++) {
            $number .= ($i % 2) ? strtoupper($characters[array_rand($characters)]) : $characters[array_rand($characters)];
        }

        return $number;

    }

    public static  function  idDocPdfSellAndSign(){
        return 21754;
}

    public static function messageErrorServer()
    {
        return "erreur du server, merci de signaler le service informatique si le problème persiste";

    }

    public static function messageErrorDuplicata($name)
    {
        return "l'element " . $name . " que vous essayer d'ajouter existe déja";
    }


    public static function messageCreateSuccess()
    {
        return "Ajout";
        $data = null;
    }

    public static function baseURL()
    {
//        return "http://localhost:4200";
        return "https://reseaumedical.fr";
    }


    public static function dataMail($body, $to, $subjet, $from, $bcc, $replyto)
    {
        $data = [];

        $data["to"] = $to;
        $data["from"] = $from;
        $data["bcc"] = $bcc;
        $data["subject"] = $subjet;
        $data["replyto"] = $replyto;
        $data["html"] = $body;

        return $data;


    }

    function explodeElementByLimit($item, $limit)
    {
        return str_split($item, $limit);
    }


    //    Partie concernant la gestion des historiques


    // permet dajouter une historique suivant un objet
    public function addHistory($object, $user)
    {
        $request = new Request();
        $filesystem = new Filesystem();
        $contenue_fichier = [];
        $tmp = $contenue_fichier;
        $annee = date('Y');
        $jour = date("l");
        $mois = date("F");
        // le mois on le formate avec la fonction (monthOrDayFrench) en specifiant true si cest le mois ou false si cest le jour
        $mois = $this->monthOrDayFrench($mois, 1);
        // verifie si le dossier de lannee nexiste pas
        $isAnneeExist = $filesystem->exists('historique/' . $annee);
        // on prend le soin de cree le nom du fichier jour_de_semaine + date + .json
        $fichier = 'historique/' . $annee . '/' . $mois . '/' . $this->monthOrDayFrench($jour, 0) . '_' . date('d') . '.json';
        if (!$isAnneeExist)
            $filesystem->mkdir('historique/' . $annee);

        $isMoisExist = $filesystem->exists('historique/' . $annee . '/' . $mois);
        if (!$isMoisExist)
            $filesystem->mkdir('historique/' . $annee . '/' . $mois);

        $isFichierExist = file_exists($fichier);
        if (!$isFichierExist)
            $filesystem->touch($fichier);
        else
            $contenue_fichier = file_get_contents($fichier) != null ? file_get_contents($fichier) : [];

        if (gettype($contenue_fichier) != 'array')
            $tmp = json_decode($contenue_fichier);

        if (gettype($user) == 'array') {
            $roles = "";
            $auteur = json_decode($user['user'])->username;
            $roles = json_decode($user['user'])->roles;
            if (in_array('ROLE_SUPER_ADMIN', $roles))
                $roles = "ROLE_SUPER_ADMIN";
            else if (in_array('ROLE_COMMERCIAL', $roles))
                $roles = 'ROLE_COMMERCIAL';
            else if (in_array('ROLE_RECRUTEUR', $roles))
                $roles = 'ROLE_RECRUTEUR';
            else if (in_array('ROLE_CANDIDAT', $roles))
                $roles = 'ROLE_CANDIDAT';
            $type = json_decode($user['user'])->username;

            $dataHistory = [
                'auteur' => $auteur,
                'date' => (new DateTime('now'))->format('d-m-Y H:i'),
                'type' => $roles
            ];
            $object_tmp = array_merge($dataHistory, $object);
            $object_tmp = json_encode($object_tmp);
            $object_tmp = json_decode($object_tmp);
            array_push($tmp, $object_tmp);
            $file = fopen($fichier, 'w+');
            fwrite($file, json_encode($tmp));
            fclose($file);
        }
    }

// Permet de mettre les mois et les jours en francais
    public function monthOrDayFrench($el, $isMonth)
    {
        $el = strtolower($el);
        $months = [
            'january' => 'janvier',
            'february' => 'février',
            'march' => 'mars',
            'april' => 'avril',
            'may' => 'mai',
            'june' => 'juin',
            'july' => 'juillet',
            'august' => 'août',
            'september' => 'septembre',
            'october' => 'octobre',
            'november' => 'novembre',
            'december' => 'décembre'
        ];

        $days = [
            'monday' => 'lundi',
            'tuesday' => 'mardi',
            'wednesday' => 'mercredi',
            'thursday' => 'jeudi',
            'friday' => 'vendredi',
            'saturday' => 'samedi',
            'sunday' => 'dimanche'
        ];
        if ($isMonth) {
            return $months[$el];
        } else {
            return $days[$el];
        }
    }

    public  function  checkEmailValide($email)
    {

        $email = "thomas@waytolearnx.com";

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo "L'adresse e-mail est valide";
        } else {
            echo "L'adresse e-mail n'est pas valide";

        }

    }
    public static function isRecruteur($user)
    {
        $rep = false;
        if(stristr($user->getGroupeRoles()[0]->getLibelle(), 'recruteur')){
            $rep = true;
        }
        return $rep;
    }
    public static function isInterimaire($user)
    {
        $rep = false;
        if(stristr($user->getGroupeRoles()[0]->getLibelle(), 'interimaire')){
            $rep = true;
        }
        return $rep;
    }

    public static function enleverCaracteresSpeciaux($str) {
        $url = $str;
        $url = preg_replace('#Ç#', 'C', $url);
        $url = preg_replace('#ç#', 'c', $url);
        $url = preg_replace('#è|é|ê|ë#', 'e', $url);
        $url = preg_replace('#È|É|Ê|Ë#', 'E', $url);
        $url = preg_replace('#à|á|â|ã|ä|å#', 'a', $url);
        $url = preg_replace('#@|À|Á|Â|Ã|Ä|Å#', 'A', $url);
        $url = preg_replace('#ì|í|î|ï#', 'i', $url);
        $url = preg_replace('#Ì|Í|Î|Ï#', 'I', $url);
        $url = preg_replace('#ð|ò|ó|ô|õ|ö#', 'o', $url);
        $url = preg_replace('#Ò|Ó|Ô|Õ|Ö#', 'O', $url);
        $url = preg_replace('#ù|ú|û|ü#', 'u', $url);
        $url = preg_replace('#Ù|Ú|Û|Ü#', 'U', $url);
        $url = preg_replace('#ý|ÿ#', 'y', $url);
        $url = preg_replace('#Ý#', 'Y', $url);

        return ($url);
    }
    public static function rmdir_recursive($dir)
    {
        //Liste le contenu du répertoire dans un tableau
        $dir_content = scandir($dir);
        //Est-ce bien un répertoire?
        if($dir_content !== FALSE){
            //Pour chaque entrée du répertoire
            foreach ($dir_content as $entry)
            {
                //Raccourcis symboliques sous Unix, on passe
                if(!in_array($entry, array('.','..'))){
                    //On retrouve le chemin par rapport au début
                    $entry = $dir . '/' . $entry;
                    //Cette entrée n'est pas un dossier: on l'efface
                    if(!is_dir($entry)){
                        unlink($entry);
                    }
                    //Cette entrée est un dossier, on recommence sur ce dossier
                    else{
                        rmdir_recursive($entry);
                    }
                }
            }
        }
        //On a bien effacé toutes les entrées du dossier, on peut à présent l'effacer
        rmdir($dir);
    }


    function refractor_json_offre($em,$allMissionEnable, $id_interim)
    {
        $contenue = [];
        $specialite = "";
        $nomSpecialite = "";

        foreach ($allMissionEnable as $itemMission) {

            // recuperation de lensemble de plannings par rapport au mission aux quelle l'interim na jamais postuler
            $contentPlanning = $em->getRepository(Mission::class)->getPlanningWithoutCandidatureForInterim($id_interim, $itemMission['id']);
            if (sizeof($contentPlanning) > 0) {
                $mission = $em->getRepository(Mission::class)->find($itemMission['id']);
                $idFiche = $mission->getFicheSpecialite()->getId();

                $contentQualif = $em->getRepository(FicheSpecialite::class)->find($idFiche);


                $qualif1 = "";

                foreach ($contentQualif->getQualification()->getValues() as $itemQualifI) {
                    $qualif1 = $itemQualifI->getQualification() . "," . $qualif1;
                    $specialite = $itemQualifI->getSpecialite()->getSpecialite();
                    $nomSpecialite = $itemQualifI->getSpecialite()->getNomspecialite();
                }


                $contenue[] = [
                    "idClient" => $idFiche,
                    "nomEtablissement" => $mission->getficheSpecialite()->getClient()->getNomEtablissement(),
                    "numeroDepartement" => $mission->getficheSpecialite()->getClient()->getNumeroDepartement(),
                    "qualif" => $qualif1,
                    "departement" => $mission->getficheSpecialite()->getClient()->getDepartement(),
                    "reference" => $mission->getReference(),
                    "mission_id" => $mission->getId(),
                    "mois" => $mission->getMois(),
                    "annee" => $mission->getAnnee(),
                    "dateenreg" => $mission->getCreateAt(),
                    "specialite" => $specialite,
                    "nomSpecialite" => $nomSpecialite,
                    "infoPratique" => $mission->getficheSpecialite()->getInfoPratique(),
                    "cp" => $mission->getficheSpecialite()->getClient()->getCp(),
                    "ville" => $mission->getficheSpecialite()->getClient()->getVille(),
                    "addresse" => $mission->getficheSpecialite()->getClient()->getAdresse(),
                    "plannings" => $contentPlanning,
                    "nombrePlannings" => sizeof($contentPlanning)
                ];
            }
        }

        return $contenue;
    }

    public  static  function formatDataSendPackToSign($data)
    {
        try {
            $client["action"] = "selectOrCreateCustomer";
            $client["number"] = Utiles::generateNumberClientSellAndSign(10, 2);
            $client["civility"] = $data["civility"];
            $client["firstname"] = $data["firstname"];
            $client["lastname"] = $data["lastname"];
            $client["email"] = $data["email"];
            $client["cell_phone"] = $data["cell_phone"];
            $client["address_1"] = $data["address_1"];
            $client["postal_code"] = $data["postal_code"];
            $client["city"] = $data["city"];
            $client["country"] = $data["country"];
            $client["customer_code"] = $data["customer_code"];
            $client["company_name"] = $data["company_name"];
            $client["registration_number"] = $data["registration_number"];
            $client["address_2"] = $data["address_2"];
            $client["job_title"] = $data["job_title"];
            $client["birthdate"] = $data["birthdate"];
            $client["birthplace"] = $data["birthplace"];
            $client["j_token"] = urldecode(Utiles::tokenSellSign());

            $signataire["firstname"] = $data["fields_firstname"];
            $signataire["lastname"] = $data["fields_lastname"];
            $signataire["civility"] = $data["fields_civility"];
            $signataire["email"] = $data["fields_email"];
            $signataire["cell_phone"] = $data["fields_cell_phone"];

            return [
                "client"=>$client,
                "signataire"=>$signataire
            ];
        }catch (\Exception $exception) {
            throw new Exception('Donnée invalide');
        }
    }

}
