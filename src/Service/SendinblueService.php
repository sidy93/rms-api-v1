<?php


namespace App\Service;


use App\Model\SenderMail;
use GuzzleHttp\Client as GuzzleHttpClient;
use SendinBlue\Client\Api\AccountApi;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\Api\EmailCampaignsApi;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Model\SendSmtpEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;

class SendinblueService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var Configuration
     */
    private $config;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * SendinblueService constructor.
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params, Environment $twig)
    {
        $this->params = $params;
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', $params->get("key_api_sendinblue"));
        $this->twig = $twig;


    }


    public function getAccount()
    {
        $apiInstance = new AccountApi(new  GuzzleHttpClient, $this->config);

    }

    /**
     * envoi mail de notification
     * @param $sender
     * @param $to
     * @param $subjet
     * @param $htmlContent
     * @param null $bcc
     * @param null $cc
     * @param null $replyto
     * @param null $attachement
     * @return \SendinBlue\Client\Model\CreateSmtpEmail
     * @throws \SendinBlue\Client\ApiException
     */

    public function notificationMail($sender, $to, $subjet, $htmlContent, $tags = null,   $cc = null, $replyto = null, $attachement = null)
    {

        $apiInstance = new SMTPApi(new  GuzzleHttpClient, $this->config);
        $sendSmtEmail = new SendSmtpEmail();




               $sendSmtEmail->setSender($sender);
               $sendSmtEmail->setTo($to);
               $sendSmtEmail->setHtmlContent($htmlContent);
               $sendSmtEmail->setSubject($subjet);
               $sendSmtEmail->setCc($cc);
               $sendSmtEmail->setTags([$tags]);
               $sendSmtEmail->setReplyTo($replyto);





               $apiInstance->sendTransacEmail($sendSmtEmail);








    }




    /**
     * genere le template à envoyer
     * @param $data
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function htmlContents($data)
    {
        return $this->twig->render("emails/base_email.html.twig", $data);
    }

    /**
     * rapport d'activité des e-mails envoyés
     * @param $startDate
     * @param $endDate
     * @param $days
     * @param $tag
     * @return \SendinBlue\Client\Model\GetAggregatedReport
     * @throws \SendinBlue\Client\ApiException
     */
    public function gregatedReport($startDate, $endDate, $days, $tag)
    {

        $apiInstance = new SMTPApi(new  GuzzleHttpClient, $this->config);

        $resultat =  $apiInstance->getAggregatedSmtpReport($startDate, $endDate, $days, $tag);

     return $resultat;

    }


    /**
     * renvoi tout les contacts
     * @param $limit
     * @param $offset
     * @param $modifiedSince
     * @return \SendinBlue\Client\Model\GetContacts
     * @throws \SendinBlue\Client\ApiException
     */
    public function getContacts($limit, $offset, $modifiedSince)
    {

        $apiInstance = new ContactsApi(new  GuzzleHttpClient, $this->config);
        $resultat =  $apiInstance->getContacts($limit, $offset, $modifiedSince);

        return $resultat;

    }

    /**
     * @param $type
     * @param $status
     * @param $startDate
     * @param $endDate
     * @param $limit
     * @param $offset
     * @return \SendinBlue\Client\Model\GetEmailCampaigns
     * @throws \SendinBlue\Client\ApiException
     */

    public function getEmailCampaigns($type, $status, $startDate, $endDate, $limit, $offset)
    {

        $apiInstance = new EmailCampaignsApi(new  GuzzleHttpClient, $this->config);
        $resultat =  $apiInstance->getEmailCampaigns();

        return $resultat;

    }

    public function  sendNotificationMail($email,$name,$type,$title,$template,$arrayValue,$action,$tag = null){

        $sender = Utiles::sender($this->params->get("sender_email"), $this->params->get("sender_name"));


        $to = Utiles::toMail($email,$name,);
        $body = [
            'url' => $this->params->get('base_url_site'),

            'type' => $type,
            'title' => $title,
            'arrayValues' => $arrayValue,
            'template' => $template ,

        ];
      //  "emails/erp/interim/creation_specialite.html.twig"
         if ($type === true)
         {
             $body["name"] = "Mr ".$name;
         }
         elseif ($type === false) {
             $body["name"] = "Dr " . $name;
         }

        $htmlContent = $this->htmlContents($body);
    $this->notificationMail($sender,$to,$action,$htmlContent,$tag);
}
}