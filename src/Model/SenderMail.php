<?php


namespace App\Model;


class SenderMail
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private  $email;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function __toString()
    {
         return  $this->name.  $this->email;
    }


}