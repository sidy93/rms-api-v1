<?php

namespace App\Repository;

use App\Entity\Signature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Signature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Signature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Signature[]    findAll()
 * @method Signature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SignatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Signature::class);
    }

    // /**
    //  * @return Signature[] Returns an array of Signature objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Signature
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllContratSignature($filter = [], $limit = 12, $offset = 0)
    {
        $qb = $this->createQueryBuilder('s');


        if (isset($filter["type"])) {
             if ($filter["type"] != 2)
                 $qb
                     ->join('s.contrat_pld', 'contrat_pld')
                     ->join('contrat_pld.candidatures',"c")
                     ->join("c.planning","p")
                     ->select(
                         "contrat_pld.idPld AS id_pld",
                         "contrat_pld.createAt As dateCreation",
                         "contrat_pld.auteur as auteurGeneration",
                         "max(c.updateAt) as dateValidationMission",
                         "min(p.date) as dateDebutMission"
                     );


        }



        if (count($filter) > 0) {


            if (!empty($filter["nom"])) {
                $qb
                    ->andWhere('s.nomSignataire LIKE :nom')
                    ->setParameter('nom', '%' . $filter["nom"] . '%');
            }
            if (!empty($filter["prenom"])) {
                $qb
                    ->andWhere('s.prenomSignataire LIKE :prenom')
                    ->setParameter('prenom', '%' . $filter["prenom"] . '%');
            }

            if (!empty($filter["reference"])) {
                $qb
                    ->andWhere('s.referenceMission LIKE :reference')
                    ->setParameter('reference', '%' . $filter["reference"] . '%');
            }

            if (!empty($filter["etablissement"])) {
                 $qb
                    ->andWhere('s.companyName LIKE :etablissement')
                    ->setParameter('etablissement', '%' . $filter["etablissement"] . '%');
            }

            if (!empty($filter["auteur"])) {
                $qb = $qb
                    ->andWhere('s.auteur LIKE :auteur')
                    ->setParameter('auteur', '%' . $filter["auteur"] . '%');
            }

            if (!empty($filter["status"])) {
                $qb = $qb
                    ->andWhere('s.statusTranasaction LIKE :status')
                    ->setParameter('status', '%' . $filter["status"] . '%');
            }

            if (!empty($filter["mode_signature"])) {
                $qb = $qb
                    ->andWhere('s.signatureMode LIKE :mode_signature')
                    ->setParameter('mode_signature', '%' . $filter["mode_signature"] . '%');
            }


            if (isset($filter["type"])) {

                $qb = $qb
                    ->andWhere('s.type = :type')
                    ->setParameter('type', $filter["type"]);
            }



            if (!empty($filter["size"] || !empty($filter["offset"]))) {
                $limit = $filter["size"];
                $offset = $filter["offset"];
            }
        }

        $qb
            ->addSelect(
                "s.id",
                "CONCAT(s.civilitySignataire, ' ', s.nomSignataire, ' ', s.prenomSignataire) AS signataire",
                "s.referenceMission As reference",
                "s.companyName As etablissement",
                "s.address1",
                "s.signatureMode",
                "s.signatureDate",
                "s.signatureStatus",
                "s.cellPhoneSignataire AS phone",
                "s.city AS ville",
                "s.dateEnvoi",
                "s.signatureDate AS dateSignatureSignataire",
                "s.dateSignature As dateValidationSignature",
                "s.emailSignataire AS email",
                "s.contratId AS contratId",
                "s.statusTranasaction AS status",
                "s.filename AS modele",
                "s.specialite",
                "s.relance",
                "s.transactionId",
                "s.dateDernierRelance AS dateRelance",
                "s.remarque"

            );

      return $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->groupBy("s.id")
            ->orderBy('s.id', 'DESC')
            ->getQuery()
            ->getResult();






    }
}
