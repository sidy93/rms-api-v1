<?php

namespace App\Repository;

use App\Entity\Planning;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Planning|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planning|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planning[]    findAll()
 * @method Planning[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Planning::class);
    }

    // /**
    //  * @return Planning[] Returns an array of Planning objects
    //  */




    /*
    public function findOneBySomeField($value): ?Planning
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getPlanningByMissionId($idMission)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT
                    m.id as mission_id, p.create_at, tv.designation, tv.id as id_type_vacation, stp.designation as statut, stp.id as statutId, tg.id as typeDeGardeId,
                    tg.heure_debut, tg.heure_fin, p.id as planning_id, p.tarif, p.salaire_net, p.date, p.qualifications, p.salaire_brute
                FROM
                    planning p
                JOIN type_de_garde tg ON 
                    tg.id = p.type_de_garde_id
                join vacation tv on 
                    tv.id = tg.type_vacation_id
                join statut_planning stp on
                stp.id = p.statut_id  
                join mission m on 
                m.id = p.mission_id
                WHERE m.id = :idMission                                             
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'idMission' => $idMission,

            ]
        );

        return $stmt->fetchAll();
    }

    public function getPlanningByMissionIdAndStatut($idMission, $statut)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT
                    m.id as mission_id, p.create_at, tv.designation, tv.id as id_type_vacation, stp.designation as statut, stp.id as statutId, tg.id as typeDeGardeId,
                    tg.heure_debut, tg.heure_fin, p.id as planning_id, p.tarif, p.salaire_net, p.date, p.qualifications, p.salaire_brute
                FROM
                    planning p
                JOIN type_de_garde tg ON 
                    tg.id = p.type_de_garde_id
                join vacation tv on 
                    tv.id = tg.type_vacation_id
                join statut_planning stp on
                stp.id = p.statut_id  
                join mission m on 
                m.id = p.mission_id
                WHERE m.id = :idMission and stp.designation = :statut                                          
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'idMission' => $idMission,
                'statut' => $statut,

            ]
        );

        return $stmt->fetchAll();
    }

    public function getPlanningByMissionIdAndStatutNonPourvueORAPourvoir($idMission)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT
                    m.id as mission_id, p.create_at, tv.designation, tv.id as id_type_vacation, stp.designation as statut, stp.id as statutId, tg.id as typeDeGardeId,
                    tg.heure_debut, tg.heure_fin, p.id as planning_id, p.tarif, p.salaire_net, p.date, p.qualifications, p.salaire_brute
                FROM
                    planning p
                JOIN type_de_garde tg ON 
                    tg.id = p.type_de_garde_id
                join vacation tv on 
                    tv.id = tg.type_vacation_id
                join statut_planning stp on
                stp.id = p.statut_id  
                join mission m on 
                m.id = p.mission_id
                WHERE m.id = :idMission and stp.designation != :aPourvoir and stp.designation != :pourvue                                         
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'idMission' => $idMission,
                'aPourvoir' => 'A Pourvoir',
                'pourvue' => 'Pourvue',

            ]
        );

        return $stmt->fetchAll();
    }

    public function getGroupByDatePlanningByMissionById($idMission)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT
                    p.date
                FROM
                    planning p
                JOIN type_de_garde tg ON 
                    tg.id = p.type_de_garde_id
                join vacation tv on 
                    tv.id = tg.type_vacation_id
                join statut_planning stp on
                stp.id = p.statut_id  
                join mission m on 
                m.id = p.mission_id
                WHERE m.id = :idMission 
                group by p.date                                        
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'idMission' => $idMission,

            ]
        );

        return $stmt->fetchAll();
    }

    public function getDatePlanningByMissionIdAndGroupeDate($idMission, $date)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT
                    p.id
                FROM
                    planning p
                JOIN type_de_garde tg ON 
                    tg.id = p.type_de_garde_id
                join vacation tv on 
                    tv.id = tg.type_vacation_id
                join statut_planning stp on
                stp.id = p.statut_id  
                join mission m on 
                m.id = p.mission_id
                WHERE m.id = :idMission and p.date = :date
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'idMission' => $idMission,
                'date' => $date,

            ]
        );

        return $stmt->fetchAll();
    }
}
