<?php

namespace App\Repository;

use App\Entity\Candidature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Candidature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Candidature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Candidature[]    findAll()
 * @method Candidature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Candidature::class);
    }

    public function getCandidatsOnMission($missionId)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
                SELECT
                    m.id as idMission, u.nom, u.prenom, u.id as userId, u.email, i.id as interimId
                FROM
                    candidature c
                JOIN planning p ON
                    p.id = c.planning_id
                JOIN mission m ON 
                    m.id = p.mission_id
                JOIN statut_candidature s ON 
                    s.id = c.statut_id
                JOIN mission_interim mi ON 
                    c.mission_interims_id = mi.id
                JOIN interim i ON 
                    i.id = mi.interim_id   
                JOIN user u on 
                    i.user_id = u.id   
                WHERE m.id = :mission_id
                group by i.id, idMission
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'mission_id' => $missionId,
            ]
        );

        return $stmt->fetchAll();
    }

    public function getNombreCandidaturesByStatutOnMission($missionId)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
                SELECT
                    count(s.designation) as occurences, s.designation, s.id
                FROM
                    candidature c
                JOIN planning p ON
                    p.id = c.planning_id
                JOIN mission m ON 
                    m.id = p.mission_id
                JOIN statut_candidature s ON 
                    s.id = c.statut_id
                WHERE m.id = :mission_id
                group by s.id
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'mission_id' => $missionId,
            ]
        );

        return $stmt->fetchAll();
    }

    public function getCandidaturesInterimOnMissionByIdMissionAndIdInterim($missionId, $interimId)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
                SELECT
                    c.id as candidatureId, DATE_FORMAT(p.date,"%d-%m-%Y") as date, DATE_FORMAT(p.date,"%m-%d-%Y") as dateMoment, p.tarif, p.salaire_net, p.salaire_brute, p.id as planningId, s.designation as statutCandidature,
                    p.qualifications, tg.heure_debut as debut, tg.heure_fin as fin, tv.id as vacationId, tv.designation as garde, s.code_couleur, i.id as interimId, mi.id as mmId
                FROM    
                    candidature c
                JOIN planning p ON
                    p.id = c.planning_id
                JOIN mission m ON 
                    m.id = p.mission_id
                JOIN statut_candidature s ON 
                    s.id = c.statut_id
                JOIN mission_interim mi ON 
                    c.mission_interims_id = mi.id
                JOIN interim i ON 
                    i.id = mi.interim_id   
                JOIN user u on 
                    i.user_id = u.id
                JOIN type_de_garde tg ON 
                    tg.id = p.type_de_garde_id
                join vacation tv on 
                    tv.id = tg.type_vacation_id   
                WHERE m.id = :mission_id
                AND i.id = :interim_id
                group by c.id
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'mission_id' => $missionId,
                'interim_id' => $interimId,
            ]
        );

        return $stmt->fetchAll();
    }
    public function getCLientByIdCandidature($idCandidature)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
            SELECT
            cl.raison_social raison_sociale,
            cl.id idClient,
            m.reference ref_mission,
            fs.id ficheSpecialite
            FROM candidature c
            inner join planning p on c.planning_id = p.id
            inner join mission m on p.mission_id = m.id
            inner join fiche_specialite fs on m.fiche_specialite_id = fs.id
            inner join client cl on fs.client_id = cl.id
            where c.id=:idCandidature 
         ";
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'idCandidature' => $idCandidature
        ]);
        return $stmt->fetchAll()[0];
    }

    public function getCandidatureRefractor($idCandidature)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
            SELECT
            c.id,
            c.contrat_pld_id contrat_pld,
            sc.designation statut_candidature,
            sc.code_couleur statut_candidature_couleur,
            p.date,
            p.tarif,
            p.salaire_net,
            p.salaire_brute,
            t.heure_debut,
            t.heure_fin,
            t.taux_reference,
            c.create_at,
            v.designation vacation,
            v.abreviation abreviationVacation
            FROM candidature c
            inner join statut_candidature sc on c.statut_id = sc.id
            inner join planning p on c.planning_id = p.id
            inner join type_de_garde t on p.type_de_garde_id = t.id
            inner join vacation v on t.type_vacation_id = v.id
            where c.id=:idCandidature 
         ";
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'idCandidature' => $idCandidature
        ]);
        return $stmt->fetchAll()[0];
    }

    public function getCandidatureOnDateWithLibelleForInterim($interim_id, $id_candidature, $planning_date, $array_libelle)
    {
        $query = $this->createQueryBuilder('c')
            ->join('c.missionInterims', 'mm')
            ->join('c.planning', 'p')
            ->join('c.statut', 's')
            ->join('p.typeDeGarde', 't')
            ->join('t.typeVacation', 'tv')
            ->join('p.mission', 'mission')
            ->join('mission.ficheSpecialite', 'f')
            ->join('f.client', 'client')
            ->join('mm.interim', 'i')
            ->select('client.nomEtablissement AS Nom_E, c.id AS id_candidature, tv.designation')
            ->andWhere('i.id = :id_interim')
            ->andWhere('p.date = :dateplanning')
            ->andWhere('c.id != :candidature_id')
            ->andWhere('s.designation = :statut')
            ->andWhere('tv.designation IN (:array_libelle)')
            ->setParameter('id_interim', $interim_id)
            ->setParameter('statut', "Validee")
            ->setParameter('candidature_id', $id_candidature)
            ->setParameter('dateplanning', $planning_date)
            ->setParameter('array_libelle', $array_libelle);
        return $query->getQuery()->getResult();
    }

    public function getSameCandidaturesforDateAndLibelleGarde($interim_id, $date, $libelleGarde, $candi_Id)
    {
        $query = $this->createQueryBuilder('c')
            ->join('c.missionInterims', 'mm')
            ->join('c.planning', 'p')
            ->join('p.typeDeGarde', 't')
            ->join('t.typeVacation', 'tv')
            ->join('c.statut', 's')
            ->join('mm.interim', 'i')
            ->andWhere('i.id = :id_interim')
            ->andWhere('p.date = :dateplanning')
            ->andWhere('c.id != :candidature_id')
            ->andWhere('tv.designation = :libelleGarde')
            ->andWhere('s.designation = :statut')
            ->setParameter('id_interim', $interim_id)
            ->setParameter('statut', "En attente")
            ->setParameter('libelleGarde', $libelleGarde)
            ->setParameter('candidature_id', $candi_Id)
            ->setParameter('dateplanning', $date);
        return $query->getQuery()->getResult();
    }

    /**
     * @param $idPld
     * @return int|mixed|string
     */
    public function getLastCandidatureValideeByIdpld($idPld)
    {
       return $this->createQueryBuilder('c')
            ->join('c.contratPld', 'contrat_pld')
            ->join("c.statut","statut")
            ->join("c.planning","p")
            ->andWhere('contrat_pld.idPld = :id_pld')
            ->andWhere('statut.designation = :st')
            ->setParameter('st', "Validee")
            ->setParameter('id_pld', $idPld)
            ->select("c.createAt as dateCreation","c.updateAt AS dateValidation","p.date As planning")
            ->getQuery()
            ->getResult();


    }

}
