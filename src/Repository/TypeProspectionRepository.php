<?php

namespace App\Repository;

use App\Entity\TypeProspection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeProspection|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeProspection|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeProspection[]    findAll()
 * @method TypeProspection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeProspectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeProspection::class);
    }

    // /**
    //  * @return TypeProspection[] Returns an array of TypeProspection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeProspection
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
