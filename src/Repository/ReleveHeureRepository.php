<?php

namespace App\Repository;

use App\Entity\ReleveHeure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReleveHeure|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReleveHeure|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReleveHeure[]    findAll()
 * @method ReleveHeure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReleveHeureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReleveHeure::class);
    }

    // /**
    //  * @return ReleveHeure[] Returns an array of ReleveHeure objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReleveHeure
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
