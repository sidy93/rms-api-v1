<?php

namespace App\Repository;

use App\Entity\Renumeration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Renumeration|null find($id, $lockMode = null, $lockVersion = null)
 * @method Renumeration|null findOneBy(array $criteria, array $orderBy = null)
 * @method Renumeration[]    findAll()
 * @method Renumeration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RenumerationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Renumeration::class);
    }

    // /**
    //  * @return Renumeration[] Returns an array of Renumeration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Renumeration
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAll()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
            SELECT
            r.id,
            con.id_pld as idContratPld,
            i.id as idInterim,
            CONCAT(u.nom, ' ', u.prenom) as interim

            FROM renumeration r
            inner join contrat con on r.contrat_id = con.id
            inner join candidature c on c.contrat_pld_id = con.id
            inner join mission_interim mm on mm.id = c.mission_interims_id
            inner join interim i on i.id = mm.interim_id
            inner join user u on u.id = i.user_id
            GROUP by idInterim
            order by u.nom ASC
         ";
        $stmt = $conn->prepare($sql);
        $stmt->execute([]);
        return $stmt->fetchAll();
    }
    public function returnNbreContratByidInterim($id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
            SELECT
            r.id,
            con.id_pld as idContratPld,
            con.id as idContratErp,
            cl.raison_social as client,
            cl.id as idClient,
            mis.reference,
            p.tarif,
            p.salaire_net
            FROM renumeration r
            inner join contrat con on r.contrat_id = con.id
            inner join candidature c on c.contrat_pld_id = con.id
            inner join mission_interim mm on mm.id = c.mission_interims_id
            inner join interim i on i.id = mm.interim_id
            inner join user u on u.id = i.user_id
            inner join planning p on p.id = c.planning_id
            inner join mission mis on mis.id = p.mission_id
            inner join fiche_specialite fs on fs.id = mis.fiche_specialite_id
            inner join client cl on cl.id = fs.client_id
            inner join fiche_specialite_qualification fsq on fsq.fiche_specialite_id = fs.id
            inner join qualification q on q.id = fsq.qualification_id
            inner join specialite s on s.id = q.specialite_id
            WHERE i.id = :idInterim
            GROUP by idContratPld
         ";
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'idInterim' => $id
        ]);
        return $stmt->fetchAll();
    }
    public function findByIdInterim($id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
            SELECT
            r.id,
            con.id_pld as idContratPld,
            r.voiture,
            r.montant_train,
            r.montant_avion,
            r.montatnt_taxi,
            r.montant_bus,
            r.montant_covoiturage,
            r.montant_parking,
            r.montant_location_voiture,
            r.montant_hotel,
            r.montant_ticket,
            r.montant_repas,
            r.create_at,
            r.auteur,
            i.id as idInterim,
            cl.raison_social as client,
            mis.reference,
            mis.mois,
            mis.annee,
            s.nom_specialite as specialite,
            cl.id as idClient,
            CONCAT(u.nom, ' ', u.prenom) as interim

            FROM renumeration r
            inner join contrat con on r.contrat_id = con.id
            inner join candidature c on c.contrat_pld_id = con.id
            inner join mission_interim mm on mm.id = c.mission_interims_id
            inner join interim i on i.id = mm.interim_id
            inner join user u on u.id = i.user_id
            inner join planning p on p.id = c.planning_id
            inner join mission mis on mis.id = p.mission_id
            inner join fiche_specialite fs on fs.id = mis.fiche_specialite_id
            inner join client cl on cl.id = fs.client_id
            inner join fiche_specialite_qualification fsq on fsq.fiche_specialite_id = fs.id
            inner join qualification q on q.id = fsq.qualification_id
            inner join specialite s on s.id = q.specialite_id
            GROUP by idInterim
         ";
        $stmt = $conn->prepare($sql);
        $stmt->execute([]);
        return $stmt->fetchAll();
    }

    public function findInterimAndClientByIdContrat($idContratPld)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
            SELECT
            cl.raison_social as client,
            cl.id as idClient,
            mis.reference,
            i.id as idInterim,
            i.coef_ik,
            CONCAT(u.adresse, ' , ', u.cp , ' ', u.ville) as adresseInterim,
            CONCAT(cl.adresse, ' , ', cl.cp , ' ', cl.ville) as adresseClient,
            CONCAT(u.nom, ' ', u.prenom) as interim
            FROM renumeration r
            inner join contrat con on r.contrat_id = con.id
            inner join candidature c on c.contrat_pld_id = con.id
            inner join mission_interim mm on mm.id = c.mission_interims_id
            inner join interim i on i.id = mm.interim_id
            inner join user u on u.id = i.user_id
            inner join planning p on p.id = c.planning_id
            inner join mission mis on mis.id = p.mission_id
            inner join fiche_specialite fs on fs.id = mis.fiche_specialite_id
            inner join client cl on cl.id = fs.client_id
            inner join fiche_specialite_qualification fsq on fsq.fiche_specialite_id = fs.id
            inner join qualification q on q.id = fsq.qualification_id
            inner join specialite s on s.id = q.specialite_id
            WHERE con.id_pld = :idContratPld
            GROUP by con.id_pld
         ";
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'idContratPld' => $idContratPld
        ]);
        return $stmt->fetchAll()[0];
    }
}
