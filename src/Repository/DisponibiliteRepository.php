<?php

namespace App\Repository;

use App\Entity\Disponibilite;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Disponibilite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Disponibilite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Disponibilite[]    findAll()
 * @method Disponibilite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisponibiliteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Disponibilite::class);
    }

    // /**
    //  * @return Disponibilite[] Returns an array of Disponibilite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Disponibilite
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */



    // cette requete recupere la liste des  disponibiltés des interimaires
    public function findAllVisibleQueryDisponibilite($data = [], $limit = 12, $offset = 0)
    {
        $qb = $this->createQueryBuilder('d');
        $qb
            ->join('d.interim', 'i')
            ->join('i.user', 'u');


        if (count($data) >  0) {

            if (!empty($data["nom"])) {
                $qb = $qb
                    ->andWhere('u.nom LIKE :nom')
                    ->setParameter('nom', '%' . $data['nom'] . '%');

            }

            if (!empty($data["email"])) {
                $qb = $qb
                    ->andWhere('u.email LIKE :email')
                    ->setParameter('email', '%' . $data['email'] . '%');

            }


            if (!empty($data["prenom"])) {

                $qb = $qb
                    ->andWhere('u.prenom LIKE :prenom')
                    ->setParameter('prenom', '%' . $data['prenom'] . '%');
            }
            if (!empty($data["ville"])) {

                $qb = $qb
                    ->andWhere('u.ville LIKE :ville')
                    ->setParameter('ville', '%' . $data['ville'] . '%');
            }
            if (!empty($data["departement"])) {

                $qb = $qb
                    ->andWhere('u.departement LIKE :departement')
                    ->setParameter('departement', '%' . $data['departement'] . '%');
            }
            if (!empty($data["adresse"])) {
                $qb = $qb
                    ->andWhere('u.adresse LIKE :adresse')
                    ->setParameter('adresse', '%' . $data['adresse'] . '%');
            }




            if (!empty($data["specialite"])) {
                $qb = $qb
                    ->leftJoin('i.qualifications', 'q')
                    ->leftJoin('q.specialite', 's')
                    ->andWhere('s.nomspecialite LIKE :specialite')
                    ->setParameter('specialite', '%' . $data['specialite'] . '%');
            }


            if (!empty($data["limit"]) &&  !empty($data["offset"])) {
                $limit = $data["limit"];
                $offset = $data["offset"];
            }

            if (!empty($data["date_debut"]) && !empty($data["date_fin"])) {

                $qb = $qb
                    ->andWhere('d.date BETWEEN :from AND :to')
                    ->setParameter('from', $data["date_debut"])
                    ->setParameter('to', data["date_fin"]);
            }
        } else {
            $qb = $qb
                ->andWhere('d.date >= :now')
                ->setParameter('now', new DateTime('now'));


        }





        return $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->select(
                "u.civilite",
                "d.id as id",
                "i.id as id_interim",
                "u.nom",
                "u.prenom",
                "u.email",
                "u.ville",
                "u.telephone as telephone",
                "u.departement"


            )
            ->orderBy('d.id', 'DESC')
            ->groupBy("i.id")
            ->getQuery()
            ->getResult();



    }

}
