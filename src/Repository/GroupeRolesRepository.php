<?php

namespace App\Repository;

use App\Entity\GroupeRoles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GroupeRoles|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroupeRoles|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroupeRoles[]    findAll()
 * @method GroupeRoles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupeRolesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroupeRoles::class);
    }

    // /**
    //  * @return GroupeRoles[] Returns an array of GroupeRoles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GroupeRoles
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
