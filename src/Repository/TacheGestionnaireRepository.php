<?php

namespace App\Repository;

use App\Entity\TacheGestionnaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TacheGestionnaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method TacheGestionnaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method TacheGestionnaire[]    findAll()
 * @method TacheGestionnaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TacheGestionnaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TacheGestionnaire::class);
    }

    // /**
    //  * @return TacheGestionnaire[] Returns an array of TacheGestionnaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TacheGestionnaire
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
