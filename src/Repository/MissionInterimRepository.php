<?php

namespace App\Repository;

use App\Entity\MissionInterim;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MissionInterim|null find($id, $lockMode = null, $lockVersion = null)
 * @method MissionInterim|null findOneBy(array $criteria, array $orderBy = null)
 * @method MissionInterim[]    findAll()
 * @method MissionInterim[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissionInterimRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MissionInterim::class);
    }

    // /**
    //  * @return MissionInterim[] Returns an array of MissionInterim objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MissionInterim
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function queryJoinMM()
    {
        $qb = $this->createQueryBuilder('mm');

        $qb
            ->join('mm.interim', 'i')
            ->join('i.user', 'u')
            ->join("mm.candidatures", 'c')
            ->join("c.planning","p")
            ->join("p.mission","mis")
            ->join('mis.fiche_specialite', 'fs')
            ->join('fs.client', 'cl')
            ->join('fs.qualifications', 'qualif')
            ->join('qualif.specialite', 's');

        return $qb;

    }

    public  function getMissionInterimWithPlanning($plannig,$interim)
    {
        return $this->createQueryBuilder('m_i')
            ->innerJoin("m_i.candidatures","c")
            ->innerJoin("c.planning","p")
            ->innerJoin("m_i.interim","i")
            ->andWhere(' p.id= :id_planning')
            ->andWhere('i.id = :interim')
            ->setParameters(
                [
                    "id_planning"=>$plannig,
                    "interim"=>$interim
                ]
            )
            ->groupBy("p.id","c.id")
            ->select("m_i.id")
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllRenew()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
                SELECT
                mm.id,
                CONCAT( u.prenom, ' ', u.nom ) as interim,
                i.id idInterim,
                mm.create_at
                FROM mission_interim mm
                inner join interim i on mm.interim_id = i.id
                inner join user u on i.user_id = u.id
                ORDER BY
                    mm.id desc
             ";
        $stmt = $conn->prepare($sql);
        $stmt->execute([]);
        return $stmt->fetchAll();
    }

    public function findAllMissisonInterim($filter = [], $limit = 12, $offset = 0)
    {
        $qb = $this->createQueryBuilder('mm');



        $qb
            ->join('mm.interim', 'i')
            ->join('i.user', 'u');
        if (count($filter) > 0) {



             if (
                 !empty($filter["mois"]) ||
                 !empty($filter["annee"]) ||
                 !empty($filter["reference"])||
                 !empty($filter["specialite"]) ||
                 !empty($filter["etablissement"] )
             ){
                   $qb ->join("mm.candidatures", 'c')
                       ->join("c.planning","p")
                       ->join("p.mission","mis")
                       ->join('mis.ficheSpecialite', 'fs');

             }

            if (!empty($filter["nom"])) {
                $qb
                    ->andWhere('u.nom LIKE :nomInterim')
                    ->setParameter('nomInterim', '%' . $filter["nom"] . '%');
            }
            if (!empty($filter["prenom"])) {
                $qb
                    ->andWhere('u.prenom LIKE :prenomInterim')
                    ->setParameter('prenomInterim', '%' . $filter["prenom"] . '%');
            }
            if (!empty($filter["mois"])) {
                $qb
                    ->andWhere('mis.mois LIKE :mois')
                    ->setParameter('mois', '%' . $filter["mois"] . '%');
            }
            if (!empty($filter["annee"])) {
                $qb
                    ->andWhere('mis.annee LIKE :annee')
                    ->setParameter('annee', '%' . $filter["annee"] . '%');
            }
            if (!empty($filter["specialite"])) {
                $qb
                    ->join('fs.qualification', 'qualif')
                    ->join('qualif.specialite', 's')
                    ->andWhere('s.nomspecialite LIKE :specialite')
                    ->setParameter('specialite', '%' .$filter["specialite"]. '%');
            }
            if (!empty($filter["etablissement"])) {
                $qb = $qb
                    ->join('fs.client', 'cl')
                    ->andWhere('cl.nomEtablissement LIKE :etablissement')
                    ->setParameter('etablissement', '%' . $filter["etablissement"] . '%');
            }

            if (!empty($filter["auteur"])) {
                $qb = $qb
                    ->andWhere('mis.auteur LIKE :auteur')
                    ->setParameter('auteur', '%' . $filter["auteur"] . '%');
            }
            if(!empty($filter["createAt"])){
                $qb = $qb->orderBy('mis.createAt', $filter["createAt"]);
            }
            if (!empty($filter["reference"])) {
                $qb = $qb
                    ->andWhere('mis.reference LIKE :reference')
                    ->setParameter('reference', '%' . $filter["reference"]. '%');
            }

            if (!empty($filter["limit"] || !empty($filter["offset"]))) {
                  $limit = $filter["limit"];
                  $offset = $filter["offset"];
            }
        }

        $qb
            ->select(
                "mm.id",
                      "CONCAT(u.nom, ' ', u.prenom) AS interim",
                      "i.id AS idInterim",
                      "mm.createAt",
                      "mm.auteur"
            );

      return $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->groupBy("mm.id")
            ->orderBy('mm.id', 'DESC')
            ->getQuery()
            ->getResult();



    }

}
