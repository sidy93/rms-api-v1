<?php

namespace App\Repository;

use App\Entity\ProspectionInterim;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProspectionInterim|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProspectionInterim|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProspectionInterim[]    findAll()
 * @method ProspectionInterim[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProspectionInterimRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProspectionInterim::class);
    }

    // /**
    //  * @return ProspectionInterimFixtures[] Returns an array of ProspectionInterimFixtures objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProspectionInterimFixtures
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
