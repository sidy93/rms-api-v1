<?php

namespace App\Repository;

use App\Entity\Mission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mission|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mission|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mission[]    findAll()
 * @method Mission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mission::class);
    }

    public function getMissionByArrayDatesDispo($dates)
    {
        return $this->createQueryBuilder('m')
            ->innerJoin("m.planning", "p")
            ->innerJoin("m.ficheSpecialite", "f")
            ->innerJoin("f.client", "c")
            ->innerJoin("p.typeDeGarde", "tg")
            ->innerJoin("tg.typeVacation", "v")
            ->andWhere('p.date IN (:dates)')
            ->setParameter('dates', $dates)
            ->orderBy('p.id', 'ASC')
            ->select("p.id as planning_id",
                "m.id as mission_id",
                "p.date",
                "m.reference",
                "c.id as client_id",
                "c.nomEtablissement",
                "p.salaireNet",
                "tg.heureDebut",
                "tg.heureFin",
                "v.designation"

            )
            ->getQuery()
            ->getResult();
    }

    // return les  dates de planning a pourvoir pour un interim
    public function getMisssionForInterim($interim)
    {
        return $this->createQueryBuilder('m')
            ->innerJoin("m.planning", "p")
            ->innerJoin("p.candidature", "c")
            ->innerJoin("c.missionInterims", "m_i")
            ->innerJoin("m_i.interim", "i")
            ->innerJoin("m.ficheSpecialite", "fs")
            ->innerJoin("fs.client", "c")
            ->andWhere('i.id = :interim')
            ->setParameters(
                [
                    "interim" => $interim
                ]
            )
            ->groupBy("p.id", "c.id")
            ->select("m_i.id", "p.date")
            ->getQuery()
            ->getResult();
    }

    public function getListMissionForClient($idClient)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT
                    m.id, m.mois, m.reference, m.annee, m.jtc, m.enable, m.create_at, fs.id as fiche_id, s.nom_specialite
                FROM
                    mission m
                JOIN Fiche_specialite fs ON
                    m.fiche_specialite_id = fs.id
                JOIN fiche_specialite_qualification fsq on 
                fsq.fiche_specialite_id = fs.id
                join qualification q on
                q.id = fsq.qualification_id
                join specialite s on 
                s.id = q.specialite_id
                join Client c on 
                fs.client_id = c.id
                WHERE c.id = :client_id 
                group by m.id                                           
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'client_id' => $idClient,

            ]
        );

        return $stmt->fetchAll();
    }

    public function getOffresMissionForClient($idClient)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT
                    m.id, DATE_FORMAT(m.debut,"%d-%m-%Y") as debut, DATE_FORMAT(m.fin,"%d-%m-%Y") as fin, m.rythme, m.domaine, m.salaire, m.reference, m.poste, m.type_remuneration,
                    DATE_FORMAT(m.create_at,"%d-%m-%Y") as createAt, s.nom_specialite, m.contrat, m.enable, DATE_FORMAT(m.debut,"%Y-%m-%d") as dateDebut, DATE_FORMAT(m.fin,"%Y-%m-%d") as dateFin
                FROM
                    offres m
                JOIN Fiche_specialite fs ON
                    m.fiche_specialite_id = fs.id
                JOIN fiche_specialite_qualification fsq on 
                fsq.fiche_specialite_id = fs.id
                join qualification q on
                q.id = fsq.qualification_id
                join specialite s on 
                s.id = q.specialite_id
                join Client c on 
                fs.client_id = c.id
                WHERE c.id = :client_id 
                group by m.id                                           
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'client_id' => $idClient,

            ]
        );

        return $stmt->fetchAll();
    }

    public function getMissionByIdClientAndReference($reference, $idClient = null)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
                SELECT
                    m.id, m.mois, m.reference, m.annee, m.jtc, m.enable, m.create_at, fs.id as fiche_id, s.nom_specialite
                FROM
                    mission m
                JOIN Fiche_specialite fs ON
                    m.fiche_specialite_id = fs.id
                JOIN fiche_specialite_qualification fsq on 
                fsq.fiche_specialite_id = fs.id
                join qualification q on
                q.id = fsq.qualification_id
                join specialite s on 
                s.id = q.specialite_id
                join Client c on 
                fs.client_id = c.id
                WHERE c.id = :client_id and m.reference = :reference
                Group by m.id
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'client_id' => $idClient,
                'reference' => $reference

            ]
        );

        return $stmt->fetchAll();
    }

    public function getOffreByIdClientAndReference($reference, $idClient = null)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
                SELECT
                    m.id, DATE_FORMAT(m.debut,"%d-%m-%Y") as debut, DATE_FORMAT(m.fin,"%d-%m-%Y") as fin, m.rythme, m.domaine, m.salaire, m.reference, m.poste, m.type_remuneration,
                    m.create_at, s.nom_specialite, m.contrat, m.enable, 
                    DATE_FORMAT(m.debut,"%Y-%m-%d") as dateDebut, DATE_FORMAT(m.fin,"%Y-%m-%d") as dateFin
                FROM
                    offres m
                JOIN Fiche_specialite fs ON
                    m.fiche_specialite_id = fs.id
                JOIN fiche_specialite_qualification fsq on 
                fsq.fiche_specialite_id = fs.id
                join qualification q on
                q.id = fsq.qualification_id
                join specialite s on 
                s.id = q.specialite_id
                join Client c on 
                fs.client_id = c.id
                WHERE c.id = :client_id and m.reference = :reference
                Group by m.id
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'client_id' => $idClient,
                'reference' => $reference

            ]
        );

        return $stmt->fetchAll();
    }

    // renvoi les dates de planning qu'un medecin peu postuler
    public function getPlanningWithoutCandidatureForInterim($id_interim, $id_mission)
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                    SELECT
                        p.id AS planning_id,
                        m.id AS mission_id,
                        m.reference AS reference,
                        p.date AS date,
                        fs.id As fs,
                        p.salaire_net AS salaireNet,
                        sp.designation AS statut,
                        tv.designation AS designation,
                        tg.id AS typeGarde_id,
                        tg.heure_debut AS heureDebut,
                        tg.heure_fin AS heureFin
                    FROM
                        planning p
                    JOIN mission m ON
                        m.id = p.mission_id
                    JOIN  fiche_specialite fs ON 
                         fs.id = m.fiche_specialite_id 
                    JOIN type_de_garde tg ON
                        tg.id = p.type_de_garde_id
                    JOIN vacation tv ON
                        tv.id = tg.type_vacation_id
                    JOIN statut_planning sp ON
                        sp.id = p.statut_id
                    WHERE
                       m.id = :mission_id AND sp.id = :planning_statut AND (
                            p.date,
                            p.tarif,
                            p.salaire_net,
                            sp.designation,
                            tg.id
                        ) NOT IN(
                        SELECT
                            pp.date,
                            pp.tarif,
                            pp.salaire_net,
                            spp.designation,
                            tg.id
                        FROM
                            candidature cc
                        JOIN mission_interim mmm ON
                            mmm.id = cc.mission_interims_id
                        JOIN interim ii ON
                            ii.id = mmm.interim_id
                        JOIN planning pp ON
                            pp.id = cc.planning_id
                        JOIN mission mis ON
                            mis.id = pp.mission_id
                        JOIN type_de_garde tg ON
                            tg.id = pp.type_de_garde_id
                        JOIN vacation tv ON
                            tv.id = tg.type_vacation_id
                        JOIN statut_planning spp ON
                            spp.id = pp.statut_id
                        WHERE
                            ii.id = :interim_id AND mis.id = :mission_id
                    )
                    GROUP BY
                        p.date,
                        m.id,
                        p.tarif,
                        p.salaire_net,
                        sp.designation,
                        tv.designation,
                        tg.id
                    ORDER BY
                        p.date
              ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'planning_statut' => 1,
                'interim_id' => $id_interim,
                'mission_id' => $id_mission
            ]
        );

        return $stmt->fetchAll();
    }

    // revoi les planning de mission a pourvoir et activer
    public function getMissionAdPlanningAPourvoir($specialite = null)
    {
        $now = new  \DateTime("now");
        $qb =  $this->createQueryBuilder('m')
            ->innerJoin("m.planning","p")
            ->innerJoin("p.statut","s")
            ->andWhere('m.enable = :enable')
            ->andWhere('s.designation = :statut')
            ->andWhere('p.date >= :now')
            ->setParameters(
                [
                    "enable" => true,
                    "now" => $now,
                    "statut" => "A Pourvoir"
                ]
            );

        if ($specialite) {
            $qb = $qb
                ->leftJoin('m.ficheSpecialite', 'fs')
                ->leftJoin('fs.qualification', 'q')
                ->andWhere('q.id  IN (:ids)')
                ->setParameter('ids', $specialite);
        }


       return
           $qb->groupBy("m.id")
            ->orderBy("m.id", "DESC")
            ->select("m.id")
            ->getQuery()
            ->getResult();
    }

    public function getMissionsForClient($idClient)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
                SELECT
                    m.id, m.reference, m.mois, m.annee,
                    m.create_at, s.nom_specialite, m.enable, m.jtc
                FROM
                    mission m
                JOIN Fiche_specialite fs ON
                    m.fiche_specialite_id = fs.id
                JOIN fiche_specialite_qualification fsq on 
                fsq.fiche_specialite_id = fs.id
                join qualification q on
                q.id = fsq.qualification_id
                join specialite s on 
                s.id = q.specialite_id
                join Client c on 
                fs.client_id = c.id
                WHERE c.id = :client_id
                Group by m.id
             ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(
            [
                'client_id' => $idClient,
            ]
        );

        return $stmt->fetchAll();
    }

}
