<?php

namespace App\Repository;

use App\Entity\RappelSecurite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RappelSecurite|null find($id, $lockMode = null, $lockVersion = null)
 * @method RappelSecurite|null findOneBy(array $criteria, array $orderBy = null)
 * @method RappelSecurite[]    findAll()
 * @method RappelSecurite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RappelSecuriteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RappelSecurite::class);
    }

    // /**
    //  * @return RappelSecurite[] Returns an array of RappelSecurite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RappelSecurite
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
