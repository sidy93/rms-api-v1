<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201201111011 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE justificatif DROP FOREIGN KEY FK_90D3C5DC1823061F');
        $this->addSql('DROP INDEX IDX_90D3C5DC1823061F ON justificatif');
        $this->addSql('ALTER TABLE justificatif ADD remuneration_id INT NOT NULL, ADD adresse_interim VARCHAR(255) DEFAULT NULL, ADD adresse_hopital VARCHAR(255) DEFAULT NULL, DROP contrat_id, DROP adresse, DROP adress_hopital, CHANGE nombre_trajet nombre_trajet DOUBLE PRECISION DEFAULT NULL, CHANGE coef_ik coef_ik DOUBLE PRECISION DEFAULT NULL, CHANGE nombre_km nombre_km DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE justificatif ADD CONSTRAINT FK_90D3C5DC1D7E2A02 FOREIGN KEY (remuneration_id) REFERENCES renumeration (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3C5DC1D7E2A02 ON justificatif (remuneration_id)');
        $this->addSql('ALTER TABLE piece ADD file VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE ressource CHANGE type type TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE specialite ADD abreviation VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE vacation ADD abreviation VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE justificatif DROP FOREIGN KEY FK_90D3C5DC1D7E2A02');
        $this->addSql('DROP INDEX UNIQ_90D3C5DC1D7E2A02 ON justificatif');
        $this->addSql('ALTER TABLE justificatif ADD contrat_id INT DEFAULT NULL, ADD adresse VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD adress_hopital VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP remuneration_id, DROP adresse_interim, DROP adresse_hopital, CHANGE nombre_trajet nombre_trajet INT DEFAULT NULL, CHANGE coef_ik coef_ik VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE nombre_km nombre_km INT DEFAULT NULL');
        $this->addSql('ALTER TABLE justificatif ADD CONSTRAINT FK_90D3C5DC1823061F FOREIGN KEY (contrat_id) REFERENCES contrat (id)');
        $this->addSql('CREATE INDEX IDX_90D3C5DC1823061F ON justificatif (contrat_id)');
        $this->addSql('ALTER TABLE piece DROP file');
        $this->addSql('ALTER TABLE ressource CHANGE type type TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE specialite DROP abreviation');
        $this->addSql('ALTER TABLE vacation DROP abreviation');
    }
}
