<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    /**
     * @Route("/site", name="site")
     */
    public function index()
    {
        return $this->render('site/index.html.twig', [
            'controller_name' => 'SiteController',
        ]);
    }

    /**
     * @Route("/", name="home")
     */

    public function home()
    {
        return $this->render('site/home.html.twig');
    }

    /**
     * @Route("/site/about", name="about")
     */

    public function about()
    {
        return $this->render('site/about.html.twig');
    }

    /**
     * @Route("/site/services", name="services")
     */

    public function services()
    {
        return $this->render('site/services.html.twig');
    }

    /**
     * @Route("/site/contact", name="contact")
     */

    public function contact()
    {
        return $this->render('site/contact.html.twig');
    }

    /**
     * @Route("/site/offres", name="details")
     */

    public function offres()
    {
        return $this->render('site/details.html.twig');
    }
}
