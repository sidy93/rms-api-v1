<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    /**
     * @Route("/site", name="site")
     */
    public function index()
    {
        return $this->render('site/index.html.twig', [
            'controller_name' => 'SiteController',
        ]);
    }

    /**
     * @Route("/", name="home")
     */

    public function home()
    {
        return $this->render('site/home.html.twig');
    }

    /**
     * @Route("/site/about", name="about")
     */

    public function about()
    {
        return $this->render('site/about.html.twig');
    }

    /**
     * @Route("/site/blogs", name="blogs")
     */

    public function blogs()
    {
        return $this->render('site/blogs.html.twig');
    }

    /**
     * @Route("/site/blog", name="blog")
     */

    public function blog()
    {
        return $this->render('site/blog.html.twig');
    }

    /**
     * @Route("/site/offres", name="offres")
     */

    public function offres()
    {
        return $this->render('site/offres.html.twig');
    }

    /**
     * @Route("/site/offre", name="offre")
     */

    public function offre()
    {
        return $this->render('site/offre.html.twig');
    }

     /**
     * @Route("/site/medecin", name="medecin")
     */

    public function medecin()
    {
        return $this->render('site/medecin.html.twig');
    }

    /**
     * @Route("/site/client", name="client")
     */

    public function client()
    {
        return $this->render('site/client.html.twig');
    }
    /**
     * @Route("/site/connexion", name="connexion")
     */

    public function connexion()
    {
        return $this->render('site/connexion.html.twig');
    }

     /**
     * @Route("/site/connexion", name="connexion")
     */

    public function connexion()
    {
        return $this->render('site/connexion.html.twig');
    }
}
